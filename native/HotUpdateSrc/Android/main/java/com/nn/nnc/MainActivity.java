package com.nn.nnc;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.egret.runtime.launcherInterface.INativePlayer;
import org.egret.egretnativeandroid.EgretNativeAndroid;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


//Android项目发布设置详见doc目录下的README_ANDROID.md

public class MainActivity extends Activity {
    public static final String MyTag = "MyTag";
    private EgretNativeAndroid nativeAndroid;

    private final String gameUrl = "http://154.216.2.131:20013/game/index.html";
    private final String zipUrl = "http://154.216.2.131:20013/game2.zip";
    private final String preloadPath = "/sdcard/egretGame/";

    private static String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private IWXAPI api;
    // APP_ID
    public static final String APP_ID = "wxd1f51e043076d0e9";
    private MainActivity app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nativeAndroid = new EgretNativeAndroid(this);
        app = this;
        //检测存储权限是否足够
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int check = checkSelfPermission(permissions[0]);
            if (check != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(permissions, 111);
            }
        }
        Log.d(MyTag,"__main oncreate");

        regToWx();

        startGame();
    }


    private void startGame() {
        nativeAndroid.setExternalInterface("hotUpdateStart", new INativePlayer.INativeInterface() {
            @Override
            public void callback(String message) {
                String str = "ready to hotupdate: ";
                str += message;
                Log.d(MyTag, str);
                System.out.printf("-----0000 from ts-----");
                preloadGame();
            }
        });

        nativeAndroid.setExternalInterface("loginByWx", new INativePlayer.INativeInterface() {
            @Override
            public void callback(String message) {
                String str = "wxLogin ";
                str += message;
                Log.d(MyTag, str);
                System.out.printf("-----sendMsgTo WX-----");
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "nnState";
                api.sendReq(req);
            }
        });

        nativeAndroid.setExternalInterface("openUrl", new INativePlayer.INativeInterface() {
            @Override
            public void callback(String url) {
                Log.d(MyTag, "open url in android");

                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        //复制到粘贴板
        nativeAndroid.setExternalInterface("copyWords", new INativePlayer.INativeInterface() {
            @Override
            public void callback(final String str) {
                app.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(MyTag, "copy words in android");
                        ClipboardManager cm = (ClipboardManager) app.getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("kk", str);
                        cm.setPrimaryClip(clip);
                    }

                });
            }
        });

        nativeAndroid.setExternalInterface("shareToWxSession", new INativePlayer.INativeInterface() {
            @Override
            public void callback(final String str) {
                //初始化一个WXWebpageObject，填写url
                WXWebpageObject webpage = new WXWebpageObject();
                webpage.webpageUrl = str;

                //用 WXWebpageObject 对象初始化一个 WXMediaMessage 对象
                WXMediaMessage msg = new WXMediaMessage(webpage);
                msg.title = "来牛统领 做大赢家";
                msg.description = "牛统领";

                Bitmap thumbBmp = BitmapFactory.decodeResource(getResources(), R.drawable.icon);
                // 缩略图 < 32k 不然拉取不了微信客户端
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                thumbBmp.compress(Bitmap.CompressFormat.JPEG, 70, outStream);
                msg.thumbData = outStream.toByteArray();

                SendMessageToWX.Req req = new SendMessageToWX.Req();
                req.transaction = "ShareWebPage";
                req.message = msg;
                req.scene = SendMessageToWX.Req.WXSceneSession;
                Log.d(MyTag, "send wx share");
                api.sendReq(req);
            }
        });

        nativeAndroid.setExternalInterface("shareToWxTimeline", new INativePlayer.INativeInterface() {
            @Override
            public void callback(final String str) {
                //初始化一个WXWebpageObject，填写url
                WXWebpageObject webpage = new WXWebpageObject();
                webpage.webpageUrl = str;

                //用 WXWebpageObject 对象初始化一个 WXMediaMessage 对象
                WXMediaMessage msg = new WXMediaMessage(webpage);
                msg.title = "来牛统领 做大赢家";
                msg.description = "牛统领";

                Bitmap thumbBmp = BitmapFactory.decodeResource(getResources(), R.drawable.icon);
                // 缩略图 < 32k 不然拉取不了微信客户端
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                thumbBmp.compress(Bitmap.CompressFormat.JPEG, 70, outStream);
                msg.thumbData = outStream.toByteArray();

                SendMessageToWX.Req req = new SendMessageToWX.Req();
                req.transaction = "ShareWebPage";
                req.message = msg;
                req.scene = SendMessageToWX.Req.WXSceneTimeline;
                Log.d(MyTag, "send wx share");
                api.sendReq(req);
            }
        });



        nativeAndroid.config.preloadPath = preloadPath;


        if (!nativeAndroid.checkGlEsVersion()) {
            Toast.makeText(this, "This device does not support OpenGL ES 2.0.",
                    Toast.LENGTH_LONG).show();
            return;
        }

        nativeAndroid.config.showFPS = false;
        nativeAndroid.config.fpsLogTime = 30;
        nativeAndroid.config.disableNativeRender = false;
        nativeAndroid.config.clearCache = false;
        nativeAndroid.config.loadingTimeout = 0;

        setExternalInterfaces();

        if (!nativeAndroid.initialize("http://154.216.2.131:20013/game/index.html")) {
            Toast.makeText(this, "Initialize native failed.",
                    Toast.LENGTH_LONG).show();
            return;
        }

        setContentView(nativeAndroid.getRootFrameLayout());
    }

    @Override
    protected void onPause() {
        super.onPause();
        nativeAndroid.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        nativeAndroid.resume();
    }

    @Override
    public boolean onKeyDown(final int keyCode, final KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            nativeAndroid.exitGame();
        }

        return super.onKeyDown(keyCode, keyEvent);
    }

    private void setExternalInterfaces() {
        nativeAndroid.setExternalInterface("sendToNative", new INativePlayer.INativeInterface() {
            @Override
            public void callback(String message) {
                String str = "Native get message: ";
                str += message;
                Log.d(MyTag, str);
                nativeAndroid.callExternalInterface("sendToJS", str);
            }
        });
    }

    private void preloadGame() {
        String dir = preloadPath + getFileDirByUrl(gameUrl);
        File dirFile = new File(dir);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        downloadGameRes(zipUrl, dir);
    }

    private void downloadGameRes(final String zipUrl, String targetDir) {
        String tempZipFileName = targetDir + "game.zip";
        final File file = new File(tempZipFileName);


        Runnable runnable = new Runnable() {
            public void run() {
                InputStream inputStream = null;
                FileOutputStream outputStream = null;
                HttpURLConnection connection = null;
                boolean finish = false;

                try {
                    URL url = new URL(zipUrl);
                    connection = (HttpURLConnection) url.openConnection();

                    int code = connection.getResponseCode();
                    System.out.printf("准备下载 热更新 ");
                    if (code == 200) {
                        inputStream = connection.getInputStream();
                        outputStream = new FileOutputStream(file, true);

                        byte[] buffer = new byte[4096];
                        int length;

                        while ((length = inputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, length);
                        }
                    }
                    finish = true;
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        if (connection != null) {
                            connection.disconnect();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                }

                if (finish) {
                    unzip(file);
                }
            }
        };

        new Thread(runnable).start();
    }

    private void unzip(File file) {
        int BUFFER = 4096;
        String strEntry;
        String targetDir = file.getParent() + "/";
        try {
            BufferedOutputStream dest = null;
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                try {
                    int count;
                    byte data[] = new byte[BUFFER];
                    strEntry = entry.getName();
                    File entryFile = new File(targetDir + strEntry);
                    if (strEntry.endsWith("/")) {
                        entryFile.mkdirs();
                        continue;
                    }
                    File entryDir = new File(entryFile.getParent());
                    if (!entryDir.exists()) {
                        entryDir.mkdirs();
                    }
                    FileOutputStream fos = new FileOutputStream(entryFile);
                    dest = new BufferedOutputStream(fos, BUFFER);
                    try{
                        while ((count = zis.read(data, 0, BUFFER)) != -1) {
                            dest.write(data, 0, count);
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    dest.flush();
                    dest.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return;
                }
            }
            zis.close();
        } catch (Exception e) {
            System.out.printf("----------unzip problem------------------------");
            e.printStackTrace();
            return;
        }
        System.out.printf("----------unzip finish------------------------");
        file.delete();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                startGame();
                nativeAndroid.exitGame();
                startGame();
            }
        });
    }

    private static String getFileDirByUrl(String urlString) {
        int lastSlash = urlString.lastIndexOf('/');
        String server = urlString.substring(0, lastSlash + 1);
        return server.replaceFirst("://", "/").replace(":", "#0A");
    }

    private void regToWx() {
        // 通过WXAPIFactory工厂，获取IWXAPI的实例
        api = WXAPIFactory.createWXAPI(this, APP_ID, false);

        // 将应用的appId注册到微信
        api.registerApp(APP_ID);


        //建议动态监听微信启动广播进行注册到微信
//        registerReceiver(new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
//                // 将该app注册到微信
//                api.registerApp(APP_ID);
//            }
//        }, new IntentFilter(ConstantsAPI.ACTION_REFRESH_WXAPP));

    }

    private static byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, output);
        if (needRecycle) {
            bmp.recycle();
        }

        byte[] result = output.toByteArray();
        try {
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra("wxCode")) {
            String wxCode = (String) intent.getExtras().get("wxCode");
            Log.d(MyTag,"Main get code " + wxCode);
            nativeAndroid.callExternalInterface("getWxCode", wxCode);
        }
        if (intent.hasExtra("wxShare")){
            Log.d(MyTag,"Main get wxShare ");
            nativeAndroid.callExternalInterface("wxShareSuccess", "success");
        }
    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
