type Long = protobuf.Long;

/** Namespace agent. */
declare namespace agent {

    /** AgentID enum. */
    enum AgentID {
        ID = 0,
        ID_RegisterServerREQ = 10101,
        ID_UnRegisterServerREQ = 10102,
        ID_PackageTransferREQ = 10103,
        ID_PackageTransferACK = 10104,
        ID_HeartBeatREQ = 10105,
        ID_HeartBeatACK = 10106,
        ID_UserDisconnectedREQ = 10107,
        ID_UserConnectedREQ = 10108,
        ID_ChannelNTF = 10109,
        ID_RegisterServerACK = 10110
    }

    /** Properties of a RegisterServerREQ. */
    interface IRegisterServerREQ {

        /** RegisterServerREQ OriginID */
        OriginID?: (number|Long|null);
    }

    /** Represents a RegisterServerREQ. */
    class RegisterServerREQ implements IRegisterServerREQ {

        /**
         * Constructs a new RegisterServerREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: agent.IRegisterServerREQ);

        /** RegisterServerREQ OriginID. */
        public OriginID: (number|Long);

        /**
         * Creates a new RegisterServerREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RegisterServerREQ instance
         */
        public static create(properties?: agent.IRegisterServerREQ): agent.RegisterServerREQ;

        /**
         * Encodes the specified RegisterServerREQ message. Does not implicitly {@link agent.RegisterServerREQ.verify|verify} messages.
         * @param message RegisterServerREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: agent.IRegisterServerREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RegisterServerREQ message, length delimited. Does not implicitly {@link agent.RegisterServerREQ.verify|verify} messages.
         * @param message RegisterServerREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: agent.IRegisterServerREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RegisterServerREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RegisterServerREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): agent.RegisterServerREQ;

        /**
         * Decodes a RegisterServerREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RegisterServerREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): agent.RegisterServerREQ;

        /**
         * Verifies a RegisterServerREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RegisterServerACK. */
    interface IRegisterServerACK {

        /** RegisterServerACK Code */
        Code?: (number|null);

        /** RegisterServerACK Error */
        Error?: (string|null);

        /** RegisterServerACK ExternalMsgID */
        ExternalMsgID?: (number[]|null);
    }

    /** Represents a RegisterServerACK. */
    class RegisterServerACK implements IRegisterServerACK {

        /**
         * Constructs a new RegisterServerACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: agent.IRegisterServerACK);

        /** RegisterServerACK Code. */
        public Code: number;

        /** RegisterServerACK Error. */
        public Error: string;

        /** RegisterServerACK ExternalMsgID. */
        public ExternalMsgID: number[];

        /**
         * Creates a new RegisterServerACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RegisterServerACK instance
         */
        public static create(properties?: agent.IRegisterServerACK): agent.RegisterServerACK;

        /**
         * Encodes the specified RegisterServerACK message. Does not implicitly {@link agent.RegisterServerACK.verify|verify} messages.
         * @param message RegisterServerACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: agent.IRegisterServerACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RegisterServerACK message, length delimited. Does not implicitly {@link agent.RegisterServerACK.verify|verify} messages.
         * @param message RegisterServerACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: agent.IRegisterServerACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RegisterServerACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RegisterServerACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): agent.RegisterServerACK;

        /**
         * Decodes a RegisterServerACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RegisterServerACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): agent.RegisterServerACK;

        /**
         * Verifies a RegisterServerACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of an UnRegisterServerREQ. */
    interface IUnRegisterServerREQ {

        /** UnRegisterServerREQ OriginID */
        OriginID?: (number|null);
    }

    /** Represents an UnRegisterServerREQ. */
    class UnRegisterServerREQ implements IUnRegisterServerREQ {

        /**
         * Constructs a new UnRegisterServerREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: agent.IUnRegisterServerREQ);

        /** UnRegisterServerREQ OriginID. */
        public OriginID: number;

        /**
         * Creates a new UnRegisterServerREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UnRegisterServerREQ instance
         */
        public static create(properties?: agent.IUnRegisterServerREQ): agent.UnRegisterServerREQ;

        /**
         * Encodes the specified UnRegisterServerREQ message. Does not implicitly {@link agent.UnRegisterServerREQ.verify|verify} messages.
         * @param message UnRegisterServerREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: agent.IUnRegisterServerREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified UnRegisterServerREQ message, length delimited. Does not implicitly {@link agent.UnRegisterServerREQ.verify|verify} messages.
         * @param message UnRegisterServerREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: agent.IUnRegisterServerREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an UnRegisterServerREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UnRegisterServerREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): agent.UnRegisterServerREQ;

        /**
         * Decodes an UnRegisterServerREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UnRegisterServerREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): agent.UnRegisterServerREQ;

        /**
         * Verifies an UnRegisterServerREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a PackageTransferREQ. */
    interface IPackageTransferREQ {

        /** PackageTransferREQ OriginID */
        OriginID?: (number|Long|null);

        /** PackageTransferREQ TargetID */
        TargetID?: (number|Long|null);

        /** PackageTransferREQ OriginSes */
        OriginSes?: (number|Long|null);

        /** PackageTransferREQ MsgID */
        MsgID?: (number|null);

        /** PackageTransferREQ StatusCode */
        StatusCode?: (number|null);

        /** PackageTransferREQ Token */
        Token?: (number|Long|null);

        /** PackageTransferREQ Content */
        Content?: (Uint8Array|null);
    }

    /** Represents a PackageTransferREQ. */
    class PackageTransferREQ implements IPackageTransferREQ {

        /**
         * Constructs a new PackageTransferREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: agent.IPackageTransferREQ);

        /** PackageTransferREQ OriginID. */
        public OriginID: (number|Long);

        /** PackageTransferREQ TargetID. */
        public TargetID: (number|Long);

        /** PackageTransferREQ OriginSes. */
        public OriginSes: (number|Long);

        /** PackageTransferREQ MsgID. */
        public MsgID: number;

        /** PackageTransferREQ StatusCode. */
        public StatusCode: number;

        /** PackageTransferREQ Token. */
        public Token: (number|Long);

        /** PackageTransferREQ Content. */
        public Content: Uint8Array;

        /**
         * Creates a new PackageTransferREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PackageTransferREQ instance
         */
        public static create(properties?: agent.IPackageTransferREQ): agent.PackageTransferREQ;

        /**
         * Encodes the specified PackageTransferREQ message. Does not implicitly {@link agent.PackageTransferREQ.verify|verify} messages.
         * @param message PackageTransferREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: agent.IPackageTransferREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified PackageTransferREQ message, length delimited. Does not implicitly {@link agent.PackageTransferREQ.verify|verify} messages.
         * @param message PackageTransferREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: agent.IPackageTransferREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a PackageTransferREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PackageTransferREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): agent.PackageTransferREQ;

        /**
         * Decodes a PackageTransferREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PackageTransferREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): agent.PackageTransferREQ;

        /**
         * Verifies a PackageTransferREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a PackageTransferACK. */
    interface IPackageTransferACK {

        /** PackageTransferACK OriginID */
        OriginID?: (number|Long|null);

        /** PackageTransferACK TargetID */
        TargetID?: (number|Long|null);

        /** PackageTransferACK OriginSes */
        OriginSes?: (number|Long|null);

        /** PackageTransferACK MsgID */
        MsgID?: (number|null);

        /** PackageTransferACK StatusCode */
        StatusCode?: (number|null);

        /** PackageTransferACK Token */
        Token?: (number|Long|null);

        /** PackageTransferACK Content */
        Content?: (Uint8Array|null);
    }

    /** Represents a PackageTransferACK. */
    class PackageTransferACK implements IPackageTransferACK {

        /**
         * Constructs a new PackageTransferACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: agent.IPackageTransferACK);

        /** PackageTransferACK OriginID. */
        public OriginID: (number|Long);

        /** PackageTransferACK TargetID. */
        public TargetID: (number|Long);

        /** PackageTransferACK OriginSes. */
        public OriginSes: (number|Long);

        /** PackageTransferACK MsgID. */
        public MsgID: number;

        /** PackageTransferACK StatusCode. */
        public StatusCode: number;

        /** PackageTransferACK Token. */
        public Token: (number|Long);

        /** PackageTransferACK Content. */
        public Content: Uint8Array;

        /**
         * Creates a new PackageTransferACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PackageTransferACK instance
         */
        public static create(properties?: agent.IPackageTransferACK): agent.PackageTransferACK;

        /**
         * Encodes the specified PackageTransferACK message. Does not implicitly {@link agent.PackageTransferACK.verify|verify} messages.
         * @param message PackageTransferACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: agent.IPackageTransferACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified PackageTransferACK message, length delimited. Does not implicitly {@link agent.PackageTransferACK.verify|verify} messages.
         * @param message PackageTransferACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: agent.IPackageTransferACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a PackageTransferACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PackageTransferACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): agent.PackageTransferACK;

        /**
         * Decodes a PackageTransferACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PackageTransferACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): agent.PackageTransferACK;

        /**
         * Verifies a PackageTransferACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a HeartBeatREQ. */
    interface IHeartBeatREQ {

        /** HeartBeatREQ Ping */
        Ping?: (number|Long|null);
    }

    /** Represents a HeartBeatREQ. */
    class HeartBeatREQ implements IHeartBeatREQ {

        /**
         * Constructs a new HeartBeatREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: agent.IHeartBeatREQ);

        /** HeartBeatREQ Ping. */
        public Ping: (number|Long);

        /**
         * Creates a new HeartBeatREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns HeartBeatREQ instance
         */
        public static create(properties?: agent.IHeartBeatREQ): agent.HeartBeatREQ;

        /**
         * Encodes the specified HeartBeatREQ message. Does not implicitly {@link agent.HeartBeatREQ.verify|verify} messages.
         * @param message HeartBeatREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: agent.IHeartBeatREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified HeartBeatREQ message, length delimited. Does not implicitly {@link agent.HeartBeatREQ.verify|verify} messages.
         * @param message HeartBeatREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: agent.IHeartBeatREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a HeartBeatREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns HeartBeatREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): agent.HeartBeatREQ;

        /**
         * Decodes a HeartBeatREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns HeartBeatREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): agent.HeartBeatREQ;

        /**
         * Verifies a HeartBeatREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a HeartBeatACK. */
    interface IHeartBeatACK {

        /** HeartBeatACK Pong */
        Pong?: (number|Long|null);
    }

    /** Represents a HeartBeatACK. */
    class HeartBeatACK implements IHeartBeatACK {

        /**
         * Constructs a new HeartBeatACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: agent.IHeartBeatACK);

        /** HeartBeatACK Pong. */
        public Pong: (number|Long);

        /**
         * Creates a new HeartBeatACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns HeartBeatACK instance
         */
        public static create(properties?: agent.IHeartBeatACK): agent.HeartBeatACK;

        /**
         * Encodes the specified HeartBeatACK message. Does not implicitly {@link agent.HeartBeatACK.verify|verify} messages.
         * @param message HeartBeatACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: agent.IHeartBeatACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified HeartBeatACK message, length delimited. Does not implicitly {@link agent.HeartBeatACK.verify|verify} messages.
         * @param message HeartBeatACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: agent.IHeartBeatACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a HeartBeatACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns HeartBeatACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): agent.HeartBeatACK;

        /**
         * Decodes a HeartBeatACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns HeartBeatACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): agent.HeartBeatACK;

        /**
         * Verifies a HeartBeatACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a UserDisconnectedREQ. */
    interface IUserDisconnectedREQ {

        /** UserDisconnectedREQ ID */
        ID?: (number|Long|null);
    }

    /** Represents a UserDisconnectedREQ. */
    class UserDisconnectedREQ implements IUserDisconnectedREQ {

        /**
         * Constructs a new UserDisconnectedREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: agent.IUserDisconnectedREQ);

        /** UserDisconnectedREQ ID. */
        public ID: (number|Long);

        /**
         * Creates a new UserDisconnectedREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UserDisconnectedREQ instance
         */
        public static create(properties?: agent.IUserDisconnectedREQ): agent.UserDisconnectedREQ;

        /**
         * Encodes the specified UserDisconnectedREQ message. Does not implicitly {@link agent.UserDisconnectedREQ.verify|verify} messages.
         * @param message UserDisconnectedREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: agent.IUserDisconnectedREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified UserDisconnectedREQ message, length delimited. Does not implicitly {@link agent.UserDisconnectedREQ.verify|verify} messages.
         * @param message UserDisconnectedREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: agent.IUserDisconnectedREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a UserDisconnectedREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UserDisconnectedREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): agent.UserDisconnectedREQ;

        /**
         * Decodes a UserDisconnectedREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UserDisconnectedREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): agent.UserDisconnectedREQ;

        /**
         * Verifies a UserDisconnectedREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a UserConnectedREQ. */
    interface IUserConnectedREQ {

        /** UserConnectedREQ ID */
        ID?: (number|Long|null);
    }

    /** Represents a UserConnectedREQ. */
    class UserConnectedREQ implements IUserConnectedREQ {

        /**
         * Constructs a new UserConnectedREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: agent.IUserConnectedREQ);

        /** UserConnectedREQ ID. */
        public ID: (number|Long);

        /**
         * Creates a new UserConnectedREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UserConnectedREQ instance
         */
        public static create(properties?: agent.IUserConnectedREQ): agent.UserConnectedREQ;

        /**
         * Encodes the specified UserConnectedREQ message. Does not implicitly {@link agent.UserConnectedREQ.verify|verify} messages.
         * @param message UserConnectedREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: agent.IUserConnectedREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified UserConnectedREQ message, length delimited. Does not implicitly {@link agent.UserConnectedREQ.verify|verify} messages.
         * @param message UserConnectedREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: agent.IUserConnectedREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a UserConnectedREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UserConnectedREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): agent.UserConnectedREQ;

        /**
         * Decodes a UserConnectedREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UserConnectedREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): agent.UserConnectedREQ;

        /**
         * Verifies a UserConnectedREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ChannelNTF. */
    interface IChannelNTF {

        /** ChannelNTF Token */
        Token?: (number|Long|null);

        /** ChannelNTF MsgId */
        MsgId?: (number|null);

        /** ChannelNTF Content */
        Content?: (Uint8Array|null);
    }

    /** Represents a ChannelNTF. */
    class ChannelNTF implements IChannelNTF {

        /**
         * Constructs a new ChannelNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: agent.IChannelNTF);

        /** ChannelNTF Token. */
        public Token: (number|Long);

        /** ChannelNTF MsgId. */
        public MsgId: number;

        /** ChannelNTF Content. */
        public Content: Uint8Array;

        /**
         * Creates a new ChannelNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ChannelNTF instance
         */
        public static create(properties?: agent.IChannelNTF): agent.ChannelNTF;

        /**
         * Encodes the specified ChannelNTF message. Does not implicitly {@link agent.ChannelNTF.verify|verify} messages.
         * @param message ChannelNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: agent.IChannelNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ChannelNTF message, length delimited. Does not implicitly {@link agent.ChannelNTF.verify|verify} messages.
         * @param message ChannelNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: agent.IChannelNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ChannelNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ChannelNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): agent.ChannelNTF;

        /**
         * Decodes a ChannelNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ChannelNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): agent.ChannelNTF;

        /**
         * Verifies a ChannelNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }
}

/** Namespace game_base. */
declare namespace game_base {

    /** ServerState enum. */
    enum ServerState {
        NORMAL = 0,
        HOT = 1,
        PREPARE = 2
    }
}

/** Namespace login. */
declare namespace login {

    /** LoginID enum. */
    enum LoginID {
        ID = 0,
        ID_LoginREQ = 20101,
        ID_LoginACK = 20102,
        ID_KickOffNTF = 20103,
        ID_SignInREQ = 20104,
        ID_SignInACK = 20105,
        ID_SignInfoREQ = 20106,
        ID_SignInfoACK = 20107,
        ID_BindIdentifyREQ = 20108,
        ID_BindIdentifyACK = 20109,
        ID_BindPhoneNumberREQ = 20110,
        ID_BindPhoneNumberACK = 20111,
        ID_SendVerifyCodeREQ = 20112,
        ID_ShareREQ = 20113,
        ID_ShareACK = 20114,
        ID_NoticeREQ = 20115,
        ID_NoticeACK = 20116,
        ID_DiamondChangeNTF = 20117,
        ID_UserInfoREQ = 20118,
        ID_USerInfoACK = 20119
    }

    /** LoginCode enum. */
    enum LoginCode {
        LOGIN_SUCCESS = 0,
        REGISTER_SUCCESS = 1,
        INVALID_PHONE_NUMBER = 101,
        INVALID_PASSWORD_OR_ACCOUNT = 102,
        INVALID_VERIFY_CODE = 103,
        UNREGISTER_USER = 104,
        TOKEN_EXPIRE = 105,
        TOKEN_INVALID = 106,
        FORBID = 107,
        MAINTAIN = 108,
        UNKNOWN = 199
    }

    /** Properties of a LoginREQ. */
    interface ILoginREQ {

        /** LoginREQ Token */
        Token?: (string|null);
    }

    /** Represents a LoginREQ. */
    class LoginREQ implements ILoginREQ {

        /**
         * Constructs a new LoginREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.ILoginREQ);

        /** LoginREQ Token. */
        public Token: string;

        /**
         * Creates a new LoginREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LoginREQ instance
         */
        public static create(properties?: login.ILoginREQ): login.LoginREQ;

        /**
         * Encodes the specified LoginREQ message. Does not implicitly {@link login.LoginREQ.verify|verify} messages.
         * @param message LoginREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.ILoginREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified LoginREQ message, length delimited. Does not implicitly {@link login.LoginREQ.verify|verify} messages.
         * @param message LoginREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.ILoginREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a LoginREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LoginREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.LoginREQ;

        /**
         * Decodes a LoginREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LoginREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.LoginREQ;

        /**
         * Verifies a LoginREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a LoginACK. */
    interface ILoginACK {

        /** LoginACK Code */
        Code?: (login.LoginCode|null);

        /** LoginACK Error */
        Error?: (string|null);

        /** LoginACK userInfo */
        userInfo?: (structure.IUserInfo|null);

        /** LoginACK GameID */
        GameID?: (number|null);

        /** LoginACK TableID */
        TableID?: (number|null);
    }

    /** Represents a LoginACK. */
    class LoginACK implements ILoginACK {

        /**
         * Constructs a new LoginACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.ILoginACK);

        /** LoginACK Code. */
        public Code: login.LoginCode;

        /** LoginACK Error. */
        public Error: string;

        /** LoginACK userInfo. */
        public userInfo?: (structure.IUserInfo|null);

        /** LoginACK GameID. */
        public GameID: number;

        /** LoginACK TableID. */
        public TableID: number;

        /**
         * Creates a new LoginACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LoginACK instance
         */
        public static create(properties?: login.ILoginACK): login.LoginACK;

        /**
         * Encodes the specified LoginACK message. Does not implicitly {@link login.LoginACK.verify|verify} messages.
         * @param message LoginACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.ILoginACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified LoginACK message, length delimited. Does not implicitly {@link login.LoginACK.verify|verify} messages.
         * @param message LoginACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.ILoginACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a LoginACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LoginACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.LoginACK;

        /**
         * Decodes a LoginACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LoginACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.LoginACK;

        /**
         * Verifies a LoginACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a KickOffNTF. */
    interface IKickOffNTF {

        /** KickOffNTF Reason */
        Reason?: (login.KickOffNTF.KickReason|null);
    }

    /** Represents a KickOffNTF. */
    class KickOffNTF implements IKickOffNTF {

        /**
         * Constructs a new KickOffNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IKickOffNTF);

        /** KickOffNTF Reason. */
        public Reason: login.KickOffNTF.KickReason;

        /**
         * Creates a new KickOffNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns KickOffNTF instance
         */
        public static create(properties?: login.IKickOffNTF): login.KickOffNTF;

        /**
         * Encodes the specified KickOffNTF message. Does not implicitly {@link login.KickOffNTF.verify|verify} messages.
         * @param message KickOffNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IKickOffNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified KickOffNTF message, length delimited. Does not implicitly {@link login.KickOffNTF.verify|verify} messages.
         * @param message KickOffNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IKickOffNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a KickOffNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns KickOffNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.KickOffNTF;

        /**
         * Decodes a KickOffNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns KickOffNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.KickOffNTF;

        /**
         * Verifies a KickOffNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace KickOffNTF {

        /** KickReason enum. */
        enum KickReason {
            DUPLICATED_LOGIN = 0,
            FORBID_LOGIN = 1
        }
    }

    /** Properties of a SignInREQ. */
    interface ISignInREQ {
    }

    /** Represents a SignInREQ. */
    class SignInREQ implements ISignInREQ {

        /**
         * Constructs a new SignInREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.ISignInREQ);

        /**
         * Creates a new SignInREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SignInREQ instance
         */
        public static create(properties?: login.ISignInREQ): login.SignInREQ;

        /**
         * Encodes the specified SignInREQ message. Does not implicitly {@link login.SignInREQ.verify|verify} messages.
         * @param message SignInREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.ISignInREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SignInREQ message, length delimited. Does not implicitly {@link login.SignInREQ.verify|verify} messages.
         * @param message SignInREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.ISignInREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SignInREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SignInREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.SignInREQ;

        /**
         * Decodes a SignInREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SignInREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.SignInREQ;

        /**
         * Verifies a SignInREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a SignInACK. */
    interface ISignInACK {

        /** SignInACK Day */
        Day?: (number[]|null);
    }

    /** Represents a SignInACK. */
    class SignInACK implements ISignInACK {

        /**
         * Constructs a new SignInACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.ISignInACK);

        /** SignInACK Day. */
        public Day: number[];

        /**
         * Creates a new SignInACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SignInACK instance
         */
        public static create(properties?: login.ISignInACK): login.SignInACK;

        /**
         * Encodes the specified SignInACK message. Does not implicitly {@link login.SignInACK.verify|verify} messages.
         * @param message SignInACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.ISignInACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SignInACK message, length delimited. Does not implicitly {@link login.SignInACK.verify|verify} messages.
         * @param message SignInACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.ISignInACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SignInACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SignInACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.SignInACK;

        /**
         * Decodes a SignInACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SignInACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.SignInACK;

        /**
         * Verifies a SignInACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a SignInfoREQ. */
    interface ISignInfoREQ {
    }

    /** Represents a SignInfoREQ. */
    class SignInfoREQ implements ISignInfoREQ {

        /**
         * Constructs a new SignInfoREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.ISignInfoREQ);

        /**
         * Creates a new SignInfoREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SignInfoREQ instance
         */
        public static create(properties?: login.ISignInfoREQ): login.SignInfoREQ;

        /**
         * Encodes the specified SignInfoREQ message. Does not implicitly {@link login.SignInfoREQ.verify|verify} messages.
         * @param message SignInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.ISignInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SignInfoREQ message, length delimited. Does not implicitly {@link login.SignInfoREQ.verify|verify} messages.
         * @param message SignInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.ISignInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SignInfoREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SignInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.SignInfoREQ;

        /**
         * Decodes a SignInfoREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SignInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.SignInfoREQ;

        /**
         * Verifies a SignInfoREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a SignInfoACK. */
    interface ISignInfoACK {

        /** SignInfoACK Day */
        Day?: (number[]|null);
    }

    /** Represents a SignInfoACK. */
    class SignInfoACK implements ISignInfoACK {

        /**
         * Constructs a new SignInfoACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.ISignInfoACK);

        /** SignInfoACK Day. */
        public Day: number[];

        /**
         * Creates a new SignInfoACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SignInfoACK instance
         */
        public static create(properties?: login.ISignInfoACK): login.SignInfoACK;

        /**
         * Encodes the specified SignInfoACK message. Does not implicitly {@link login.SignInfoACK.verify|verify} messages.
         * @param message SignInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.ISignInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SignInfoACK message, length delimited. Does not implicitly {@link login.SignInfoACK.verify|verify} messages.
         * @param message SignInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.ISignInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SignInfoACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SignInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.SignInfoACK;

        /**
         * Decodes a SignInfoACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SignInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.SignInfoACK;

        /**
         * Verifies a SignInfoACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ShareREQ. */
    interface IShareREQ {
    }

    /** Represents a ShareREQ. */
    class ShareREQ implements IShareREQ {

        /**
         * Constructs a new ShareREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IShareREQ);

        /**
         * Creates a new ShareREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ShareREQ instance
         */
        public static create(properties?: login.IShareREQ): login.ShareREQ;

        /**
         * Encodes the specified ShareREQ message. Does not implicitly {@link login.ShareREQ.verify|verify} messages.
         * @param message ShareREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IShareREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ShareREQ message, length delimited. Does not implicitly {@link login.ShareREQ.verify|verify} messages.
         * @param message ShareREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IShareREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ShareREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ShareREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.ShareREQ;

        /**
         * Decodes a ShareREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ShareREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.ShareREQ;

        /**
         * Verifies a ShareREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ShareACK. */
    interface IShareACK {

        /** ShareACK Code */
        Code?: (login.ShareACK.ShareCode|null);
    }

    /** Represents a ShareACK. */
    class ShareACK implements IShareACK {

        /**
         * Constructs a new ShareACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IShareACK);

        /** ShareACK Code. */
        public Code: login.ShareACK.ShareCode;

        /**
         * Creates a new ShareACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ShareACK instance
         */
        public static create(properties?: login.IShareACK): login.ShareACK;

        /**
         * Encodes the specified ShareACK message. Does not implicitly {@link login.ShareACK.verify|verify} messages.
         * @param message ShareACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IShareACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ShareACK message, length delimited. Does not implicitly {@link login.ShareACK.verify|verify} messages.
         * @param message ShareACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IShareACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ShareACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ShareACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.ShareACK;

        /**
         * Decodes a ShareACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ShareACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.ShareACK;

        /**
         * Verifies a ShareACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace ShareACK {

        /** ShareCode enum. */
        enum ShareCode {
            SUCCESS = 0,
            ALREADY_SHARED = 1
        }
    }

    /** Properties of a BindIdentifyREQ. */
    interface IBindIdentifyREQ {

        /** BindIdentifyREQ RealName */
        RealName?: (string|null);

        /** BindIdentifyREQ Identify */
        Identify?: (string|null);
    }

    /** Represents a BindIdentifyREQ. */
    class BindIdentifyREQ implements IBindIdentifyREQ {

        /**
         * Constructs a new BindIdentifyREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IBindIdentifyREQ);

        /** BindIdentifyREQ RealName. */
        public RealName: string;

        /** BindIdentifyREQ Identify. */
        public Identify: string;

        /**
         * Creates a new BindIdentifyREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BindIdentifyREQ instance
         */
        public static create(properties?: login.IBindIdentifyREQ): login.BindIdentifyREQ;

        /**
         * Encodes the specified BindIdentifyREQ message. Does not implicitly {@link login.BindIdentifyREQ.verify|verify} messages.
         * @param message BindIdentifyREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IBindIdentifyREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BindIdentifyREQ message, length delimited. Does not implicitly {@link login.BindIdentifyREQ.verify|verify} messages.
         * @param message BindIdentifyREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IBindIdentifyREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BindIdentifyREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BindIdentifyREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.BindIdentifyREQ;

        /**
         * Decodes a BindIdentifyREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BindIdentifyREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.BindIdentifyREQ;

        /**
         * Verifies a BindIdentifyREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a BindIdentifyACK. */
    interface IBindIdentifyACK {

        /** BindIdentifyACK Code */
        Code?: (login.BindIdentifyACK.BindIdentifyACKCode|null);
    }

    /** Represents a BindIdentifyACK. */
    class BindIdentifyACK implements IBindIdentifyACK {

        /**
         * Constructs a new BindIdentifyACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IBindIdentifyACK);

        /** BindIdentifyACK Code. */
        public Code: login.BindIdentifyACK.BindIdentifyACKCode;

        /**
         * Creates a new BindIdentifyACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BindIdentifyACK instance
         */
        public static create(properties?: login.IBindIdentifyACK): login.BindIdentifyACK;

        /**
         * Encodes the specified BindIdentifyACK message. Does not implicitly {@link login.BindIdentifyACK.verify|verify} messages.
         * @param message BindIdentifyACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IBindIdentifyACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BindIdentifyACK message, length delimited. Does not implicitly {@link login.BindIdentifyACK.verify|verify} messages.
         * @param message BindIdentifyACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IBindIdentifyACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BindIdentifyACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BindIdentifyACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.BindIdentifyACK;

        /**
         * Decodes a BindIdentifyACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BindIdentifyACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.BindIdentifyACK;

        /**
         * Verifies a BindIdentifyACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace BindIdentifyACK {

        /** BindIdentifyACKCode enum. */
        enum BindIdentifyACKCode {
            SUCCESS = 0,
            NAME_INVALID = 1,
            IDENTIFY_FAIL = 3
        }
    }

    /** Properties of a BindPhoneNumberREQ. */
    interface IBindPhoneNumberREQ {

        /** BindPhoneNumberREQ PhoneNumber */
        PhoneNumber?: (string|null);

        /** BindPhoneNumberREQ VerifyCode */
        VerifyCode?: (string|null);
    }

    /** Represents a BindPhoneNumberREQ. */
    class BindPhoneNumberREQ implements IBindPhoneNumberREQ {

        /**
         * Constructs a new BindPhoneNumberREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IBindPhoneNumberREQ);

        /** BindPhoneNumberREQ PhoneNumber. */
        public PhoneNumber: string;

        /** BindPhoneNumberREQ VerifyCode. */
        public VerifyCode: string;

        /**
         * Creates a new BindPhoneNumberREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BindPhoneNumberREQ instance
         */
        public static create(properties?: login.IBindPhoneNumberREQ): login.BindPhoneNumberREQ;

        /**
         * Encodes the specified BindPhoneNumberREQ message. Does not implicitly {@link login.BindPhoneNumberREQ.verify|verify} messages.
         * @param message BindPhoneNumberREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IBindPhoneNumberREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BindPhoneNumberREQ message, length delimited. Does not implicitly {@link login.BindPhoneNumberREQ.verify|verify} messages.
         * @param message BindPhoneNumberREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IBindPhoneNumberREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BindPhoneNumberREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BindPhoneNumberREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.BindPhoneNumberREQ;

        /**
         * Decodes a BindPhoneNumberREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BindPhoneNumberREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.BindPhoneNumberREQ;

        /**
         * Verifies a BindPhoneNumberREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a BindPhoneNumberACK. */
    interface IBindPhoneNumberACK {

        /** BindPhoneNumberACK Code */
        Code?: (login.BindPhoneNumberACK.BindPhoneNumberACK|null);
    }

    /** Represents a BindPhoneNumberACK. */
    class BindPhoneNumberACK implements IBindPhoneNumberACK {

        /**
         * Constructs a new BindPhoneNumberACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IBindPhoneNumberACK);

        /** BindPhoneNumberACK Code. */
        public Code: login.BindPhoneNumberACK.BindPhoneNumberACK;

        /**
         * Creates a new BindPhoneNumberACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BindPhoneNumberACK instance
         */
        public static create(properties?: login.IBindPhoneNumberACK): login.BindPhoneNumberACK;

        /**
         * Encodes the specified BindPhoneNumberACK message. Does not implicitly {@link login.BindPhoneNumberACK.verify|verify} messages.
         * @param message BindPhoneNumberACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IBindPhoneNumberACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BindPhoneNumberACK message, length delimited. Does not implicitly {@link login.BindPhoneNumberACK.verify|verify} messages.
         * @param message BindPhoneNumberACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IBindPhoneNumberACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BindPhoneNumberACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BindPhoneNumberACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.BindPhoneNumberACK;

        /**
         * Decodes a BindPhoneNumberACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BindPhoneNumberACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.BindPhoneNumberACK;

        /**
         * Verifies a BindPhoneNumberACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace BindPhoneNumberACK {

        /** BindPhoneNumberACK enum. */
        enum BindPhoneNumberACK {
            SUCCESS = 0,
            ALREADY_BIND = 1,
            FETAL = 2
        }
    }

    /** Properties of a SendVerifyCodeREQ. */
    interface ISendVerifyCodeREQ {

        /** SendVerifyCodeREQ PhoneNumber */
        PhoneNumber?: (string|null);
    }

    /** Represents a SendVerifyCodeREQ. */
    class SendVerifyCodeREQ implements ISendVerifyCodeREQ {

        /**
         * Constructs a new SendVerifyCodeREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.ISendVerifyCodeREQ);

        /** SendVerifyCodeREQ PhoneNumber. */
        public PhoneNumber: string;

        /**
         * Creates a new SendVerifyCodeREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SendVerifyCodeREQ instance
         */
        public static create(properties?: login.ISendVerifyCodeREQ): login.SendVerifyCodeREQ;

        /**
         * Encodes the specified SendVerifyCodeREQ message. Does not implicitly {@link login.SendVerifyCodeREQ.verify|verify} messages.
         * @param message SendVerifyCodeREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.ISendVerifyCodeREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SendVerifyCodeREQ message, length delimited. Does not implicitly {@link login.SendVerifyCodeREQ.verify|verify} messages.
         * @param message SendVerifyCodeREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.ISendVerifyCodeREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SendVerifyCodeREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SendVerifyCodeREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.SendVerifyCodeREQ;

        /**
         * Decodes a SendVerifyCodeREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SendVerifyCodeREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.SendVerifyCodeREQ;

        /**
         * Verifies a SendVerifyCodeREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a NoticeREQ. */
    interface INoticeREQ {
    }

    /** Represents a NoticeREQ. */
    class NoticeREQ implements INoticeREQ {

        /**
         * Constructs a new NoticeREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.INoticeREQ);

        /**
         * Creates a new NoticeREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns NoticeREQ instance
         */
        public static create(properties?: login.INoticeREQ): login.NoticeREQ;

        /**
         * Encodes the specified NoticeREQ message. Does not implicitly {@link login.NoticeREQ.verify|verify} messages.
         * @param message NoticeREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.INoticeREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified NoticeREQ message, length delimited. Does not implicitly {@link login.NoticeREQ.verify|verify} messages.
         * @param message NoticeREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.INoticeREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a NoticeREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns NoticeREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.NoticeREQ;

        /**
         * Decodes a NoticeREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns NoticeREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.NoticeREQ;

        /**
         * Verifies a NoticeREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a NoticeACK. */
    interface INoticeACK {

        /** NoticeACK Notice */
        Notice?: (string|null);
    }

    /** Represents a NoticeACK. */
    class NoticeACK implements INoticeACK {

        /**
         * Constructs a new NoticeACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.INoticeACK);

        /** NoticeACK Notice. */
        public Notice: string;

        /**
         * Creates a new NoticeACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns NoticeACK instance
         */
        public static create(properties?: login.INoticeACK): login.NoticeACK;

        /**
         * Encodes the specified NoticeACK message. Does not implicitly {@link login.NoticeACK.verify|verify} messages.
         * @param message NoticeACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.INoticeACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified NoticeACK message, length delimited. Does not implicitly {@link login.NoticeACK.verify|verify} messages.
         * @param message NoticeACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.INoticeACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a NoticeACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns NoticeACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.NoticeACK;

        /**
         * Decodes a NoticeACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns NoticeACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.NoticeACK;

        /**
         * Verifies a NoticeACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DiamondChangeNTF. */
    interface IDiamondChangeNTF {

        /** DiamondChangeNTF Diamond */
        Diamond?: (number|Long|null);

        /** DiamondChangeNTF Delta */
        Delta?: (number|Long|null);

        /** DiamondChangeNTF Reason */
        Reason?: (string|null);

        /** DiamondChangeNTF UserID */
        UserID?: (number|Long|null);

        /** DiamondChangeNTF Date */
        Date?: (number|Long|null);
    }

    /** Represents a DiamondChangeNTF. */
    class DiamondChangeNTF implements IDiamondChangeNTF {

        /**
         * Constructs a new DiamondChangeNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IDiamondChangeNTF);

        /** DiamondChangeNTF Diamond. */
        public Diamond: (number|Long);

        /** DiamondChangeNTF Delta. */
        public Delta: (number|Long);

        /** DiamondChangeNTF Reason. */
        public Reason: string;

        /** DiamondChangeNTF UserID. */
        public UserID: (number|Long);

        /** DiamondChangeNTF Date. */
        public Date: (number|Long);

        /**
         * Creates a new DiamondChangeNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DiamondChangeNTF instance
         */
        public static create(properties?: login.IDiamondChangeNTF): login.DiamondChangeNTF;

        /**
         * Encodes the specified DiamondChangeNTF message. Does not implicitly {@link login.DiamondChangeNTF.verify|verify} messages.
         * @param message DiamondChangeNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IDiamondChangeNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DiamondChangeNTF message, length delimited. Does not implicitly {@link login.DiamondChangeNTF.verify|verify} messages.
         * @param message DiamondChangeNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IDiamondChangeNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DiamondChangeNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DiamondChangeNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.DiamondChangeNTF;

        /**
         * Decodes a DiamondChangeNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DiamondChangeNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.DiamondChangeNTF;

        /**
         * Verifies a DiamondChangeNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a UserInfoREQ. */
    interface IUserInfoREQ {

        /** UserInfoREQ UserID */
        UserID?: ((number|Long)[]|null);
    }

    /** Represents a UserInfoREQ. */
    class UserInfoREQ implements IUserInfoREQ {

        /**
         * Constructs a new UserInfoREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IUserInfoREQ);

        /** UserInfoREQ UserID. */
        public UserID: (number|Long)[];

        /**
         * Creates a new UserInfoREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UserInfoREQ instance
         */
        public static create(properties?: login.IUserInfoREQ): login.UserInfoREQ;

        /**
         * Encodes the specified UserInfoREQ message. Does not implicitly {@link login.UserInfoREQ.verify|verify} messages.
         * @param message UserInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IUserInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified UserInfoREQ message, length delimited. Does not implicitly {@link login.UserInfoREQ.verify|verify} messages.
         * @param message UserInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IUserInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a UserInfoREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UserInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.UserInfoREQ;

        /**
         * Decodes a UserInfoREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UserInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.UserInfoREQ;

        /**
         * Verifies a UserInfoREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a UserInfoACK. */
    interface IUserInfoACK {

        /** UserInfoACK userInfo */
        userInfo?: (structure.IUserInfo[]|null);
    }

    /** Represents a UserInfoACK. */
    class UserInfoACK implements IUserInfoACK {

        /**
         * Constructs a new UserInfoACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IUserInfoACK);

        /** UserInfoACK userInfo. */
        public userInfo: structure.IUserInfo[];

        /**
         * Creates a new UserInfoACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UserInfoACK instance
         */
        public static create(properties?: login.IUserInfoACK): login.UserInfoACK;

        /**
         * Encodes the specified UserInfoACK message. Does not implicitly {@link login.UserInfoACK.verify|verify} messages.
         * @param message UserInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IUserInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified UserInfoACK message, length delimited. Does not implicitly {@link login.UserInfoACK.verify|verify} messages.
         * @param message UserInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IUserInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a UserInfoACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UserInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.UserInfoACK;

        /**
         * Decodes a UserInfoACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UserInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.UserInfoACK;

        /**
         * Verifies a UserInfoACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** MailID enum. */
    enum MailID {
        ID = 0,
        ID_MailREQ = 20301,
        ID_MailACK = 20302,
        ID_MailNTF = 20303,
        ID_PublishMailNTF = 20304,
        ID_TeahouseMailREQ = 20305,
        ID_TeahouseMailACK = 20306
    }

    /** Properties of a MailREQ. */
    interface IMailREQ {
    }

    /** Represents a MailREQ. */
    class MailREQ implements IMailREQ {

        /**
         * Constructs a new MailREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IMailREQ);

        /**
         * Creates a new MailREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns MailREQ instance
         */
        public static create(properties?: login.IMailREQ): login.MailREQ;

        /**
         * Encodes the specified MailREQ message. Does not implicitly {@link login.MailREQ.verify|verify} messages.
         * @param message MailREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IMailREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified MailREQ message, length delimited. Does not implicitly {@link login.MailREQ.verify|verify} messages.
         * @param message MailREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IMailREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a MailREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns MailREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.MailREQ;

        /**
         * Decodes a MailREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns MailREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.MailREQ;

        /**
         * Verifies a MailREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a MailACK. */
    interface IMailACK {

        /** MailACK Mail */
        Mail?: (structure.IMail[]|null);
    }

    /** Represents a MailACK. */
    class MailACK implements IMailACK {

        /**
         * Constructs a new MailACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IMailACK);

        /** MailACK Mail. */
        public Mail: structure.IMail[];

        /**
         * Creates a new MailACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns MailACK instance
         */
        public static create(properties?: login.IMailACK): login.MailACK;

        /**
         * Encodes the specified MailACK message. Does not implicitly {@link login.MailACK.verify|verify} messages.
         * @param message MailACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IMailACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified MailACK message, length delimited. Does not implicitly {@link login.MailACK.verify|verify} messages.
         * @param message MailACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IMailACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a MailACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns MailACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.MailACK;

        /**
         * Decodes a MailACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns MailACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.MailACK;

        /**
         * Verifies a MailACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a MailNTF. */
    interface IMailNTF {

        /** MailNTF Mail */
        Mail?: (structure.IMail|null);
    }

    /** Represents a MailNTF. */
    class MailNTF implements IMailNTF {

        /**
         * Constructs a new MailNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IMailNTF);

        /** MailNTF Mail. */
        public Mail?: (structure.IMail|null);

        /**
         * Creates a new MailNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns MailNTF instance
         */
        public static create(properties?: login.IMailNTF): login.MailNTF;

        /**
         * Encodes the specified MailNTF message. Does not implicitly {@link login.MailNTF.verify|verify} messages.
         * @param message MailNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IMailNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified MailNTF message, length delimited. Does not implicitly {@link login.MailNTF.verify|verify} messages.
         * @param message MailNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IMailNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a MailNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns MailNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.MailNTF;

        /**
         * Decodes a MailNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns MailNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.MailNTF;

        /**
         * Verifies a MailNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a PublishMailNTF. */
    interface IPublishMailNTF {

        /** PublishMailNTF Token */
        Token?: (number|Long|null);

        /** PublishMailNTF Mail */
        Mail?: (structure.IMail|null);
    }

    /** Represents a PublishMailNTF. */
    class PublishMailNTF implements IPublishMailNTF {

        /**
         * Constructs a new PublishMailNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.IPublishMailNTF);

        /** PublishMailNTF Token. */
        public Token: (number|Long);

        /** PublishMailNTF Mail. */
        public Mail?: (structure.IMail|null);

        /**
         * Creates a new PublishMailNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PublishMailNTF instance
         */
        public static create(properties?: login.IPublishMailNTF): login.PublishMailNTF;

        /**
         * Encodes the specified PublishMailNTF message. Does not implicitly {@link login.PublishMailNTF.verify|verify} messages.
         * @param message PublishMailNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.IPublishMailNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified PublishMailNTF message, length delimited. Does not implicitly {@link login.PublishMailNTF.verify|verify} messages.
         * @param message PublishMailNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.IPublishMailNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a PublishMailNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PublishMailNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.PublishMailNTF;

        /**
         * Decodes a PublishMailNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PublishMailNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.PublishMailNTF;

        /**
         * Verifies a PublishMailNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeahouseMailREQ. */
    interface ITeahouseMailREQ {

        /** TeahouseMailREQ HouseID */
        HouseID?: (number|Long|null);
    }

    /** Represents a TeahouseMailREQ. */
    class TeahouseMailREQ implements ITeahouseMailREQ {

        /**
         * Constructs a new TeahouseMailREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.ITeahouseMailREQ);

        /** TeahouseMailREQ HouseID. */
        public HouseID: (number|Long);

        /**
         * Creates a new TeahouseMailREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeahouseMailREQ instance
         */
        public static create(properties?: login.ITeahouseMailREQ): login.TeahouseMailREQ;

        /**
         * Encodes the specified TeahouseMailREQ message. Does not implicitly {@link login.TeahouseMailREQ.verify|verify} messages.
         * @param message TeahouseMailREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.ITeahouseMailREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeahouseMailREQ message, length delimited. Does not implicitly {@link login.TeahouseMailREQ.verify|verify} messages.
         * @param message TeahouseMailREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.ITeahouseMailREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeahouseMailREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeahouseMailREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.TeahouseMailREQ;

        /**
         * Decodes a TeahouseMailREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeahouseMailREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.TeahouseMailREQ;

        /**
         * Verifies a TeahouseMailREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeahouseMailACK. */
    interface ITeahouseMailACK {

        /** TeahouseMailACK HouseID */
        HouseID?: (number|Long|null);

        /** TeahouseMailACK Mail */
        Mail?: (structure.IMail[]|null);
    }

    /** Represents a TeahouseMailACK. */
    class TeahouseMailACK implements ITeahouseMailACK {

        /**
         * Constructs a new TeahouseMailACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: login.ITeahouseMailACK);

        /** TeahouseMailACK HouseID. */
        public HouseID: (number|Long);

        /** TeahouseMailACK Mail. */
        public Mail: structure.IMail[];

        /**
         * Creates a new TeahouseMailACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeahouseMailACK instance
         */
        public static create(properties?: login.ITeahouseMailACK): login.TeahouseMailACK;

        /**
         * Encodes the specified TeahouseMailACK message. Does not implicitly {@link login.TeahouseMailACK.verify|verify} messages.
         * @param message TeahouseMailACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: login.ITeahouseMailACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeahouseMailACK message, length delimited. Does not implicitly {@link login.TeahouseMailACK.verify|verify} messages.
         * @param message TeahouseMailACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: login.ITeahouseMailACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeahouseMailACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeahouseMailACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): login.TeahouseMailACK;

        /**
         * Decodes a TeahouseMailACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeahouseMailACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): login.TeahouseMailACK;

        /**
         * Verifies a TeahouseMailACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }
}

/** Namespace structure. */
declare namespace structure {

    /** GameID enum. */
    enum GameID {
        None = 0,
        Niuniu = 501,
        Zhajinhua = 502,
        Fish = 503,
        Lord = 504,
        MAX = 600
    }

    /** TableStage enum. */
    enum TableStage {
        Wait = 0,
        Play = 2
    }

    /** Properties of a UserInfo. */
    interface IUserInfo {

        /** UserInfo ID */
        ID?: (number|Long|null);

        /** UserInfo Nickname */
        Nickname?: (string|null);

        /** UserInfo Diamond */
        Diamond?: (number|Long|null);

        /** UserInfo HeadIcon */
        HeadIcon?: (string|null);

        /** UserInfo Sex */
        Sex?: (number|null);

        /** UserInfo Round */
        Round?: (number|null);

        /** UserInfo RegistDate */
        RegistDate?: (number|Long|null);

        /** UserInfo IpAddr */
        IpAddr?: (string|null);

        /** UserInfo BindPhone */
        BindPhone?: (boolean|null);

        /** UserInfo BindIdentify */
        BindIdentify?: (boolean|null);
    }

    /** Represents a UserInfo. */
    class UserInfo implements IUserInfo {

        /**
         * Constructs a new UserInfo.
         * @param [properties] Properties to set
         */
        constructor(properties?: structure.IUserInfo);

        /** UserInfo ID. */
        public ID: (number|Long);

        /** UserInfo Nickname. */
        public Nickname: string;

        /** UserInfo Diamond. */
        public Diamond: (number|Long);

        /** UserInfo HeadIcon. */
        public HeadIcon: string;

        /** UserInfo Sex. */
        public Sex: number;

        /** UserInfo Round. */
        public Round: number;

        /** UserInfo RegistDate. */
        public RegistDate: (number|Long);

        /** UserInfo IpAddr. */
        public IpAddr: string;

        /** UserInfo BindPhone. */
        public BindPhone: boolean;

        /** UserInfo BindIdentify. */
        public BindIdentify: boolean;

        /**
         * Creates a new UserInfo instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UserInfo instance
         */
        public static create(properties?: structure.IUserInfo): structure.UserInfo;

        /**
         * Encodes the specified UserInfo message. Does not implicitly {@link structure.UserInfo.verify|verify} messages.
         * @param message UserInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: structure.IUserInfo, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified UserInfo message, length delimited. Does not implicitly {@link structure.UserInfo.verify|verify} messages.
         * @param message UserInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: structure.IUserInfo, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a UserInfo message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UserInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.UserInfo;

        /**
         * Decodes a UserInfo message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UserInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.UserInfo;

        /**
         * Verifies a UserInfo message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of an Item. */
    interface IItem {

        /** Item ID */
        ID?: (number|null);

        /** Item Count */
        Count?: (number|null);
    }

    /** Represents an Item. */
    class Item implements IItem {

        /**
         * Constructs a new Item.
         * @param [properties] Properties to set
         */
        constructor(properties?: structure.IItem);

        /** Item ID. */
        public ID: number;

        /** Item Count. */
        public Count: number;

        /**
         * Creates a new Item instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Item instance
         */
        public static create(properties?: structure.IItem): structure.Item;

        /**
         * Encodes the specified Item message. Does not implicitly {@link structure.Item.verify|verify} messages.
         * @param message Item message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: structure.IItem, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified Item message, length delimited. Does not implicitly {@link structure.Item.verify|verify} messages.
         * @param message Item message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: structure.IItem, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an Item message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Item
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.Item;

        /**
         * Decodes an Item message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Item
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.Item;

        /**
         * Verifies an Item message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a Mail. */
    interface IMail {

        /** Mail Type */
        Type?: (structure.Mail.MailType|null);

        /** Mail Title */
        Title?: (string|null);

        /** Mail Content */
        Content?: (string|null);

        /** Mail Items */
        Items?: (structure.IItem[]|null);

        /** Mail Read */
        Read?: (boolean|null);

        /** Mail CreateDate */
        CreateDate?: (number|Long|null);

        /** Mail OrderID */
        OrderID?: (string|null);

        /** Mail MailID */
        MailID?: (string|null);

        /** Mail Player */
        Player?: (structure.Mail.IMailPlayer|null);
    }

    /** Represents a Mail. */
    class Mail implements IMail {

        /**
         * Constructs a new Mail.
         * @param [properties] Properties to set
         */
        constructor(properties?: structure.IMail);

        /** Mail Type. */
        public Type: structure.Mail.MailType;

        /** Mail Title. */
        public Title: string;

        /** Mail Content. */
        public Content: string;

        /** Mail Items. */
        public Items: structure.IItem[];

        /** Mail Read. */
        public Read: boolean;

        /** Mail CreateDate. */
        public CreateDate: (number|Long);

        /** Mail OrderID. */
        public OrderID: string;

        /** Mail MailID. */
        public MailID: string;

        /** Mail Player. */
        public Player?: (structure.Mail.IMailPlayer|null);

        /**
         * Creates a new Mail instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Mail instance
         */
        public static create(properties?: structure.IMail): structure.Mail;

        /**
         * Encodes the specified Mail message. Does not implicitly {@link structure.Mail.verify|verify} messages.
         * @param message Mail message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: structure.IMail, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified Mail message, length delimited. Does not implicitly {@link structure.Mail.verify|verify} messages.
         * @param message Mail message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: structure.IMail, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a Mail message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Mail
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.Mail;

        /**
         * Decodes a Mail message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Mail
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.Mail;

        /**
         * Verifies a Mail message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace Mail {

        /** MailType enum. */
        enum MailType {
            Text = 0,
            Transfer = 1,
            Invite = 2,
            SetCharger = 3,
            Freeze = 4,
            Remove = 5,
            Join = 6,
            PartnerReq = 7
        }

        /** Properties of a MailPlayer. */
        interface IMailPlayer {

            /** MailPlayer UserID */
            UserID?: (number|Long|null);

            /** MailPlayer NickName */
            NickName?: (string|null);

            /** MailPlayer HeadIcon */
            HeadIcon?: (string|null);
        }

        /** Represents a MailPlayer. */
        class MailPlayer implements IMailPlayer {

            /**
             * Constructs a new MailPlayer.
             * @param [properties] Properties to set
             */
            constructor(properties?: structure.Mail.IMailPlayer);

            /** MailPlayer UserID. */
            public UserID: (number|Long);

            /** MailPlayer NickName. */
            public NickName: string;

            /** MailPlayer HeadIcon. */
            public HeadIcon: string;

            /**
             * Creates a new MailPlayer instance using the specified properties.
             * @param [properties] Properties to set
             * @returns MailPlayer instance
             */
            public static create(properties?: structure.Mail.IMailPlayer): structure.Mail.MailPlayer;

            /**
             * Encodes the specified MailPlayer message. Does not implicitly {@link structure.Mail.MailPlayer.verify|verify} messages.
             * @param message MailPlayer message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: structure.Mail.IMailPlayer, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Encodes the specified MailPlayer message, length delimited. Does not implicitly {@link structure.Mail.MailPlayer.verify|verify} messages.
             * @param message MailPlayer message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: structure.Mail.IMailPlayer, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Decodes a MailPlayer message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns MailPlayer
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.Mail.MailPlayer;

            /**
             * Decodes a MailPlayer message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns MailPlayer
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.Mail.MailPlayer;

            /**
             * Verifies a MailPlayer message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);
        }
    }

    /** Properties of a TableConfig. */
    interface ITableConfig {

        /** TableConfig MaxPlayer */
        MaxPlayer?: (number|null);

        /** TableConfig ChargeCfg */
        ChargeCfg?: (structure.TableConfig.ChargeConfig|null);

        /** TableConfig MaxRound */
        MaxRound?: (number|null);

        /** TableConfig BaseScoreCfg */
        BaseScoreCfg?: (structure.TableConfig.IBaseScoreConfig|null);

        /** TableConfig AutoStartCfg */
        AutoStartCfg?: (structure.TableConfig.IAutoStartConfig|null);

        /** TableConfig GameId */
        GameId?: (number|null);

        /** TableConfig MaxRankerMulti */
        MaxRankerMulti?: (number|null);

        /** TableConfig MultiRuler */
        MultiRuler?: (number[]|null);

        /** TableConfig SpecialCardMulti */
        SpecialCardMulti?: (structure.TableConfig.SpecialCardMultiple[]|null);

        /** TableConfig AdvancedCfg */
        AdvancedCfg?: (structure.TableConfig.AdvancedConfig[]|null);

        /** TableConfig JokerType */
        JokerType?: (structure.TableConfig.JokerTypeConfig|null);

        /** TableConfig TuizhuRule */
        TuizhuRule?: (number|null);

        /** TableConfig MatchType */
        MatchType?: (structure.TableConfig.MatchTypeConfig|null);

        /** TableConfig PlayRuler */
        PlayRuler?: (structure.TableConfig.PlayRule|null);

        /** TableConfig Stage */
        Stage?: (structure.TableStage|null);
    }

    /** Represents a TableConfig. */
    class TableConfig implements ITableConfig {

        /**
         * Constructs a new TableConfig.
         * @param [properties] Properties to set
         */
        constructor(properties?: structure.ITableConfig);

        /** TableConfig MaxPlayer. */
        public MaxPlayer: number;

        /** TableConfig ChargeCfg. */
        public ChargeCfg: structure.TableConfig.ChargeConfig;

        /** TableConfig MaxRound. */
        public MaxRound: number;

        /** TableConfig BaseScoreCfg. */
        public BaseScoreCfg?: (structure.TableConfig.IBaseScoreConfig|null);

        /** TableConfig AutoStartCfg. */
        public AutoStartCfg?: (structure.TableConfig.IAutoStartConfig|null);

        /** TableConfig GameId. */
        public GameId: number;

        /** TableConfig MaxRankerMulti. */
        public MaxRankerMulti: number;

        /** TableConfig MultiRuler. */
        public MultiRuler: number[];

        /** TableConfig SpecialCardMulti. */
        public SpecialCardMulti: structure.TableConfig.SpecialCardMultiple[];

        /** TableConfig AdvancedCfg. */
        public AdvancedCfg: structure.TableConfig.AdvancedConfig[];

        /** TableConfig JokerType. */
        public JokerType: structure.TableConfig.JokerTypeConfig;

        /** TableConfig TuizhuRule. */
        public TuizhuRule: number;

        /** TableConfig MatchType. */
        public MatchType: structure.TableConfig.MatchTypeConfig;

        /** TableConfig PlayRuler. */
        public PlayRuler: structure.TableConfig.PlayRule;

        /** TableConfig Stage. */
        public Stage: structure.TableStage;

        /**
         * Creates a new TableConfig instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TableConfig instance
         */
        public static create(properties?: structure.ITableConfig): structure.TableConfig;

        /**
         * Encodes the specified TableConfig message. Does not implicitly {@link structure.TableConfig.verify|verify} messages.
         * @param message TableConfig message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: structure.ITableConfig, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TableConfig message, length delimited. Does not implicitly {@link structure.TableConfig.verify|verify} messages.
         * @param message TableConfig message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: structure.ITableConfig, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TableConfig message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TableConfig
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.TableConfig;

        /**
         * Decodes a TableConfig message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TableConfig
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.TableConfig;

        /**
         * Verifies a TableConfig message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace TableConfig {

        /** ChargeConfig enum. */
        enum ChargeConfig {
            OwnerCharge = 0,
            AACharge = 2
        }

        /** AutoStart enum. */
        enum AutoStart {
            OwnerStart = 0,
            FirstStart = 1,
            PlayerCount = 2
        }

        /** Properties of a BaseScoreConfig. */
        interface IBaseScoreConfig {

            /** BaseScoreConfig BaseScore */
            BaseScore?: (number|Long|null);

            /** BaseScoreConfig ValidBaseScore */
            ValidBaseScore?: ((number|Long)[]|null);

            /** BaseScoreConfig MaxScore */
            MaxScore?: (number|Long|null);
        }

        /** Represents a BaseScoreConfig. */
        class BaseScoreConfig implements IBaseScoreConfig {

            /**
             * Constructs a new BaseScoreConfig.
             * @param [properties] Properties to set
             */
            constructor(properties?: structure.TableConfig.IBaseScoreConfig);

            /** BaseScoreConfig BaseScore. */
            public BaseScore: (number|Long);

            /** BaseScoreConfig ValidBaseScore. */
            public ValidBaseScore: (number|Long)[];

            /** BaseScoreConfig MaxScore. */
            public MaxScore: (number|Long);

            /**
             * Creates a new BaseScoreConfig instance using the specified properties.
             * @param [properties] Properties to set
             * @returns BaseScoreConfig instance
             */
            public static create(properties?: structure.TableConfig.IBaseScoreConfig): structure.TableConfig.BaseScoreConfig;

            /**
             * Encodes the specified BaseScoreConfig message. Does not implicitly {@link structure.TableConfig.BaseScoreConfig.verify|verify} messages.
             * @param message BaseScoreConfig message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: structure.TableConfig.IBaseScoreConfig, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Encodes the specified BaseScoreConfig message, length delimited. Does not implicitly {@link structure.TableConfig.BaseScoreConfig.verify|verify} messages.
             * @param message BaseScoreConfig message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: structure.TableConfig.IBaseScoreConfig, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Decodes a BaseScoreConfig message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns BaseScoreConfig
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.TableConfig.BaseScoreConfig;

            /**
             * Decodes a BaseScoreConfig message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns BaseScoreConfig
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.TableConfig.BaseScoreConfig;

            /**
             * Verifies a BaseScoreConfig message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);
        }

        /** Properties of an AutoStartConfig. */
        interface IAutoStartConfig {

            /** AutoStartConfig AutoStart */
            AutoStart?: (structure.TableConfig.AutoStart|null);

            /** AutoStartConfig PlayerCount */
            PlayerCount?: (number|null);
        }

        /** Represents an AutoStartConfig. */
        class AutoStartConfig implements IAutoStartConfig {

            /**
             * Constructs a new AutoStartConfig.
             * @param [properties] Properties to set
             */
            constructor(properties?: structure.TableConfig.IAutoStartConfig);

            /** AutoStartConfig AutoStart. */
            public AutoStart: structure.TableConfig.AutoStart;

            /** AutoStartConfig PlayerCount. */
            public PlayerCount: number;

            /**
             * Creates a new AutoStartConfig instance using the specified properties.
             * @param [properties] Properties to set
             * @returns AutoStartConfig instance
             */
            public static create(properties?: structure.TableConfig.IAutoStartConfig): structure.TableConfig.AutoStartConfig;

            /**
             * Encodes the specified AutoStartConfig message. Does not implicitly {@link structure.TableConfig.AutoStartConfig.verify|verify} messages.
             * @param message AutoStartConfig message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: structure.TableConfig.IAutoStartConfig, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Encodes the specified AutoStartConfig message, length delimited. Does not implicitly {@link structure.TableConfig.AutoStartConfig.verify|verify} messages.
             * @param message AutoStartConfig message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: structure.TableConfig.IAutoStartConfig, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Decodes an AutoStartConfig message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns AutoStartConfig
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.TableConfig.AutoStartConfig;

            /**
             * Decodes an AutoStartConfig message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns AutoStartConfig
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.TableConfig.AutoStartConfig;

            /**
             * Verifies an AutoStartConfig message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);
        }

        /** MaxRankerMultiple enum. */
        enum MaxRankerMultiple {
            Zero = 0,
            One = 1,
            Two = 2,
            Three = 3,
            Four = 4,
            Ten = 10
        }

        /** SpecialCardMultiple enum. */
        enum SpecialCardMultiple {
            Straight = 0,
            Wuhua = 2,
            Flush = 3,
            Hulu = 4,
            Bomb = 5,
            Wuxiao = 6,
            StraightFlush = 7
        }

        /** AdvancedConfig enum. */
        enum AdvancedConfig {
            Joker = 0,
            ForbidenEnter = 1,
            ForbidenCuopai = 2,
            TuizhuLimit = 3,
            BetLimit = 4
        }

        /** JokerTypeConfig enum. */
        enum JokerTypeConfig {
            None = 0,
            Classic = 1,
            Crazy = 2
        }

        /** MatchTypeConfig enum. */
        enum MatchTypeConfig {
            Normal = 0,
            Match = 1
        }

        /** PlayRule enum. */
        enum PlayRule {
            QiangZhuangNiuniu = 0
        }
    }

    /** Properties of a TableDrawConfig. */
    interface ITableDrawConfig {

        /** TableDrawConfig GameScore */
        GameScore?: (number|Long|null);

        /** TableDrawConfig RankScore */
        RankScore?: (number|Long|null);

        /** TableDrawConfig Ruler */
        Ruler?: (structure.TableDrawConfig.ScoreRuler|null);

        /** TableDrawConfig Adjust */
        Adjust?: (structure.TableDrawConfig.ScoreAdjust|null);

        /** TableDrawConfig Draw */
        Draw?: (structure.TableDrawConfig.IDrawRuler|null);
    }

    /** Represents a TableDrawConfig. */
    class TableDrawConfig implements ITableDrawConfig {

        /**
         * Constructs a new TableDrawConfig.
         * @param [properties] Properties to set
         */
        constructor(properties?: structure.ITableDrawConfig);

        /** TableDrawConfig GameScore. */
        public GameScore: (number|Long);

        /** TableDrawConfig RankScore. */
        public RankScore: (number|Long);

        /** TableDrawConfig Ruler. */
        public Ruler: structure.TableDrawConfig.ScoreRuler;

        /** TableDrawConfig Adjust. */
        public Adjust: structure.TableDrawConfig.ScoreAdjust;

        /** TableDrawConfig Draw. */
        public Draw?: (structure.TableDrawConfig.IDrawRuler|null);

        /**
         * Creates a new TableDrawConfig instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TableDrawConfig instance
         */
        public static create(properties?: structure.ITableDrawConfig): structure.TableDrawConfig;

        /**
         * Encodes the specified TableDrawConfig message. Does not implicitly {@link structure.TableDrawConfig.verify|verify} messages.
         * @param message TableDrawConfig message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: structure.ITableDrawConfig, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TableDrawConfig message, length delimited. Does not implicitly {@link structure.TableDrawConfig.verify|verify} messages.
         * @param message TableDrawConfig message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: structure.ITableDrawConfig, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TableDrawConfig message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TableDrawConfig
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.TableDrawConfig;

        /**
         * Decodes a TableDrawConfig message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TableDrawConfig
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.TableDrawConfig;

        /**
         * Verifies a TableDrawConfig message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace TableDrawConfig {

        /** ScoreRuler enum. */
        enum ScoreRuler {
            NoneNagetive = 0,
            Nagetive = 1
        }

        /** ScoreAdjust enum. */
        enum ScoreAdjust {
            NoneInGame = 0,
            InGame = 1
        }

        /** DrawFrom enum. */
        enum DrawFrom {
            AllWin = 0,
            MaxWin = 1
        }

        /** Properties of a DrawRuler. */
        interface IDrawRuler {

            /** DrawRuler DrawFrom */
            DrawFrom?: (structure.TableDrawConfig.DrawFrom|null);

            /** DrawRuler Rate */
            Rate?: (number|Long|null);

            /** DrawRuler BaseScore */
            BaseScore?: (number|Long|null);
        }

        /** Represents a DrawRuler. */
        class DrawRuler implements IDrawRuler {

            /**
             * Constructs a new DrawRuler.
             * @param [properties] Properties to set
             */
            constructor(properties?: structure.TableDrawConfig.IDrawRuler);

            /** DrawRuler DrawFrom. */
            public DrawFrom: structure.TableDrawConfig.DrawFrom;

            /** DrawRuler Rate. */
            public Rate: (number|Long);

            /** DrawRuler BaseScore. */
            public BaseScore: (number|Long);

            /**
             * Creates a new DrawRuler instance using the specified properties.
             * @param [properties] Properties to set
             * @returns DrawRuler instance
             */
            public static create(properties?: structure.TableDrawConfig.IDrawRuler): structure.TableDrawConfig.DrawRuler;

            /**
             * Encodes the specified DrawRuler message. Does not implicitly {@link structure.TableDrawConfig.DrawRuler.verify|verify} messages.
             * @param message DrawRuler message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: structure.TableDrawConfig.IDrawRuler, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Encodes the specified DrawRuler message, length delimited. Does not implicitly {@link structure.TableDrawConfig.DrawRuler.verify|verify} messages.
             * @param message DrawRuler message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: structure.TableDrawConfig.IDrawRuler, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Decodes a DrawRuler message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns DrawRuler
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.TableDrawConfig.DrawRuler;

            /**
             * Decodes a DrawRuler message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns DrawRuler
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.TableDrawConfig.DrawRuler;

            /**
             * Verifies a DrawRuler message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);
        }
    }

    /** Properties of a TablePlayer. */
    interface ITablePlayer {

        /** TablePlayer Seat */
        Seat?: (number|null);

        /** TablePlayer UserID */
        UserID?: (number|Long|null);

        /** TablePlayer NickName */
        NickName?: (string|null);

        /** TablePlayer HeadIcon */
        HeadIcon?: (string|null);

        /** TablePlayer Score */
        Score?: (number|Long|null);
    }

    /** Represents a TablePlayer. */
    class TablePlayer implements ITablePlayer {

        /**
         * Constructs a new TablePlayer.
         * @param [properties] Properties to set
         */
        constructor(properties?: structure.ITablePlayer);

        /** TablePlayer Seat. */
        public Seat: number;

        /** TablePlayer UserID. */
        public UserID: (number|Long);

        /** TablePlayer NickName. */
        public NickName: string;

        /** TablePlayer HeadIcon. */
        public HeadIcon: string;

        /** TablePlayer Score. */
        public Score: (number|Long);

        /**
         * Creates a new TablePlayer instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TablePlayer instance
         */
        public static create(properties?: structure.ITablePlayer): structure.TablePlayer;

        /**
         * Encodes the specified TablePlayer message. Does not implicitly {@link structure.TablePlayer.verify|verify} messages.
         * @param message TablePlayer message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: structure.ITablePlayer, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TablePlayer message, length delimited. Does not implicitly {@link structure.TablePlayer.verify|verify} messages.
         * @param message TablePlayer message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: structure.ITablePlayer, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TablePlayer message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TablePlayer
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.TablePlayer;

        /**
         * Decodes a TablePlayer message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TablePlayer
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.TablePlayer;

        /**
         * Verifies a TablePlayer message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TableInfo. */
    interface ITableInfo {

        /** TableInfo TableID */
        TableID?: (number|null);

        /** TableInfo Owner */
        Owner?: (number|Long|null);

        /** TableInfo Player */
        Player?: (structure.ITablePlayer[]|null);

        /** TableInfo Config */
        Config?: (structure.ITableConfig|null);

        /** TableInfo DrawConfig */
        DrawConfig?: (structure.ITableDrawConfig|null);

        /** TableInfo IsHandPlay */
        IsHandPlay?: (boolean|null);

        /** TableInfo Hand */
        Hand?: (number|null);

        /** TableInfo HouseID */
        HouseID?: (number|Long|null);
    }

    /** Represents a TableInfo. */
    class TableInfo implements ITableInfo {

        /**
         * Constructs a new TableInfo.
         * @param [properties] Properties to set
         */
        constructor(properties?: structure.ITableInfo);

        /** TableInfo TableID. */
        public TableID: number;

        /** TableInfo Owner. */
        public Owner: (number|Long);

        /** TableInfo Player. */
        public Player: structure.ITablePlayer[];

        /** TableInfo Config. */
        public Config?: (structure.ITableConfig|null);

        /** TableInfo DrawConfig. */
        public DrawConfig?: (structure.ITableDrawConfig|null);

        /** TableInfo IsHandPlay. */
        public IsHandPlay: boolean;

        /** TableInfo Hand. */
        public Hand: number;

        /** TableInfo HouseID. */
        public HouseID: (number|Long);

        /**
         * Creates a new TableInfo instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TableInfo instance
         */
        public static create(properties?: structure.ITableInfo): structure.TableInfo;

        /**
         * Encodes the specified TableInfo message. Does not implicitly {@link structure.TableInfo.verify|verify} messages.
         * @param message TableInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: structure.ITableInfo, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TableInfo message, length delimited. Does not implicitly {@link structure.TableInfo.verify|verify} messages.
         * @param message TableInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: structure.ITableInfo, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TableInfo message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TableInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.TableInfo;

        /**
         * Decodes a TableInfo message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TableInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.TableInfo;

        /**
         * Verifies a TableInfo message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a Rank. */
    interface IRank {

        /** Rank Player */
        Player?: (structure.Rank.IRankPlayer[]|null);

        /** Rank Start */
        Start?: (number|null);

        /** Rank PageCount */
        PageCount?: (number|null);
    }

    /** Represents a Rank. */
    class Rank implements IRank {

        /**
         * Constructs a new Rank.
         * @param [properties] Properties to set
         */
        constructor(properties?: structure.IRank);

        /** Rank Player. */
        public Player: structure.Rank.IRankPlayer[];

        /** Rank Start. */
        public Start: number;

        /** Rank PageCount. */
        public PageCount: number;

        /**
         * Creates a new Rank instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Rank instance
         */
        public static create(properties?: structure.IRank): structure.Rank;

        /**
         * Encodes the specified Rank message. Does not implicitly {@link structure.Rank.verify|verify} messages.
         * @param message Rank message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: structure.IRank, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified Rank message, length delimited. Does not implicitly {@link structure.Rank.verify|verify} messages.
         * @param message Rank message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: structure.IRank, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a Rank message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Rank
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.Rank;

        /**
         * Decodes a Rank message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Rank
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.Rank;

        /**
         * Verifies a Rank message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace Rank {

        /** Properties of a RankPlayer. */
        interface IRankPlayer {

            /** RankPlayer UserID */
            UserID?: (number|Long|null);

            /** RankPlayer NickName */
            NickName?: (string|null);

            /** RankPlayer HeadIcon */
            HeadIcon?: (string|null);

            /** RankPlayer Value */
            Value?: (number|Long|null);
        }

        /** Represents a RankPlayer. */
        class RankPlayer implements IRankPlayer {

            /**
             * Constructs a new RankPlayer.
             * @param [properties] Properties to set
             */
            constructor(properties?: structure.Rank.IRankPlayer);

            /** RankPlayer UserID. */
            public UserID: (number|Long);

            /** RankPlayer NickName. */
            public NickName: string;

            /** RankPlayer HeadIcon. */
            public HeadIcon: string;

            /** RankPlayer Value. */
            public Value: (number|Long);

            /**
             * Creates a new RankPlayer instance using the specified properties.
             * @param [properties] Properties to set
             * @returns RankPlayer instance
             */
            public static create(properties?: structure.Rank.IRankPlayer): structure.Rank.RankPlayer;

            /**
             * Encodes the specified RankPlayer message. Does not implicitly {@link structure.Rank.RankPlayer.verify|verify} messages.
             * @param message RankPlayer message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: structure.Rank.IRankPlayer, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Encodes the specified RankPlayer message, length delimited. Does not implicitly {@link structure.Rank.RankPlayer.verify|verify} messages.
             * @param message RankPlayer message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: structure.Rank.IRankPlayer, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Decodes a RankPlayer message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns RankPlayer
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): structure.Rank.RankPlayer;

            /**
             * Decodes a RankPlayer message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns RankPlayer
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): structure.Rank.RankPlayer;

            /**
             * Verifies a RankPlayer message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);
        }
    }
}

/** Namespace match. */
declare namespace match {

    /** MatchID enum. */
    enum MatchID {
        ID = 0,
        ID_TableInfoNTF = 40201,
        ID_TeahouseTableInfoNTF = 40202
    }

    /** Properties of a TableInfoNTF. */
    interface ITableInfoNTF {

        /** TableInfoNTF UserID */
        UserID?: (number|Long|null);

        /** TableInfoNTF Info */
        Info?: (structure.ITableInfo[]|null);
    }

    /** Represents a TableInfoNTF. */
    class TableInfoNTF implements ITableInfoNTF {

        /**
         * Constructs a new TableInfoNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: match.ITableInfoNTF);

        /** TableInfoNTF UserID. */
        public UserID: (number|Long);

        /** TableInfoNTF Info. */
        public Info: structure.ITableInfo[];

        /**
         * Creates a new TableInfoNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TableInfoNTF instance
         */
        public static create(properties?: match.ITableInfoNTF): match.TableInfoNTF;

        /**
         * Encodes the specified TableInfoNTF message. Does not implicitly {@link match.TableInfoNTF.verify|verify} messages.
         * @param message TableInfoNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: match.ITableInfoNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TableInfoNTF message, length delimited. Does not implicitly {@link match.TableInfoNTF.verify|verify} messages.
         * @param message TableInfoNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: match.ITableInfoNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TableInfoNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TableInfoNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): match.TableInfoNTF;

        /**
         * Decodes a TableInfoNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TableInfoNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): match.TableInfoNTF;

        /**
         * Verifies a TableInfoNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeahouseTableInfoNTF. */
    interface ITeahouseTableInfoNTF {

        /** TeahouseTableInfoNTF UserID */
        UserID?: (number|Long|null);

        /** TeahouseTableInfoNTF Info */
        Info?: (structure.ITableInfo[]|null);
    }

    /** Represents a TeahouseTableInfoNTF. */
    class TeahouseTableInfoNTF implements ITeahouseTableInfoNTF {

        /**
         * Constructs a new TeahouseTableInfoNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: match.ITeahouseTableInfoNTF);

        /** TeahouseTableInfoNTF UserID. */
        public UserID: (number|Long);

        /** TeahouseTableInfoNTF Info. */
        public Info: structure.ITableInfo[];

        /**
         * Creates a new TeahouseTableInfoNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeahouseTableInfoNTF instance
         */
        public static create(properties?: match.ITeahouseTableInfoNTF): match.TeahouseTableInfoNTF;

        /**
         * Encodes the specified TeahouseTableInfoNTF message. Does not implicitly {@link match.TeahouseTableInfoNTF.verify|verify} messages.
         * @param message TeahouseTableInfoNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: match.ITeahouseTableInfoNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeahouseTableInfoNTF message, length delimited. Does not implicitly {@link match.TeahouseTableInfoNTF.verify|verify} messages.
         * @param message TeahouseTableInfoNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: match.ITeahouseTableInfoNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeahouseTableInfoNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeahouseTableInfoNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): match.TeahouseTableInfoNTF;

        /**
         * Decodes a TeahouseTableInfoNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeahouseTableInfoNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): match.TeahouseTableInfoNTF;

        /**
         * Verifies a TeahouseTableInfoNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }
}

/** Namespace niuniu. */
declare namespace niuniu {

    /** NiuniuID enum. */
    enum NiuniuID {
        ID = 0,
        ID_CreateTableREQ = 50101,
        ID_CreateTableACK = 50102,
        ID_EnterGameREQ = 50103,
        ID_EnterGameACK = 50104,
        ID_StartGameREQ = 50105,
        ID_StartGameACK = 50106,
        ID_GameConfigNTF = 50107,
        ID_TeahouseTableInfoREQ = 50109,
        ID_TeahouseTableInfoACK = 50110,
        ID_TeahouseTableInfoNTF = 50111,
        ID_ScoreChangeNTF = 50112,
        ID_LeaveGameREQ = 50113,
        ID_LeaveGameACK = 50114,
        ID_OwnTableInfoREQ = 50115,
        ID_OwnTableInfoACK = 50116,
        ID_ModifyCardREQ = 50117,
        ID_BeginHandNTF = 50120,
        ID_DispatchFirstFourCardNTF = 50121,
        ID_DispatchLastCardNTF = 50122,
        ID_SettlementNTF = 50123,
        ID_BetREQ = 50124,
        ID_BetACK = 50125,
        ID_RankREQ = 50126,
        ID_RankACK = 50127,
        ID_RankNTF = 50128,
        ID_RoundOverNTF = 50129,
        ID_SitREQ = 50130,
        ID_SitACK = 50131,
        ID_ShowCardREQ = 50132,
        ID_ShowCardACK = 50133,
        ID_SimpelActionREQ = 50134,
        ID_SimpelActionACK = 50135,
        ID_EndHandNTF = 50136,
        ID_TipREQ = 50137,
        ID_TipACK = 50138,
        ID_ObserverTableListREQ = 50140,
        ID_DisObserverTableListREQ = 50141,
        ID_TableConfigChangeREQ = 50142,
        ID_TableConfigChangeACK = 50143,
        ID_RounRecordNTF = 50144,
        ID_DismissREQ = 50145,
        ID_DismissACK = 50146,
        ID_DismissNTF = 50147,
        ID_DismissedNTF = 50148,
        ID_DeleteRoomREQ = 50149,
        ID_DeleteRoomACK = 50150,
        ID_BeginHistoryNTF = 50151,
        ID_EndHistoryNTF = 50152,
        ID_PrepareREQ = 50153,
        ID_PrepareACK = 50154,
        ID_TableInfoNTF = 50155
    }

    /** GameCode enum. */
    enum GameCode {
        SUCCESS = 0,
        TABLE_NUMBER_INVALID = 1,
        DUPLICATED_ENTER = 2,
        TABLE_FULL = 3,
        NOT_ENTER_RIGHT = 4,
        SERVER_BUSY = 5,
        QUIT_FAIL_GAME_START = 6,
        TEAHOUSE_TABLE_FULL = 7,
        TEAHOUSE_ACC_FREEZE = 8,
        DIAMOND_NOT_ENOUGH = 9,
        INVALID_REQ = 10,
        TEAHOUSE_CLOSED = 11,
        SCORE_NOT_ENOUGH = 12,
        PLAYER_EXISTS = 13,
        CHARGET_DIAMOND_NOT_ENOUGH = 14,
        UNKOWN = 199
    }

    /** NiuniuCardType enum. */
    enum NiuniuCardType {
        CardType_None = 0,
        CardType_Niu1 = 1,
        CardType_Niu2 = 2,
        CardType_Niu3 = 3,
        CardType_Niu4 = 4,
        CardType_Niu5 = 5,
        CardType_Niu6 = 6,
        CardType_Niu7 = 7,
        CardType_Niu8 = 8,
        CardType_Niu9 = 9,
        CardType_Niuniu = 10,
        CardType_Straight = 11,
        CardType_Wuhua = 12,
        CardType_Flush = 13,
        CardType_Fullhouse = 14,
        CardType_Bomb = 15,
        CardType_Wuxiao = 16,
        CardType_StraightFlush = 17
    }

    /** Properties of an EnterGameREQ. */
    interface IEnterGameREQ {

        /** EnterGameREQ GameID */
        GameID?: (number|null);

        /** EnterGameREQ TableID */
        TableID?: (number|null);
    }

    /** Represents an EnterGameREQ. */
    class EnterGameREQ implements IEnterGameREQ {

        /**
         * Constructs a new EnterGameREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IEnterGameREQ);

        /** EnterGameREQ GameID. */
        public GameID: number;

        /** EnterGameREQ TableID. */
        public TableID: number;

        /**
         * Creates a new EnterGameREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns EnterGameREQ instance
         */
        public static create(properties?: niuniu.IEnterGameREQ): niuniu.EnterGameREQ;

        /**
         * Encodes the specified EnterGameREQ message. Does not implicitly {@link niuniu.EnterGameREQ.verify|verify} messages.
         * @param message EnterGameREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IEnterGameREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified EnterGameREQ message, length delimited. Does not implicitly {@link niuniu.EnterGameREQ.verify|verify} messages.
         * @param message EnterGameREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IEnterGameREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an EnterGameREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns EnterGameREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.EnterGameREQ;

        /**
         * Decodes an EnterGameREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns EnterGameREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.EnterGameREQ;

        /**
         * Verifies an EnterGameREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of an EnterGameACK. */
    interface IEnterGameACK {

        /** EnterGameACK GameID */
        GameID?: (number|null);

        /** EnterGameACK TableID */
        TableID?: (number|null);

        /** EnterGameACK Code */
        Code?: (niuniu.GameCode|null);

        /** EnterGameACK UserID */
        UserID?: (number|Long|null);

        /** EnterGameACK TableInfo */
        TableInfo?: (structure.ITableInfo|null);

        /** EnterGameACK Reconnect */
        Reconnect?: (boolean|null);
    }

    /** Represents an EnterGameACK. */
    class EnterGameACK implements IEnterGameACK {

        /**
         * Constructs a new EnterGameACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IEnterGameACK);

        /** EnterGameACK GameID. */
        public GameID: number;

        /** EnterGameACK TableID. */
        public TableID: number;

        /** EnterGameACK Code. */
        public Code: niuniu.GameCode;

        /** EnterGameACK UserID. */
        public UserID: (number|Long);

        /** EnterGameACK TableInfo. */
        public TableInfo?: (structure.ITableInfo|null);

        /** EnterGameACK Reconnect. */
        public Reconnect: boolean;

        /**
         * Creates a new EnterGameACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns EnterGameACK instance
         */
        public static create(properties?: niuniu.IEnterGameACK): niuniu.EnterGameACK;

        /**
         * Encodes the specified EnterGameACK message. Does not implicitly {@link niuniu.EnterGameACK.verify|verify} messages.
         * @param message EnterGameACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IEnterGameACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified EnterGameACK message, length delimited. Does not implicitly {@link niuniu.EnterGameACK.verify|verify} messages.
         * @param message EnterGameACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IEnterGameACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an EnterGameACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns EnterGameACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.EnterGameACK;

        /**
         * Decodes an EnterGameACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns EnterGameACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.EnterGameACK;

        /**
         * Verifies an EnterGameACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a StartGameREQ. */
    interface IStartGameREQ {
    }

    /** Represents a StartGameREQ. */
    class StartGameREQ implements IStartGameREQ {

        /**
         * Constructs a new StartGameREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IStartGameREQ);

        /**
         * Creates a new StartGameREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns StartGameREQ instance
         */
        public static create(properties?: niuniu.IStartGameREQ): niuniu.StartGameREQ;

        /**
         * Encodes the specified StartGameREQ message. Does not implicitly {@link niuniu.StartGameREQ.verify|verify} messages.
         * @param message StartGameREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IStartGameREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified StartGameREQ message, length delimited. Does not implicitly {@link niuniu.StartGameREQ.verify|verify} messages.
         * @param message StartGameREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IStartGameREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a StartGameREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns StartGameREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.StartGameREQ;

        /**
         * Decodes a StartGameREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns StartGameREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.StartGameREQ;

        /**
         * Verifies a StartGameREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a StartGameACK. */
    interface IStartGameACK {

        /** StartGameACK Code */
        Code?: (niuniu.GameCode|null);

        /** StartGameACK GameID */
        GameID?: (number|null);

        /** StartGameACK TableID */
        TableID?: (number|null);
    }

    /** Represents a StartGameACK. */
    class StartGameACK implements IStartGameACK {

        /**
         * Constructs a new StartGameACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IStartGameACK);

        /** StartGameACK Code. */
        public Code: niuniu.GameCode;

        /** StartGameACK GameID. */
        public GameID: number;

        /** StartGameACK TableID. */
        public TableID: number;

        /**
         * Creates a new StartGameACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns StartGameACK instance
         */
        public static create(properties?: niuniu.IStartGameACK): niuniu.StartGameACK;

        /**
         * Encodes the specified StartGameACK message. Does not implicitly {@link niuniu.StartGameACK.verify|verify} messages.
         * @param message StartGameACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IStartGameACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified StartGameACK message, length delimited. Does not implicitly {@link niuniu.StartGameACK.verify|verify} messages.
         * @param message StartGameACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IStartGameACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a StartGameACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns StartGameACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.StartGameACK;

        /**
         * Decodes a StartGameACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns StartGameACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.StartGameACK;

        /**
         * Verifies a StartGameACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a BeginHandNTF. */
    interface IBeginHandNTF {

        /** BeginHandNTF GameID */
        GameID?: (number|null);

        /** BeginHandNTF TableID */
        TableID?: (number|null);

        /** BeginHandNTF Hand */
        Hand?: (number|null);

        /** BeginHandNTF Info */
        Info?: (structure.ITablePlayer[]|null);
    }

    /** Represents a BeginHandNTF. */
    class BeginHandNTF implements IBeginHandNTF {

        /**
         * Constructs a new BeginHandNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IBeginHandNTF);

        /** BeginHandNTF GameID. */
        public GameID: number;

        /** BeginHandNTF TableID. */
        public TableID: number;

        /** BeginHandNTF Hand. */
        public Hand: number;

        /** BeginHandNTF Info. */
        public Info: structure.ITablePlayer[];

        /**
         * Creates a new BeginHandNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BeginHandNTF instance
         */
        public static create(properties?: niuniu.IBeginHandNTF): niuniu.BeginHandNTF;

        /**
         * Encodes the specified BeginHandNTF message. Does not implicitly {@link niuniu.BeginHandNTF.verify|verify} messages.
         * @param message BeginHandNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IBeginHandNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BeginHandNTF message, length delimited. Does not implicitly {@link niuniu.BeginHandNTF.verify|verify} messages.
         * @param message BeginHandNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IBeginHandNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BeginHandNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BeginHandNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.BeginHandNTF;

        /**
         * Decodes a BeginHandNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BeginHandNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.BeginHandNTF;

        /**
         * Verifies a BeginHandNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a CreateTableREQ. */
    interface ICreateTableREQ {

        /** CreateTableREQ Cfg */
        Cfg?: (structure.ITableConfig|null);

        /** CreateTableREQ HouseID */
        HouseID?: (number|Long|null);
    }

    /** Represents a CreateTableREQ. */
    class CreateTableREQ implements ICreateTableREQ {

        /**
         * Constructs a new CreateTableREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ICreateTableREQ);

        /** CreateTableREQ Cfg. */
        public Cfg?: (structure.ITableConfig|null);

        /** CreateTableREQ HouseID. */
        public HouseID: (number|Long);

        /**
         * Creates a new CreateTableREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns CreateTableREQ instance
         */
        public static create(properties?: niuniu.ICreateTableREQ): niuniu.CreateTableREQ;

        /**
         * Encodes the specified CreateTableREQ message. Does not implicitly {@link niuniu.CreateTableREQ.verify|verify} messages.
         * @param message CreateTableREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ICreateTableREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified CreateTableREQ message, length delimited. Does not implicitly {@link niuniu.CreateTableREQ.verify|verify} messages.
         * @param message CreateTableREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ICreateTableREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a CreateTableREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns CreateTableREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.CreateTableREQ;

        /**
         * Decodes a CreateTableREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns CreateTableREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.CreateTableREQ;

        /**
         * Verifies a CreateTableREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a CreateTableACK. */
    interface ICreateTableACK {

        /** CreateTableACK Code */
        Code?: (niuniu.GameCode|null);

        /** CreateTableACK TableID */
        TableID?: (number|null);

        /** CreateTableACK HouseID */
        HouseID?: (number|Long|null);

        /** CreateTableACK TableInfo */
        TableInfo?: (structure.ITableInfo|null);
    }

    /** Represents a CreateTableACK. */
    class CreateTableACK implements ICreateTableACK {

        /**
         * Constructs a new CreateTableACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ICreateTableACK);

        /** CreateTableACK Code. */
        public Code: niuniu.GameCode;

        /** CreateTableACK TableID. */
        public TableID: number;

        /** CreateTableACK HouseID. */
        public HouseID: (number|Long);

        /** CreateTableACK TableInfo. */
        public TableInfo?: (structure.ITableInfo|null);

        /**
         * Creates a new CreateTableACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns CreateTableACK instance
         */
        public static create(properties?: niuniu.ICreateTableACK): niuniu.CreateTableACK;

        /**
         * Encodes the specified CreateTableACK message. Does not implicitly {@link niuniu.CreateTableACK.verify|verify} messages.
         * @param message CreateTableACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ICreateTableACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified CreateTableACK message, length delimited. Does not implicitly {@link niuniu.CreateTableACK.verify|verify} messages.
         * @param message CreateTableACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ICreateTableACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a CreateTableACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns CreateTableACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.CreateTableACK;

        /**
         * Decodes a CreateTableACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns CreateTableACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.CreateTableACK;

        /**
         * Verifies a CreateTableACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a GameConfigNTF. */
    interface IGameConfigNTF {
    }

    /** Represents a GameConfigNTF. */
    class GameConfigNTF implements IGameConfigNTF {

        /**
         * Constructs a new GameConfigNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IGameConfigNTF);

        /**
         * Creates a new GameConfigNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GameConfigNTF instance
         */
        public static create(properties?: niuniu.IGameConfigNTF): niuniu.GameConfigNTF;

        /**
         * Encodes the specified GameConfigNTF message. Does not implicitly {@link niuniu.GameConfigNTF.verify|verify} messages.
         * @param message GameConfigNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IGameConfigNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified GameConfigNTF message, length delimited. Does not implicitly {@link niuniu.GameConfigNTF.verify|verify} messages.
         * @param message GameConfigNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IGameConfigNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a GameConfigNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GameConfigNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.GameConfigNTF;

        /**
         * Decodes a GameConfigNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GameConfigNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.GameConfigNTF;

        /**
         * Verifies a GameConfigNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DispatchFirstFourCardNTF. */
    interface IDispatchFirstFourCardNTF {

        /** DispatchFirstFourCardNTF UserID */
        UserID?: (number|Long|null);

        /** DispatchFirstFourCardNTF Cards */
        Cards?: (number[]|null);

        /** DispatchFirstFourCardNTF CountDown */
        CountDown?: (number|null);

        /** DispatchFirstFourCardNTF CanBet */
        CanBet?: ((number|Long)[]|null);
    }

    /** Represents a DispatchFirstFourCardNTF. */
    class DispatchFirstFourCardNTF implements IDispatchFirstFourCardNTF {

        /**
         * Constructs a new DispatchFirstFourCardNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IDispatchFirstFourCardNTF);

        /** DispatchFirstFourCardNTF UserID. */
        public UserID: (number|Long);

        /** DispatchFirstFourCardNTF Cards. */
        public Cards: number[];

        /** DispatchFirstFourCardNTF CountDown. */
        public CountDown: number;

        /** DispatchFirstFourCardNTF CanBet. */
        public CanBet: (number|Long)[];

        /**
         * Creates a new DispatchFirstFourCardNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DispatchFirstFourCardNTF instance
         */
        public static create(properties?: niuniu.IDispatchFirstFourCardNTF): niuniu.DispatchFirstFourCardNTF;

        /**
         * Encodes the specified DispatchFirstFourCardNTF message. Does not implicitly {@link niuniu.DispatchFirstFourCardNTF.verify|verify} messages.
         * @param message DispatchFirstFourCardNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IDispatchFirstFourCardNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DispatchFirstFourCardNTF message, length delimited. Does not implicitly {@link niuniu.DispatchFirstFourCardNTF.verify|verify} messages.
         * @param message DispatchFirstFourCardNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IDispatchFirstFourCardNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DispatchFirstFourCardNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DispatchFirstFourCardNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.DispatchFirstFourCardNTF;

        /**
         * Decodes a DispatchFirstFourCardNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DispatchFirstFourCardNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.DispatchFirstFourCardNTF;

        /**
         * Verifies a DispatchFirstFourCardNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DispatchLastCardNTF. */
    interface IDispatchLastCardNTF {

        /** DispatchLastCardNTF UserID */
        UserID?: (number|Long|null);

        /** DispatchLastCardNTF Card */
        Card?: (number|null);

        /** DispatchLastCardNTF CountDown */
        CountDown?: (number|null);
    }

    /** Represents a DispatchLastCardNTF. */
    class DispatchLastCardNTF implements IDispatchLastCardNTF {

        /**
         * Constructs a new DispatchLastCardNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IDispatchLastCardNTF);

        /** DispatchLastCardNTF UserID. */
        public UserID: (number|Long);

        /** DispatchLastCardNTF Card. */
        public Card: number;

        /** DispatchLastCardNTF CountDown. */
        public CountDown: number;

        /**
         * Creates a new DispatchLastCardNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DispatchLastCardNTF instance
         */
        public static create(properties?: niuniu.IDispatchLastCardNTF): niuniu.DispatchLastCardNTF;

        /**
         * Encodes the specified DispatchLastCardNTF message. Does not implicitly {@link niuniu.DispatchLastCardNTF.verify|verify} messages.
         * @param message DispatchLastCardNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IDispatchLastCardNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DispatchLastCardNTF message, length delimited. Does not implicitly {@link niuniu.DispatchLastCardNTF.verify|verify} messages.
         * @param message DispatchLastCardNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IDispatchLastCardNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DispatchLastCardNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DispatchLastCardNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.DispatchLastCardNTF;

        /**
         * Decodes a DispatchLastCardNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DispatchLastCardNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.DispatchLastCardNTF;

        /**
         * Verifies a DispatchLastCardNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a SettlementNTF. */
    interface ISettlementNTF {

        /** SettlementNTF TableID */
        TableID?: (number|null);

        /** SettlementNTF HouseID */
        HouseID?: (number|null);

        /** SettlementNTF Users */
        Users?: (niuniu.SettlementNTF.ISettleUser[]|null);

        /** SettlementNTF Ranker */
        Ranker?: (number|Long|null);

        /** SettlementNTF RankerWin */
        RankerWin?: (niuniu.SettlementNTF.ISettleUser[]|null);

        /** SettlementNTF RankerLose */
        RankerLose?: (niuniu.SettlementNTF.ISettleUser[]|null);

        /** SettlementNTF CountDown */
        CountDown?: (number|null);
    }

    /** Represents a SettlementNTF. */
    class SettlementNTF implements ISettlementNTF {

        /**
         * Constructs a new SettlementNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ISettlementNTF);

        /** SettlementNTF TableID. */
        public TableID: number;

        /** SettlementNTF HouseID. */
        public HouseID: number;

        /** SettlementNTF Users. */
        public Users: niuniu.SettlementNTF.ISettleUser[];

        /** SettlementNTF Ranker. */
        public Ranker: (number|Long);

        /** SettlementNTF RankerWin. */
        public RankerWin: niuniu.SettlementNTF.ISettleUser[];

        /** SettlementNTF RankerLose. */
        public RankerLose: niuniu.SettlementNTF.ISettleUser[];

        /** SettlementNTF CountDown. */
        public CountDown: number;

        /**
         * Creates a new SettlementNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SettlementNTF instance
         */
        public static create(properties?: niuniu.ISettlementNTF): niuniu.SettlementNTF;

        /**
         * Encodes the specified SettlementNTF message. Does not implicitly {@link niuniu.SettlementNTF.verify|verify} messages.
         * @param message SettlementNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ISettlementNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SettlementNTF message, length delimited. Does not implicitly {@link niuniu.SettlementNTF.verify|verify} messages.
         * @param message SettlementNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ISettlementNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SettlementNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SettlementNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.SettlementNTF;

        /**
         * Decodes a SettlementNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SettlementNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.SettlementNTF;

        /**
         * Verifies a SettlementNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace SettlementNTF {

        /** Properties of a SettleUser. */
        interface ISettleUser {

            /** SettleUser UserID */
            UserID?: (number|Long|null);

            /** SettleUser PokerType */
            PokerType?: (number|null);

            /** SettleUser Pokers */
            Pokers?: (number[]|null);

            /** SettleUser Win */
            Win?: (number|Long|null);
        }

        /** Represents a SettleUser. */
        class SettleUser implements ISettleUser {

            /**
             * Constructs a new SettleUser.
             * @param [properties] Properties to set
             */
            constructor(properties?: niuniu.SettlementNTF.ISettleUser);

            /** SettleUser UserID. */
            public UserID: (number|Long);

            /** SettleUser PokerType. */
            public PokerType: number;

            /** SettleUser Pokers. */
            public Pokers: number[];

            /** SettleUser Win. */
            public Win: (number|Long);

            /**
             * Creates a new SettleUser instance using the specified properties.
             * @param [properties] Properties to set
             * @returns SettleUser instance
             */
            public static create(properties?: niuniu.SettlementNTF.ISettleUser): niuniu.SettlementNTF.SettleUser;

            /**
             * Encodes the specified SettleUser message. Does not implicitly {@link niuniu.SettlementNTF.SettleUser.verify|verify} messages.
             * @param message SettleUser message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: niuniu.SettlementNTF.ISettleUser, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Encodes the specified SettleUser message, length delimited. Does not implicitly {@link niuniu.SettlementNTF.SettleUser.verify|verify} messages.
             * @param message SettleUser message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: niuniu.SettlementNTF.ISettleUser, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Decodes a SettleUser message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns SettleUser
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.SettlementNTF.SettleUser;

            /**
             * Decodes a SettleUser message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns SettleUser
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.SettlementNTF.SettleUser;

            /**
             * Verifies a SettleUser message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);
        }
    }

    /** Properties of a BetREQ. */
    interface IBetREQ {

        /** BetREQ Count */
        Count?: (number|Long|null);
    }

    /** Represents a BetREQ. */
    class BetREQ implements IBetREQ {

        /**
         * Constructs a new BetREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IBetREQ);

        /** BetREQ Count. */
        public Count: (number|Long);

        /**
         * Creates a new BetREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BetREQ instance
         */
        public static create(properties?: niuniu.IBetREQ): niuniu.BetREQ;

        /**
         * Encodes the specified BetREQ message. Does not implicitly {@link niuniu.BetREQ.verify|verify} messages.
         * @param message BetREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IBetREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BetREQ message, length delimited. Does not implicitly {@link niuniu.BetREQ.verify|verify} messages.
         * @param message BetREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IBetREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BetREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BetREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.BetREQ;

        /**
         * Decodes a BetREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BetREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.BetREQ;

        /**
         * Verifies a BetREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a BetACK. */
    interface IBetACK {

        /** BetACK Code */
        Code?: (niuniu.GameCode|null);

        /** BetACK Count */
        Count?: (number|Long|null);

        /** BetACK UserID */
        UserID?: (number|Long|null);

        /** BetACK CountDown */
        CountDown?: (number|null);
    }

    /** Represents a BetACK. */
    class BetACK implements IBetACK {

        /**
         * Constructs a new BetACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IBetACK);

        /** BetACK Code. */
        public Code: niuniu.GameCode;

        /** BetACK Count. */
        public Count: (number|Long);

        /** BetACK UserID. */
        public UserID: (number|Long);

        /** BetACK CountDown. */
        public CountDown: number;

        /**
         * Creates a new BetACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BetACK instance
         */
        public static create(properties?: niuniu.IBetACK): niuniu.BetACK;

        /**
         * Encodes the specified BetACK message. Does not implicitly {@link niuniu.BetACK.verify|verify} messages.
         * @param message BetACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IBetACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BetACK message, length delimited. Does not implicitly {@link niuniu.BetACK.verify|verify} messages.
         * @param message BetACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IBetACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BetACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BetACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.BetACK;

        /**
         * Decodes a BetACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BetACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.BetACK;

        /**
         * Verifies a BetACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RankREQ. */
    interface IRankREQ {

        /** RankREQ Multiple */
        Multiple?: (number|null);
    }

    /** Represents a RankREQ. */
    class RankREQ implements IRankREQ {

        /**
         * Constructs a new RankREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IRankREQ);

        /** RankREQ Multiple. */
        public Multiple: number;

        /**
         * Creates a new RankREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RankREQ instance
         */
        public static create(properties?: niuniu.IRankREQ): niuniu.RankREQ;

        /**
         * Encodes the specified RankREQ message. Does not implicitly {@link niuniu.RankREQ.verify|verify} messages.
         * @param message RankREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IRankREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RankREQ message, length delimited. Does not implicitly {@link niuniu.RankREQ.verify|verify} messages.
         * @param message RankREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IRankREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RankREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RankREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.RankREQ;

        /**
         * Decodes a RankREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RankREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.RankREQ;

        /**
         * Verifies a RankREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RankACK. */
    interface IRankACK {

        /** RankACK Code */
        Code?: (niuniu.GameCode|null);

        /** RankACK Multiple */
        Multiple?: (number|null);

        /** RankACK Stage */
        Stage?: (number|null);

        /** RankACK Countdown */
        Countdown?: (number|null);

        /** RankACK UserId */
        UserId?: (number|Long|null);
    }

    /** Represents a RankACK. */
    class RankACK implements IRankACK {

        /**
         * Constructs a new RankACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IRankACK);

        /** RankACK Code. */
        public Code: niuniu.GameCode;

        /** RankACK Multiple. */
        public Multiple: number;

        /** RankACK Stage. */
        public Stage: number;

        /** RankACK Countdown. */
        public Countdown: number;

        /** RankACK UserId. */
        public UserId: (number|Long);

        /**
         * Creates a new RankACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RankACK instance
         */
        public static create(properties?: niuniu.IRankACK): niuniu.RankACK;

        /**
         * Encodes the specified RankACK message. Does not implicitly {@link niuniu.RankACK.verify|verify} messages.
         * @param message RankACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IRankACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RankACK message, length delimited. Does not implicitly {@link niuniu.RankACK.verify|verify} messages.
         * @param message RankACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IRankACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RankACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RankACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.RankACK;

        /**
         * Decodes a RankACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RankACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.RankACK;

        /**
         * Verifies a RankACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RankNTF. */
    interface IRankNTF {

        /** RankNTF RankUser */
        RankUser?: ((number|Long)[]|null);

        /** RankNTF RankerUser */
        RankerUser?: (number|Long|null);

        /** RankNTF RankerMultiple */
        RankerMultiple?: (number|null);

        /** RankNTF CountDown */
        CountDown?: (number|null);
    }

    /** Represents a RankNTF. */
    class RankNTF implements IRankNTF {

        /**
         * Constructs a new RankNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IRankNTF);

        /** RankNTF RankUser. */
        public RankUser: (number|Long)[];

        /** RankNTF RankerUser. */
        public RankerUser: (number|Long);

        /** RankNTF RankerMultiple. */
        public RankerMultiple: number;

        /** RankNTF CountDown. */
        public CountDown: number;

        /**
         * Creates a new RankNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RankNTF instance
         */
        public static create(properties?: niuniu.IRankNTF): niuniu.RankNTF;

        /**
         * Encodes the specified RankNTF message. Does not implicitly {@link niuniu.RankNTF.verify|verify} messages.
         * @param message RankNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IRankNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RankNTF message, length delimited. Does not implicitly {@link niuniu.RankNTF.verify|verify} messages.
         * @param message RankNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IRankNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RankNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RankNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.RankNTF;

        /**
         * Decodes a RankNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RankNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.RankNTF;

        /**
         * Verifies a RankNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a Route. */
    interface IRoute {

        /** Route UserID */
        UserID?: ((number|Long)[]|null);
    }

    /** Represents a Route. */
    class Route implements IRoute {

        /**
         * Constructs a new Route.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IRoute);

        /** Route UserID. */
        public UserID: (number|Long)[];

        /**
         * Creates a new Route instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Route instance
         */
        public static create(properties?: niuniu.IRoute): niuniu.Route;

        /**
         * Encodes the specified Route message. Does not implicitly {@link niuniu.Route.verify|verify} messages.
         * @param message Route message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IRoute, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified Route message, length delimited. Does not implicitly {@link niuniu.Route.verify|verify} messages.
         * @param message Route message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IRoute, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a Route message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Route
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.Route;

        /**
         * Decodes a Route message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Route
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.Route;

        /**
         * Verifies a Route message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RoundOverNTF. */
    interface IRoundOverNTF {
    }

    /** Represents a RoundOverNTF. */
    class RoundOverNTF implements IRoundOverNTF {

        /**
         * Constructs a new RoundOverNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IRoundOverNTF);

        /**
         * Creates a new RoundOverNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RoundOverNTF instance
         */
        public static create(properties?: niuniu.IRoundOverNTF): niuniu.RoundOverNTF;

        /**
         * Encodes the specified RoundOverNTF message. Does not implicitly {@link niuniu.RoundOverNTF.verify|verify} messages.
         * @param message RoundOverNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IRoundOverNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RoundOverNTF message, length delimited. Does not implicitly {@link niuniu.RoundOverNTF.verify|verify} messages.
         * @param message RoundOverNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IRoundOverNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RoundOverNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RoundOverNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.RoundOverNTF;

        /**
         * Decodes a RoundOverNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RoundOverNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.RoundOverNTF;

        /**
         * Verifies a RoundOverNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ScoreChangeNTF. */
    interface IScoreChangeNTF {

        /** ScoreChangeNTF UserID */
        UserID?: (number|Long|null);

        /** ScoreChangeNTF DeltaScore */
        DeltaScore?: (number|Long|null);

        /** ScoreChangeNTF Score */
        Score?: (number|Long|null);

        /** ScoreChangeNTF HouseID */
        HouseID?: (number|Long|null);

        /** ScoreChangeNTF TableID */
        TableID?: (number|null);

        /** ScoreChangeNTF Reason */
        Reason?: (string|null);

        /** ScoreChangeNTF Date */
        Date?: (number|Long|null);

        /** ScoreChangeNTF Tag */
        Tag?: (number|null);

        /** ScoreChangeNTF Operator */
        Operator?: (number|Long|null);
    }

    /** Represents a ScoreChangeNTF. */
    class ScoreChangeNTF implements IScoreChangeNTF {

        /**
         * Constructs a new ScoreChangeNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IScoreChangeNTF);

        /** ScoreChangeNTF UserID. */
        public UserID: (number|Long);

        /** ScoreChangeNTF DeltaScore. */
        public DeltaScore: (number|Long);

        /** ScoreChangeNTF Score. */
        public Score: (number|Long);

        /** ScoreChangeNTF HouseID. */
        public HouseID: (number|Long);

        /** ScoreChangeNTF TableID. */
        public TableID: number;

        /** ScoreChangeNTF Reason. */
        public Reason: string;

        /** ScoreChangeNTF Date. */
        public Date: (number|Long);

        /** ScoreChangeNTF Tag. */
        public Tag: number;

        /** ScoreChangeNTF Operator. */
        public Operator: (number|Long);

        /**
         * Creates a new ScoreChangeNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ScoreChangeNTF instance
         */
        public static create(properties?: niuniu.IScoreChangeNTF): niuniu.ScoreChangeNTF;

        /**
         * Encodes the specified ScoreChangeNTF message. Does not implicitly {@link niuniu.ScoreChangeNTF.verify|verify} messages.
         * @param message ScoreChangeNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IScoreChangeNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ScoreChangeNTF message, length delimited. Does not implicitly {@link niuniu.ScoreChangeNTF.verify|verify} messages.
         * @param message ScoreChangeNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IScoreChangeNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ScoreChangeNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ScoreChangeNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.ScoreChangeNTF;

        /**
         * Decodes a ScoreChangeNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ScoreChangeNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.ScoreChangeNTF;

        /**
         * Verifies a ScoreChangeNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeahouseTableInfoREQ. */
    interface ITeahouseTableInfoREQ {

        /** TeahouseTableInfoREQ HouseId */
        HouseId?: (number|Long|null);
    }

    /** Represents a TeahouseTableInfoREQ. */
    class TeahouseTableInfoREQ implements ITeahouseTableInfoREQ {

        /**
         * Constructs a new TeahouseTableInfoREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ITeahouseTableInfoREQ);

        /** TeahouseTableInfoREQ HouseId. */
        public HouseId: (number|Long);

        /**
         * Creates a new TeahouseTableInfoREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeahouseTableInfoREQ instance
         */
        public static create(properties?: niuniu.ITeahouseTableInfoREQ): niuniu.TeahouseTableInfoREQ;

        /**
         * Encodes the specified TeahouseTableInfoREQ message. Does not implicitly {@link niuniu.TeahouseTableInfoREQ.verify|verify} messages.
         * @param message TeahouseTableInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ITeahouseTableInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeahouseTableInfoREQ message, length delimited. Does not implicitly {@link niuniu.TeahouseTableInfoREQ.verify|verify} messages.
         * @param message TeahouseTableInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ITeahouseTableInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeahouseTableInfoREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeahouseTableInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.TeahouseTableInfoREQ;

        /**
         * Decodes a TeahouseTableInfoREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeahouseTableInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.TeahouseTableInfoREQ;

        /**
         * Verifies a TeahouseTableInfoREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeahouseTableInfoACK. */
    interface ITeahouseTableInfoACK {

        /** TeahouseTableInfoACK Info */
        Info?: (structure.ITableInfo[]|null);
    }

    /** Represents a TeahouseTableInfoACK. */
    class TeahouseTableInfoACK implements ITeahouseTableInfoACK {

        /**
         * Constructs a new TeahouseTableInfoACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ITeahouseTableInfoACK);

        /** TeahouseTableInfoACK Info. */
        public Info: structure.ITableInfo[];

        /**
         * Creates a new TeahouseTableInfoACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeahouseTableInfoACK instance
         */
        public static create(properties?: niuniu.ITeahouseTableInfoACK): niuniu.TeahouseTableInfoACK;

        /**
         * Encodes the specified TeahouseTableInfoACK message. Does not implicitly {@link niuniu.TeahouseTableInfoACK.verify|verify} messages.
         * @param message TeahouseTableInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ITeahouseTableInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeahouseTableInfoACK message, length delimited. Does not implicitly {@link niuniu.TeahouseTableInfoACK.verify|verify} messages.
         * @param message TeahouseTableInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ITeahouseTableInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeahouseTableInfoACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeahouseTableInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.TeahouseTableInfoACK;

        /**
         * Decodes a TeahouseTableInfoACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeahouseTableInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.TeahouseTableInfoACK;

        /**
         * Verifies a TeahouseTableInfoACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeahouseTableInfoNTF. */
    interface ITeahouseTableInfoNTF {

        /** TeahouseTableInfoNTF Info */
        Info?: (structure.ITableInfo[]|null);
    }

    /** Represents a TeahouseTableInfoNTF. */
    class TeahouseTableInfoNTF implements ITeahouseTableInfoNTF {

        /**
         * Constructs a new TeahouseTableInfoNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ITeahouseTableInfoNTF);

        /** TeahouseTableInfoNTF Info. */
        public Info: structure.ITableInfo[];

        /**
         * Creates a new TeahouseTableInfoNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeahouseTableInfoNTF instance
         */
        public static create(properties?: niuniu.ITeahouseTableInfoNTF): niuniu.TeahouseTableInfoNTF;

        /**
         * Encodes the specified TeahouseTableInfoNTF message. Does not implicitly {@link niuniu.TeahouseTableInfoNTF.verify|verify} messages.
         * @param message TeahouseTableInfoNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ITeahouseTableInfoNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeahouseTableInfoNTF message, length delimited. Does not implicitly {@link niuniu.TeahouseTableInfoNTF.verify|verify} messages.
         * @param message TeahouseTableInfoNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ITeahouseTableInfoNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeahouseTableInfoNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeahouseTableInfoNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.TeahouseTableInfoNTF;

        /**
         * Decodes a TeahouseTableInfoNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeahouseTableInfoNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.TeahouseTableInfoNTF;

        /**
         * Verifies a TeahouseTableInfoNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a SitREQ. */
    interface ISitREQ {
    }

    /** Represents a SitREQ. */
    class SitREQ implements ISitREQ {

        /**
         * Constructs a new SitREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ISitREQ);

        /**
         * Creates a new SitREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SitREQ instance
         */
        public static create(properties?: niuniu.ISitREQ): niuniu.SitREQ;

        /**
         * Encodes the specified SitREQ message. Does not implicitly {@link niuniu.SitREQ.verify|verify} messages.
         * @param message SitREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ISitREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SitREQ message, length delimited. Does not implicitly {@link niuniu.SitREQ.verify|verify} messages.
         * @param message SitREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ISitREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SitREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SitREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.SitREQ;

        /**
         * Decodes a SitREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SitREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.SitREQ;

        /**
         * Verifies a SitREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a SitACK. */
    interface ISitACK {

        /** SitACK Code */
        Code?: (niuniu.GameCode|null);

        /** SitACK Seat */
        Seat?: (number|null);

        /** SitACK UserID */
        UserID?: (number|Long|null);
    }

    /** Represents a SitACK. */
    class SitACK implements ISitACK {

        /**
         * Constructs a new SitACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ISitACK);

        /** SitACK Code. */
        public Code: niuniu.GameCode;

        /** SitACK Seat. */
        public Seat: number;

        /** SitACK UserID. */
        public UserID: (number|Long);

        /**
         * Creates a new SitACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SitACK instance
         */
        public static create(properties?: niuniu.ISitACK): niuniu.SitACK;

        /**
         * Encodes the specified SitACK message. Does not implicitly {@link niuniu.SitACK.verify|verify} messages.
         * @param message SitACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ISitACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SitACK message, length delimited. Does not implicitly {@link niuniu.SitACK.verify|verify} messages.
         * @param message SitACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ISitACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SitACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SitACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.SitACK;

        /**
         * Decodes a SitACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SitACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.SitACK;

        /**
         * Verifies a SitACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a LeaveGameREQ. */
    interface ILeaveGameREQ {
    }

    /** Represents a LeaveGameREQ. */
    class LeaveGameREQ implements ILeaveGameREQ {

        /**
         * Constructs a new LeaveGameREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ILeaveGameREQ);

        /**
         * Creates a new LeaveGameREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LeaveGameREQ instance
         */
        public static create(properties?: niuniu.ILeaveGameREQ): niuniu.LeaveGameREQ;

        /**
         * Encodes the specified LeaveGameREQ message. Does not implicitly {@link niuniu.LeaveGameREQ.verify|verify} messages.
         * @param message LeaveGameREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ILeaveGameREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified LeaveGameREQ message, length delimited. Does not implicitly {@link niuniu.LeaveGameREQ.verify|verify} messages.
         * @param message LeaveGameREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ILeaveGameREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a LeaveGameREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LeaveGameREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.LeaveGameREQ;

        /**
         * Decodes a LeaveGameREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LeaveGameREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.LeaveGameREQ;

        /**
         * Verifies a LeaveGameREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a LeaveGameACK. */
    interface ILeaveGameACK {

        /** LeaveGameACK Code */
        Code?: (niuniu.GameCode|null);

        /** LeaveGameACK Reason */
        Reason?: (niuniu.LeaveGameACK.LeaveReason|null);

        /** LeaveGameACK UserID */
        UserID?: (number|Long|null);
    }

    /** Represents a LeaveGameACK. */
    class LeaveGameACK implements ILeaveGameACK {

        /**
         * Constructs a new LeaveGameACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ILeaveGameACK);

        /** LeaveGameACK Code. */
        public Code: niuniu.GameCode;

        /** LeaveGameACK Reason. */
        public Reason: niuniu.LeaveGameACK.LeaveReason;

        /** LeaveGameACK UserID. */
        public UserID: (number|Long);

        /**
         * Creates a new LeaveGameACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LeaveGameACK instance
         */
        public static create(properties?: niuniu.ILeaveGameACK): niuniu.LeaveGameACK;

        /**
         * Encodes the specified LeaveGameACK message. Does not implicitly {@link niuniu.LeaveGameACK.verify|verify} messages.
         * @param message LeaveGameACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ILeaveGameACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified LeaveGameACK message, length delimited. Does not implicitly {@link niuniu.LeaveGameACK.verify|verify} messages.
         * @param message LeaveGameACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ILeaveGameACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a LeaveGameACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LeaveGameACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.LeaveGameACK;

        /**
         * Decodes a LeaveGameACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LeaveGameACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.LeaveGameACK;

        /**
         * Verifies a LeaveGameACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace LeaveGameACK {

        /** LeaveReason enum. */
        enum LeaveReason {
            UserLeave = 0,
            GameOver = 1,
            KickByManager = 2,
            Timeout = 3,
            Dismiss = 4,
            ScoreNotEnough = 5
        }
    }

    /** Properties of a ShowCardREQ. */
    interface IShowCardREQ {
    }

    /** Represents a ShowCardREQ. */
    class ShowCardREQ implements IShowCardREQ {

        /**
         * Constructs a new ShowCardREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IShowCardREQ);

        /**
         * Creates a new ShowCardREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ShowCardREQ instance
         */
        public static create(properties?: niuniu.IShowCardREQ): niuniu.ShowCardREQ;

        /**
         * Encodes the specified ShowCardREQ message. Does not implicitly {@link niuniu.ShowCardREQ.verify|verify} messages.
         * @param message ShowCardREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IShowCardREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ShowCardREQ message, length delimited. Does not implicitly {@link niuniu.ShowCardREQ.verify|verify} messages.
         * @param message ShowCardREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IShowCardREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ShowCardREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ShowCardREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.ShowCardREQ;

        /**
         * Decodes a ShowCardREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ShowCardREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.ShowCardREQ;

        /**
         * Verifies a ShowCardREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ShowCardACK. */
    interface IShowCardACK {

        /** ShowCardACK UserId */
        UserId?: (number|Long|null);

        /** ShowCardACK Card */
        Card?: (number[]|null);

        /** ShowCardACK CardType */
        CardType?: (number|null);

        /** ShowCardACK Stage */
        Stage?: (number|null);

        /** ShowCardACK CountDown */
        CountDown?: (number|null);
    }

    /** Represents a ShowCardACK. */
    class ShowCardACK implements IShowCardACK {

        /**
         * Constructs a new ShowCardACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IShowCardACK);

        /** ShowCardACK UserId. */
        public UserId: (number|Long);

        /** ShowCardACK Card. */
        public Card: number[];

        /** ShowCardACK CardType. */
        public CardType: number;

        /** ShowCardACK Stage. */
        public Stage: number;

        /** ShowCardACK CountDown. */
        public CountDown: number;

        /**
         * Creates a new ShowCardACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ShowCardACK instance
         */
        public static create(properties?: niuniu.IShowCardACK): niuniu.ShowCardACK;

        /**
         * Encodes the specified ShowCardACK message. Does not implicitly {@link niuniu.ShowCardACK.verify|verify} messages.
         * @param message ShowCardACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IShowCardACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ShowCardACK message, length delimited. Does not implicitly {@link niuniu.ShowCardACK.verify|verify} messages.
         * @param message ShowCardACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IShowCardACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ShowCardACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ShowCardACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.ShowCardACK;

        /**
         * Decodes a ShowCardACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ShowCardACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.ShowCardACK;

        /**
         * Verifies a ShowCardACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a SimpelActionREQ. */
    interface ISimpelActionREQ {

        /** SimpelActionREQ TargetUserID */
        TargetUserID?: (number|Long|null);

        /** SimpelActionREQ ActionID */
        ActionID?: (number|null);

        /** SimpelActionREQ Content */
        Content?: (string|null);
    }

    /** Represents a SimpelActionREQ. */
    class SimpelActionREQ implements ISimpelActionREQ {

        /**
         * Constructs a new SimpelActionREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ISimpelActionREQ);

        /** SimpelActionREQ TargetUserID. */
        public TargetUserID: (number|Long);

        /** SimpelActionREQ ActionID. */
        public ActionID: number;

        /** SimpelActionREQ Content. */
        public Content: string;

        /**
         * Creates a new SimpelActionREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SimpelActionREQ instance
         */
        public static create(properties?: niuniu.ISimpelActionREQ): niuniu.SimpelActionREQ;

        /**
         * Encodes the specified SimpelActionREQ message. Does not implicitly {@link niuniu.SimpelActionREQ.verify|verify} messages.
         * @param message SimpelActionREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ISimpelActionREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SimpelActionREQ message, length delimited. Does not implicitly {@link niuniu.SimpelActionREQ.verify|verify} messages.
         * @param message SimpelActionREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ISimpelActionREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SimpelActionREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SimpelActionREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.SimpelActionREQ;

        /**
         * Decodes a SimpelActionREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SimpelActionREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.SimpelActionREQ;

        /**
         * Verifies a SimpelActionREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a SimpelActionACK. */
    interface ISimpelActionACK {

        /** SimpelActionACK TargetUserID */
        TargetUserID?: (number|Long|null);

        /** SimpelActionACK ActionID */
        ActionID?: (number|null);

        /** SimpelActionACK Content */
        Content?: (string|null);
    }

    /** Represents a SimpelActionACK. */
    class SimpelActionACK implements ISimpelActionACK {

        /**
         * Constructs a new SimpelActionACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ISimpelActionACK);

        /** SimpelActionACK TargetUserID. */
        public TargetUserID: (number|Long);

        /** SimpelActionACK ActionID. */
        public ActionID: number;

        /** SimpelActionACK Content. */
        public Content: string;

        /**
         * Creates a new SimpelActionACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SimpelActionACK instance
         */
        public static create(properties?: niuniu.ISimpelActionACK): niuniu.SimpelActionACK;

        /**
         * Encodes the specified SimpelActionACK message. Does not implicitly {@link niuniu.SimpelActionACK.verify|verify} messages.
         * @param message SimpelActionACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ISimpelActionACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SimpelActionACK message, length delimited. Does not implicitly {@link niuniu.SimpelActionACK.verify|verify} messages.
         * @param message SimpelActionACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ISimpelActionACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SimpelActionACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SimpelActionACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.SimpelActionACK;

        /**
         * Decodes a SimpelActionACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SimpelActionACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.SimpelActionACK;

        /**
         * Verifies a SimpelActionACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a BeginHistoryNTF. */
    interface IBeginHistoryNTF {

        /** BeginHistoryNTF UserID */
        UserID?: (number|Long|null);

        /** BeginHistoryNTF TableID */
        TableID?: (number|Long|null);
    }

    /** Represents a BeginHistoryNTF. */
    class BeginHistoryNTF implements IBeginHistoryNTF {

        /**
         * Constructs a new BeginHistoryNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IBeginHistoryNTF);

        /** BeginHistoryNTF UserID. */
        public UserID: (number|Long);

        /** BeginHistoryNTF TableID. */
        public TableID: (number|Long);

        /**
         * Creates a new BeginHistoryNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BeginHistoryNTF instance
         */
        public static create(properties?: niuniu.IBeginHistoryNTF): niuniu.BeginHistoryNTF;

        /**
         * Encodes the specified BeginHistoryNTF message. Does not implicitly {@link niuniu.BeginHistoryNTF.verify|verify} messages.
         * @param message BeginHistoryNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IBeginHistoryNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BeginHistoryNTF message, length delimited. Does not implicitly {@link niuniu.BeginHistoryNTF.verify|verify} messages.
         * @param message BeginHistoryNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IBeginHistoryNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BeginHistoryNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BeginHistoryNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.BeginHistoryNTF;

        /**
         * Decodes a BeginHistoryNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BeginHistoryNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.BeginHistoryNTF;

        /**
         * Verifies a BeginHistoryNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of an EndHistoryNTF. */
    interface IEndHistoryNTF {

        /** EndHistoryNTF UserID */
        UserID?: (number|Long|null);

        /** EndHistoryNTF TableID */
        TableID?: (number|Long|null);
    }

    /** Represents an EndHistoryNTF. */
    class EndHistoryNTF implements IEndHistoryNTF {

        /**
         * Constructs a new EndHistoryNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IEndHistoryNTF);

        /** EndHistoryNTF UserID. */
        public UserID: (number|Long);

        /** EndHistoryNTF TableID. */
        public TableID: (number|Long);

        /**
         * Creates a new EndHistoryNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns EndHistoryNTF instance
         */
        public static create(properties?: niuniu.IEndHistoryNTF): niuniu.EndHistoryNTF;

        /**
         * Encodes the specified EndHistoryNTF message. Does not implicitly {@link niuniu.EndHistoryNTF.verify|verify} messages.
         * @param message EndHistoryNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IEndHistoryNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified EndHistoryNTF message, length delimited. Does not implicitly {@link niuniu.EndHistoryNTF.verify|verify} messages.
         * @param message EndHistoryNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IEndHistoryNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an EndHistoryNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns EndHistoryNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.EndHistoryNTF;

        /**
         * Decodes an EndHistoryNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns EndHistoryNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.EndHistoryNTF;

        /**
         * Verifies an EndHistoryNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of an OwnTableInfoREQ. */
    interface IOwnTableInfoREQ {
    }

    /** Represents an OwnTableInfoREQ. */
    class OwnTableInfoREQ implements IOwnTableInfoREQ {

        /**
         * Constructs a new OwnTableInfoREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IOwnTableInfoREQ);

        /**
         * Creates a new OwnTableInfoREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns OwnTableInfoREQ instance
         */
        public static create(properties?: niuniu.IOwnTableInfoREQ): niuniu.OwnTableInfoREQ;

        /**
         * Encodes the specified OwnTableInfoREQ message. Does not implicitly {@link niuniu.OwnTableInfoREQ.verify|verify} messages.
         * @param message OwnTableInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IOwnTableInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified OwnTableInfoREQ message, length delimited. Does not implicitly {@link niuniu.OwnTableInfoREQ.verify|verify} messages.
         * @param message OwnTableInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IOwnTableInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an OwnTableInfoREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns OwnTableInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.OwnTableInfoREQ;

        /**
         * Decodes an OwnTableInfoREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns OwnTableInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.OwnTableInfoREQ;

        /**
         * Verifies an OwnTableInfoREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of an OwnTableInfoACK. */
    interface IOwnTableInfoACK {

        /** OwnTableInfoACK TableInfo */
        TableInfo?: (structure.ITableInfo[]|null);
    }

    /** Represents an OwnTableInfoACK. */
    class OwnTableInfoACK implements IOwnTableInfoACK {

        /**
         * Constructs a new OwnTableInfoACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IOwnTableInfoACK);

        /** OwnTableInfoACK TableInfo. */
        public TableInfo: structure.ITableInfo[];

        /**
         * Creates a new OwnTableInfoACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns OwnTableInfoACK instance
         */
        public static create(properties?: niuniu.IOwnTableInfoACK): niuniu.OwnTableInfoACK;

        /**
         * Encodes the specified OwnTableInfoACK message. Does not implicitly {@link niuniu.OwnTableInfoACK.verify|verify} messages.
         * @param message OwnTableInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IOwnTableInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified OwnTableInfoACK message, length delimited. Does not implicitly {@link niuniu.OwnTableInfoACK.verify|verify} messages.
         * @param message OwnTableInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IOwnTableInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an OwnTableInfoACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns OwnTableInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.OwnTableInfoACK;

        /**
         * Decodes an OwnTableInfoACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns OwnTableInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.OwnTableInfoACK;

        /**
         * Verifies an OwnTableInfoACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a PlayerEnterNTF. */
    interface IPlayerEnterNTF {

        /** PlayerEnterNTF TableID */
        TableID?: (number|null);

        /** PlayerEnterNTF UserID */
        UserID?: (number|Long|null);

        /** PlayerEnterNTF NickName */
        NickName?: (string|null);

        /** PlayerEnterNTF HeadIcon */
        HeadIcon?: (string|null);

        /** PlayerEnterNTF Score */
        Score?: (number|Long|null);
    }

    /** Represents a PlayerEnterNTF. */
    class PlayerEnterNTF implements IPlayerEnterNTF {

        /**
         * Constructs a new PlayerEnterNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IPlayerEnterNTF);

        /** PlayerEnterNTF TableID. */
        public TableID: number;

        /** PlayerEnterNTF UserID. */
        public UserID: (number|Long);

        /** PlayerEnterNTF NickName. */
        public NickName: string;

        /** PlayerEnterNTF HeadIcon. */
        public HeadIcon: string;

        /** PlayerEnterNTF Score. */
        public Score: (number|Long);

        /**
         * Creates a new PlayerEnterNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PlayerEnterNTF instance
         */
        public static create(properties?: niuniu.IPlayerEnterNTF): niuniu.PlayerEnterNTF;

        /**
         * Encodes the specified PlayerEnterNTF message. Does not implicitly {@link niuniu.PlayerEnterNTF.verify|verify} messages.
         * @param message PlayerEnterNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IPlayerEnterNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified PlayerEnterNTF message, length delimited. Does not implicitly {@link niuniu.PlayerEnterNTF.verify|verify} messages.
         * @param message PlayerEnterNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IPlayerEnterNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a PlayerEnterNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PlayerEnterNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.PlayerEnterNTF;

        /**
         * Decodes a PlayerEnterNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PlayerEnterNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.PlayerEnterNTF;

        /**
         * Verifies a PlayerEnterNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of an ObserverTableListREQ. */
    interface IObserverTableListREQ {

        /** ObserverTableListREQ HouseID */
        HouseID?: (number|Long|null);
    }

    /** Represents an ObserverTableListREQ. */
    class ObserverTableListREQ implements IObserverTableListREQ {

        /**
         * Constructs a new ObserverTableListREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IObserverTableListREQ);

        /** ObserverTableListREQ HouseID. */
        public HouseID: (number|Long);

        /**
         * Creates a new ObserverTableListREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ObserverTableListREQ instance
         */
        public static create(properties?: niuniu.IObserverTableListREQ): niuniu.ObserverTableListREQ;

        /**
         * Encodes the specified ObserverTableListREQ message. Does not implicitly {@link niuniu.ObserverTableListREQ.verify|verify} messages.
         * @param message ObserverTableListREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IObserverTableListREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ObserverTableListREQ message, length delimited. Does not implicitly {@link niuniu.ObserverTableListREQ.verify|verify} messages.
         * @param message ObserverTableListREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IObserverTableListREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an ObserverTableListREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ObserverTableListREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.ObserverTableListREQ;

        /**
         * Decodes an ObserverTableListREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ObserverTableListREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.ObserverTableListREQ;

        /**
         * Verifies an ObserverTableListREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DisObserverTableListREQ. */
    interface IDisObserverTableListREQ {

        /** DisObserverTableListREQ HouseID */
        HouseID?: (number|Long|null);
    }

    /** Represents a DisObserverTableListREQ. */
    class DisObserverTableListREQ implements IDisObserverTableListREQ {

        /**
         * Constructs a new DisObserverTableListREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IDisObserverTableListREQ);

        /** DisObserverTableListREQ HouseID. */
        public HouseID: (number|Long);

        /**
         * Creates a new DisObserverTableListREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DisObserverTableListREQ instance
         */
        public static create(properties?: niuniu.IDisObserverTableListREQ): niuniu.DisObserverTableListREQ;

        /**
         * Encodes the specified DisObserverTableListREQ message. Does not implicitly {@link niuniu.DisObserverTableListREQ.verify|verify} messages.
         * @param message DisObserverTableListREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IDisObserverTableListREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DisObserverTableListREQ message, length delimited. Does not implicitly {@link niuniu.DisObserverTableListREQ.verify|verify} messages.
         * @param message DisObserverTableListREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IDisObserverTableListREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DisObserverTableListREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DisObserverTableListREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.DisObserverTableListREQ;

        /**
         * Decodes a DisObserverTableListREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DisObserverTableListREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.DisObserverTableListREQ;

        /**
         * Verifies a DisObserverTableListREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TableConfigChangeREQ. */
    interface ITableConfigChangeREQ {

        /** TableConfigChangeREQ TableId */
        TableId?: (number|null);

        /** TableConfigChangeREQ Cfg */
        Cfg?: (structure.ITableConfig|null);

        /** TableConfigChangeREQ DrawConfig */
        DrawConfig?: (structure.ITableDrawConfig|null);
    }

    /** Represents a TableConfigChangeREQ. */
    class TableConfigChangeREQ implements ITableConfigChangeREQ {

        /**
         * Constructs a new TableConfigChangeREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ITableConfigChangeREQ);

        /** TableConfigChangeREQ TableId. */
        public TableId: number;

        /** TableConfigChangeREQ Cfg. */
        public Cfg?: (structure.ITableConfig|null);

        /** TableConfigChangeREQ DrawConfig. */
        public DrawConfig?: (structure.ITableDrawConfig|null);

        /**
         * Creates a new TableConfigChangeREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TableConfigChangeREQ instance
         */
        public static create(properties?: niuniu.ITableConfigChangeREQ): niuniu.TableConfigChangeREQ;

        /**
         * Encodes the specified TableConfigChangeREQ message. Does not implicitly {@link niuniu.TableConfigChangeREQ.verify|verify} messages.
         * @param message TableConfigChangeREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ITableConfigChangeREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TableConfigChangeREQ message, length delimited. Does not implicitly {@link niuniu.TableConfigChangeREQ.verify|verify} messages.
         * @param message TableConfigChangeREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ITableConfigChangeREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TableConfigChangeREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TableConfigChangeREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.TableConfigChangeREQ;

        /**
         * Decodes a TableConfigChangeREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TableConfigChangeREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.TableConfigChangeREQ;

        /**
         * Verifies a TableConfigChangeREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TableConfigChangeACK. */
    interface ITableConfigChangeACK {

        /** TableConfigChangeACK Code */
        Code?: (niuniu.GameCode|null);
    }

    /** Represents a TableConfigChangeACK. */
    class TableConfigChangeACK implements ITableConfigChangeACK {

        /**
         * Constructs a new TableConfigChangeACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ITableConfigChangeACK);

        /** TableConfigChangeACK Code. */
        public Code: niuniu.GameCode;

        /**
         * Creates a new TableConfigChangeACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TableConfigChangeACK instance
         */
        public static create(properties?: niuniu.ITableConfigChangeACK): niuniu.TableConfigChangeACK;

        /**
         * Encodes the specified TableConfigChangeACK message. Does not implicitly {@link niuniu.TableConfigChangeACK.verify|verify} messages.
         * @param message TableConfigChangeACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ITableConfigChangeACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TableConfigChangeACK message, length delimited. Does not implicitly {@link niuniu.TableConfigChangeACK.verify|verify} messages.
         * @param message TableConfigChangeACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ITableConfigChangeACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TableConfigChangeACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TableConfigChangeACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.TableConfigChangeACK;

        /**
         * Decodes a TableConfigChangeACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TableConfigChangeACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.TableConfigChangeACK;

        /**
         * Verifies a TableConfigChangeACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of an EndHandNTF. */
    interface IEndHandNTF {
    }

    /** Represents an EndHandNTF. */
    class EndHandNTF implements IEndHandNTF {

        /**
         * Constructs a new EndHandNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IEndHandNTF);

        /**
         * Creates a new EndHandNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns EndHandNTF instance
         */
        public static create(properties?: niuniu.IEndHandNTF): niuniu.EndHandNTF;

        /**
         * Encodes the specified EndHandNTF message. Does not implicitly {@link niuniu.EndHandNTF.verify|verify} messages.
         * @param message EndHandNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IEndHandNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified EndHandNTF message, length delimited. Does not implicitly {@link niuniu.EndHandNTF.verify|verify} messages.
         * @param message EndHandNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IEndHandNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an EndHandNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns EndHandNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.EndHandNTF;

        /**
         * Decodes an EndHandNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns EndHandNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.EndHandNTF;

        /**
         * Verifies an EndHandNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RounRecordNTF. */
    interface IRounRecordNTF {

        /** RounRecordNTF Record */
        Record?: (record.INiuniuRoundRecord|null);
    }

    /** Represents a RounRecordNTF. */
    class RounRecordNTF implements IRounRecordNTF {

        /**
         * Constructs a new RounRecordNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IRounRecordNTF);

        /** RounRecordNTF Record. */
        public Record?: (record.INiuniuRoundRecord|null);

        /**
         * Creates a new RounRecordNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RounRecordNTF instance
         */
        public static create(properties?: niuniu.IRounRecordNTF): niuniu.RounRecordNTF;

        /**
         * Encodes the specified RounRecordNTF message. Does not implicitly {@link niuniu.RounRecordNTF.verify|verify} messages.
         * @param message RounRecordNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IRounRecordNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RounRecordNTF message, length delimited. Does not implicitly {@link niuniu.RounRecordNTF.verify|verify} messages.
         * @param message RounRecordNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IRounRecordNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RounRecordNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RounRecordNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.RounRecordNTF;

        /**
         * Decodes a RounRecordNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RounRecordNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.RounRecordNTF;

        /**
         * Verifies a RounRecordNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** DismissActionDef enum. */
    enum DismissActionDef {
        None = 0,
        Accept = 1,
        Reject = 2,
        Request = 3
    }

    /** Properties of a DismissREQ. */
    interface IDismissREQ {

        /** DismissREQ Action */
        Action?: (niuniu.DismissActionDef|null);
    }

    /** Represents a DismissREQ. */
    class DismissREQ implements IDismissREQ {

        /**
         * Constructs a new DismissREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IDismissREQ);

        /** DismissREQ Action. */
        public Action: niuniu.DismissActionDef;

        /**
         * Creates a new DismissREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DismissREQ instance
         */
        public static create(properties?: niuniu.IDismissREQ): niuniu.DismissREQ;

        /**
         * Encodes the specified DismissREQ message. Does not implicitly {@link niuniu.DismissREQ.verify|verify} messages.
         * @param message DismissREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IDismissREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DismissREQ message, length delimited. Does not implicitly {@link niuniu.DismissREQ.verify|verify} messages.
         * @param message DismissREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IDismissREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DismissREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DismissREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.DismissREQ;

        /**
         * Decodes a DismissREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DismissREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.DismissREQ;

        /**
         * Verifies a DismissREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DismissNTF. */
    interface IDismissNTF {

        /** DismissNTF StartUTC */
        StartUTC?: (number|Long|null);

        /** DismissNTF DurationUTC */
        DurationUTC?: (number|Long|null);

        /** DismissNTF Action */
        Action?: (niuniu.DismissActionDef[]|null);

        /** DismissNTF User */
        User?: ((number|Long)[]|null);
    }

    /** Represents a DismissNTF. */
    class DismissNTF implements IDismissNTF {

        /**
         * Constructs a new DismissNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IDismissNTF);

        /** DismissNTF StartUTC. */
        public StartUTC: (number|Long);

        /** DismissNTF DurationUTC. */
        public DurationUTC: (number|Long);

        /** DismissNTF Action. */
        public Action: niuniu.DismissActionDef[];

        /** DismissNTF User. */
        public User: (number|Long)[];

        /**
         * Creates a new DismissNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DismissNTF instance
         */
        public static create(properties?: niuniu.IDismissNTF): niuniu.DismissNTF;

        /**
         * Encodes the specified DismissNTF message. Does not implicitly {@link niuniu.DismissNTF.verify|verify} messages.
         * @param message DismissNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IDismissNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DismissNTF message, length delimited. Does not implicitly {@link niuniu.DismissNTF.verify|verify} messages.
         * @param message DismissNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IDismissNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DismissNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DismissNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.DismissNTF;

        /**
         * Decodes a DismissNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DismissNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.DismissNTF;

        /**
         * Verifies a DismissNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DismissACK. */
    interface IDismissACK {

        /** DismissACK Code */
        Code?: (niuniu.GameCode|null);

        /** DismissACK UserID */
        UserID?: (number|Long|null);

        /** DismissACK Action */
        Action?: (niuniu.DismissActionDef|null);

        /** DismissACK StartUTC */
        StartUTC?: (number|Long|null);

        /** DismissACK DurationUTC */
        DurationUTC?: (number|Long|null);
    }

    /** Represents a DismissACK. */
    class DismissACK implements IDismissACK {

        /**
         * Constructs a new DismissACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IDismissACK);

        /** DismissACK Code. */
        public Code: niuniu.GameCode;

        /** DismissACK UserID. */
        public UserID: (number|Long);

        /** DismissACK Action. */
        public Action: niuniu.DismissActionDef;

        /** DismissACK StartUTC. */
        public StartUTC: (number|Long);

        /** DismissACK DurationUTC. */
        public DurationUTC: (number|Long);

        /**
         * Creates a new DismissACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DismissACK instance
         */
        public static create(properties?: niuniu.IDismissACK): niuniu.DismissACK;

        /**
         * Encodes the specified DismissACK message. Does not implicitly {@link niuniu.DismissACK.verify|verify} messages.
         * @param message DismissACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IDismissACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DismissACK message, length delimited. Does not implicitly {@link niuniu.DismissACK.verify|verify} messages.
         * @param message DismissACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IDismissACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DismissACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DismissACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.DismissACK;

        /**
         * Decodes a DismissACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DismissACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.DismissACK;

        /**
         * Verifies a DismissACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DismissedNTF. */
    interface IDismissedNTF {

        /** DismissedNTF UserId */
        UserId?: (number|Long|null);

        /** DismissedNTF TableID */
        TableID?: (number|Long|null);
    }

    /** Represents a DismissedNTF. */
    class DismissedNTF implements IDismissedNTF {

        /**
         * Constructs a new DismissedNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IDismissedNTF);

        /** DismissedNTF UserId. */
        public UserId: (number|Long);

        /** DismissedNTF TableID. */
        public TableID: (number|Long);

        /**
         * Creates a new DismissedNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DismissedNTF instance
         */
        public static create(properties?: niuniu.IDismissedNTF): niuniu.DismissedNTF;

        /**
         * Encodes the specified DismissedNTF message. Does not implicitly {@link niuniu.DismissedNTF.verify|verify} messages.
         * @param message DismissedNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IDismissedNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DismissedNTF message, length delimited. Does not implicitly {@link niuniu.DismissedNTF.verify|verify} messages.
         * @param message DismissedNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IDismissedNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DismissedNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DismissedNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.DismissedNTF;

        /**
         * Decodes a DismissedNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DismissedNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.DismissedNTF;

        /**
         * Verifies a DismissedNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ModifyCardREQ. */
    interface IModifyCardREQ {

        /** ModifyCardREQ UserID */
        UserID?: (number|Long|null);

        /** ModifyCardREQ Card */
        Card?: (number[]|null);
    }

    /** Represents a ModifyCardREQ. */
    class ModifyCardREQ implements IModifyCardREQ {

        /**
         * Constructs a new ModifyCardREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IModifyCardREQ);

        /** ModifyCardREQ UserID. */
        public UserID: (number|Long);

        /** ModifyCardREQ Card. */
        public Card: number[];

        /**
         * Creates a new ModifyCardREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ModifyCardREQ instance
         */
        public static create(properties?: niuniu.IModifyCardREQ): niuniu.ModifyCardREQ;

        /**
         * Encodes the specified ModifyCardREQ message. Does not implicitly {@link niuniu.ModifyCardREQ.verify|verify} messages.
         * @param message ModifyCardREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IModifyCardREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ModifyCardREQ message, length delimited. Does not implicitly {@link niuniu.ModifyCardREQ.verify|verify} messages.
         * @param message ModifyCardREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IModifyCardREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ModifyCardREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ModifyCardREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.ModifyCardREQ;

        /**
         * Decodes a ModifyCardREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ModifyCardREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.ModifyCardREQ;

        /**
         * Verifies a ModifyCardREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DeleteRoomREQ. */
    interface IDeleteRoomREQ {

        /** DeleteRoomREQ TableID */
        TableID?: (number|null);
    }

    /** Represents a DeleteRoomREQ. */
    class DeleteRoomREQ implements IDeleteRoomREQ {

        /**
         * Constructs a new DeleteRoomREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IDeleteRoomREQ);

        /** DeleteRoomREQ TableID. */
        public TableID: number;

        /**
         * Creates a new DeleteRoomREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DeleteRoomREQ instance
         */
        public static create(properties?: niuniu.IDeleteRoomREQ): niuniu.DeleteRoomREQ;

        /**
         * Encodes the specified DeleteRoomREQ message. Does not implicitly {@link niuniu.DeleteRoomREQ.verify|verify} messages.
         * @param message DeleteRoomREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IDeleteRoomREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DeleteRoomREQ message, length delimited. Does not implicitly {@link niuniu.DeleteRoomREQ.verify|verify} messages.
         * @param message DeleteRoomREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IDeleteRoomREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DeleteRoomREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DeleteRoomREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.DeleteRoomREQ;

        /**
         * Decodes a DeleteRoomREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DeleteRoomREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.DeleteRoomREQ;

        /**
         * Verifies a DeleteRoomREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DeleteRoomACK. */
    interface IDeleteRoomACK {

        /** DeleteRoomACK Code */
        Code?: (niuniu.GameCode|null);

        /** DeleteRoomACK TableID */
        TableID?: (number|null);
    }

    /** Represents a DeleteRoomACK. */
    class DeleteRoomACK implements IDeleteRoomACK {

        /**
         * Constructs a new DeleteRoomACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IDeleteRoomACK);

        /** DeleteRoomACK Code. */
        public Code: niuniu.GameCode;

        /** DeleteRoomACK TableID. */
        public TableID: number;

        /**
         * Creates a new DeleteRoomACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DeleteRoomACK instance
         */
        public static create(properties?: niuniu.IDeleteRoomACK): niuniu.DeleteRoomACK;

        /**
         * Encodes the specified DeleteRoomACK message. Does not implicitly {@link niuniu.DeleteRoomACK.verify|verify} messages.
         * @param message DeleteRoomACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IDeleteRoomACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DeleteRoomACK message, length delimited. Does not implicitly {@link niuniu.DeleteRoomACK.verify|verify} messages.
         * @param message DeleteRoomACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IDeleteRoomACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DeleteRoomACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DeleteRoomACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.DeleteRoomACK;

        /**
         * Decodes a DeleteRoomACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DeleteRoomACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.DeleteRoomACK;

        /**
         * Verifies a DeleteRoomACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TipREQ. */
    interface ITipREQ {
    }

    /** Represents a TipREQ. */
    class TipREQ implements ITipREQ {

        /**
         * Constructs a new TipREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ITipREQ);

        /**
         * Creates a new TipREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TipREQ instance
         */
        public static create(properties?: niuniu.ITipREQ): niuniu.TipREQ;

        /**
         * Encodes the specified TipREQ message. Does not implicitly {@link niuniu.TipREQ.verify|verify} messages.
         * @param message TipREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ITipREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TipREQ message, length delimited. Does not implicitly {@link niuniu.TipREQ.verify|verify} messages.
         * @param message TipREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ITipREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TipREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TipREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.TipREQ;

        /**
         * Decodes a TipREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TipREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.TipREQ;

        /**
         * Verifies a TipREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TipACK. */
    interface ITipACK {

        /** TipACK Card */
        Card?: (number[]|null);

        /** TipACK CardType */
        CardType?: (number|null);
    }

    /** Represents a TipACK. */
    class TipACK implements ITipACK {

        /**
         * Constructs a new TipACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ITipACK);

        /** TipACK Card. */
        public Card: number[];

        /** TipACK CardType. */
        public CardType: number;

        /**
         * Creates a new TipACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TipACK instance
         */
        public static create(properties?: niuniu.ITipACK): niuniu.TipACK;

        /**
         * Encodes the specified TipACK message. Does not implicitly {@link niuniu.TipACK.verify|verify} messages.
         * @param message TipACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ITipACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TipACK message, length delimited. Does not implicitly {@link niuniu.TipACK.verify|verify} messages.
         * @param message TipACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ITipACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TipACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TipACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.TipACK;

        /**
         * Decodes a TipACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TipACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.TipACK;

        /**
         * Verifies a TipACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a PrepareREQ. */
    interface IPrepareREQ {
    }

    /** Represents a PrepareREQ. */
    class PrepareREQ implements IPrepareREQ {

        /**
         * Constructs a new PrepareREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IPrepareREQ);

        /**
         * Creates a new PrepareREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PrepareREQ instance
         */
        public static create(properties?: niuniu.IPrepareREQ): niuniu.PrepareREQ;

        /**
         * Encodes the specified PrepareREQ message. Does not implicitly {@link niuniu.PrepareREQ.verify|verify} messages.
         * @param message PrepareREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IPrepareREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified PrepareREQ message, length delimited. Does not implicitly {@link niuniu.PrepareREQ.verify|verify} messages.
         * @param message PrepareREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IPrepareREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a PrepareREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PrepareREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.PrepareREQ;

        /**
         * Decodes a PrepareREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PrepareREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.PrepareREQ;

        /**
         * Verifies a PrepareREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a PrepareACK. */
    interface IPrepareACK {

        /** PrepareACK UserID */
        UserID?: (number|Long|null);
    }

    /** Represents a PrepareACK. */
    class PrepareACK implements IPrepareACK {

        /**
         * Constructs a new PrepareACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.IPrepareACK);

        /** PrepareACK UserID. */
        public UserID: (number|Long);

        /**
         * Creates a new PrepareACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PrepareACK instance
         */
        public static create(properties?: niuniu.IPrepareACK): niuniu.PrepareACK;

        /**
         * Encodes the specified PrepareACK message. Does not implicitly {@link niuniu.PrepareACK.verify|verify} messages.
         * @param message PrepareACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.IPrepareACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified PrepareACK message, length delimited. Does not implicitly {@link niuniu.PrepareACK.verify|verify} messages.
         * @param message PrepareACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.IPrepareACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a PrepareACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PrepareACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.PrepareACK;

        /**
         * Decodes a PrepareACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PrepareACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.PrepareACK;

        /**
         * Verifies a PrepareACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TableInfoNTF. */
    interface ITableInfoNTF {

        /** TableInfoNTF Info */
        Info?: (structure.ITableInfo[]|null);
    }

    /** Represents a TableInfoNTF. */
    class TableInfoNTF implements ITableInfoNTF {

        /**
         * Constructs a new TableInfoNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: niuniu.ITableInfoNTF);

        /** TableInfoNTF Info. */
        public Info: structure.ITableInfo[];

        /**
         * Creates a new TableInfoNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TableInfoNTF instance
         */
        public static create(properties?: niuniu.ITableInfoNTF): niuniu.TableInfoNTF;

        /**
         * Encodes the specified TableInfoNTF message. Does not implicitly {@link niuniu.TableInfoNTF.verify|verify} messages.
         * @param message TableInfoNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: niuniu.ITableInfoNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TableInfoNTF message, length delimited. Does not implicitly {@link niuniu.TableInfoNTF.verify|verify} messages.
         * @param message TableInfoNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: niuniu.ITableInfoNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TableInfoNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TableInfoNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): niuniu.TableInfoNTF;

        /**
         * Decodes a TableInfoNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TableInfoNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): niuniu.TableInfoNTF;

        /**
         * Verifies a TableInfoNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }
}

/** Namespace record. */
declare namespace record {

    /** Properties of a NiuniuUserRecord. */
    interface INiuniuUserRecord {

        /** NiuniuUserRecord UserId */
        UserId?: (number|Long|null);

        /** NiuniuUserRecord NickName */
        NickName?: (string|null);

        /** NiuniuUserRecord RankerCount */
        RankerCount?: (number|null);

        /** NiuniuUserRecord TuizhuCount */
        TuizhuCount?: (number|null);

        /** NiuniuUserRecord HandRec */
        HandRec?: (record.NiuniuUserRecord.IHandRecord[]|null);

        /** NiuniuUserRecord RankCount */
        RankCount?: (number|null);

        /** NiuniuUserRecord HeadIcon */
        HeadIcon?: (string|null);

        /** NiuniuUserRecord Score */
        Score?: (number|Long|null);
    }

    /** Represents a NiuniuUserRecord. */
    class NiuniuUserRecord implements INiuniuUserRecord {

        /**
         * Constructs a new NiuniuUserRecord.
         * @param [properties] Properties to set
         */
        constructor(properties?: record.INiuniuUserRecord);

        /** NiuniuUserRecord UserId. */
        public UserId: (number|Long);

        /** NiuniuUserRecord NickName. */
        public NickName: string;

        /** NiuniuUserRecord RankerCount. */
        public RankerCount: number;

        /** NiuniuUserRecord TuizhuCount. */
        public TuizhuCount: number;

        /** NiuniuUserRecord HandRec. */
        public HandRec: record.NiuniuUserRecord.IHandRecord[];

        /** NiuniuUserRecord RankCount. */
        public RankCount: number;

        /** NiuniuUserRecord HeadIcon. */
        public HeadIcon: string;

        /** NiuniuUserRecord Score. */
        public Score: (number|Long);

        /**
         * Creates a new NiuniuUserRecord instance using the specified properties.
         * @param [properties] Properties to set
         * @returns NiuniuUserRecord instance
         */
        public static create(properties?: record.INiuniuUserRecord): record.NiuniuUserRecord;

        /**
         * Encodes the specified NiuniuUserRecord message. Does not implicitly {@link record.NiuniuUserRecord.verify|verify} messages.
         * @param message NiuniuUserRecord message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: record.INiuniuUserRecord, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified NiuniuUserRecord message, length delimited. Does not implicitly {@link record.NiuniuUserRecord.verify|verify} messages.
         * @param message NiuniuUserRecord message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: record.INiuniuUserRecord, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a NiuniuUserRecord message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns NiuniuUserRecord
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): record.NiuniuUserRecord;

        /**
         * Decodes a NiuniuUserRecord message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns NiuniuUserRecord
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): record.NiuniuUserRecord;

        /**
         * Verifies a NiuniuUserRecord message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace NiuniuUserRecord {

        /** Properties of a HandRecord. */
        interface IHandRecord {

            /** HandRecord Date */
            Date?: (number|Long|null);

            /** HandRecord Hand */
            Hand?: (number|null);

            /** HandRecord CardType */
            CardType?: (number|null);

            /** HandRecord Cards */
            Cards?: (number[]|null);

            /** HandRecord Score */
            Score?: (number|Long|null);

            /** HandRecord Ranker */
            Ranker?: (boolean|null);

            /** HandRecord Tuizhu */
            Tuizhu?: (boolean|null);

            /** HandRecord Rank */
            Rank?: (boolean|null);

            /** HandRecord OwnDraw */
            OwnDraw?: (number|Long|null);

            /** HandRecord PartnerDraw */
            PartnerDraw?: (number|Long|null);

            /** HandRecord SystemDraw */
            SystemDraw?: (number|Long|null);

            /** HandRecord PromoterDraw */
            PromoterDraw?: (number|Long|null);

            /** HandRecord RankMulti */
            RankMulti?: (number|null);

            /** HandRecord UserScore */
            UserScore?: (number|Long|null);

            /** HandRecord Bet */
            Bet?: (number|Long|null);
        }

        /** Represents a HandRecord. */
        class HandRecord implements IHandRecord {

            /**
             * Constructs a new HandRecord.
             * @param [properties] Properties to set
             */
            constructor(properties?: record.NiuniuUserRecord.IHandRecord);

            /** HandRecord Date. */
            public Date: (number|Long);

            /** HandRecord Hand. */
            public Hand: number;

            /** HandRecord CardType. */
            public CardType: number;

            /** HandRecord Cards. */
            public Cards: number[];

            /** HandRecord Score. */
            public Score: (number|Long);

            /** HandRecord Ranker. */
            public Ranker: boolean;

            /** HandRecord Tuizhu. */
            public Tuizhu: boolean;

            /** HandRecord Rank. */
            public Rank: boolean;

            /** HandRecord OwnDraw. */
            public OwnDraw: (number|Long);

            /** HandRecord PartnerDraw. */
            public PartnerDraw: (number|Long);

            /** HandRecord SystemDraw. */
            public SystemDraw: (number|Long);

            /** HandRecord PromoterDraw. */
            public PromoterDraw: (number|Long);

            /** HandRecord RankMulti. */
            public RankMulti: number;

            /** HandRecord UserScore. */
            public UserScore: (number|Long);

            /** HandRecord Bet. */
            public Bet: (number|Long);

            /**
             * Creates a new HandRecord instance using the specified properties.
             * @param [properties] Properties to set
             * @returns HandRecord instance
             */
            public static create(properties?: record.NiuniuUserRecord.IHandRecord): record.NiuniuUserRecord.HandRecord;

            /**
             * Encodes the specified HandRecord message. Does not implicitly {@link record.NiuniuUserRecord.HandRecord.verify|verify} messages.
             * @param message HandRecord message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: record.NiuniuUserRecord.IHandRecord, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Encodes the specified HandRecord message, length delimited. Does not implicitly {@link record.NiuniuUserRecord.HandRecord.verify|verify} messages.
             * @param message HandRecord message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: record.NiuniuUserRecord.IHandRecord, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Decodes a HandRecord message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns HandRecord
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): record.NiuniuUserRecord.HandRecord;

            /**
             * Decodes a HandRecord message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns HandRecord
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): record.NiuniuUserRecord.HandRecord;

            /**
             * Verifies a HandRecord message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);
        }
    }

    /** Properties of a NiuniuRoundRecord. */
    interface INiuniuRoundRecord {

        /** NiuniuRoundRecord Date */
        Date?: (number|Long|null);

        /** NiuniuRoundRecord TableId */
        TableId?: (number|null);

        /** NiuniuRoundRecord HouseID */
        HouseID?: (number|Long|null);

        /** NiuniuRoundRecord Rec */
        Rec?: (record.INiuniuUserRecord[]|null);

        /** NiuniuRoundRecord MatchType */
        MatchType?: (structure.TableConfig.MatchTypeConfig|null);

        /** NiuniuRoundRecord HandCount */
        HandCount?: (number|null);

        /** NiuniuRoundRecord MaxRound */
        MaxRound?: (number|null);

        /** NiuniuRoundRecord BaseScore */
        BaseScore?: ((number|Long)[]|null);
    }

    /** Represents a NiuniuRoundRecord. */
    class NiuniuRoundRecord implements INiuniuRoundRecord {

        /**
         * Constructs a new NiuniuRoundRecord.
         * @param [properties] Properties to set
         */
        constructor(properties?: record.INiuniuRoundRecord);

        /** NiuniuRoundRecord Date. */
        public Date: (number|Long);

        /** NiuniuRoundRecord TableId. */
        public TableId: number;

        /** NiuniuRoundRecord HouseID. */
        public HouseID: (number|Long);

        /** NiuniuRoundRecord Rec. */
        public Rec: record.INiuniuUserRecord[];

        /** NiuniuRoundRecord MatchType. */
        public MatchType: structure.TableConfig.MatchTypeConfig;

        /** NiuniuRoundRecord HandCount. */
        public HandCount: number;

        /** NiuniuRoundRecord MaxRound. */
        public MaxRound: number;

        /** NiuniuRoundRecord BaseScore. */
        public BaseScore: (number|Long)[];

        /**
         * Creates a new NiuniuRoundRecord instance using the specified properties.
         * @param [properties] Properties to set
         * @returns NiuniuRoundRecord instance
         */
        public static create(properties?: record.INiuniuRoundRecord): record.NiuniuRoundRecord;

        /**
         * Encodes the specified NiuniuRoundRecord message. Does not implicitly {@link record.NiuniuRoundRecord.verify|verify} messages.
         * @param message NiuniuRoundRecord message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: record.INiuniuRoundRecord, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified NiuniuRoundRecord message, length delimited. Does not implicitly {@link record.NiuniuRoundRecord.verify|verify} messages.
         * @param message NiuniuRoundRecord message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: record.INiuniuRoundRecord, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a NiuniuRoundRecord message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns NiuniuRoundRecord
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): record.NiuniuRoundRecord;

        /**
         * Decodes a NiuniuRoundRecord message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns NiuniuRoundRecord
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): record.NiuniuRoundRecord;

        /**
         * Verifies a NiuniuRoundRecord message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a Record. */
    interface IRecord {

        /** Record ModuleID */
        ModuleID?: (number|null);

        /** Record NiuNiuRoundRec */
        NiuNiuRoundRec?: (record.INiuniuRoundRecord[]|null);
    }

    /** Represents a Record. */
    class Record implements IRecord {

        /**
         * Constructs a new Record.
         * @param [properties] Properties to set
         */
        constructor(properties?: record.IRecord);

        /** Record ModuleID. */
        public ModuleID: number;

        /** Record NiuNiuRoundRec. */
        public NiuNiuRoundRec: record.INiuniuRoundRecord[];

        /**
         * Creates a new Record instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Record instance
         */
        public static create(properties?: record.IRecord): record.Record;

        /**
         * Encodes the specified Record message. Does not implicitly {@link record.Record.verify|verify} messages.
         * @param message Record message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: record.IRecord, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified Record message, length delimited. Does not implicitly {@link record.Record.verify|verify} messages.
         * @param message Record message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: record.IRecord, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a Record message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Record
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): record.Record;

        /**
         * Decodes a Record message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Record
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): record.Record;

        /**
         * Verifies a Record message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ScoreRecord. */
    interface IScoreRecord {

        /** ScoreRecord Rea */
        Rea?: (record.ScoreRecord.Reason|null);

        /** ScoreRecord TeaHouseID */
        TeaHouseID?: (number|Long|null);

        /** ScoreRecord ManageID */
        ManageID?: (number|Long|null);

        /** ScoreRecord ManageNickName */
        ManageNickName?: (string|null);

        /** ScoreRecord UserID */
        UserID?: (number|Long|null);

        /** ScoreRecord UserNickName */
        UserNickName?: (string|null);

        /** ScoreRecord BeforeScore */
        BeforeScore?: (number|Long|null);

        /** ScoreRecord Score */
        Score?: (number|Long|null);

        /** ScoreRecord AfterScore */
        AfterScore?: (number|Long|null);
    }

    /** Represents a ScoreRecord. */
    class ScoreRecord implements IScoreRecord {

        /**
         * Constructs a new ScoreRecord.
         * @param [properties] Properties to set
         */
        constructor(properties?: record.IScoreRecord);

        /** ScoreRecord Rea. */
        public Rea: record.ScoreRecord.Reason;

        /** ScoreRecord TeaHouseID. */
        public TeaHouseID: (number|Long);

        /** ScoreRecord ManageID. */
        public ManageID: (number|Long);

        /** ScoreRecord ManageNickName. */
        public ManageNickName: string;

        /** ScoreRecord UserID. */
        public UserID: (number|Long);

        /** ScoreRecord UserNickName. */
        public UserNickName: string;

        /** ScoreRecord BeforeScore. */
        public BeforeScore: (number|Long);

        /** ScoreRecord Score. */
        public Score: (number|Long);

        /** ScoreRecord AfterScore. */
        public AfterScore: (number|Long);

        /**
         * Creates a new ScoreRecord instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ScoreRecord instance
         */
        public static create(properties?: record.IScoreRecord): record.ScoreRecord;

        /**
         * Encodes the specified ScoreRecord message. Does not implicitly {@link record.ScoreRecord.verify|verify} messages.
         * @param message ScoreRecord message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: record.IScoreRecord, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ScoreRecord message, length delimited. Does not implicitly {@link record.ScoreRecord.verify|verify} messages.
         * @param message ScoreRecord message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: record.IScoreRecord, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ScoreRecord message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ScoreRecord
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): record.ScoreRecord;

        /**
         * Decodes a ScoreRecord message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ScoreRecord
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): record.ScoreRecord;

        /**
         * Verifies a ScoreRecord message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace ScoreRecord {

        /** Reason enum. */
        enum Reason {
            Manage_Add = 0,
            WebAdmin_Add = 1
        }
    }
}

/** Namespace teahouse. */
declare namespace teahouse {

    /** TeaHouseID enum. */
    enum TeaHouseID {
        ID = 0,
        ID_CreateTeaHouseREQ = 20401,
        ID_CreateTeaHouseACK = 20402,
        ID_TeaHouseInfoREQ = 20403,
        ID_TeaHouseInfoACK = 20404,
        ID_TeaHouseActionREQ = 20405,
        ID_TeaHouseActionACK = 20406,
        ID_TeaHouseJoinedREQ = 20407,
        ID_TeeHouseJoinedACK = 20408,
        ID_TeaHouseHandleOrderREQ = 20409,
        ID_TeaHouseHandleOrderACK = 20410,
        ID_TeaHouseChangeInfoREQ = 20411,
        ID_TeaHouseChangeInfoACK = 20412,
        ID_RankREQ = 20413,
        ID_RankACK = 20414,
        ID_RecordREQ = 20415,
        ID_RecordACK = 20416,
        ID_UserRecordREQ = 20418,
        ID_UserRecordACK = 20419,
        ID_ScoreLogREQ = 20420,
        ID_ScoreLogACK = 20421,
        ID_UserScoreLogREQ = 20422,
        ID_UserScoreLogACK = 20423,
        ID_TableStatisticsREQ = 20424,
        ID_TableStatisticsACK = 20425,
        ID_DrawREQ = 20426,
        ID_DrawACK = 20427,
        ID_ChargeOrderREQ = 20428,
        ID_ChargeOrderACK = 20429,
        ID_ChargeOrderNTF = 20430
    }

    /** TeaHouseCode enum. */
    enum TeaHouseCode {
        Success = 0,
        Invalid_Name = 1,
        Duplicate_Name = 2,
        Invalid_User = 3,
        Invalid_TeaHouse = 4,
        Duplicate_User = 5,
        Invalid_OrderID = 6,
        Invalid_Order = 7,
        User_Not_In_TeaHouse = 8,
        User_Not_Normal = 9,
        Table_Not_Allow_AddScore = 10,
        Message_Send = 11,
        Partner_Exits = 12,
        Already_Manager = 13,
        Already_Partner = 14,
        GetOrder_Failed = 15,
        Unknown = 199
    }

    /** TeaHouseLimit enum. */
    enum TeaHouseLimit {
        Owner = 0,
        Partner = 1,
        Manager = 2,
        Member = 3,
        Promoter = 4
    }

    /** MemberState enum. */
    enum MemberState {
        Normal = 0,
        Frozen = 1,
        Deleted = 2
    }

    /** Properties of a TeaHouseMember. */
    interface ITeaHouseMember {

        /** TeaHouseMember NickName */
        NickName?: (string|null);

        /** TeaHouseMember ID */
        ID?: (number|Long|null);

        /** TeaHouseMember Limit */
        Limit?: (teahouse.TeaHouseLimit|null);

        /** TeaHouseMember State */
        State?: (teahouse.MemberState|null);

        /** TeaHouseMember Score */
        Score?: (number|Long|null);

        /** TeaHouseMember PartnerRate */
        PartnerRate?: (number|Long|null);

        /** TeaHouseMember Invitor */
        Invitor?: (number|Long|null);

        /** TeaHouseMember BelongToPartner */
        BelongToPartner?: (number|Long|null);

        /** TeaHouseMember HeadIcon */
        HeadIcon?: (string|null);
    }

    /** Represents a TeaHouseMember. */
    class TeaHouseMember implements ITeaHouseMember {

        /**
         * Constructs a new TeaHouseMember.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseMember);

        /** TeaHouseMember NickName. */
        public NickName: string;

        /** TeaHouseMember ID. */
        public ID: (number|Long);

        /** TeaHouseMember Limit. */
        public Limit: teahouse.TeaHouseLimit;

        /** TeaHouseMember State. */
        public State: teahouse.MemberState;

        /** TeaHouseMember Score. */
        public Score: (number|Long);

        /** TeaHouseMember PartnerRate. */
        public PartnerRate: (number|Long);

        /** TeaHouseMember Invitor. */
        public Invitor: (number|Long);

        /** TeaHouseMember BelongToPartner. */
        public BelongToPartner: (number|Long);

        /** TeaHouseMember HeadIcon. */
        public HeadIcon: string;

        /**
         * Creates a new TeaHouseMember instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseMember instance
         */
        public static create(properties?: teahouse.ITeaHouseMember): teahouse.TeaHouseMember;

        /**
         * Encodes the specified TeaHouseMember message. Does not implicitly {@link teahouse.TeaHouseMember.verify|verify} messages.
         * @param message TeaHouseMember message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseMember, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseMember message, length delimited. Does not implicitly {@link teahouse.TeaHouseMember.verify|verify} messages.
         * @param message TeaHouseMember message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseMember, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseMember message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseMember
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseMember;

        /**
         * Decodes a TeaHouseMember message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseMember
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseMember;

        /**
         * Verifies a TeaHouseMember message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeaHouseInfo. */
    interface ITeaHouseInfo {

        /** TeaHouseInfo ID */
        ID?: (number|Long|null);

        /** TeaHouseInfo Config */
        Config?: (teahouse.ITeaHouseConfig|null);

        /** TeaHouseInfo Ruler */
        Ruler?: (string|null);

        /** TeaHouseInfo Charger */
        Charger?: (number|Long|null);

        /** TeaHouseInfo MemberCount */
        MemberCount?: (number|Long|null);

        /** TeaHouseInfo OwnerNickname */
        OwnerNickname?: (string|null);

        /** TeaHouseInfo OwnerID */
        OwnerID?: (number|Long|null);

        /** TeaHouseInfo Member */
        Member?: (teahouse.ITeaHouseMember[]|null);
    }

    /** Represents a TeaHouseInfo. */
    class TeaHouseInfo implements ITeaHouseInfo {

        /**
         * Constructs a new TeaHouseInfo.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseInfo);

        /** TeaHouseInfo ID. */
        public ID: (number|Long);

        /** TeaHouseInfo Config. */
        public Config?: (teahouse.ITeaHouseConfig|null);

        /** TeaHouseInfo Ruler. */
        public Ruler: string;

        /** TeaHouseInfo Charger. */
        public Charger: (number|Long);

        /** TeaHouseInfo MemberCount. */
        public MemberCount: (number|Long);

        /** TeaHouseInfo OwnerNickname. */
        public OwnerNickname: string;

        /** TeaHouseInfo OwnerID. */
        public OwnerID: (number|Long);

        /** TeaHouseInfo Member. */
        public Member: teahouse.ITeaHouseMember[];

        /**
         * Creates a new TeaHouseInfo instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseInfo instance
         */
        public static create(properties?: teahouse.ITeaHouseInfo): teahouse.TeaHouseInfo;

        /**
         * Encodes the specified TeaHouseInfo message. Does not implicitly {@link teahouse.TeaHouseInfo.verify|verify} messages.
         * @param message TeaHouseInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseInfo, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseInfo message, length delimited. Does not implicitly {@link teahouse.TeaHouseInfo.verify|verify} messages.
         * @param message TeaHouseInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseInfo, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseInfo message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseInfo;

        /**
         * Decodes a TeaHouseInfo message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseInfo;

        /**
         * Verifies a TeaHouseInfo message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeaHouseConfig. */
    interface ITeaHouseConfig {

        /** TeaHouseConfig Verify */
        Verify?: (boolean|null);

        /** TeaHouseConfig ViewRank */
        ViewRank?: (boolean|null);

        /** TeaHouseConfig Close */
        Close?: (boolean|null);

        /** TeaHouseConfig HideUserID */
        HideUserID?: (boolean|null);

        /** TeaHouseConfig Name */
        Name?: (string|null);

        /** TeaHouseConfig Notice */
        Notice?: (string|null);

        /** TeaHouseConfig MatchType */
        MatchType?: (structure.TableConfig.MatchTypeConfig|null);
    }

    /** Represents a TeaHouseConfig. */
    class TeaHouseConfig implements ITeaHouseConfig {

        /**
         * Constructs a new TeaHouseConfig.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseConfig);

        /** TeaHouseConfig Verify. */
        public Verify: boolean;

        /** TeaHouseConfig ViewRank. */
        public ViewRank: boolean;

        /** TeaHouseConfig Close. */
        public Close: boolean;

        /** TeaHouseConfig HideUserID. */
        public HideUserID: boolean;

        /** TeaHouseConfig Name. */
        public Name: string;

        /** TeaHouseConfig Notice. */
        public Notice: string;

        /** TeaHouseConfig MatchType. */
        public MatchType: structure.TableConfig.MatchTypeConfig;

        /**
         * Creates a new TeaHouseConfig instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseConfig instance
         */
        public static create(properties?: teahouse.ITeaHouseConfig): teahouse.TeaHouseConfig;

        /**
         * Encodes the specified TeaHouseConfig message. Does not implicitly {@link teahouse.TeaHouseConfig.verify|verify} messages.
         * @param message TeaHouseConfig message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseConfig, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseConfig message, length delimited. Does not implicitly {@link teahouse.TeaHouseConfig.verify|verify} messages.
         * @param message TeaHouseConfig message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseConfig, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseConfig message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseConfig
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseConfig;

        /**
         * Decodes a TeaHouseConfig message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseConfig
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseConfig;

        /**
         * Verifies a TeaHouseConfig message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a CreateTeaHouseREQ. */
    interface ICreateTeaHouseREQ {

        /** CreateTeaHouseREQ Name */
        Name?: (string|null);

        /** CreateTeaHouseREQ MatchType */
        MatchType?: (structure.TableConfig.MatchTypeConfig|null);
    }

    /** Represents a CreateTeaHouseREQ. */
    class CreateTeaHouseREQ implements ICreateTeaHouseREQ {

        /**
         * Constructs a new CreateTeaHouseREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ICreateTeaHouseREQ);

        /** CreateTeaHouseREQ Name. */
        public Name: string;

        /** CreateTeaHouseREQ MatchType. */
        public MatchType: structure.TableConfig.MatchTypeConfig;

        /**
         * Creates a new CreateTeaHouseREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns CreateTeaHouseREQ instance
         */
        public static create(properties?: teahouse.ICreateTeaHouseREQ): teahouse.CreateTeaHouseREQ;

        /**
         * Encodes the specified CreateTeaHouseREQ message. Does not implicitly {@link teahouse.CreateTeaHouseREQ.verify|verify} messages.
         * @param message CreateTeaHouseREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ICreateTeaHouseREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified CreateTeaHouseREQ message, length delimited. Does not implicitly {@link teahouse.CreateTeaHouseREQ.verify|verify} messages.
         * @param message CreateTeaHouseREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ICreateTeaHouseREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a CreateTeaHouseREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns CreateTeaHouseREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.CreateTeaHouseREQ;

        /**
         * Decodes a CreateTeaHouseREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns CreateTeaHouseREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.CreateTeaHouseREQ;

        /**
         * Verifies a CreateTeaHouseREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a CreateTeaHouseACK. */
    interface ICreateTeaHouseACK {

        /** CreateTeaHouseACK Code */
        Code?: (teahouse.TeaHouseCode|null);

        /** CreateTeaHouseACK Info */
        Info?: (teahouse.ITeaHouseInfo|null);
    }

    /** Represents a CreateTeaHouseACK. */
    class CreateTeaHouseACK implements ICreateTeaHouseACK {

        /**
         * Constructs a new CreateTeaHouseACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ICreateTeaHouseACK);

        /** CreateTeaHouseACK Code. */
        public Code: teahouse.TeaHouseCode;

        /** CreateTeaHouseACK Info. */
        public Info?: (teahouse.ITeaHouseInfo|null);

        /**
         * Creates a new CreateTeaHouseACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns CreateTeaHouseACK instance
         */
        public static create(properties?: teahouse.ICreateTeaHouseACK): teahouse.CreateTeaHouseACK;

        /**
         * Encodes the specified CreateTeaHouseACK message. Does not implicitly {@link teahouse.CreateTeaHouseACK.verify|verify} messages.
         * @param message CreateTeaHouseACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ICreateTeaHouseACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified CreateTeaHouseACK message, length delimited. Does not implicitly {@link teahouse.CreateTeaHouseACK.verify|verify} messages.
         * @param message CreateTeaHouseACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ICreateTeaHouseACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a CreateTeaHouseACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns CreateTeaHouseACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.CreateTeaHouseACK;

        /**
         * Decodes a CreateTeaHouseACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns CreateTeaHouseACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.CreateTeaHouseACK;

        /**
         * Verifies a CreateTeaHouseACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeaHouseInfoREQ. */
    interface ITeaHouseInfoREQ {

        /** TeaHouseInfoREQ ID */
        ID?: ((number|Long)[]|null);
    }

    /** Represents a TeaHouseInfoREQ. */
    class TeaHouseInfoREQ implements ITeaHouseInfoREQ {

        /**
         * Constructs a new TeaHouseInfoREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseInfoREQ);

        /** TeaHouseInfoREQ ID. */
        public ID: (number|Long)[];

        /**
         * Creates a new TeaHouseInfoREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseInfoREQ instance
         */
        public static create(properties?: teahouse.ITeaHouseInfoREQ): teahouse.TeaHouseInfoREQ;

        /**
         * Encodes the specified TeaHouseInfoREQ message. Does not implicitly {@link teahouse.TeaHouseInfoREQ.verify|verify} messages.
         * @param message TeaHouseInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseInfoREQ message, length delimited. Does not implicitly {@link teahouse.TeaHouseInfoREQ.verify|verify} messages.
         * @param message TeaHouseInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseInfoREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseInfoREQ;

        /**
         * Decodes a TeaHouseInfoREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseInfoREQ;

        /**
         * Verifies a TeaHouseInfoREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeaHouseInfoACK. */
    interface ITeaHouseInfoACK {

        /** TeaHouseInfoACK Info */
        Info?: (teahouse.ITeaHouseInfo[]|null);
    }

    /** Represents a TeaHouseInfoACK. */
    class TeaHouseInfoACK implements ITeaHouseInfoACK {

        /**
         * Constructs a new TeaHouseInfoACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseInfoACK);

        /** TeaHouseInfoACK Info. */
        public Info: teahouse.ITeaHouseInfo[];

        /**
         * Creates a new TeaHouseInfoACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseInfoACK instance
         */
        public static create(properties?: teahouse.ITeaHouseInfoACK): teahouse.TeaHouseInfoACK;

        /**
         * Encodes the specified TeaHouseInfoACK message. Does not implicitly {@link teahouse.TeaHouseInfoACK.verify|verify} messages.
         * @param message TeaHouseInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseInfoACK message, length delimited. Does not implicitly {@link teahouse.TeaHouseInfoACK.verify|verify} messages.
         * @param message TeaHouseInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseInfoACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseInfoACK;

        /**
         * Decodes a TeaHouseInfoACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseInfoACK;

        /**
         * Verifies a TeaHouseInfoACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** TeaHouseAction enum. */
    enum TeaHouseAction {
        Dismiss = 0,
        Transfer = 1,
        Join = 2,
        Invite = 3,
        Freeze = 4,
        Remove = 5,
        AddPartner = 6,
        AddManager = 7,
        ApplyTransfer = 8,
        SetCharger = 9,
        Assign = 10,
        AddScore = 11,
        JoinSuccess = 12,
        Exit = 13,
        Free = 14,
        Recycle = 15,
        PartnerReq = 16,
        ActionRate = 17,
        RemoveAssign = 18,
        ChargeScore = 19
    }

    /** Properties of a TeaHouseActionREQ. */
    interface ITeaHouseActionREQ {

        /** TeaHouseActionREQ Action */
        Action?: (teahouse.TeaHouseAction|null);

        /** TeaHouseActionREQ UserID */
        UserID?: (number|Long|null);

        /** TeaHouseActionREQ HouseID */
        HouseID?: (number|Long|null);

        /** TeaHouseActionREQ Frozen */
        Frozen?: (boolean|null);

        /** TeaHouseActionREQ AssignID */
        AssignID?: (number|Long|null);

        /** TeaHouseActionREQ Score */
        Score?: (number|Long|null);

        /** TeaHouseActionREQ RecyleUserID */
        RecyleUserID?: ((number|Long)[]|null);

        /** TeaHouseActionREQ Rate */
        Rate?: (number|Long|null);

        /** TeaHouseActionREQ SetLimit */
        SetLimit?: (boolean|null);

        /** TeaHouseActionREQ RemoveFromPartner */
        RemoveFromPartner?: ((number|Long)[]|null);
    }

    /** Represents a TeaHouseActionREQ. */
    class TeaHouseActionREQ implements ITeaHouseActionREQ {

        /**
         * Constructs a new TeaHouseActionREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseActionREQ);

        /** TeaHouseActionREQ Action. */
        public Action: teahouse.TeaHouseAction;

        /** TeaHouseActionREQ UserID. */
        public UserID: (number|Long);

        /** TeaHouseActionREQ HouseID. */
        public HouseID: (number|Long);

        /** TeaHouseActionREQ Frozen. */
        public Frozen: boolean;

        /** TeaHouseActionREQ AssignID. */
        public AssignID: (number|Long);

        /** TeaHouseActionREQ Score. */
        public Score: (number|Long);

        /** TeaHouseActionREQ RecyleUserID. */
        public RecyleUserID: (number|Long)[];

        /** TeaHouseActionREQ Rate. */
        public Rate: (number|Long);

        /** TeaHouseActionREQ SetLimit. */
        public SetLimit: boolean;

        /** TeaHouseActionREQ RemoveFromPartner. */
        public RemoveFromPartner: (number|Long)[];

        /**
         * Creates a new TeaHouseActionREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseActionREQ instance
         */
        public static create(properties?: teahouse.ITeaHouseActionREQ): teahouse.TeaHouseActionREQ;

        /**
         * Encodes the specified TeaHouseActionREQ message. Does not implicitly {@link teahouse.TeaHouseActionREQ.verify|verify} messages.
         * @param message TeaHouseActionREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseActionREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseActionREQ message, length delimited. Does not implicitly {@link teahouse.TeaHouseActionREQ.verify|verify} messages.
         * @param message TeaHouseActionREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseActionREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseActionREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseActionREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseActionREQ;

        /**
         * Decodes a TeaHouseActionREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseActionREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseActionREQ;

        /**
         * Verifies a TeaHouseActionREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeaHouseActionACK. */
    interface ITeaHouseActionACK {

        /** TeaHouseActionACK Action */
        Action?: (teahouse.TeaHouseAction|null);

        /** TeaHouseActionACK Code */
        Code?: (teahouse.TeaHouseCode|null);

        /** TeaHouseActionACK HouseID */
        HouseID?: (number|Long|null);
    }

    /** Represents a TeaHouseActionACK. */
    class TeaHouseActionACK implements ITeaHouseActionACK {

        /**
         * Constructs a new TeaHouseActionACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseActionACK);

        /** TeaHouseActionACK Action. */
        public Action: teahouse.TeaHouseAction;

        /** TeaHouseActionACK Code. */
        public Code: teahouse.TeaHouseCode;

        /** TeaHouseActionACK HouseID. */
        public HouseID: (number|Long);

        /**
         * Creates a new TeaHouseActionACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseActionACK instance
         */
        public static create(properties?: teahouse.ITeaHouseActionACK): teahouse.TeaHouseActionACK;

        /**
         * Encodes the specified TeaHouseActionACK message. Does not implicitly {@link teahouse.TeaHouseActionACK.verify|verify} messages.
         * @param message TeaHouseActionACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseActionACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseActionACK message, length delimited. Does not implicitly {@link teahouse.TeaHouseActionACK.verify|verify} messages.
         * @param message TeaHouseActionACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseActionACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseActionACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseActionACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseActionACK;

        /**
         * Decodes a TeaHouseActionACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseActionACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseActionACK;

        /**
         * Verifies a TeaHouseActionACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeaHouseJoinedREQ. */
    interface ITeaHouseJoinedREQ {
    }

    /** Represents a TeaHouseJoinedREQ. */
    class TeaHouseJoinedREQ implements ITeaHouseJoinedREQ {

        /**
         * Constructs a new TeaHouseJoinedREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseJoinedREQ);

        /**
         * Creates a new TeaHouseJoinedREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseJoinedREQ instance
         */
        public static create(properties?: teahouse.ITeaHouseJoinedREQ): teahouse.TeaHouseJoinedREQ;

        /**
         * Encodes the specified TeaHouseJoinedREQ message. Does not implicitly {@link teahouse.TeaHouseJoinedREQ.verify|verify} messages.
         * @param message TeaHouseJoinedREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseJoinedREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseJoinedREQ message, length delimited. Does not implicitly {@link teahouse.TeaHouseJoinedREQ.verify|verify} messages.
         * @param message TeaHouseJoinedREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseJoinedREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseJoinedREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseJoinedREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseJoinedREQ;

        /**
         * Decodes a TeaHouseJoinedREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseJoinedREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseJoinedREQ;

        /**
         * Verifies a TeaHouseJoinedREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeeHouseJoinedACK. */
    interface ITeeHouseJoinedACK {

        /** TeeHouseJoinedACK ID */
        ID?: ((number|Long)[]|null);

        /** TeeHouseJoinedACK info */
        info?: (teahouse.ITeaHouseInfo[]|null);
    }

    /** Represents a TeeHouseJoinedACK. */
    class TeeHouseJoinedACK implements ITeeHouseJoinedACK {

        /**
         * Constructs a new TeeHouseJoinedACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeeHouseJoinedACK);

        /** TeeHouseJoinedACK ID. */
        public ID: (number|Long)[];

        /** TeeHouseJoinedACK info. */
        public info: teahouse.ITeaHouseInfo[];

        /**
         * Creates a new TeeHouseJoinedACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeeHouseJoinedACK instance
         */
        public static create(properties?: teahouse.ITeeHouseJoinedACK): teahouse.TeeHouseJoinedACK;

        /**
         * Encodes the specified TeeHouseJoinedACK message. Does not implicitly {@link teahouse.TeeHouseJoinedACK.verify|verify} messages.
         * @param message TeeHouseJoinedACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeeHouseJoinedACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeeHouseJoinedACK message, length delimited. Does not implicitly {@link teahouse.TeeHouseJoinedACK.verify|verify} messages.
         * @param message TeeHouseJoinedACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeeHouseJoinedACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeeHouseJoinedACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeeHouseJoinedACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeeHouseJoinedACK;

        /**
         * Decodes a TeeHouseJoinedACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeeHouseJoinedACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeeHouseJoinedACK;

        /**
         * Verifies a TeeHouseJoinedACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeaHouseHandleOrderREQ. */
    interface ITeaHouseHandleOrderREQ {

        /** TeaHouseHandleOrderREQ OrderID */
        OrderID?: (string|null);

        /** TeaHouseHandleOrderREQ Accept */
        Accept?: (boolean|null);
    }

    /** Represents a TeaHouseHandleOrderREQ. */
    class TeaHouseHandleOrderREQ implements ITeaHouseHandleOrderREQ {

        /**
         * Constructs a new TeaHouseHandleOrderREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseHandleOrderREQ);

        /** TeaHouseHandleOrderREQ OrderID. */
        public OrderID: string;

        /** TeaHouseHandleOrderREQ Accept. */
        public Accept: boolean;

        /**
         * Creates a new TeaHouseHandleOrderREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseHandleOrderREQ instance
         */
        public static create(properties?: teahouse.ITeaHouseHandleOrderREQ): teahouse.TeaHouseHandleOrderREQ;

        /**
         * Encodes the specified TeaHouseHandleOrderREQ message. Does not implicitly {@link teahouse.TeaHouseHandleOrderREQ.verify|verify} messages.
         * @param message TeaHouseHandleOrderREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseHandleOrderREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseHandleOrderREQ message, length delimited. Does not implicitly {@link teahouse.TeaHouseHandleOrderREQ.verify|verify} messages.
         * @param message TeaHouseHandleOrderREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseHandleOrderREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseHandleOrderREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseHandleOrderREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseHandleOrderREQ;

        /**
         * Decodes a TeaHouseHandleOrderREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseHandleOrderREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseHandleOrderREQ;

        /**
         * Verifies a TeaHouseHandleOrderREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeaHouseHandleOrderACK. */
    interface ITeaHouseHandleOrderACK {

        /** TeaHouseHandleOrderACK Code */
        Code?: (teahouse.TeaHouseCode|null);
    }

    /** Represents a TeaHouseHandleOrderACK. */
    class TeaHouseHandleOrderACK implements ITeaHouseHandleOrderACK {

        /**
         * Constructs a new TeaHouseHandleOrderACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseHandleOrderACK);

        /** TeaHouseHandleOrderACK Code. */
        public Code: teahouse.TeaHouseCode;

        /**
         * Creates a new TeaHouseHandleOrderACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseHandleOrderACK instance
         */
        public static create(properties?: teahouse.ITeaHouseHandleOrderACK): teahouse.TeaHouseHandleOrderACK;

        /**
         * Encodes the specified TeaHouseHandleOrderACK message. Does not implicitly {@link teahouse.TeaHouseHandleOrderACK.verify|verify} messages.
         * @param message TeaHouseHandleOrderACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseHandleOrderACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseHandleOrderACK message, length delimited. Does not implicitly {@link teahouse.TeaHouseHandleOrderACK.verify|verify} messages.
         * @param message TeaHouseHandleOrderACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseHandleOrderACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseHandleOrderACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseHandleOrderACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseHandleOrderACK;

        /**
         * Decodes a TeaHouseHandleOrderACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseHandleOrderACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseHandleOrderACK;

        /**
         * Verifies a TeaHouseHandleOrderACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeaHouseChangeInfoREQ. */
    interface ITeaHouseChangeInfoREQ {

        /** TeaHouseChangeInfoREQ HouseID */
        HouseID?: (number|Long|null);

        /** TeaHouseChangeInfoREQ Config */
        Config?: (teahouse.ITeaHouseConfig|null);

        /** TeaHouseChangeInfoREQ Ruler */
        Ruler?: (string|null);
    }

    /** Represents a TeaHouseChangeInfoREQ. */
    class TeaHouseChangeInfoREQ implements ITeaHouseChangeInfoREQ {

        /**
         * Constructs a new TeaHouseChangeInfoREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseChangeInfoREQ);

        /** TeaHouseChangeInfoREQ HouseID. */
        public HouseID: (number|Long);

        /** TeaHouseChangeInfoREQ Config. */
        public Config?: (teahouse.ITeaHouseConfig|null);

        /** TeaHouseChangeInfoREQ Ruler. */
        public Ruler: string;

        /**
         * Creates a new TeaHouseChangeInfoREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseChangeInfoREQ instance
         */
        public static create(properties?: teahouse.ITeaHouseChangeInfoREQ): teahouse.TeaHouseChangeInfoREQ;

        /**
         * Encodes the specified TeaHouseChangeInfoREQ message. Does not implicitly {@link teahouse.TeaHouseChangeInfoREQ.verify|verify} messages.
         * @param message TeaHouseChangeInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseChangeInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseChangeInfoREQ message, length delimited. Does not implicitly {@link teahouse.TeaHouseChangeInfoREQ.verify|verify} messages.
         * @param message TeaHouseChangeInfoREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseChangeInfoREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseChangeInfoREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseChangeInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseChangeInfoREQ;

        /**
         * Decodes a TeaHouseChangeInfoREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseChangeInfoREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseChangeInfoREQ;

        /**
         * Verifies a TeaHouseChangeInfoREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TeaHouseChangeInfoACK. */
    interface ITeaHouseChangeInfoACK {

        /** TeaHouseChangeInfoACK Code */
        Code?: (teahouse.TeaHouseCode|null);

        /** TeaHouseChangeInfoACK HouseID */
        HouseID?: (number|Long|null);

        /** TeaHouseChangeInfoACK Config */
        Config?: (teahouse.ITeaHouseConfig|null);

        /** TeaHouseChangeInfoACK Ruler */
        Ruler?: (string|null);
    }

    /** Represents a TeaHouseChangeInfoACK. */
    class TeaHouseChangeInfoACK implements ITeaHouseChangeInfoACK {

        /**
         * Constructs a new TeaHouseChangeInfoACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITeaHouseChangeInfoACK);

        /** TeaHouseChangeInfoACK Code. */
        public Code: teahouse.TeaHouseCode;

        /** TeaHouseChangeInfoACK HouseID. */
        public HouseID: (number|Long);

        /** TeaHouseChangeInfoACK Config. */
        public Config?: (teahouse.ITeaHouseConfig|null);

        /** TeaHouseChangeInfoACK Ruler. */
        public Ruler: string;

        /**
         * Creates a new TeaHouseChangeInfoACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TeaHouseChangeInfoACK instance
         */
        public static create(properties?: teahouse.ITeaHouseChangeInfoACK): teahouse.TeaHouseChangeInfoACK;

        /**
         * Encodes the specified TeaHouseChangeInfoACK message. Does not implicitly {@link teahouse.TeaHouseChangeInfoACK.verify|verify} messages.
         * @param message TeaHouseChangeInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITeaHouseChangeInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TeaHouseChangeInfoACK message, length delimited. Does not implicitly {@link teahouse.TeaHouseChangeInfoACK.verify|verify} messages.
         * @param message TeaHouseChangeInfoACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITeaHouseChangeInfoACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TeaHouseChangeInfoACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TeaHouseChangeInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TeaHouseChangeInfoACK;

        /**
         * Decodes a TeaHouseChangeInfoACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TeaHouseChangeInfoACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TeaHouseChangeInfoACK;

        /**
         * Verifies a TeaHouseChangeInfoACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** RankType enum. */
    enum RankType {
        WinTotal = 0,
        LoseTotal = 1,
        WinWeek = 2,
        LoseWeek = 3,
        WinDay = 4,
        LoseDay = 5,
        PartnerHandDay = 7,
        PartnerDrawDay = 8,
        PartnerHand = 9,
        PartnerDraw = 10,
        UserDraw = 11,
        PartnerUserScoreDay = 12,
        PartnerUserScoreTotal = 13
    }

    /** Properties of a RankREQ. */
    interface IRankREQ {

        /** RankREQ Type */
        Type?: (teahouse.RankType|null);

        /** RankREQ HouseID */
        HouseID?: (number|Long|null);

        /** RankREQ Start */
        Start?: (number|null);

        /** RankREQ PageCount */
        PageCount?: (number|null);

        /** RankREQ PartnerID */
        PartnerID?: (number|Long|null);

        /** RankREQ UserID */
        UserID?: (number|Long|null);
    }

    /** Represents a RankREQ. */
    class RankREQ implements IRankREQ {

        /**
         * Constructs a new RankREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IRankREQ);

        /** RankREQ Type. */
        public Type: teahouse.RankType;

        /** RankREQ HouseID. */
        public HouseID: (number|Long);

        /** RankREQ Start. */
        public Start: number;

        /** RankREQ PageCount. */
        public PageCount: number;

        /** RankREQ PartnerID. */
        public PartnerID: (number|Long);

        /** RankREQ UserID. */
        public UserID: (number|Long);

        /**
         * Creates a new RankREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RankREQ instance
         */
        public static create(properties?: teahouse.IRankREQ): teahouse.RankREQ;

        /**
         * Encodes the specified RankREQ message. Does not implicitly {@link teahouse.RankREQ.verify|verify} messages.
         * @param message RankREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IRankREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RankREQ message, length delimited. Does not implicitly {@link teahouse.RankREQ.verify|verify} messages.
         * @param message RankREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IRankREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RankREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RankREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.RankREQ;

        /**
         * Decodes a RankREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RankREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.RankREQ;

        /**
         * Verifies a RankREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RankACK. */
    interface IRankACK {

        /** RankACK Type */
        Type?: (teahouse.RankType|null);

        /** RankACK HouseID */
        HouseID?: (number|Long|null);

        /** RankACK PartnerID */
        PartnerID?: (number|Long|null);

        /** RankACK UserId */
        UserId?: (number|Long|null);

        /** RankACK Content */
        Content?: (structure.IRank|null);
    }

    /** Represents a RankACK. */
    class RankACK implements IRankACK {

        /**
         * Constructs a new RankACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IRankACK);

        /** RankACK Type. */
        public Type: teahouse.RankType;

        /** RankACK HouseID. */
        public HouseID: (number|Long);

        /** RankACK PartnerID. */
        public PartnerID: (number|Long);

        /** RankACK UserId. */
        public UserId: (number|Long);

        /** RankACK Content. */
        public Content?: (structure.IRank|null);

        /**
         * Creates a new RankACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RankACK instance
         */
        public static create(properties?: teahouse.IRankACK): teahouse.RankACK;

        /**
         * Encodes the specified RankACK message. Does not implicitly {@link teahouse.RankACK.verify|verify} messages.
         * @param message RankACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IRankACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RankACK message, length delimited. Does not implicitly {@link teahouse.RankACK.verify|verify} messages.
         * @param message RankACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IRankACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RankACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RankACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.RankACK;

        /**
         * Decodes a RankACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RankACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.RankACK;

        /**
         * Verifies a RankACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RecordREQ. */
    interface IRecordREQ {

        /** RecordREQ HouseID */
        HouseID?: (number|Long|null);

        /** RecordREQ Start */
        Start?: (number|null);

        /** RecordREQ PageCount */
        PageCount?: (number|null);
    }

    /** Represents a RecordREQ. */
    class RecordREQ implements IRecordREQ {

        /**
         * Constructs a new RecordREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IRecordREQ);

        /** RecordREQ HouseID. */
        public HouseID: (number|Long);

        /** RecordREQ Start. */
        public Start: number;

        /** RecordREQ PageCount. */
        public PageCount: number;

        /**
         * Creates a new RecordREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RecordREQ instance
         */
        public static create(properties?: teahouse.IRecordREQ): teahouse.RecordREQ;

        /**
         * Encodes the specified RecordREQ message. Does not implicitly {@link teahouse.RecordREQ.verify|verify} messages.
         * @param message RecordREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IRecordREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RecordREQ message, length delimited. Does not implicitly {@link teahouse.RecordREQ.verify|verify} messages.
         * @param message RecordREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IRecordREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RecordREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RecordREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.RecordREQ;

        /**
         * Decodes a RecordREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RecordREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.RecordREQ;

        /**
         * Verifies a RecordREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RecordACK. */
    interface IRecordACK {

        /** RecordACK HouseID */
        HouseID?: (number|Long|null);

        /** RecordACK Start */
        Start?: (number|null);

        /** RecordACK PageCount */
        PageCount?: (number|null);

        /** RecordACK TotalCount */
        TotalCount?: (number|null);

        /** RecordACK Rec */
        Rec?: (record.IRecord|null);
    }

    /** Represents a RecordACK. */
    class RecordACK implements IRecordACK {

        /**
         * Constructs a new RecordACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IRecordACK);

        /** RecordACK HouseID. */
        public HouseID: (number|Long);

        /** RecordACK Start. */
        public Start: number;

        /** RecordACK PageCount. */
        public PageCount: number;

        /** RecordACK TotalCount. */
        public TotalCount: number;

        /** RecordACK Rec. */
        public Rec?: (record.IRecord|null);

        /**
         * Creates a new RecordACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RecordACK instance
         */
        public static create(properties?: teahouse.IRecordACK): teahouse.RecordACK;

        /**
         * Encodes the specified RecordACK message. Does not implicitly {@link teahouse.RecordACK.verify|verify} messages.
         * @param message RecordACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IRecordACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RecordACK message, length delimited. Does not implicitly {@link teahouse.RecordACK.verify|verify} messages.
         * @param message RecordACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IRecordACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RecordACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RecordACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.RecordACK;

        /**
         * Decodes a RecordACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RecordACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.RecordACK;

        /**
         * Verifies a RecordACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a UserRecordREQ. */
    interface IUserRecordREQ {

        /** UserRecordREQ Start */
        Start?: (number|null);

        /** UserRecordREQ PageCount */
        PageCount?: (number|null);

        /** UserRecordREQ MatchType */
        MatchType?: (structure.TableConfig.MatchTypeConfig|null);
    }

    /** Represents a UserRecordREQ. */
    class UserRecordREQ implements IUserRecordREQ {

        /**
         * Constructs a new UserRecordREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IUserRecordREQ);

        /** UserRecordREQ Start. */
        public Start: number;

        /** UserRecordREQ PageCount. */
        public PageCount: number;

        /** UserRecordREQ MatchType. */
        public MatchType: structure.TableConfig.MatchTypeConfig;

        /**
         * Creates a new UserRecordREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UserRecordREQ instance
         */
        public static create(properties?: teahouse.IUserRecordREQ): teahouse.UserRecordREQ;

        /**
         * Encodes the specified UserRecordREQ message. Does not implicitly {@link teahouse.UserRecordREQ.verify|verify} messages.
         * @param message UserRecordREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IUserRecordREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified UserRecordREQ message, length delimited. Does not implicitly {@link teahouse.UserRecordREQ.verify|verify} messages.
         * @param message UserRecordREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IUserRecordREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a UserRecordREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UserRecordREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.UserRecordREQ;

        /**
         * Decodes a UserRecordREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UserRecordREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.UserRecordREQ;

        /**
         * Verifies a UserRecordREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a UserRecordACK. */
    interface IUserRecordACK {

        /** UserRecordACK Start */
        Start?: (number|null);

        /** UserRecordACK PageCount */
        PageCount?: (number|null);

        /** UserRecordACK TotalCount */
        TotalCount?: (number|null);

        /** UserRecordACK Rec */
        Rec?: (record.IRecord|null);

        /** UserRecordACK MatchType */
        MatchType?: (structure.TableConfig.MatchTypeConfig|null);
    }

    /** Represents a UserRecordACK. */
    class UserRecordACK implements IUserRecordACK {

        /**
         * Constructs a new UserRecordACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IUserRecordACK);

        /** UserRecordACK Start. */
        public Start: number;

        /** UserRecordACK PageCount. */
        public PageCount: number;

        /** UserRecordACK TotalCount. */
        public TotalCount: number;

        /** UserRecordACK Rec. */
        public Rec?: (record.IRecord|null);

        /** UserRecordACK MatchType. */
        public MatchType: structure.TableConfig.MatchTypeConfig;

        /**
         * Creates a new UserRecordACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UserRecordACK instance
         */
        public static create(properties?: teahouse.IUserRecordACK): teahouse.UserRecordACK;

        /**
         * Encodes the specified UserRecordACK message. Does not implicitly {@link teahouse.UserRecordACK.verify|verify} messages.
         * @param message UserRecordACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IUserRecordACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified UserRecordACK message, length delimited. Does not implicitly {@link teahouse.UserRecordACK.verify|verify} messages.
         * @param message UserRecordACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IUserRecordACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a UserRecordACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UserRecordACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.UserRecordACK;

        /**
         * Decodes a UserRecordACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UserRecordACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.UserRecordACK;

        /**
         * Verifies a UserRecordACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ScoreLogREQ. */
    interface IScoreLogREQ {

        /** ScoreLogREQ HouseID */
        HouseID?: (number|Long|null);

        /** ScoreLogREQ Start */
        Start?: (number|Long|null);

        /** ScoreLogREQ PageCount */
        PageCount?: (number|Long|null);
    }

    /** Represents a ScoreLogREQ. */
    class ScoreLogREQ implements IScoreLogREQ {

        /**
         * Constructs a new ScoreLogREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IScoreLogREQ);

        /** ScoreLogREQ HouseID. */
        public HouseID: (number|Long);

        /** ScoreLogREQ Start. */
        public Start: (number|Long);

        /** ScoreLogREQ PageCount. */
        public PageCount: (number|Long);

        /**
         * Creates a new ScoreLogREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ScoreLogREQ instance
         */
        public static create(properties?: teahouse.IScoreLogREQ): teahouse.ScoreLogREQ;

        /**
         * Encodes the specified ScoreLogREQ message. Does not implicitly {@link teahouse.ScoreLogREQ.verify|verify} messages.
         * @param message ScoreLogREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IScoreLogREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ScoreLogREQ message, length delimited. Does not implicitly {@link teahouse.ScoreLogREQ.verify|verify} messages.
         * @param message ScoreLogREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IScoreLogREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ScoreLogREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ScoreLogREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.ScoreLogREQ;

        /**
         * Decodes a ScoreLogREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ScoreLogREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.ScoreLogREQ;

        /**
         * Verifies a ScoreLogREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ScoreLogACK. */
    interface IScoreLogACK {

        /** ScoreLogACK Start */
        Start?: (number|Long|null);

        /** ScoreLogACK PageCount */
        PageCount?: (number|Long|null);

        /** ScoreLogACK Total */
        Total?: (number|Long|null);

        /** ScoreLogACK ChangeLog */
        ChangeLog?: (niuniu.IScoreChangeNTF[]|null);
    }

    /** Represents a ScoreLogACK. */
    class ScoreLogACK implements IScoreLogACK {

        /**
         * Constructs a new ScoreLogACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IScoreLogACK);

        /** ScoreLogACK Start. */
        public Start: (number|Long);

        /** ScoreLogACK PageCount. */
        public PageCount: (number|Long);

        /** ScoreLogACK Total. */
        public Total: (number|Long);

        /** ScoreLogACK ChangeLog. */
        public ChangeLog: niuniu.IScoreChangeNTF[];

        /**
         * Creates a new ScoreLogACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ScoreLogACK instance
         */
        public static create(properties?: teahouse.IScoreLogACK): teahouse.ScoreLogACK;

        /**
         * Encodes the specified ScoreLogACK message. Does not implicitly {@link teahouse.ScoreLogACK.verify|verify} messages.
         * @param message ScoreLogACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IScoreLogACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ScoreLogACK message, length delimited. Does not implicitly {@link teahouse.ScoreLogACK.verify|verify} messages.
         * @param message ScoreLogACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IScoreLogACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ScoreLogACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ScoreLogACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.ScoreLogACK;

        /**
         * Decodes a ScoreLogACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ScoreLogACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.ScoreLogACK;

        /**
         * Verifies a ScoreLogACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** RecordType enum. */
    enum RecordType {
        Game = 0,
        Score = 1
    }

    /** Properties of a UserScoreLogREQ. */
    interface IUserScoreLogREQ {

        /** UserScoreLogREQ HouseID */
        HouseID?: (number|Long|null);

        /** UserScoreLogREQ UserID */
        UserID?: (number|Long|null);

        /** UserScoreLogREQ Start */
        Start?: (number|Long|null);

        /** UserScoreLogREQ PageCount */
        PageCount?: (number|Long|null);

        /** UserScoreLogREQ Type */
        Type?: (teahouse.RecordType|null);
    }

    /** Represents a UserScoreLogREQ. */
    class UserScoreLogREQ implements IUserScoreLogREQ {

        /**
         * Constructs a new UserScoreLogREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IUserScoreLogREQ);

        /** UserScoreLogREQ HouseID. */
        public HouseID: (number|Long);

        /** UserScoreLogREQ UserID. */
        public UserID: (number|Long);

        /** UserScoreLogREQ Start. */
        public Start: (number|Long);

        /** UserScoreLogREQ PageCount. */
        public PageCount: (number|Long);

        /** UserScoreLogREQ Type. */
        public Type: teahouse.RecordType;

        /**
         * Creates a new UserScoreLogREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UserScoreLogREQ instance
         */
        public static create(properties?: teahouse.IUserScoreLogREQ): teahouse.UserScoreLogREQ;

        /**
         * Encodes the specified UserScoreLogREQ message. Does not implicitly {@link teahouse.UserScoreLogREQ.verify|verify} messages.
         * @param message UserScoreLogREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IUserScoreLogREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified UserScoreLogREQ message, length delimited. Does not implicitly {@link teahouse.UserScoreLogREQ.verify|verify} messages.
         * @param message UserScoreLogREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IUserScoreLogREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a UserScoreLogREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UserScoreLogREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.UserScoreLogREQ;

        /**
         * Decodes a UserScoreLogREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UserScoreLogREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.UserScoreLogREQ;

        /**
         * Verifies a UserScoreLogREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a UserScoreLogACK. */
    interface IUserScoreLogACK {

        /** UserScoreLogACK Start */
        Start?: (number|Long|null);

        /** UserScoreLogACK PageCount */
        PageCount?: (number|Long|null);

        /** UserScoreLogACK Total */
        Total?: (number|Long|null);

        /** UserScoreLogACK ChangeLog */
        ChangeLog?: (niuniu.IScoreChangeNTF[]|null);

        /** UserScoreLogACK Type */
        Type?: (teahouse.RecordType|null);
    }

    /** Represents a UserScoreLogACK. */
    class UserScoreLogACK implements IUserScoreLogACK {

        /**
         * Constructs a new UserScoreLogACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IUserScoreLogACK);

        /** UserScoreLogACK Start. */
        public Start: (number|Long);

        /** UserScoreLogACK PageCount. */
        public PageCount: (number|Long);

        /** UserScoreLogACK Total. */
        public Total: (number|Long);

        /** UserScoreLogACK ChangeLog. */
        public ChangeLog: niuniu.IScoreChangeNTF[];

        /** UserScoreLogACK Type. */
        public Type: teahouse.RecordType;

        /**
         * Creates a new UserScoreLogACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UserScoreLogACK instance
         */
        public static create(properties?: teahouse.IUserScoreLogACK): teahouse.UserScoreLogACK;

        /**
         * Encodes the specified UserScoreLogACK message. Does not implicitly {@link teahouse.UserScoreLogACK.verify|verify} messages.
         * @param message UserScoreLogACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IUserScoreLogACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified UserScoreLogACK message, length delimited. Does not implicitly {@link teahouse.UserScoreLogACK.verify|verify} messages.
         * @param message UserScoreLogACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IUserScoreLogACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a UserScoreLogACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UserScoreLogACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.UserScoreLogACK;

        /**
         * Decodes a UserScoreLogACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UserScoreLogACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.UserScoreLogACK;

        /**
         * Verifies a UserScoreLogACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TableStatisticsREQ. */
    interface ITableStatisticsREQ {

        /** TableStatisticsREQ HouseID */
        HouseID?: (number|Long|null);
    }

    /** Represents a TableStatisticsREQ. */
    class TableStatisticsREQ implements ITableStatisticsREQ {

        /**
         * Constructs a new TableStatisticsREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITableStatisticsREQ);

        /** TableStatisticsREQ HouseID. */
        public HouseID: (number|Long);

        /**
         * Creates a new TableStatisticsREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TableStatisticsREQ instance
         */
        public static create(properties?: teahouse.ITableStatisticsREQ): teahouse.TableStatisticsREQ;

        /**
         * Encodes the specified TableStatisticsREQ message. Does not implicitly {@link teahouse.TableStatisticsREQ.verify|verify} messages.
         * @param message TableStatisticsREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITableStatisticsREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TableStatisticsREQ message, length delimited. Does not implicitly {@link teahouse.TableStatisticsREQ.verify|verify} messages.
         * @param message TableStatisticsREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITableStatisticsREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TableStatisticsREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TableStatisticsREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TableStatisticsREQ;

        /**
         * Decodes a TableStatisticsREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TableStatisticsREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TableStatisticsREQ;

        /**
         * Verifies a TableStatisticsREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TableStatisticsACK. */
    interface ITableStatisticsACK {

        /** TableStatisticsACK HouseID */
        HouseID?: (number|Long|null);

        /** TableStatisticsACK YesertdayTable */
        YesertdayTable?: (number|Long|null);

        /** TableStatisticsACK YesertdayDiamond */
        YesertdayDiamond?: (number|Long|null);

        /** TableStatisticsACK TodayTable */
        TodayTable?: (number|Long|null);

        /** TableStatisticsACK TodayDiamond */
        TodayDiamond?: (number|Long|null);

        /** TableStatisticsACK TotalTable */
        TotalTable?: (number|Long|null);

        /** TableStatisticsACK TotalDiamond */
        TotalDiamond?: (number|Long|null);
    }

    /** Represents a TableStatisticsACK. */
    class TableStatisticsACK implements ITableStatisticsACK {

        /**
         * Constructs a new TableStatisticsACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.ITableStatisticsACK);

        /** TableStatisticsACK HouseID. */
        public HouseID: (number|Long);

        /** TableStatisticsACK YesertdayTable. */
        public YesertdayTable: (number|Long);

        /** TableStatisticsACK YesertdayDiamond. */
        public YesertdayDiamond: (number|Long);

        /** TableStatisticsACK TodayTable. */
        public TodayTable: (number|Long);

        /** TableStatisticsACK TodayDiamond. */
        public TodayDiamond: (number|Long);

        /** TableStatisticsACK TotalTable. */
        public TotalTable: (number|Long);

        /** TableStatisticsACK TotalDiamond. */
        public TotalDiamond: (number|Long);

        /**
         * Creates a new TableStatisticsACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TableStatisticsACK instance
         */
        public static create(properties?: teahouse.ITableStatisticsACK): teahouse.TableStatisticsACK;

        /**
         * Encodes the specified TableStatisticsACK message. Does not implicitly {@link teahouse.TableStatisticsACK.verify|verify} messages.
         * @param message TableStatisticsACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.ITableStatisticsACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TableStatisticsACK message, length delimited. Does not implicitly {@link teahouse.TableStatisticsACK.verify|verify} messages.
         * @param message TableStatisticsACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.ITableStatisticsACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TableStatisticsACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TableStatisticsACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.TableStatisticsACK;

        /**
         * Decodes a TableStatisticsACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TableStatisticsACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.TableStatisticsACK;

        /**
         * Verifies a TableStatisticsACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DrawREQ. */
    interface IDrawREQ {

        /** DrawREQ HouseID */
        HouseID?: (number|Long|null);
    }

    /** Represents a DrawREQ. */
    class DrawREQ implements IDrawREQ {

        /**
         * Constructs a new DrawREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IDrawREQ);

        /** DrawREQ HouseID. */
        public HouseID: (number|Long);

        /**
         * Creates a new DrawREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DrawREQ instance
         */
        public static create(properties?: teahouse.IDrawREQ): teahouse.DrawREQ;

        /**
         * Encodes the specified DrawREQ message. Does not implicitly {@link teahouse.DrawREQ.verify|verify} messages.
         * @param message DrawREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IDrawREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DrawREQ message, length delimited. Does not implicitly {@link teahouse.DrawREQ.verify|verify} messages.
         * @param message DrawREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IDrawREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DrawREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DrawREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.DrawREQ;

        /**
         * Decodes a DrawREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DrawREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.DrawREQ;

        /**
         * Verifies a DrawREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DrawACK. */
    interface IDrawACK {

        /** DrawACK HouseID */
        HouseID?: (number|Long|null);

        /** DrawACK Total */
        Total?: (number|Long|null);

        /** DrawACK Today */
        Today?: (number|Long|null);

        /** DrawACK Yesterday */
        Yesterday?: (number|Long|null);
    }

    /** Represents a DrawACK. */
    class DrawACK implements IDrawACK {

        /**
         * Constructs a new DrawACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IDrawACK);

        /** DrawACK HouseID. */
        public HouseID: (number|Long);

        /** DrawACK Total. */
        public Total: (number|Long);

        /** DrawACK Today. */
        public Today: (number|Long);

        /** DrawACK Yesterday. */
        public Yesterday: (number|Long);

        /**
         * Creates a new DrawACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DrawACK instance
         */
        public static create(properties?: teahouse.IDrawACK): teahouse.DrawACK;

        /**
         * Encodes the specified DrawACK message. Does not implicitly {@link teahouse.DrawACK.verify|verify} messages.
         * @param message DrawACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IDrawACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DrawACK message, length delimited. Does not implicitly {@link teahouse.DrawACK.verify|verify} messages.
         * @param message DrawACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IDrawACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DrawACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DrawACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.DrawACK;

        /**
         * Decodes a DrawACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DrawACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.DrawACK;

        /**
         * Verifies a DrawACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ChargeOrderREQ. */
    interface IChargeOrderREQ {

        /** ChargeOrderREQ Money */
        Money?: (number|null);

        /** ChargeOrderREQ Score */
        Score?: (number|Long|null);

        /** ChargeOrderREQ HouseID */
        HouseID?: (number|Long|null);

        /** ChargeOrderREQ Type */
        Type?: (string|null);
    }

    /** Represents a ChargeOrderREQ. */
    class ChargeOrderREQ implements IChargeOrderREQ {

        /**
         * Constructs a new ChargeOrderREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IChargeOrderREQ);

        /** ChargeOrderREQ Money. */
        public Money: number;

        /** ChargeOrderREQ Score. */
        public Score: (number|Long);

        /** ChargeOrderREQ HouseID. */
        public HouseID: (number|Long);

        /** ChargeOrderREQ Type. */
        public Type: string;

        /**
         * Creates a new ChargeOrderREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ChargeOrderREQ instance
         */
        public static create(properties?: teahouse.IChargeOrderREQ): teahouse.ChargeOrderREQ;

        /**
         * Encodes the specified ChargeOrderREQ message. Does not implicitly {@link teahouse.ChargeOrderREQ.verify|verify} messages.
         * @param message ChargeOrderREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IChargeOrderREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ChargeOrderREQ message, length delimited. Does not implicitly {@link teahouse.ChargeOrderREQ.verify|verify} messages.
         * @param message ChargeOrderREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IChargeOrderREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ChargeOrderREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ChargeOrderREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.ChargeOrderREQ;

        /**
         * Decodes a ChargeOrderREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ChargeOrderREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.ChargeOrderREQ;

        /**
         * Verifies a ChargeOrderREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ChargeOrderACK. */
    interface IChargeOrderACK {

        /** ChargeOrderACK Code */
        Code?: (teahouse.TeaHouseCode|null);

        /** ChargeOrderACK OrderHash */
        OrderHash?: (string|null);

        /** ChargeOrderACK VerifyCode */
        VerifyCode?: (string|null);

        /** ChargeOrderACK Type */
        Type?: (string|null);

        /** ChargeOrderACK Url */
        Url?: (string|null);
    }

    /** Represents a ChargeOrderACK. */
    class ChargeOrderACK implements IChargeOrderACK {

        /**
         * Constructs a new ChargeOrderACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IChargeOrderACK);

        /** ChargeOrderACK Code. */
        public Code: teahouse.TeaHouseCode;

        /** ChargeOrderACK OrderHash. */
        public OrderHash: string;

        /** ChargeOrderACK VerifyCode. */
        public VerifyCode: string;

        /** ChargeOrderACK Type. */
        public Type: string;

        /** ChargeOrderACK Url. */
        public Url: string;

        /**
         * Creates a new ChargeOrderACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ChargeOrderACK instance
         */
        public static create(properties?: teahouse.IChargeOrderACK): teahouse.ChargeOrderACK;

        /**
         * Encodes the specified ChargeOrderACK message. Does not implicitly {@link teahouse.ChargeOrderACK.verify|verify} messages.
         * @param message ChargeOrderACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IChargeOrderACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ChargeOrderACK message, length delimited. Does not implicitly {@link teahouse.ChargeOrderACK.verify|verify} messages.
         * @param message ChargeOrderACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IChargeOrderACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ChargeOrderACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ChargeOrderACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.ChargeOrderACK;

        /**
         * Decodes a ChargeOrderACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ChargeOrderACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.ChargeOrderACK;

        /**
         * Verifies a ChargeOrderACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ChargeOrderNTF. */
    interface IChargeOrderNTF {

        /** ChargeOrderNTF Code */
        Code?: (teahouse.TeaHouseCode|null);

        /** ChargeOrderNTF OrderHash */
        OrderHash?: (string|null);

        /** ChargeOrderNTF Money */
        Money?: (number|Long|null);

        /** ChargeOrderNTF HouseID */
        HouseID?: (number|Long|null);

        /** ChargeOrderNTF Score */
        Score?: (number|Long|null);

        /** ChargeOrderNTF UserID */
        UserID?: (number|Long|null);
    }

    /** Represents a ChargeOrderNTF. */
    class ChargeOrderNTF implements IChargeOrderNTF {

        /**
         * Constructs a new ChargeOrderNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: teahouse.IChargeOrderNTF);

        /** ChargeOrderNTF Code. */
        public Code: teahouse.TeaHouseCode;

        /** ChargeOrderNTF OrderHash. */
        public OrderHash: string;

        /** ChargeOrderNTF Money. */
        public Money: (number|Long);

        /** ChargeOrderNTF HouseID. */
        public HouseID: (number|Long);

        /** ChargeOrderNTF Score. */
        public Score: (number|Long);

        /** ChargeOrderNTF UserID. */
        public UserID: (number|Long);

        /**
         * Creates a new ChargeOrderNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ChargeOrderNTF instance
         */
        public static create(properties?: teahouse.IChargeOrderNTF): teahouse.ChargeOrderNTF;

        /**
         * Encodes the specified ChargeOrderNTF message. Does not implicitly {@link teahouse.ChargeOrderNTF.verify|verify} messages.
         * @param message ChargeOrderNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: teahouse.IChargeOrderNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ChargeOrderNTF message, length delimited. Does not implicitly {@link teahouse.ChargeOrderNTF.verify|verify} messages.
         * @param message ChargeOrderNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: teahouse.IChargeOrderNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ChargeOrderNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ChargeOrderNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): teahouse.ChargeOrderNTF;

        /**
         * Decodes a ChargeOrderNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ChargeOrderNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): teahouse.ChargeOrderNTF;

        /**
         * Verifies a ChargeOrderNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }
}

/** Namespace web. */
declare namespace web {

    /** MessageID enum. */
    enum MessageID {
        ID = 0,
        ID_ScoreChangeNTF = 30101,
        ID_DiamondChangeNTF = 30102
    }

    /** Properties of a ScoreChangeNTF. */
    interface IScoreChangeNTF {

        /** ScoreChangeNTF UserID */
        UserID?: (number|Long|null);

        /** ScoreChangeNTF DeltaScore */
        DeltaScore?: (number|Long|null);

        /** ScoreChangeNTF Score */
        Score?: (number|Long|null);

        /** ScoreChangeNTF HouseID */
        HouseID?: (number|Long|null);

        /** ScoreChangeNTF TableID */
        TableID?: (number|null);

        /** ScoreChangeNTF Reason */
        Reason?: (string|null);

        /** ScoreChangeNTF Date */
        Date?: (number|Long|null);

        /** ScoreChangeNTF Tag */
        Tag?: (number|null);
    }

    /** Represents a ScoreChangeNTF. */
    class ScoreChangeNTF implements IScoreChangeNTF {

        /**
         * Constructs a new ScoreChangeNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: web.IScoreChangeNTF);

        /** ScoreChangeNTF UserID. */
        public UserID: (number|Long);

        /** ScoreChangeNTF DeltaScore. */
        public DeltaScore: (number|Long);

        /** ScoreChangeNTF Score. */
        public Score: (number|Long);

        /** ScoreChangeNTF HouseID. */
        public HouseID: (number|Long);

        /** ScoreChangeNTF TableID. */
        public TableID: number;

        /** ScoreChangeNTF Reason. */
        public Reason: string;

        /** ScoreChangeNTF Date. */
        public Date: (number|Long);

        /** ScoreChangeNTF Tag. */
        public Tag: number;

        /**
         * Creates a new ScoreChangeNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ScoreChangeNTF instance
         */
        public static create(properties?: web.IScoreChangeNTF): web.ScoreChangeNTF;

        /**
         * Encodes the specified ScoreChangeNTF message. Does not implicitly {@link web.ScoreChangeNTF.verify|verify} messages.
         * @param message ScoreChangeNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: web.IScoreChangeNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ScoreChangeNTF message, length delimited. Does not implicitly {@link web.ScoreChangeNTF.verify|verify} messages.
         * @param message ScoreChangeNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: web.IScoreChangeNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ScoreChangeNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ScoreChangeNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): web.ScoreChangeNTF;

        /**
         * Decodes a ScoreChangeNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ScoreChangeNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): web.ScoreChangeNTF;

        /**
         * Verifies a ScoreChangeNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DiamondChangeNTF. */
    interface IDiamondChangeNTF {

        /** DiamondChangeNTF Diamond */
        Diamond?: (number|Long|null);

        /** DiamondChangeNTF Delta */
        Delta?: (number|Long|null);

        /** DiamondChangeNTF Reason */
        Reason?: (string|null);

        /** DiamondChangeNTF UserID */
        UserID?: (number|Long|null);

        /** DiamondChangeNTF Date */
        Date?: (number|Long|null);
    }

    /** Represents a DiamondChangeNTF. */
    class DiamondChangeNTF implements IDiamondChangeNTF {

        /**
         * Constructs a new DiamondChangeNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: web.IDiamondChangeNTF);

        /** DiamondChangeNTF Diamond. */
        public Diamond: (number|Long);

        /** DiamondChangeNTF Delta. */
        public Delta: (number|Long);

        /** DiamondChangeNTF Reason. */
        public Reason: string;

        /** DiamondChangeNTF UserID. */
        public UserID: (number|Long);

        /** DiamondChangeNTF Date. */
        public Date: (number|Long);

        /**
         * Creates a new DiamondChangeNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DiamondChangeNTF instance
         */
        public static create(properties?: web.IDiamondChangeNTF): web.DiamondChangeNTF;

        /**
         * Encodes the specified DiamondChangeNTF message. Does not implicitly {@link web.DiamondChangeNTF.verify|verify} messages.
         * @param message DiamondChangeNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: web.IDiamondChangeNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DiamondChangeNTF message, length delimited. Does not implicitly {@link web.DiamondChangeNTF.verify|verify} messages.
         * @param message DiamondChangeNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: web.IDiamondChangeNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DiamondChangeNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DiamondChangeNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): web.DiamondChangeNTF;

        /**
         * Decodes a DiamondChangeNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DiamondChangeNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): web.DiamondChangeNTF;

        /**
         * Verifies a DiamondChangeNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }
}

/** Namespace wrapper. */
declare namespace wrapper {

    /** Properties of a ProtoBase. */
    interface IProtoBase {

        /** ProtoBase cmd */
        cmd?: (number|null);
    }

    /** Represents a ProtoBase. */
    class ProtoBase implements IProtoBase {

        /**
         * Constructs a new ProtoBase.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IProtoBase);

        /** ProtoBase cmd. */
        public cmd: number;

        /**
         * Creates a new ProtoBase instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ProtoBase instance
         */
        public static create(properties?: wrapper.IProtoBase): wrapper.ProtoBase;

        /**
         * Encodes the specified ProtoBase message. Does not implicitly {@link wrapper.ProtoBase.verify|verify} messages.
         * @param message ProtoBase message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IProtoBase, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ProtoBase message, length delimited. Does not implicitly {@link wrapper.ProtoBase.verify|verify} messages.
         * @param message ProtoBase message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IProtoBase, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ProtoBase message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ProtoBase
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.ProtoBase;

        /**
         * Decodes a ProtoBase message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ProtoBase
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.ProtoBase;

        /**
         * Verifies a ProtoBase message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a PlayerInfo. */
    interface IPlayerInfo {

        /** PlayerInfo user_id */
        user_id?: (number|Long|null);

        /** PlayerInfo money */
        money?: (number|Long|null);

        /** PlayerInfo is_robot */
        is_robot?: (number|null);

        /** PlayerInfo rate */
        rate?: (number|null);

        /** PlayerInfo history */
        history?: (wrapper.IHistory[]|null);

        /** PlayerInfo tuizhu */
        tuizhu?: ((number|Long)[]|null);
    }

    /** Represents a PlayerInfo. */
    class PlayerInfo implements IPlayerInfo {

        /**
         * Constructs a new PlayerInfo.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IPlayerInfo);

        /** PlayerInfo user_id. */
        public user_id: (number|Long);

        /** PlayerInfo money. */
        public money: (number|Long);

        /** PlayerInfo is_robot. */
        public is_robot: number;

        /** PlayerInfo rate. */
        public rate: number;

        /** PlayerInfo history. */
        public history: wrapper.IHistory[];

        /** PlayerInfo tuizhu. */
        public tuizhu: (number|Long)[];

        /**
         * Creates a new PlayerInfo instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PlayerInfo instance
         */
        public static create(properties?: wrapper.IPlayerInfo): wrapper.PlayerInfo;

        /**
         * Encodes the specified PlayerInfo message. Does not implicitly {@link wrapper.PlayerInfo.verify|verify} messages.
         * @param message PlayerInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IPlayerInfo, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified PlayerInfo message, length delimited. Does not implicitly {@link wrapper.PlayerInfo.verify|verify} messages.
         * @param message PlayerInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IPlayerInfo, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a PlayerInfo message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PlayerInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.PlayerInfo;

        /**
         * Decodes a PlayerInfo message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PlayerInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.PlayerInfo;

        /**
         * Verifies a PlayerInfo message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a History. */
    interface IHistory {

        /** History ranker */
        ranker?: (number|null);

        /** History win */
        win?: (number|Long|null);

        /** History tuizhu */
        tuizhu?: (number|null);
    }

    /** Represents a History. */
    class History implements IHistory {

        /**
         * Constructs a new History.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IHistory);

        /** History ranker. */
        public ranker: number;

        /** History win. */
        public win: (number|Long);

        /** History tuizhu. */
        public tuizhu: number;

        /**
         * Creates a new History instance using the specified properties.
         * @param [properties] Properties to set
         * @returns History instance
         */
        public static create(properties?: wrapper.IHistory): wrapper.History;

        /**
         * Encodes the specified History message. Does not implicitly {@link wrapper.History.verify|verify} messages.
         * @param message History message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IHistory, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified History message, length delimited. Does not implicitly {@link wrapper.History.verify|verify} messages.
         * @param message History message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IHistory, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a History message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns History
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.History;

        /**
         * Decodes a History message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns History
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.History;

        /**
         * Verifies a History message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a BeginHandNTF. */
    interface IBeginHandNTF {

        /** BeginHandNTF user_id */
        user_id?: (number|Long|null);

        /** BeginHandNTF cmd */
        cmd?: (number|null);

        /** BeginHandNTF room_id */
        room_id?: (number|null);

        /** BeginHandNTF config */
        config?: (structure.ITableConfig|null);

        /** BeginHandNTF seat */
        seat?: (wrapper.IPlayerInfo[]|null);

        /** BeginHandNTF drawConfig */
        drawConfig?: (structure.ITableDrawConfig|null);

        /** BeginHandNTF hand */
        hand?: (number|null);
    }

    /** Represents a BeginHandNTF. */
    class BeginHandNTF implements IBeginHandNTF {

        /**
         * Constructs a new BeginHandNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IBeginHandNTF);

        /** BeginHandNTF user_id. */
        public user_id: (number|Long);

        /** BeginHandNTF cmd. */
        public cmd: number;

        /** BeginHandNTF room_id. */
        public room_id: number;

        /** BeginHandNTF config. */
        public config?: (structure.ITableConfig|null);

        /** BeginHandNTF seat. */
        public seat: wrapper.IPlayerInfo[];

        /** BeginHandNTF drawConfig. */
        public drawConfig?: (structure.ITableDrawConfig|null);

        /** BeginHandNTF hand. */
        public hand: number;

        /**
         * Creates a new BeginHandNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BeginHandNTF instance
         */
        public static create(properties?: wrapper.IBeginHandNTF): wrapper.BeginHandNTF;

        /**
         * Encodes the specified BeginHandNTF message. Does not implicitly {@link wrapper.BeginHandNTF.verify|verify} messages.
         * @param message BeginHandNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IBeginHandNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BeginHandNTF message, length delimited. Does not implicitly {@link wrapper.BeginHandNTF.verify|verify} messages.
         * @param message BeginHandNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IBeginHandNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BeginHandNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BeginHandNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.BeginHandNTF;

        /**
         * Decodes a BeginHandNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BeginHandNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.BeginHandNTF;

        /**
         * Verifies a BeginHandNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DisconnectNTF. */
    interface IDisconnectNTF {

        /** DisconnectNTF user_id */
        user_id?: (number|Long|null);

        /** DisconnectNTF cmd */
        cmd?: (number|null);

        /** DisconnectNTF room_id */
        room_id?: (number|null);
    }

    /** Represents a DisconnectNTF. */
    class DisconnectNTF implements IDisconnectNTF {

        /**
         * Constructs a new DisconnectNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IDisconnectNTF);

        /** DisconnectNTF user_id. */
        public user_id: (number|Long);

        /** DisconnectNTF cmd. */
        public cmd: number;

        /** DisconnectNTF room_id. */
        public room_id: number;

        /**
         * Creates a new DisconnectNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DisconnectNTF instance
         */
        public static create(properties?: wrapper.IDisconnectNTF): wrapper.DisconnectNTF;

        /**
         * Encodes the specified DisconnectNTF message. Does not implicitly {@link wrapper.DisconnectNTF.verify|verify} messages.
         * @param message DisconnectNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IDisconnectNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DisconnectNTF message, length delimited. Does not implicitly {@link wrapper.DisconnectNTF.verify|verify} messages.
         * @param message DisconnectNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IDisconnectNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DisconnectNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DisconnectNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.DisconnectNTF;

        /**
         * Decodes a DisconnectNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DisconnectNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.DisconnectNTF;

        /**
         * Verifies a DisconnectNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ConnectedNTF. */
    interface IConnectedNTF {

        /** ConnectedNTF user_id */
        user_id?: (number|Long|null);

        /** ConnectedNTF cmd */
        cmd?: (number|null);

        /** ConnectedNTF room_id */
        room_id?: (number|null);
    }

    /** Represents a ConnectedNTF. */
    class ConnectedNTF implements IConnectedNTF {

        /**
         * Constructs a new ConnectedNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IConnectedNTF);

        /** ConnectedNTF user_id. */
        public user_id: (number|Long);

        /** ConnectedNTF cmd. */
        public cmd: number;

        /** ConnectedNTF room_id. */
        public room_id: number;

        /**
         * Creates a new ConnectedNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ConnectedNTF instance
         */
        public static create(properties?: wrapper.IConnectedNTF): wrapper.ConnectedNTF;

        /**
         * Encodes the specified ConnectedNTF message. Does not implicitly {@link wrapper.ConnectedNTF.verify|verify} messages.
         * @param message ConnectedNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IConnectedNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ConnectedNTF message, length delimited. Does not implicitly {@link wrapper.ConnectedNTF.verify|verify} messages.
         * @param message ConnectedNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IConnectedNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ConnectedNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ConnectedNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.ConnectedNTF;

        /**
         * Decodes a ConnectedNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ConnectedNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.ConnectedNTF;

        /**
         * Verifies a ConnectedNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a SettlementNTF. */
    interface ISettlementNTF {

        /** SettlementNTF route */
        route?: ((number|Long)[]|null);

        /** SettlementNTF cmd */
        cmd?: (number|null);

        /** SettlementNTF room_id */
        room_id?: (number|null);

        /** SettlementNTF stage */
        stage?: (number|null);

        /** SettlementNTF countdown */
        countdown?: (number|null);

        /** SettlementNTF users */
        users?: (wrapper.SettlementNTF.ISettleUser[]|null);

        /** SettlementNTF center */
        center?: (number|Long|null);

        /** SettlementNTF center_win */
        center_win?: (wrapper.SettlementNTF.ICenterWin[]|null);

        /** SettlementNTF center_lose */
        center_lose?: (wrapper.SettlementNTF.ICenterWin[]|null);

        /** SettlementNTF user_id */
        user_id?: (number|Long|null);
    }

    /** Represents a SettlementNTF. */
    class SettlementNTF implements ISettlementNTF {

        /**
         * Constructs a new SettlementNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.ISettlementNTF);

        /** SettlementNTF route. */
        public route: (number|Long)[];

        /** SettlementNTF cmd. */
        public cmd: number;

        /** SettlementNTF room_id. */
        public room_id: number;

        /** SettlementNTF stage. */
        public stage: number;

        /** SettlementNTF countdown. */
        public countdown: number;

        /** SettlementNTF users. */
        public users: wrapper.SettlementNTF.ISettleUser[];

        /** SettlementNTF center. */
        public center: (number|Long);

        /** SettlementNTF center_win. */
        public center_win: wrapper.SettlementNTF.ICenterWin[];

        /** SettlementNTF center_lose. */
        public center_lose: wrapper.SettlementNTF.ICenterWin[];

        /** SettlementNTF user_id. */
        public user_id: (number|Long);

        /**
         * Creates a new SettlementNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SettlementNTF instance
         */
        public static create(properties?: wrapper.ISettlementNTF): wrapper.SettlementNTF;

        /**
         * Encodes the specified SettlementNTF message. Does not implicitly {@link wrapper.SettlementNTF.verify|verify} messages.
         * @param message SettlementNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.ISettlementNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified SettlementNTF message, length delimited. Does not implicitly {@link wrapper.SettlementNTF.verify|verify} messages.
         * @param message SettlementNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.ISettlementNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a SettlementNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SettlementNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.SettlementNTF;

        /**
         * Decodes a SettlementNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SettlementNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.SettlementNTF;

        /**
         * Verifies a SettlementNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    namespace SettlementNTF {

        /** Properties of a SettleUser. */
        interface ISettleUser {

            /** SettleUser user_id */
            user_id?: (number|Long|null);

            /** SettleUser poker_type */
            poker_type?: (number|null);

            /** SettleUser pokers */
            pokers?: (number[]|null);

            /** SettleUser win */
            win?: (number|Long|null);
        }

        /** Represents a SettleUser. */
        class SettleUser implements ISettleUser {

            /**
             * Constructs a new SettleUser.
             * @param [properties] Properties to set
             */
            constructor(properties?: wrapper.SettlementNTF.ISettleUser);

            /** SettleUser user_id. */
            public user_id: (number|Long);

            /** SettleUser poker_type. */
            public poker_type: number;

            /** SettleUser pokers. */
            public pokers: number[];

            /** SettleUser win. */
            public win: (number|Long);

            /**
             * Creates a new SettleUser instance using the specified properties.
             * @param [properties] Properties to set
             * @returns SettleUser instance
             */
            public static create(properties?: wrapper.SettlementNTF.ISettleUser): wrapper.SettlementNTF.SettleUser;

            /**
             * Encodes the specified SettleUser message. Does not implicitly {@link wrapper.SettlementNTF.SettleUser.verify|verify} messages.
             * @param message SettleUser message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: wrapper.SettlementNTF.ISettleUser, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Encodes the specified SettleUser message, length delimited. Does not implicitly {@link wrapper.SettlementNTF.SettleUser.verify|verify} messages.
             * @param message SettleUser message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: wrapper.SettlementNTF.ISettleUser, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Decodes a SettleUser message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns SettleUser
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.SettlementNTF.SettleUser;

            /**
             * Decodes a SettleUser message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns SettleUser
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.SettlementNTF.SettleUser;

            /**
             * Verifies a SettleUser message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);
        }

        /** Properties of a CenterWin. */
        interface ICenterWin {

            /** CenterWin user_id */
            user_id?: (number|Long|null);

            /** CenterWin win */
            win?: (number|Long|null);
        }

        /** Represents a CenterWin. */
        class CenterWin implements ICenterWin {

            /**
             * Constructs a new CenterWin.
             * @param [properties] Properties to set
             */
            constructor(properties?: wrapper.SettlementNTF.ICenterWin);

            /** CenterWin user_id. */
            public user_id: (number|Long);

            /** CenterWin win. */
            public win: (number|Long);

            /**
             * Creates a new CenterWin instance using the specified properties.
             * @param [properties] Properties to set
             * @returns CenterWin instance
             */
            public static create(properties?: wrapper.SettlementNTF.ICenterWin): wrapper.SettlementNTF.CenterWin;

            /**
             * Encodes the specified CenterWin message. Does not implicitly {@link wrapper.SettlementNTF.CenterWin.verify|verify} messages.
             * @param message CenterWin message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: wrapper.SettlementNTF.ICenterWin, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Encodes the specified CenterWin message, length delimited. Does not implicitly {@link wrapper.SettlementNTF.CenterWin.verify|verify} messages.
             * @param message CenterWin message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: wrapper.SettlementNTF.ICenterWin, writer?: protobuf.Writer): protobuf.Writer;

            /**
             * Decodes a CenterWin message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns CenterWin
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.SettlementNTF.CenterWin;

            /**
             * Decodes a CenterWin message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns CenterWin
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.SettlementNTF.CenterWin;

            /**
             * Verifies a CenterWin message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);
        }
    }

    /** Properties of a RankerREQ. */
    interface IRankerREQ {

        /** RankerREQ user_id */
        user_id?: (number|Long|null);

        /** RankerREQ cmd */
        cmd?: (number|null);

        /** RankerREQ multiple */
        multiple?: (number|null);

        /** RankerREQ stage */
        stage?: (number|null);

        /** RankerREQ countdown */
        countdown?: (number|null);

        /** RankerREQ room_id */
        room_id?: (number|Long|null);
    }

    /** Represents a RankerREQ. */
    class RankerREQ implements IRankerREQ {

        /**
         * Constructs a new RankerREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IRankerREQ);

        /** RankerREQ user_id. */
        public user_id: (number|Long);

        /** RankerREQ cmd. */
        public cmd: number;

        /** RankerREQ multiple. */
        public multiple: number;

        /** RankerREQ stage. */
        public stage: number;

        /** RankerREQ countdown. */
        public countdown: number;

        /** RankerREQ room_id. */
        public room_id: (number|Long);

        /**
         * Creates a new RankerREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RankerREQ instance
         */
        public static create(properties?: wrapper.IRankerREQ): wrapper.RankerREQ;

        /**
         * Encodes the specified RankerREQ message. Does not implicitly {@link wrapper.RankerREQ.verify|verify} messages.
         * @param message RankerREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IRankerREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RankerREQ message, length delimited. Does not implicitly {@link wrapper.RankerREQ.verify|verify} messages.
         * @param message RankerREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IRankerREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RankerREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RankerREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.RankerREQ;

        /**
         * Decodes a RankerREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RankerREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.RankerREQ;

        /**
         * Verifies a RankerREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RankerACK. */
    interface IRankerACK {

        /** RankerACK route */
        route?: ((number|Long)[]|null);

        /** RankerACK user_id */
        user_id?: (number|Long|null);

        /** RankerACK cmd */
        cmd?: (number|null);

        /** RankerACK room_id */
        room_id?: (number|null);

        /** RankerACK multiple */
        multiple?: (number|null);

        /** RankerACK stage */
        stage?: (number|null);

        /** RankerACK countdown */
        countdown?: (number|null);
    }

    /** Represents a RankerACK. */
    class RankerACK implements IRankerACK {

        /**
         * Constructs a new RankerACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IRankerACK);

        /** RankerACK route. */
        public route: (number|Long)[];

        /** RankerACK user_id. */
        public user_id: (number|Long);

        /** RankerACK cmd. */
        public cmd: number;

        /** RankerACK room_id. */
        public room_id: number;

        /** RankerACK multiple. */
        public multiple: number;

        /** RankerACK stage. */
        public stage: number;

        /** RankerACK countdown. */
        public countdown: number;

        /**
         * Creates a new RankerACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RankerACK instance
         */
        public static create(properties?: wrapper.IRankerACK): wrapper.RankerACK;

        /**
         * Encodes the specified RankerACK message. Does not implicitly {@link wrapper.RankerACK.verify|verify} messages.
         * @param message RankerACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IRankerACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RankerACK message, length delimited. Does not implicitly {@link wrapper.RankerACK.verify|verify} messages.
         * @param message RankerACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IRankerACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RankerACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RankerACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.RankerACK;

        /**
         * Decodes a RankerACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RankerACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.RankerACK;

        /**
         * Verifies a RankerACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a BetREQ. */
    interface IBetREQ {

        /** BetREQ route */
        route?: ((number|Long)[]|null);

        /** BetREQ cmd */
        cmd?: (number|null);

        /** BetREQ user_id */
        user_id?: (number|Long|null);

        /** BetREQ room_id */
        room_id?: (number|null);

        /** BetREQ stage */
        stage?: (number|null);

        /** BetREQ count */
        count?: (number|Long|null);

        /** BetREQ countdown */
        countdown?: (number|null);
    }

    /** Represents a BetREQ. */
    class BetREQ implements IBetREQ {

        /**
         * Constructs a new BetREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IBetREQ);

        /** BetREQ route. */
        public route: (number|Long)[];

        /** BetREQ cmd. */
        public cmd: number;

        /** BetREQ user_id. */
        public user_id: (number|Long);

        /** BetREQ room_id. */
        public room_id: number;

        /** BetREQ stage. */
        public stage: number;

        /** BetREQ count. */
        public count: (number|Long);

        /** BetREQ countdown. */
        public countdown: number;

        /**
         * Creates a new BetREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BetREQ instance
         */
        public static create(properties?: wrapper.IBetREQ): wrapper.BetREQ;

        /**
         * Encodes the specified BetREQ message. Does not implicitly {@link wrapper.BetREQ.verify|verify} messages.
         * @param message BetREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IBetREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BetREQ message, length delimited. Does not implicitly {@link wrapper.BetREQ.verify|verify} messages.
         * @param message BetREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IBetREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BetREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BetREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.BetREQ;

        /**
         * Decodes a BetREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BetREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.BetREQ;

        /**
         * Verifies a BetREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a BetACK. */
    interface IBetACK {

        /** BetACK route */
        route?: ((number|Long)[]|null);

        /** BetACK user_id */
        user_id?: (number|Long|null);

        /** BetACK cmd */
        cmd?: (number|null);

        /** BetACK room_id */
        room_id?: (number|null);

        /** BetACK Count */
        Count?: (number|Long|null);

        /** BetACK stage */
        stage?: (number|null);

        /** BetACK countdown */
        countdown?: (number|null);

        /** BetACK error */
        error?: (number|null);
    }

    /** Represents a BetACK. */
    class BetACK implements IBetACK {

        /**
         * Constructs a new BetACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IBetACK);

        /** BetACK route. */
        public route: (number|Long)[];

        /** BetACK user_id. */
        public user_id: (number|Long);

        /** BetACK cmd. */
        public cmd: number;

        /** BetACK room_id. */
        public room_id: number;

        /** BetACK Count. */
        public Count: (number|Long);

        /** BetACK stage. */
        public stage: number;

        /** BetACK countdown. */
        public countdown: number;

        /** BetACK error. */
        public error: number;

        /**
         * Creates a new BetACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BetACK instance
         */
        public static create(properties?: wrapper.IBetACK): wrapper.BetACK;

        /**
         * Encodes the specified BetACK message. Does not implicitly {@link wrapper.BetACK.verify|verify} messages.
         * @param message BetACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IBetACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BetACK message, length delimited. Does not implicitly {@link wrapper.BetACK.verify|verify} messages.
         * @param message BetACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IBetACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BetACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BetACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.BetACK;

        /**
         * Decodes a BetACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BetACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.BetACK;

        /**
         * Verifies a BetACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ShowCardREQ. */
    interface IShowCardREQ {

        /** ShowCardREQ user_id */
        user_id?: (number|Long|null);

        /** ShowCardREQ cmd */
        cmd?: (number|null);

        /** ShowCardREQ room_id */
        room_id?: (number|null);

        /** ShowCardREQ stage */
        stage?: (number|null);

        /** ShowCardREQ countdown */
        countdown?: (number|null);
    }

    /** Represents a ShowCardREQ. */
    class ShowCardREQ implements IShowCardREQ {

        /**
         * Constructs a new ShowCardREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IShowCardREQ);

        /** ShowCardREQ user_id. */
        public user_id: (number|Long);

        /** ShowCardREQ cmd. */
        public cmd: number;

        /** ShowCardREQ room_id. */
        public room_id: number;

        /** ShowCardREQ stage. */
        public stage: number;

        /** ShowCardREQ countdown. */
        public countdown: number;

        /**
         * Creates a new ShowCardREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ShowCardREQ instance
         */
        public static create(properties?: wrapper.IShowCardREQ): wrapper.ShowCardREQ;

        /**
         * Encodes the specified ShowCardREQ message. Does not implicitly {@link wrapper.ShowCardREQ.verify|verify} messages.
         * @param message ShowCardREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IShowCardREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ShowCardREQ message, length delimited. Does not implicitly {@link wrapper.ShowCardREQ.verify|verify} messages.
         * @param message ShowCardREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IShowCardREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ShowCardREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ShowCardREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.ShowCardREQ;

        /**
         * Decodes a ShowCardREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ShowCardREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.ShowCardREQ;

        /**
         * Verifies a ShowCardREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ShowCardACK. */
    interface IShowCardACK {

        /** ShowCardACK route */
        route?: ((number|Long)[]|null);

        /** ShowCardACK user_id */
        user_id?: (number|Long|null);

        /** ShowCardACK cmd */
        cmd?: (number|null);

        /** ShowCardACK room_id */
        room_id?: (number|null);

        /** ShowCardACK pokers */
        pokers?: (number[]|null);

        /** ShowCardACK poker_type */
        poker_type?: (number|null);

        /** ShowCardACK stage */
        stage?: (number|null);

        /** ShowCardACK countdown */
        countdown?: (number|null);

        /** ShowCardACK error */
        error?: (number|null);
    }

    /** Represents a ShowCardACK. */
    class ShowCardACK implements IShowCardACK {

        /**
         * Constructs a new ShowCardACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IShowCardACK);

        /** ShowCardACK route. */
        public route: (number|Long)[];

        /** ShowCardACK user_id. */
        public user_id: (number|Long);

        /** ShowCardACK cmd. */
        public cmd: number;

        /** ShowCardACK room_id. */
        public room_id: number;

        /** ShowCardACK pokers. */
        public pokers: number[];

        /** ShowCardACK poker_type. */
        public poker_type: number;

        /** ShowCardACK stage. */
        public stage: number;

        /** ShowCardACK countdown. */
        public countdown: number;

        /** ShowCardACK error. */
        public error: number;

        /**
         * Creates a new ShowCardACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ShowCardACK instance
         */
        public static create(properties?: wrapper.IShowCardACK): wrapper.ShowCardACK;

        /**
         * Encodes the specified ShowCardACK message. Does not implicitly {@link wrapper.ShowCardACK.verify|verify} messages.
         * @param message ShowCardACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IShowCardACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ShowCardACK message, length delimited. Does not implicitly {@link wrapper.ShowCardACK.verify|verify} messages.
         * @param message ShowCardACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IShowCardACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ShowCardACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ShowCardACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.ShowCardACK;

        /**
         * Decodes a ShowCardACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ShowCardACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.ShowCardACK;

        /**
         * Verifies a ShowCardACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DispatchCardNTF. */
    interface IDispatchCardNTF {

        /** DispatchCardNTF route */
        route?: ((number|Long)[]|null);

        /** DispatchCardNTF user_id */
        user_id?: (number|Long|null);

        /** DispatchCardNTF cmd */
        cmd?: (number|null);

        /** DispatchCardNTF room_id */
        room_id?: (number|null);

        /** DispatchCardNTF pokers */
        pokers?: (number[]|null);

        /** DispatchCardNTF stage */
        stage?: (number|null);

        /** DispatchCardNTF countdown */
        countdown?: (number|null);

        /** DispatchCardNTF can_bet */
        can_bet?: ((number|Long)[]|null);
    }

    /** Represents a DispatchCardNTF. */
    class DispatchCardNTF implements IDispatchCardNTF {

        /**
         * Constructs a new DispatchCardNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IDispatchCardNTF);

        /** DispatchCardNTF route. */
        public route: (number|Long)[];

        /** DispatchCardNTF user_id. */
        public user_id: (number|Long);

        /** DispatchCardNTF cmd. */
        public cmd: number;

        /** DispatchCardNTF room_id. */
        public room_id: number;

        /** DispatchCardNTF pokers. */
        public pokers: number[];

        /** DispatchCardNTF stage. */
        public stage: number;

        /** DispatchCardNTF countdown. */
        public countdown: number;

        /** DispatchCardNTF can_bet. */
        public can_bet: (number|Long)[];

        /**
         * Creates a new DispatchCardNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DispatchCardNTF instance
         */
        public static create(properties?: wrapper.IDispatchCardNTF): wrapper.DispatchCardNTF;

        /**
         * Encodes the specified DispatchCardNTF message. Does not implicitly {@link wrapper.DispatchCardNTF.verify|verify} messages.
         * @param message DispatchCardNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IDispatchCardNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DispatchCardNTF message, length delimited. Does not implicitly {@link wrapper.DispatchCardNTF.verify|verify} messages.
         * @param message DispatchCardNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IDispatchCardNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DispatchCardNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DispatchCardNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.DispatchCardNTF;

        /**
         * Decodes a DispatchCardNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DispatchCardNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.DispatchCardNTF;

        /**
         * Verifies a DispatchCardNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a DispatchFinalCardNTF. */
    interface IDispatchFinalCardNTF {

        /** DispatchFinalCardNTF route */
        route?: ((number|Long)[]|null);

        /** DispatchFinalCardNTF user_id */
        user_id?: (number|Long|null);

        /** DispatchFinalCardNTF cmd */
        cmd?: (number|null);

        /** DispatchFinalCardNTF room_id */
        room_id?: (number|null);

        /** DispatchFinalCardNTF poker */
        poker?: (number|null);

        /** DispatchFinalCardNTF poker_type */
        poker_type?: (number|null);

        /** DispatchFinalCardNTF stage */
        stage?: (number|null);

        /** DispatchFinalCardNTF countdown */
        countdown?: (number|null);
    }

    /** Represents a DispatchFinalCardNTF. */
    class DispatchFinalCardNTF implements IDispatchFinalCardNTF {

        /**
         * Constructs a new DispatchFinalCardNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IDispatchFinalCardNTF);

        /** DispatchFinalCardNTF route. */
        public route: (number|Long)[];

        /** DispatchFinalCardNTF user_id. */
        public user_id: (number|Long);

        /** DispatchFinalCardNTF cmd. */
        public cmd: number;

        /** DispatchFinalCardNTF room_id. */
        public room_id: number;

        /** DispatchFinalCardNTF poker. */
        public poker: number;

        /** DispatchFinalCardNTF poker_type. */
        public poker_type: number;

        /** DispatchFinalCardNTF stage. */
        public stage: number;

        /** DispatchFinalCardNTF countdown. */
        public countdown: number;

        /**
         * Creates a new DispatchFinalCardNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DispatchFinalCardNTF instance
         */
        public static create(properties?: wrapper.IDispatchFinalCardNTF): wrapper.DispatchFinalCardNTF;

        /**
         * Encodes the specified DispatchFinalCardNTF message. Does not implicitly {@link wrapper.DispatchFinalCardNTF.verify|verify} messages.
         * @param message DispatchFinalCardNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IDispatchFinalCardNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified DispatchFinalCardNTF message, length delimited. Does not implicitly {@link wrapper.DispatchFinalCardNTF.verify|verify} messages.
         * @param message DispatchFinalCardNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IDispatchFinalCardNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a DispatchFinalCardNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DispatchFinalCardNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.DispatchFinalCardNTF;

        /**
         * Decodes a DispatchFinalCardNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DispatchFinalCardNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.DispatchFinalCardNTF;

        /**
         * Verifies a DispatchFinalCardNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a RankerNTF. */
    interface IRankerNTF {

        /** RankerNTF route */
        route?: ((number|Long)[]|null);

        /** RankerNTF user_id */
        user_id?: (number|Long|null);

        /** RankerNTF cmd */
        cmd?: (number|null);

        /** RankerNTF room_id */
        room_id?: (number|null);

        /** RankerNTF multiple */
        multiple?: (number|null);

        /** RankerNTF stage */
        stage?: (number|null);

        /** RankerNTF countdown */
        countdown?: (number|null);

        /** RankerNTF hogged */
        hogged?: ((number|Long)[]|null);
    }

    /** Represents a RankerNTF. */
    class RankerNTF implements IRankerNTF {

        /**
         * Constructs a new RankerNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IRankerNTF);

        /** RankerNTF route. */
        public route: (number|Long)[];

        /** RankerNTF user_id. */
        public user_id: (number|Long);

        /** RankerNTF cmd. */
        public cmd: number;

        /** RankerNTF room_id. */
        public room_id: number;

        /** RankerNTF multiple. */
        public multiple: number;

        /** RankerNTF stage. */
        public stage: number;

        /** RankerNTF countdown. */
        public countdown: number;

        /** RankerNTF hogged. */
        public hogged: (number|Long)[];

        /**
         * Creates a new RankerNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RankerNTF instance
         */
        public static create(properties?: wrapper.IRankerNTF): wrapper.RankerNTF;

        /**
         * Encodes the specified RankerNTF message. Does not implicitly {@link wrapper.RankerNTF.verify|verify} messages.
         * @param message RankerNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IRankerNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified RankerNTF message, length delimited. Does not implicitly {@link wrapper.RankerNTF.verify|verify} messages.
         * @param message RankerNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IRankerNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a RankerNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RankerNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.RankerNTF;

        /**
         * Decodes a RankerNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RankerNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.RankerNTF;

        /**
         * Verifies a RankerNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a BeginHistory. */
    interface IBeginHistory {

        /** BeginHistory user_id */
        user_id?: (number|Long|null);

        /** BeginHistory cmd */
        cmd?: (number|null);

        /** BeginHistory room_id */
        room_id?: (number|Long|null);
    }

    /** Represents a BeginHistory. */
    class BeginHistory implements IBeginHistory {

        /**
         * Constructs a new BeginHistory.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IBeginHistory);

        /** BeginHistory user_id. */
        public user_id: (number|Long);

        /** BeginHistory cmd. */
        public cmd: number;

        /** BeginHistory room_id. */
        public room_id: (number|Long);

        /**
         * Creates a new BeginHistory instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BeginHistory instance
         */
        public static create(properties?: wrapper.IBeginHistory): wrapper.BeginHistory;

        /**
         * Encodes the specified BeginHistory message. Does not implicitly {@link wrapper.BeginHistory.verify|verify} messages.
         * @param message BeginHistory message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IBeginHistory, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified BeginHistory message, length delimited. Does not implicitly {@link wrapper.BeginHistory.verify|verify} messages.
         * @param message BeginHistory message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IBeginHistory, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a BeginHistory message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BeginHistory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.BeginHistory;

        /**
         * Decodes a BeginHistory message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BeginHistory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.BeginHistory;

        /**
         * Verifies a BeginHistory message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of an EndHistory. */
    interface IEndHistory {

        /** EndHistory user_id */
        user_id?: (number|Long|null);

        /** EndHistory cmd */
        cmd?: (number|null);

        /** EndHistory room_id */
        room_id?: (number|Long|null);
    }

    /** Represents an EndHistory. */
    class EndHistory implements IEndHistory {

        /**
         * Constructs a new EndHistory.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IEndHistory);

        /** EndHistory user_id. */
        public user_id: (number|Long);

        /** EndHistory cmd. */
        public cmd: number;

        /** EndHistory room_id. */
        public room_id: (number|Long);

        /**
         * Creates a new EndHistory instance using the specified properties.
         * @param [properties] Properties to set
         * @returns EndHistory instance
         */
        public static create(properties?: wrapper.IEndHistory): wrapper.EndHistory;

        /**
         * Encodes the specified EndHistory message. Does not implicitly {@link wrapper.EndHistory.verify|verify} messages.
         * @param message EndHistory message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IEndHistory, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified EndHistory message, length delimited. Does not implicitly {@link wrapper.EndHistory.verify|verify} messages.
         * @param message EndHistory message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IEndHistory, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an EndHistory message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns EndHistory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.EndHistory;

        /**
         * Decodes an EndHistory message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns EndHistory
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.EndHistory;

        /**
         * Verifies an EndHistory message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of an EndHandNTF. */
    interface IEndHandNTF {

        /** EndHandNTF cmd */
        cmd?: (number|null);

        /** EndHandNTF room_id */
        room_id?: (number|Long|null);
    }

    /** Represents an EndHandNTF. */
    class EndHandNTF implements IEndHandNTF {

        /**
         * Constructs a new EndHandNTF.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IEndHandNTF);

        /** EndHandNTF cmd. */
        public cmd: number;

        /** EndHandNTF room_id. */
        public room_id: (number|Long);

        /**
         * Creates a new EndHandNTF instance using the specified properties.
         * @param [properties] Properties to set
         * @returns EndHandNTF instance
         */
        public static create(properties?: wrapper.IEndHandNTF): wrapper.EndHandNTF;

        /**
         * Encodes the specified EndHandNTF message. Does not implicitly {@link wrapper.EndHandNTF.verify|verify} messages.
         * @param message EndHandNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IEndHandNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified EndHandNTF message, length delimited. Does not implicitly {@link wrapper.EndHandNTF.verify|verify} messages.
         * @param message EndHandNTF message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IEndHandNTF, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes an EndHandNTF message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns EndHandNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.EndHandNTF;

        /**
         * Decodes an EndHandNTF message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns EndHandNTF
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.EndHandNTF;

        /**
         * Verifies an EndHandNTF message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a CancelTable. */
    interface ICancelTable {

        /** CancelTable cmd */
        cmd?: (number|null);

        /** CancelTable room_id */
        room_id?: (number|Long|null);
    }

    /** Represents a CancelTable. */
    class CancelTable implements ICancelTable {

        /**
         * Constructs a new CancelTable.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.ICancelTable);

        /** CancelTable cmd. */
        public cmd: number;

        /** CancelTable room_id. */
        public room_id: (number|Long);

        /**
         * Creates a new CancelTable instance using the specified properties.
         * @param [properties] Properties to set
         * @returns CancelTable instance
         */
        public static create(properties?: wrapper.ICancelTable): wrapper.CancelTable;

        /**
         * Encodes the specified CancelTable message. Does not implicitly {@link wrapper.CancelTable.verify|verify} messages.
         * @param message CancelTable message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.ICancelTable, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified CancelTable message, length delimited. Does not implicitly {@link wrapper.CancelTable.verify|verify} messages.
         * @param message CancelTable message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.ICancelTable, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a CancelTable message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns CancelTable
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.CancelTable;

        /**
         * Decodes a CancelTable message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns CancelTable
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.CancelTable;

        /**
         * Verifies a CancelTable message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TipREQ. */
    interface ITipREQ {

        /** TipREQ cmd */
        cmd?: (number|null);

        /** TipREQ user_id */
        user_id?: (number|Long|null);

        /** TipREQ room_id */
        room_id?: (number|Long|null);
    }

    /** Represents a TipREQ. */
    class TipREQ implements ITipREQ {

        /**
         * Constructs a new TipREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.ITipREQ);

        /** TipREQ cmd. */
        public cmd: number;

        /** TipREQ user_id. */
        public user_id: (number|Long);

        /** TipREQ room_id. */
        public room_id: (number|Long);

        /**
         * Creates a new TipREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TipREQ instance
         */
        public static create(properties?: wrapper.ITipREQ): wrapper.TipREQ;

        /**
         * Encodes the specified TipREQ message. Does not implicitly {@link wrapper.TipREQ.verify|verify} messages.
         * @param message TipREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.ITipREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TipREQ message, length delimited. Does not implicitly {@link wrapper.TipREQ.verify|verify} messages.
         * @param message TipREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.ITipREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TipREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TipREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.TipREQ;

        /**
         * Decodes a TipREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TipREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.TipREQ;

        /**
         * Verifies a TipREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a TipACK. */
    interface ITipACK {

        /** TipACK cmd */
        cmd?: (number|Long|null);

        /** TipACK room_id */
        room_id?: (number|Long|null);

        /** TipACK pokers */
        pokers?: (number[]|null);

        /** TipACK poker_type */
        poker_type?: (number|null);

        /** TipACK stage */
        stage?: (number|null);

        /** TipACK countdown */
        countdown?: (number|null);

        /** TipACK user_id */
        user_id?: (number|Long|null);

        /** TipACK route */
        route?: ((number|Long)[]|null);
    }

    /** Represents a TipACK. */
    class TipACK implements ITipACK {

        /**
         * Constructs a new TipACK.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.ITipACK);

        /** TipACK cmd. */
        public cmd: (number|Long);

        /** TipACK room_id. */
        public room_id: (number|Long);

        /** TipACK pokers. */
        public pokers: number[];

        /** TipACK poker_type. */
        public poker_type: number;

        /** TipACK stage. */
        public stage: number;

        /** TipACK countdown. */
        public countdown: number;

        /** TipACK user_id. */
        public user_id: (number|Long);

        /** TipACK route. */
        public route: (number|Long)[];

        /**
         * Creates a new TipACK instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TipACK instance
         */
        public static create(properties?: wrapper.ITipACK): wrapper.TipACK;

        /**
         * Encodes the specified TipACK message. Does not implicitly {@link wrapper.TipACK.verify|verify} messages.
         * @param message TipACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.ITipACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified TipACK message, length delimited. Does not implicitly {@link wrapper.TipACK.verify|verify} messages.
         * @param message TipACK message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.ITipACK, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a TipACK message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TipACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.TipACK;

        /**
         * Decodes a TipACK message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TipACK
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.TipACK;

        /**
         * Verifies a TipACK message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a CardToolsREQ. */
    interface ICardToolsREQ {

        /** CardToolsREQ Cards */
        Cards?: (number[]|null);
    }

    /** Represents a CardToolsREQ. */
    class CardToolsREQ implements ICardToolsREQ {

        /**
         * Constructs a new CardToolsREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.ICardToolsREQ);

        /** CardToolsREQ Cards. */
        public Cards: number[];

        /**
         * Creates a new CardToolsREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns CardToolsREQ instance
         */
        public static create(properties?: wrapper.ICardToolsREQ): wrapper.CardToolsREQ;

        /**
         * Encodes the specified CardToolsREQ message. Does not implicitly {@link wrapper.CardToolsREQ.verify|verify} messages.
         * @param message CardToolsREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.ICardToolsREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified CardToolsREQ message, length delimited. Does not implicitly {@link wrapper.CardToolsREQ.verify|verify} messages.
         * @param message CardToolsREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.ICardToolsREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a CardToolsREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns CardToolsREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.CardToolsREQ;

        /**
         * Decodes a CardToolsREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns CardToolsREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.CardToolsREQ;

        /**
         * Verifies a CardToolsREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }

    /** Properties of a ModifyCardREQ. */
    interface IModifyCardREQ {

        /** ModifyCardREQ cmd */
        cmd?: (number|null);

        /** ModifyCardREQ user_id */
        user_id?: (number|Long|null);

        /** ModifyCardREQ pokers */
        pokers?: (number[]|null);
    }

    /** Represents a ModifyCardREQ. */
    class ModifyCardREQ implements IModifyCardREQ {

        /**
         * Constructs a new ModifyCardREQ.
         * @param [properties] Properties to set
         */
        constructor(properties?: wrapper.IModifyCardREQ);

        /** ModifyCardREQ cmd. */
        public cmd: number;

        /** ModifyCardREQ user_id. */
        public user_id: (number|Long);

        /** ModifyCardREQ pokers. */
        public pokers: number[];

        /**
         * Creates a new ModifyCardREQ instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ModifyCardREQ instance
         */
        public static create(properties?: wrapper.IModifyCardREQ): wrapper.ModifyCardREQ;

        /**
         * Encodes the specified ModifyCardREQ message. Does not implicitly {@link wrapper.ModifyCardREQ.verify|verify} messages.
         * @param message ModifyCardREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: wrapper.IModifyCardREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Encodes the specified ModifyCardREQ message, length delimited. Does not implicitly {@link wrapper.ModifyCardREQ.verify|verify} messages.
         * @param message ModifyCardREQ message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: wrapper.IModifyCardREQ, writer?: protobuf.Writer): protobuf.Writer;

        /**
         * Decodes a ModifyCardREQ message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ModifyCardREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: (protobuf.Reader|Uint8Array), length?: number): wrapper.ModifyCardREQ;

        /**
         * Decodes a ModifyCardREQ message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ModifyCardREQ
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: (protobuf.Reader|Uint8Array)): wrapper.ModifyCardREQ;

        /**
         * Verifies a ModifyCardREQ message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);
    }
}
