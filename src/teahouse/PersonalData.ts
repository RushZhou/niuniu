
class PersonalData extends eui.Component {

    public constructor(userID) {
        super();
        this.skinName = "skins.teahouse.PersonalData";
        this.userId = userID;
    }
    private closeBtn: eui.Button;
    private gameRadioBtn: eui.RadioButton;
    private gameViewStack: eui.ViewStack;

    private gameChangBar: eui.Scroller;
    private gameChaneList: eui.List;
    private scoreChangeBar: eui.Scroller;
    private scoreChangeList: eui.List;

    private userId = null;
    private showNoRec: eui.Group;
    private GameChangeData = [];
    private ScoreChangeData = [];
    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            this.gameRadioBtn.group.removeEventListener(eui.UIEvent.CHANGE, this.onChange, this);
            MsgMgr.inst.offMsgAll(this);
            this.parent.removeChild(this);
            Main.inst.dispatchEventWith(GameData.GameEvent.UpdateAward);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.gameRadioBtn.group.$name = 'PersonalData';
        this.gameRadioBtn.group.addEventListener(eui.UIEvent.CHANGE, this.onChange, this);
        this.initViewData();
    }

    private onChange(e: eui.UIEvent) {
        let radioGroup: eui.RadioButtonGroup = e.target;
        this.gameViewStack.selectedIndex = radioGroup.selectedValue;
        console.log('PersonalData', 'radio');
        if (radioGroup.$name !== 'PersonalData') return;
        let userScoreLogReq = new teahouse.UserScoreLogREQ();
        userScoreLogReq.HouseID = GameData.teahouseID;
        userScoreLogReq.UserID = this.userId;
        userScoreLogReq.Start = 0;
        userScoreLogReq.PageCount = 30;


        if (this.gameViewStack.selectedIndex === 0) {
            userScoreLogReq.Type = teahouse.RecordType.Game;
        } else {
            userScoreLogReq.Type = teahouse.RecordType.Score;
        }


        console.log("userScoreLogRes", userScoreLogReq);
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_UserScoreLogREQ, teahouse.UserScoreLogREQ, userScoreLogReq);
    }

    private initViewData() {
        //个人数据
        let userScoreLogReq = new teahouse.UserScoreLogREQ();
        userScoreLogReq.HouseID = GameData.teahouseID;
        userScoreLogReq.UserID = this.userId;
        userScoreLogReq.Start = 0;
        userScoreLogReq.PageCount = 30;
        userScoreLogReq.Type = teahouse.RecordType.Game;
        console.log("userScoreLogRes", userScoreLogReq);
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_UserScoreLogREQ, teahouse.UserScoreLogREQ, userScoreLogReq);
        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_UserScoreLogACK, teahouse.UserScoreLogACK, (msg: teahouse.UserScoreLogACK) => {
            console.log("userScoreLogRes", msg);
            if (msg.ChangeLog.length === 0) this.showNoRec.visible = true;
            if (msg.Type === teahouse.RecordType.Game) {
                let logs = [];
                msg.ChangeLog.forEach(changelog => {
                    changelog['type'] = msg.Type;
                    logs.push(changelog);
                }, this);
                let gameChangeData = new eui.ArrayCollection(logs);
                this.gameChaneList.dataProvider = gameChangeData;
                this.gameChaneList.itemRenderer = PersonalDataItem;
                this.gameChangBar.viewport = this.gameChaneList;
                this.showNoRec.visible = msg.ChangeLog.length > 0 ? false : true;
            } else if (msg.Type === teahouse.RecordType.Score) {
                let logs = [];
                msg.ChangeLog.forEach(changelog => {
                    changelog['type'] = msg.Type;
                    logs.push(changelog);
                }, this);
                let scoreChangeData = new eui.ArrayCollection(logs);
                this.scoreChangeList.dataProvider = scoreChangeData;
                this.scoreChangeList.itemRenderer = PersonalDataItem;
                this.scoreChangeBar.viewport = this.scoreChangeList;
                this.showNoRec.visible = msg.ChangeLog.length > 0 ? false : true;
            }
            // this.GameChangeData = [];
            // this.ScoreChangeData = [];
            // for (let i = 0; i < msg.ChangeLog.length; i++) {
            //     if (msg.ChangeLog[i].Tag === 0) {
            //         this.GameChangeData.push(msg.ChangeLog[i]);
            //     } else if (msg.ChangeLog[i].Tag === 1) {
            //         this.ScoreChangeData.push(msg.ChangeLog[i]);
            //     }
            // }
            // let gameChangeData = new eui.ArrayCollection(this.GameChangeData);
            // this.gameChaneList.dataProvider = gameChangeData;
            // this.gameChaneList.itemRenderer = PersonalDataItem;
            // this.gameChangBar.viewport = this.gameChaneList;
            // this.showNoRec.visible = this.GameChangeData.length > 0 ? false : true;

            // let scoreChangeData = new eui.ArrayCollection(this.ScoreChangeData);
            // this.scoreChangeList.dataProvider = scoreChangeData;
            // this.scoreChangeList.itemRenderer = PersonalDataItem;
            // this.scoreChangeBar.viewport = this.scoreChangeList;

            // MsgMgr.inst.offMsgAll(this);
        }, this);



    }
}

class PersonalDataItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "skins.teahouse.PersonalDataItem";
    }
    private GameChangeItem: eui.Group;
    private g_roomId: eui.Label;
    private g_scoreChange: eui.Label;
    private g_date: eui.Label;
    private IntegralvariationItem: eui.Group;
    private i_type: eui.Label;
    private i_player: eui.Label;
    private i_score: eui.Label;
    private i_date: eui.Label;

    protected childrenCreated() {
        super.childrenCreated();


    }
    protected dataChanged() {
        console.log("PersonalDataItem", this.data);

        if (this.data.type === teahouse.RecordType.Game) {
            this.GameChangeItem.visible = true;
            this.g_roomId.text = this.data.TableID;
            this.g_scoreChange.text = this.data.DeltaScore;
            this.g_date.text = GameData.getTimeStr(this.data.Date);

        } else if (this.data.type === teahouse.RecordType.Score) {
            this.IntegralvariationItem.visible = true;
            this.i_date.text = GameData.getTimeStr(this.data.Date);
            this.i_player.text = "ID:" + this.data.UserID;
            this.i_score.text = this.data.DeltaScore;
            this.i_type.text = this.data.Reason;
        }

    }
}

