class ModifPumpPnl extends eui.Component {

    public constructor(tableInfo: structure.ITableInfo) {
        super();
        this.skinName = "skins.teahouse.ModifPump";
        this.tableInfo = tableInfo;
    }

    private bk_fufen: eui.RadioButton;
    private k_fufen: eui.RadioButton;
    private bk_tiaozheng: eui.RadioButton;
    private k_tiaozheng: eui.RadioButton;
    private matchSaveChange: eui.Button;
    private closeBtn: eui.Button;
    private modifyGameScore: eui.Button;
    private modifyRushVillageScore: eui.Button;
    private people: ModifCheckBox;
    private pumping: ModifCheckBox;
    private score: ModifCheckBox;
    private GameScoreInput: eui.Label;
    private RushVillageScoreInput: eui.Label;


    private keFu: eui.RadioButtonGroup;
    private tiaozheng: eui.RadioButtonGroup;

    private tableInfo: structure.ITableInfo;
    private scoreRuler: structure.TableDrawConfig.ScoreRuler;
    private scoreAdjust: structure.TableDrawConfig.ScoreAdjust;
    protected childrenCreated() {
        super.childrenCreated();


        this.keFu = new eui.RadioButtonGroup();
        this.bk_fufen.group = this.keFu;
        this.k_fufen.group = this.keFu;
        if (!this.tableInfo.DrawConfig.Ruler) {
            this.bk_fufen.selected = true;
        } else {
            this.k_fufen.selected = true;
        }


        this.tiaozheng = new eui.RadioButtonGroup();
        this.k_tiaozheng.group = this.tiaozheng;
        this.bk_tiaozheng.group = this.tiaozheng;

        if (!this.tableInfo.DrawConfig.Adjust) {
            this.bk_tiaozheng.selected = true;
        } else {
            this.k_tiaozheng.selected = true;
        }


        this.bk_fufen.group.addEventListener(eui.UIEvent.CHANGE, this.onChange, this);
        this.k_tiaozheng.group.addEventListener(eui.UIEvent.CHANGE, this.onChange, this);



        this.people.addItem({ name: "所有赢家", value: structure.TableDrawConfig.DrawFrom.AllWin });
        this.people.addItem({ name: "最大赢家", value: structure.TableDrawConfig.DrawFrom.MaxWin });
        this.people.setSelect(parseInt(this.tableInfo.DrawConfig.Draw.DrawFrom.toString()));
        this.pumping.addItem({ name: "1%", value: 1 });
        this.pumping.addItem({ name: "2%", value: 2 });
        this.pumping.addItem({ name: "3%", value: 3 });
        this.pumping.addItem({ name: "4%", value: 4 });
        this.pumping.addItem({ name: "5%", value: 5 });
        this.pumping.addItem({ name: "6%", value: 6 });
        this.pumping.addItem({ name: "7%", value: 7 });
        this.pumping.addItem({ name: "8%", value: 8 });
        this.pumping.addItem({ name: "9%", value: 9 });
        this.pumping.addItem({ name: "10%", value: 10 });
        this.pumping.setSelect(parseInt(this.tableInfo.DrawConfig.Draw.Rate.toString()) - 1);

        this.score.addItem({ name: "0分", value: 0 });
        this.score.addItem({ name: "50分", value: 50 });
        this.score.addItem({ name: "100分", value: 100 });
        this.score.addItem({ name: "200分", value: 200 });
        this.score.addItem({ name: "500分", value: 500 });
        this.score.setSelect(parseInt(this.tableInfo.DrawConfig.Draw.BaseScore.toString()));

        this.GameScoreInput.text = this.tableInfo.DrawConfig.GameScore.toString();
        this.RushVillageScoreInput.text = this.tableInfo.DrawConfig.RankScore.toString();




        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            this.bk_fufen.group.removeEventListener(eui.UIEvent.CHANGE, this.onChange, this);
            this.k_tiaozheng.group.removeEventListener(eui.UIEvent.CHANGE, this.onChange, this);
            MsgMgr.inst.offMsgAll(this);
            this.parent.removeChild(this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
        this.matchSaveChange.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let tableConfigChangeReq = new niuniu.TableConfigChangeREQ();
            let tableDrawCfg = new structure.TableDrawConfig();
            tableDrawCfg.GameScore = parseInt(this.GameScoreInput.text);
            tableDrawCfg.RankScore = parseInt(this.RushVillageScoreInput.text);

            tableDrawCfg.Ruler = this.scoreRuler;
            tableDrawCfg.Adjust = this.scoreAdjust;

            let drawFrom = new structure.TableDrawConfig.DrawRuler();
            drawFrom.DrawFrom = this.people.selectItem.value;
            drawFrom.Rate = this.pumping.selectItem.value;
            drawFrom.BaseScore = this.score.selectItem.value;
            tableDrawCfg.Draw = drawFrom;

            tableConfigChangeReq.TableId = parseInt(this.tableInfo.TableID.toString());
            tableConfigChangeReq.DrawConfig = tableDrawCfg;

            let isSend = true;

            if (tableDrawCfg.GameScore < 200) {
                Tips.addTip('参与游戏积分不能小于200');
                isSend = false;
            }
            if (tableDrawCfg.RankScore < 500) {
                Tips.addTip('参与抢庄积分不能小于500');
                isSend = false;
            }

            console.log("修改抽水请求", tableConfigChangeReq);
            if (isSend) {
                MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_TableConfigChangeREQ, niuniu.TableConfigChangeREQ, tableConfigChangeReq);
            }

            MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_TableConfigChangeACK, niuniu.TableConfigChangeACK, (msg: niuniu.TableConfigChangeACK) => {
                console.log('修改抽水返回', msg.Code);
                if (msg.Code === niuniu.GameCode.SUCCESS) {

                    MsgMgr.inst.offMsgAll(this);
                    this.parent.removeChild(this);
                    //刷新桌子信息 发送
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseTable, false, { TableID: this.tableInfo.TableID, Action: "updatePump" });

                } else {
                    Tips.addTip("修改抽水未生效");
                }

            }, this);


        }, this);
        this.modifyGameScore.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let modifScore = new ModifyScore("modifyGameScore");
            Main.inst.addChild(modifScore);

        }, this);

        this.modifyRushVillageScore.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let modifScore = new ModifyScore("modifyRushVillageScore");
            Main.inst.addChild(modifScore);
        }, this);


        Main.inst.addEventListener(GameData.GameEvent.ReturnString, (evt: egret.Event) => {
            if (evt.data.typeStr === 'modifyGameScore') {
                if (evt.data.ReturnString < 200) {
                    Tips.addTip('参与游戏积分不能小于200');
                    evt.data.ReturnString = 200;
                }
                this.GameScoreInput.text = evt.data.ReturnString;
            } else if (evt.data.typeStr === 'modifyRushVillageScore') {
                if (evt.data.ReturnString < 500) {
                    Tips.addTip('参与抢庄积分不能小于500');
                    evt.data.ReturnString = 500;
                }
                this.RushVillageScoreInput.text = evt.data.ReturnString;
            }

        }, this);
    }


    private onChange(e: eui.UIEvent) {
        let radioGroup: eui.RadioButtonGroup = e.target;

        switch (e.target) {
            case this.keFu:
                console.log('keFu', radioGroup.selectedValue);
                if (radioGroup.selectedValue === "0") {
                    this.scoreRuler = structure.TableDrawConfig.ScoreRuler.NoneNagetive;
                } else if (radioGroup.selectedValue === "1") {
                    this.scoreRuler = structure.TableDrawConfig.ScoreRuler.Nagetive;
                }


                break;
            case this.tiaozheng:
                console.log('tiaozheng', radioGroup.selectedValue);
                if (radioGroup.selectedValue === "0") {
                    this.scoreAdjust = structure.TableDrawConfig.ScoreAdjust.NoneInGame;
                } else if (radioGroup.selectedValue === "1") {
                    this.scoreAdjust = structure.TableDrawConfig.ScoreAdjust.InGame;
                }
                break;
            default:
                break;
        }

    }


}