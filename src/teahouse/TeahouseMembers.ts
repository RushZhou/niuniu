class TeahouseMember extends eui.Component {
    public constructor() {
        super();
        this.skinName = TeahouseMembers;

    }

    private closeBtn: eui.Button;
    private radioBtn: eui.RadioButton;
    private viewStack: eui.ViewStack;
    private membersList: eui.List;
    private memberScroll: eui.Scroller;
    private examineList: eui.List;
    private examineScroll: eui.Scroller;
    private seniorList: eui.List;
    private seniorScroll: eui.Scroller;
    private invitaBtn: eui.Button;

    private membersData: eui.ArrayCollection;
    private seniorData: eui.ArrayCollection;

    private examine: eui.RadioButton;
    private super: eui.RadioButton;
    private noRecord: eui.Group;

    public pageMember: eui.Label;
    public excludeMember: eui.Button;
    public seekMember: eui.TextInput;
    public upPageBtn: eui.Button;
    public nextPageBtn: eui.Button;
    public searchGroup: eui.Group;
    public mangerRed: eui.Image;


    private pageCount: number = 6;//每页允许有15个人
    private nowPage: number = 1;//当前页

    private examineData: eui.ArrayCollection;

    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            this.radioBtn.group.removeEventListener(eui.UIEvent.CHANGE, this.onChange, this);
            MsgMgr.inst.offMsgAll(this);
            this.parent.removeChild(this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        //判断是否有新审核
        let teahouseMailReq = new login.TeahouseMailREQ();
        teahouseMailReq.HouseID = GameData.teahouseID;
        MsgMgr.inst.sendMsg(login.MailID.ID_TeahouseMailREQ, login.TeahouseMailREQ, teahouseMailReq);
        //刷新茶楼审核红点

        MsgMgr.inst.onMsg(login.MailID.ID_TeahouseMailACK, login.TeahouseMailACK, (msg: login.TeahouseMailACK) => {
            this.mangerRed.visible = msg.Mail.length > 0 ? true : false;
            MsgMgr.inst.offMsg(login.MailID.ID_TeahouseMailACK, this);
        }, this);

        this.radioBtn.group.$name = 'TeahouseMember';
        this.radioBtn.group.addEventListener(eui.UIEvent.CHANGE, this.onChange, this);
        //茶楼成员
        this.initViewData(0);

        //addEventListener
        this.onListener();

        //限制条件 普通成员不能查看审核管理和高级管理
        this.limitAdmin();

        this.invitaBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let changeScorePnl = new ChangeScorePnl(null, teahouse.TeaHouseAction.Invite);
            Main.inst.addChild(changeScorePnl);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);



        this.seekMember.prompt = "请输入您要查找人的ID";
        this.updatePageMember();

        this.upPageBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            if (this.viewStack.selectedIndex === 0) {
                this.nowPage--;
                this.initViewData(0);
            } else if (this.viewStack.selectedIndex === 2) {
                this.nowPage--;
                this.initViewData(2);
            }
            this.updatePageMember();
        }, this);

        this.nextPageBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            if (this.viewStack.selectedIndex === 0) {
                this.nowPage++;
                this.initViewData(0);
            } else if (this.viewStack.selectedIndex === 2) {
                this.nowPage++;
                this.initViewData(2);
            }
            this.updatePageMember();
        }, this);

        this.excludeMember.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let memberID = parseInt(this.seekMember.text);
            let tagetIndex = 0;
            let searchMember = false;
            GameData.teahouseInfo.Member.forEach((member, index) => {
                if (memberID === member.ID) {
                    tagetIndex = index;
                    searchMember = true;
                }
            }, this);

            if (!searchMember) {
                Tips.addTip("此茶楼无此玩家");
                return;
            }
            this.nowPage = Math.ceil((tagetIndex + 1) / this.pageCount);
            if (this.viewStack.selectedIndex === 0) {
                this.initViewData(0, memberID);
            } else if (this.viewStack.selectedIndex === 2) {
                this.initViewData(2, memberID);
            }
            this.updatePageMember();
        }, this);


    }
    private updatePageMember() {
        let count = Math.ceil(GameData.teahouseInfo.Member.length / this.pageCount);
        this.pageMember.text = this.nowPage + '/' + count;
        if (count <= 1) {
            this.upPageBtn.visible = false;
            this.nextPageBtn.visible = false;

        } else {
            if (this.nowPage < count) {
                this.nextPageBtn.visible = true;
                this.upPageBtn.visible = false;
                if (this.nowPage > 1) {
                    this.upPageBtn.visible = true;
                }
            } else {
                this.nextPageBtn.visible = false;
                this.upPageBtn.visible = true;
            }
        }
    }
    private limitAdmin() {

        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
            if (member.ID === GameData.userInfo.ID) {
                if (member.Limit !== teahouse.TeaHouseLimit.Owner && member.Limit !== teahouse.TeaHouseLimit.Manager) {
                    this.examine.visible = false;
                    this.super.visible = false;
                }
            }
        });


    }

    private onListener() {
        Main.inst.addEventListener(GameData.GameEvent.UpdateTeaHouseMenbersItems, (evt: egret.Event) => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            console.log('刷新列表UpdateItems', evt, evt.data);
            let actionType = evt.data.type;
            let removeItemIndex = evt.data.itemIndex;
            let teahouseInfo = new teahouse.TeaHouseInfoREQ();
            teahouseInfo.ID = [GameData.teahouseID];
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseInfoREQ, teahouse.TeaHouseInfoREQ, teahouseInfo);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, teahouse.TeaHouseInfoACK, (msg: teahouse.TeaHouseInfoACK) => {
                let teahouseInfo = msg.Info[0];
                GameData.teahouseInfo = msg.Info[0];
                teahouseInfo.Member.sort(function (member1, member2) {
                    return member1.Limit - member2.Limit;
                });
                let newMember = [];
                for (let i = 0; i < GameData.teahouseInfo.Member.length; i++) {
                    newMember.push(GameData.teahouseInfo.Member[i]);
                }
                switch (actionType) {
                    case teahouse.TeaHouseAction.Free:
                    case teahouse.TeaHouseAction.Freeze:
                    case teahouse.TeaHouseAction.Remove:
                        {
                            let membersData = newMember;
                            let nowMembersData = [];
                            if (membersData.length <= this.pageCount) {
                                nowMembersData = membersData;
                            } else {
                                let startIdx = (this.nowPage - 1) * this.pageCount;
                                let endIdx = this.nowPage * this.pageCount > membersData.length ? this.nowPage * this.pageCount - membersData.length : this.pageCount;
                                nowMembersData = membersData.splice(startIdx, endIdx);
                                console.log("成员列表", startIdx, endIdx, nowMembersData);
                            }
                            this.membersData.replaceAll(nowMembersData);
                        }
                        break;
                    // case teahouse.TeaHouseAction.Remove:
                    //     {
                    //         this.membersData.removeItemAt(removeItemIndex);
                    //         this.membersList.dataProviderRefreshed;

                    //     }
                    //     break;
                    case teahouse.TeaHouseAction.SetCharger:
                    case teahouse.TeaHouseAction.AddManager:
                    case teahouse.TeaHouseAction.Transfer:
                        {
                            let membersData = newMember;
                            let nowMembersData = [];
                            if (membersData.length <= this.pageCount) {
                                nowMembersData = membersData;
                            } else {
                                let startIdx = (this.nowPage - 1) * this.pageCount;
                                let endIdx = this.nowPage * this.pageCount > membersData.length ? this.nowPage * this.pageCount - membersData.length : this.pageCount;
                                nowMembersData = membersData.splice(startIdx, endIdx);
                                console.log("成员列表", startIdx, endIdx, nowMembersData);
                            }
                            this.seniorData.replaceAll(nowMembersData);
                        }

                        break;
                    default:
                        break;
                }

                MsgMgr.inst.offMsgAll(this);
            }, this);

        }, this);

        Main.inst.addEventListener(GameData.GameEvent.UpdateExamineItem, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            this.searchGroup.visible = false;
            let teahouseMailReq = new login.TeahouseMailREQ();
            teahouseMailReq.HouseID = GameData.teahouseID;
            MsgMgr.inst.sendMsg(login.MailID.ID_TeahouseMailREQ, login.TeahouseMailREQ, teahouseMailReq);
            MsgMgr.inst.onMsg(login.MailID.ID_TeahouseMailACK, login.TeahouseMailACK, (msg: login.TeahouseMailACK) => {
                console.log("茶楼审核邮件", msg);
                // this.examineData = this.examineList.dataProvider = new eui.ArrayCollection(msg.Mail);
                // this.examineList.itemRenderer = ExamineItem;
                // this.examineScroll.viewport = this.examineList;
                this.examineData.replaceAll(msg.Mail);
                // this.examineData.refresh();
                // this.examineList.dataProviderRefreshed;
                this.noRecord.visible = msg.Mail.length > 0 ? false : true;
                MsgMgr.inst.offMsgAll(this);
            }, this);
        }, this);

        Main.inst.addEventListener(GameData.GameEvent.UpdateMyMember, () => {
            // console.log('sssssssssssss');
            // let newMember = [];
            // for (let i = 0; i < GameData.teahouseInfo.Member.length; i++) {
            //     newMember.push(GameData.teahouseInfo.Member[i]);
            // }

            // let membersData = newMember;
            // let nowMembersData = [];
            // if (membersData.length <= this.pageCount) {
            //     nowMembersData = membersData;
            // } else {
            //     let startIdx = (this.nowPage - 1) * this.pageCount;
            //     let endIdx = this.nowPage * this.pageCount > membersData.length ? this.nowPage * this.pageCount - membersData.length : this.pageCount;
            //     nowMembersData = membersData.splice(startIdx, endIdx);
            //     console.log("成员列表", startIdx, endIdx, nowMembersData);
            // }
            // if (this.membersData) {
            //     this.membersData.replaceAll(nowMembersData);
            // }

        }, this);
    }

    private onChange(e: eui.UIEvent) {
        let radioGroup: eui.RadioButtonGroup = e.target;
        this.viewStack.selectedIndex = radioGroup.selectedValue;
        console.log('TeahouseMembers', this.viewStack.selectedIndex);
        if (radioGroup.$name !== 'TeahouseMember') return;
        switch (this.viewStack.selectedIndex) {
            case 0:
            case 2:
                this.searchGroup.visible = true;
                this.nowPage = 1;
                this.updatePageMember();
                this.initViewData(this.viewStack.selectedIndex);
                break
            case 1:
                {
                    this.searchGroup.visible = false;
                    let teahouseMailReq = new login.TeahouseMailREQ();
                    teahouseMailReq.HouseID = GameData.teahouseID;
                    MsgMgr.inst.sendMsg(login.MailID.ID_TeahouseMailREQ, login.TeahouseMailREQ, teahouseMailReq);
                    MsgMgr.inst.onMsg(login.MailID.ID_TeahouseMailACK, login.TeahouseMailACK, (msg: login.TeahouseMailACK) => {
                        console.log("茶楼审核邮件", msg);
                        this.examineData = this.examineList.dataProvider = new eui.ArrayCollection(msg.Mail);
                        this.examineList.itemRenderer = ExamineItem;
                        this.examineScroll.viewport = this.examineList;
                        this.noRecord.visible = msg.Mail.length > 0 ? false : true;
                        MsgMgr.inst.offMsgAll(this);
                    }, this);
                }

                break;
            default:
                this.noRecord.visible = true;
                break;
        }
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private initViewData(type, memberID?) {
        let teahouseInfo = new teahouse.TeaHouseInfoREQ();
        teahouseInfo.ID = [GameData.teahouseID];
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseInfoREQ, teahouse.TeaHouseInfoREQ, teahouseInfo);

        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, teahouse.TeaHouseInfoACK, (msg: teahouse.TeaHouseInfoACK) => {
            console.log('查询当前茶楼信息', msg);
            if (msg.Info.length === 0) return;
            let teahouseInfo = msg.Info[0];
            GameData.teahouseInfo = msg.Info[0];
            GameData.teahouseInfo.Member.sort(function (member1, member2) {
                return member1.Limit - member2.Limit;
            });
            let newMember = [];
            for (let i = 0; i < GameData.teahouseInfo.Member.length; i++) {
                if (typeof memberID !== 'undefined') {
                    GameData.teahouseInfo.Member[i]['SearchMemberID'] = memberID;
                } else {
                    GameData.teahouseInfo.Member[i]['SearchMemberID'] = 0;
                }
                newMember.push(GameData.teahouseInfo.Member[i]);
            }
            if (type === 0) {
                let membersData = newMember;
                let nowMembersData = [];
                if (membersData.length <= this.pageCount) {
                    nowMembersData = membersData;
                } else {
                    let startIdx = (this.nowPage - 1) * this.pageCount;
                    let endIdx = this.nowPage * this.pageCount > membersData.length ? membersData.length - (this.nowPage - 1) * this.pageCount : this.pageCount;
                    nowMembersData = membersData.splice(startIdx, endIdx);
                    console.log("成员列表", startIdx, endIdx, nowMembersData);
                }

                this.membersData = this.membersList.dataProvider = new eui.ArrayCollection(nowMembersData);
                this.membersList.itemRenderer = MembersItem;
                this.memberScroll.viewport = this.membersList;
                this.membersData.refresh();
                this.noRecord.visible = membersData.length > 0 ? false : true;
            } else if (type === 2) {
                let seniorManagementData = newMember;

                let nowMembersData = [];
                if (seniorManagementData.length <= this.pageCount) {
                    nowMembersData = seniorManagementData;
                } else {
                    let startIdx = (this.nowPage - 1) * this.pageCount;
                    let endIdx = this.nowPage * this.pageCount > seniorManagementData.length ? seniorManagementData.length - (this.nowPage - 1) * this.pageCount : this.pageCount;
                    nowMembersData = seniorManagementData.splice(startIdx, endIdx);
                    console.log(startIdx, endIdx);
                }

                this.seniorData = this.seniorList.dataProvider = new eui.ArrayCollection(nowMembersData);
                this.seniorList.itemRenderer = SeniorManagementItem;
                this.seniorScroll.viewport = this.seniorList;
                this.seniorData.refresh();
                this.noRecord.visible = seniorManagementData.length > 0 ? false : true;
            }
            MsgMgr.inst.offMsgAll(this);

        }, this);
    }


}



//成员列表
class MembersItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "skins.teahouse.MembersItem";

    }
    private headImg: eui.Image;
    private nickName: eui.Label;
    private invitationCode: eui.Label;
    private id: eui.Label;
    private frozen: eui.Button;
    private remove: eui.Button;
    private frozened: eui.Label;

    private manger: eui.Image;
    private partner: eui.Image;
    private boss: eui.Image;
    private light: eui.Image;

    private selfData: teahouse.ITeaHouseMember;
    protected childrenCreated() {
        super.childrenCreated();



        this.frozen.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            if (this.selfData.State === teahouse.MemberState.Frozen) {
                joinTeaHouseReq.Action = teahouse.TeaHouseAction.Free;
            } else {
                joinTeaHouseReq.Action = teahouse.TeaHouseAction.Freeze;
            }
            joinTeaHouseReq.UserID = this.selfData.ID;
            joinTeaHouseReq.HouseID = GameData.teahouseID;
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('冻结玩家', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('操作返回', msg);
                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                if (msg.Code === 0) {
                    //冻结
                    if (msg.Action === teahouse.TeaHouseAction.Freeze) {
                        this.frozen.label = '解冻玩家';
                        this.frozened.visible = true;
                    } else {
                        this.frozen.label = '冻结玩家';
                        this.frozened.visible = false;
                    }

                }
                Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMenbersItems, false, { type: msg.Action, itemIndex: this.itemIndex });
            }, this);
        }, this);
        this.remove.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            // let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            // joinTeaHouseReq.Action = teahouse.TeaHouseAction.Remove;
            // joinTeaHouseReq.UserID = this.selfData.ID;
            // joinTeaHouseReq.HouseID = GameData.teahouseID;
            // MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            // console.log('移除玩家', joinTeaHouseReq);
            // MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
            //     console.log('操作返回', msg);
            //     if (msg.Code === 0) {
            //         if (msg.Action === teahouse.TeaHouseAction.Remove) {
            //             this.parent.removeChild(this);

            //         }
            //     }
            //     MsgMgr.inst.offMsgAll(this);
            //     Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMenbersItems, false, { type: msg.Action, itemIndex: this.itemIndex });
            // }, this);


            let obj = { action: teahouse.TeaHouseAction.Remove, isTrue: true, tagetID: this.selfData.ID, itemidx: this.itemIndex, tag: this }
            let str = "是否确认移除" + this.selfData.NickName + "玩家?";
            let partnerApplyPnl = new PartnerApplyPnl(str, obj);
            Main.inst.addChild(partnerApplyPnl);

        }, this);



    }

    private limitAdmin() {
        let ismanger = false;
        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
            if (member.ID === GameData.userInfo.ID) {

                if (member.Limit === teahouse.TeaHouseLimit.Owner || member.Limit === teahouse.TeaHouseLimit.Manager) {
                    this.frozen.visible = true;
                    this.remove.visible = true;
                    if (member.ID === this.selfData.ID) {
                        this.frozen.visible = false;
                        this.remove.visible = false;
                    }
                } else {
                    this.frozen.visible = false;
                    this.remove.visible = false;
                }
                if (member.Limit === teahouse.TeaHouseLimit.Manager) ismanger = true;
            }


            if (member.ID === this.selfData.ID) {        //判断要冻结的人是管理员还是 合伙人 这两个不能被冻结

                if (member.Limit === teahouse.TeaHouseLimit.Owner) {
                    this.boss.visible = true;
                } else if (member.Limit === teahouse.TeaHouseLimit.Manager) {
                    this.manger.visible = true;
                    // this.frozen.visible = false;
                } else if (member.Limit === teahouse.TeaHouseLimit.Partner) {
                    this.partner.visible = true;
                    // this.frozen.visible = false;
                } else {
                    this.boss.visible = false;
                    this.manger.visible = false;
                    this.partner.visible = false;
                }


            }
        });

        if (ismanger) {
            GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                if (this.selfData.ID === member.ID) {
                    if (member.Limit === teahouse.TeaHouseLimit.Owner
                        || member.Limit === teahouse.TeaHouseLimit.Manager || member.Limit === teahouse.TeaHouseLimit.Partner) {
                        this.frozen.visible = false;
                        this.remove.visible = false;
                    } else {
                        this.frozen.visible = true;
                        this.remove.visible = true;
                    }
                }
            });
        }


    }
    protected dataChanged() {
        this.selfData = this.data;
        this.initView();

        //限制条件 
        this.limitAdmin();
    }

    private initView() {
        this.nickName.text = this.selfData.NickName;
        this.invitationCode.text = "邀请码:" + this.selfData.Invitor;
        this.id.text = this.selfData.ID.toString();
        let label = "";
        if (this.selfData.State === 0) {
            label = "冻结玩家";
            this.frozened.visible = false;
        } else if (this.selfData.State === 1) {
            label = '解冻玩家';
            this.frozened.visible = true;
        }
        this.frozen.label = label;
        GameData.handleHeadImg(this.headImg, this.selfData.HeadIcon);
        if (this.data.SearchMemberID === this.selfData.ID) {
            this.light.visible = true;
        }

        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {

            if (GameData.userInfo.ID === member.ID) {
                if (member.Limit === teahouse.TeaHouseLimit.Owner
                    || member.Limit === teahouse.TeaHouseLimit.Manager) {

                } else {
                    if (GameData.teahouseInfo.Config.HideUserID) {
                        let idStr = this.selfData.ID.toString().split('');
                        let n = idStr.splice(3, idStr.length);
                        let nowStr = idStr[0] + idStr[1] + idStr[2] + "***";
                        this.id.text = nowStr;
                    }
                }
            }

            // if (GameData.teahouseInfo.Config.HideUserID) {

            //     if (this.selfData.ID === member.ID && (member.Limit === teahouse.TeaHouseLimit.Member || member.Limit === teahouse.TeaHouseLimit.Partner)) {
            //         this.id.text = "******";
            //     }
            // }
        });

    }

}
//审核管理
class ExamineItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "skins.teahouse.ExamineItem";

    }
    private headImg: eui.Image;
    private nickname: eui.Label;
    private id: eui.Label;

    private refuse: eui.Button;
    private agree: eui.Button;
    private selfData = null;
    private touchTag = -1;
    protected childrenCreated() {
        super.childrenCreated();


        this.refuse.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let teaHouseOrderReq = new teahouse.TeaHouseHandleOrderREQ();
            teaHouseOrderReq.OrderID = this.selfData.OrderID;
            teaHouseOrderReq.Accept = false;
            this.touchTag = 1;
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseHandleOrderREQ, teahouse.TeaHouseHandleOrderREQ, teaHouseOrderReq);
        }, this);
        this.agree.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let teaHouseOrderReq = new teahouse.TeaHouseHandleOrderREQ();
            teaHouseOrderReq.OrderID = this.selfData.OrderID;
            teaHouseOrderReq.Accept = true;
            this.touchTag = 2;
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseHandleOrderREQ, teahouse.TeaHouseHandleOrderREQ, teaHouseOrderReq);
        }, this);


        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseHandleOrderACK, teahouse.TeaHouseHandleOrderACK, (msg: teahouse.TeaHouseHandleOrderACK) => {
            console.log("msg", msg.Code, msg);
            if (msg.Code === 0) {
                if (this.touchTag === 1) {
                    Tips.addTip("已拒绝玩家申请");
                } else if (this.touchTag === 2) {
                    Tips.addTip("已同意玩家申请");
                }
                Main.inst.dispatchEventWith(GameData.GameEvent.UpdateExamineItem);
            } else {
                Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
            }
            MsgMgr.inst.offMsgAll(this);
        }, this);
        //限制条件 
        this.limitAdmin();

    }

    private limitAdmin() {

        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
            if (member.ID === GameData.userInfo.ID) {
                if (member.Limit !== teahouse.TeaHouseLimit.Owner && member.Limit !== teahouse.TeaHouseLimit.Manager) {
                    this.agree.visible = false;
                    this.refuse.visible = false;
                }
            }
        });

    }
    protected dataChanged() {
        console.log("change data", this.data);
        this.selfData = this.data;
        this.initView();

    }

    private initView() {
        this.nickname.text = this.selfData.Player.NickName;
        this.id.text = this.selfData.Player.UserID;
        GameData.handleHeadImg(this.headImg, this.selfData.HeadIcon);
    }

}

//高级管理
class SeniorManagementItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "skins.teahouse.SeniorManagementItem";

    }

    private nickname: eui.Label;
    private headImg: eui.Image;
    private invitationCode: eui.Label;
    private id: eui.Label;
    private payment: eui.Button;
    private administrators: eui.Button;
    private transfer: eui.Button;
    private manger: eui.Image;
    private partner: eui.Image;
    private boss: eui.Image
    private frozened: eui.Label;
    private light: eui.Image;
    private selfData = null;
    protected childrenCreated() {
        super.childrenCreated();

        this.payment.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            if (this.selfData.ID === GameData.teahouseInfo.Charger) {
                joinTeaHouseReq.UserID = this.queryBoosId();
            } else {
                joinTeaHouseReq.UserID = this.selfData.ID;
            }
            joinTeaHouseReq.Action = teahouse.TeaHouseAction.SetCharger;

            joinTeaHouseReq.HouseID = GameData.teahouseID;
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('设为代付', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('操作返回', msg);
                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                if (msg.Code === 0) {
                    Tips.addTip('已发送代付邀请到玩家邮箱');
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMenbersItems, false, { type: msg.Action, itemIndex: this.itemIndex });
                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
            }, this);
        }, this);
        this.administrators.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            joinTeaHouseReq.Action = teahouse.TeaHouseAction.AddManager;
            joinTeaHouseReq.UserID = this.selfData.ID;
            joinTeaHouseReq.HouseID = GameData.teahouseID;
            if (this.manger.visible === true) {
                joinTeaHouseReq.SetLimit = false;
            } else {
                joinTeaHouseReq.SetLimit = true;
            }

            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('设为管理员', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('操作返回', msg);
                if (msg.Code === 0) {
                    if (msg.Action === teahouse.TeaHouseAction.AddManager) {
                        Tips.addTip('设为管理员成功');
                        Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMenbersItems, false, { type: msg.Action, itemIndex: this.itemIndex });
                    }
                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
                MsgMgr.inst.offMsgAll(this);
            }, this);
        }, this);
        this.transfer.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            //这里要加转让给谁的id 得二级界面
            let changeScorePnl = new ChangeScorePnl(parseInt(this.selfData.ID.toString()), teahouse.TeaHouseAction.Transfer, false, this.itemIndex);
            Main.inst.addChild(changeScorePnl);

            // let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            // joinTeaHouseReq.Action = teahouse.TeaHouseAction.Transfer;
            // joinTeaHouseReq.UserID = this.selfData.ID;
            // joinTeaHouseReq.HouseID = GameData.teahouseID;
            // MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            // console.log('转让茶楼', joinTeaHouseReq);
            // MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
            //     console.log('操作返回', msg);
            //     if (msg.Code === 0) {
            //         if (msg.Action === teahouse.TeaHouseAction.Transfer) {
            //             Tips.addTip('已发送转让茶楼申请到玩家邮箱');
            //         }
            //     } else {
            //         Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
            //     }
            //     MsgMgr.inst.offMsgAll(this);
            //     Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMenbersItems, false, { type: msg.Action, itemIndex: this.itemIndex });
            // }, this);


        }, this);

    }

    private queryBoosId() {
        let bossId;
        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
            if (member.Limit === teahouse.TeaHouseLimit.Owner) {
                bossId = member.ID;
            }
        }, this);
        return bossId;
    }

    private limitAdmin() {
        this.payment.visible = false;
        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {

            if (member.ID === GameData.userInfo.ID) {
                if (member.Limit === teahouse.TeaHouseLimit.Owner && member.ID !== this.selfData.ID) {
                    this.payment.visible = true;
                    this.administrators.visible = true;
                    this.transfer.visible = true;
                } else {
                    this.payment.visible = false;
                    this.administrators.visible = false;
                    this.transfer.visible = false;
                }
            }
            if (member.ID === this.selfData.ID) {
                if (member.Limit === teahouse.TeaHouseLimit.Owner) {
                    this.boss.visible = true;
                } else if (member.Limit === teahouse.TeaHouseLimit.Manager) {
                    this.manger.visible = true;
                    if (this.selfData.ID !== GameData.userInfo.ID) {
                        this.payment.visible = true;
                    } else {
                        this.payment.visible = false;
                    }
                    this.administrators.label = "取消管理员"
                } else if (member.Limit === teahouse.TeaHouseLimit.Partner) {
                    this.administrators.visible = false;  //合夥人不能設置為管理員
                    this.partner.visible = true;
                    this.payment.visible = false;
                } else {
                    this.boss.visible = false;
                    this.manger.visible = false;
                    this.partner.visible = false;
                    this.payment.visible = false;
                    this.administrators.label = "设为管理员"
                }
            }

        });
        if (this.selfData.State === 0) {
            this.frozened.visible = false;
        } else if (this.selfData.State === 1) {
            this.frozened.visible = true;
        }


        if (this.selfData.ID === GameData.teahouseInfo.Charger) {
            this.payment.label = "取消代付";
        } else {
            this.payment.label = "设为代付";
        }
        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
            if (member.ID === GameData.userInfo.ID && member.Limit === teahouse.TeaHouseLimit.Manager) {
                this.payment.visible = false;
            }
        });


    }
    protected dataChanged() {
        this.selfData = this.data;
        this.initView();
        //限制条件 
        this.limitAdmin();
    }

    private initView() {
        this.nickname.text = this.selfData.NickName;
        this.invitationCode.text = "邀请码:" + this.selfData.Invitor;
        this.id.text = this.selfData.ID;
        GameData.handleHeadImg(this.headImg, this.selfData.HeadIcon);
        if (this.selfData.ID === GameData.userInfo.ID) {
            this.payment.visible = false;
            this.administrators.visible = false;
            this.transfer.visible = false;
        }

        if (this.selfData.ID === this.selfData.Charger) {
            this.payment.label = "取消代付";
        } else {
            this.payment.label = "设为代付";
        }

        if (this.data.SearchMemberID === this.selfData.ID) {
            this.light.visible = true;
        }
    }

}

