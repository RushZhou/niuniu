class TeaHouseMgr extends eui.Component {
    public constructor() {
        super();
        this.skinName = skins.teahouse.TeaHouseMgrPnl;
    }
    private closeBtn: eui.Button;
    private radio: eui.RadioButton;
    private saveChange: eui.Button;

    private viewStack: eui.ViewStack;


    private joinClub: eui.List;
    private myClub: eui.List;



    private examine: eui.CheckBox;
    private ranking: eui.CheckBox;
    private closeClub: eui.CheckBox;
    private privacy: eui.CheckBox;
    private enterClubName: eui.TextInput;
    private clubPublicMsg: eui.TextInput;

    private joinClubBar: eui.VScrollBar;
    private myClubBar: eui.VScrollBar;

    private manger: eui.Group;
    private tips: eui.Group;
    private joinHouseData: eui.ArrayCollection;
    private myClubData: eui.ArrayCollection;
    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);

        this.radio.group.$name = 'teahouseMgrPnl';
        this.radio.group.addEventListener(eui.UIEvent.CHANGE, this.onChange, this);


        this.enterClubName.prompt = "请输入茶楼名称";
        this.clubPublicMsg.prompt = "请输入公告，比如邀请玩家加入您的微信群等等";


        this.sendNetMsg();
        this.onNetMsg();

        Main.inst.addEventListener(GameData.GameEvent.UpdateTeaHouseMgr, (evt: egret.Event) => {
            console.log("茶楼管理信息改变-----------", evt.data);
            if (evt.data.type === 'dismissTeaHouse') {
                this.myClubData.removeItemAt(evt.data.index);
                this.myClub.dataProviderRefreshed;
            } else if (evt.data.type === 'joinQuitTeaHouse') {
                this.joinHouseData.removeItemAt(evt.data.index);
                this.joinClub.dataProviderRefreshed;
            } else if (evt.data.type === 'myQuitTeaHouse') {
                this.myClubData.removeItemAt(evt.data.index);
                this.myClub.dataProviderRefreshed;
            }
        }, this);
    }
    private sendNetMsg() {
        //茶楼设置
        this.saveChange.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onChangeHouseInfoFunc, this);

        //入驻茶楼 
        //查询加入了哪些茶楼
        let queryTeaHouseReq = new teahouse.TeaHouseJoinedREQ();
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseJoinedREQ, teahouse.TeaHouseJoinedREQ, queryTeaHouseReq);



    }
    private onNetMsg() {
        //茶楼设置修改返回
        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseChangeInfoACK, teahouse.TeaHouseChangeInfoACK, (msg: teahouse.TeaHouseChangeInfoACK) => {
            MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseChangeInfoACK, this);
            console.log('修改设置返回参数', msg, msg.Code);
            if (msg.Code === 0) {
                Tips.addTip("修改设置成功");
                GameData.teahouseInfo.Config = msg.Config;
                Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMgr, false, { type: 'changeSetting' });
            } else {
                Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
            }
        }, this);
        //查询加入了哪些茶楼返回
        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeeHouseJoinedACK, teahouse.TeeHouseJoinedACK, (msg: teahouse.TeeHouseJoinedACK) => {
            console.log('查询茶楼房间返回1  ', msg);

            //根据茶楼ID查找茶楼信息
            let teahouseInfo = new teahouse.TeaHouseInfoREQ();
            teahouseInfo.ID = msg.ID;
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseInfoREQ, teahouse.TeaHouseInfoREQ, teahouseInfo);

        }, this);

        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, teahouse.TeaHouseInfoACK, (msg: teahouse.TeaHouseInfoACK) => {
            console.log('查询茶楼房间返回2  ', msg);
            MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, this);
            if (msg.Info.length === 0) return;
            let joinHouseListmsg = [];//入驻茶楼
            let myClubListmsg = [];//我的茶楼
            for (let i = 0; i < msg.Info.length; i++) {
                if (this.judgeBoss(msg.Info[i].Member)) {//自己是茶楼老板 管理员或者合伙人
                    myClubListmsg.push(msg.Info[i]);
                } else {
                    joinHouseListmsg.push(msg.Info[i]);
                }

            }
            this.joinHouseData = new eui.ArrayCollection(joinHouseListmsg);
            this.joinClub.dataProvider = this.joinHouseData;
            this.joinClub.itemRenderer = JoinHouseItem;
            this.joinClubBar.viewport = this.joinClub;
            this.myClubData = new eui.ArrayCollection(myClubListmsg);
            this.myClub.dataProvider = this.myClubData;
            this.myClub.itemRenderer = MyHouseItem;
            this.myClubBar.viewport = this.myClub;

            this.initNowTeaHouseInfo(msg.Info);
        }, this);

    }
    private initNowTeaHouseInfo(data: teahouse.ITeaHouseInfo[]) {
        for (let i = 0; i < data.length; i++) {
            if (data[i].ID === GameData.teahouseID) {
                this.examine.selected = data[i].Config.Verify;
                this.ranking.selected = data[i].Config.ViewRank;
                this.closeClub.selected = data[i].Config.Close;
                this.privacy.selected = data[i].Config.HideUserID;
                this.enterClubName.text = data[i].Config.Name;
                this.clubPublicMsg.text = data[i].Config.Notice;
            }
        }
    }
    //判断自己是不是老板
    private judgeBoss(data) {
        let isBoss = false;
        for (let i = 0; i < data.length; i++) {
            if (data[i].ID === GameData.userInfo.ID) { //自己是茶楼老板 合伙人 管理员
                if (data[i].Limit === teahouse.TeaHouseLimit.Owner
                    || data[i].Limit === teahouse.TeaHouseLimit.Manager) {//|| data[i].Limit === teahouse.TeaHouseLimit.Partner
                    isBoss = true;
                }
            }
        }
        return isBoss;
    }
    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.radio.group.removeEventListener(eui.UIEvent.CHANGE, this.onChange, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
    private onChange(e: eui.UIEvent) {
        console.log('teahouseMgrPnl', 'radio');
        let radioGroup: eui.RadioButtonGroup = e.target;
        if (radioGroup.$name !== 'teahouseMgrPnl') return;
        this.viewStack.selectedIndex = radioGroup.selectedValue;

        if (this.viewStack.selectedIndex === 2) {
            let isBoss = false;
            GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                if (member.ID === GameData.userInfo.ID && member.Limit === teahouse.TeaHouseLimit.Owner) {
                    isBoss = true;
                }
            });

            if (isBoss) {
                this.manger.visible = true;
            } else {
                this.tips.visible = true;
            }
        } else if (this.viewStack.selectedIndex === 0) {//入驻茶楼
            this.joinClub.dataProviderRefreshed;
            this.joinHouseData.refresh();
        } else if (this.viewStack.selectedIndex === 1) {//我的茶楼
            this.myClub.dataProviderRefreshed;
            this.myClubData.refresh();
        }
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private onChangeHouseInfoFunc() {
        SoundManager.ins.playEffect('sfx_button_click_mp3');
        let examine = this.examine.selected;
        let ranking = this.ranking.selected;
        let closeClub = this.closeClub.selected;
        let privacy = this.privacy.selected;
        let enterClubName = this.enterClubName.text;
        let clubPublicMsg = this.clubPublicMsg.text;
        if (enterClubName.split('').length > 6) {
            Tips.addTip('茶馆名字应不大于6个字');
            return;
        }
        if (clubPublicMsg.split('').length > 50) {
            Tips.addTip('茶馆公告应不大于50个字');
            return;
        }
        console.log('------------', examine, ranking, closeClub, privacy, enterClubName, clubPublicMsg);

        let changeTeaHouseInfoReq = new teahouse.TeaHouseChangeInfoREQ();
        changeTeaHouseInfoReq.HouseID = GameData.teahouseID;
        let teahouseConfig = new teahouse.TeaHouseConfig();

        teahouseConfig.Close = closeClub;
        teahouseConfig.ViewRank = ranking;
        teahouseConfig.Notice = clubPublicMsg;
        teahouseConfig.Name = enterClubName;
        teahouseConfig.Verify = examine;
        teahouseConfig.HideUserID = privacy;
        changeTeaHouseInfoReq.Config = teahouseConfig;

        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseChangeInfoREQ, teahouse.TeaHouseChangeInfoREQ, changeTeaHouseInfoReq);

    }



}

//入驻茶楼
class JoinHouseItem extends eui.ItemRenderer {
    private headImg: eui.Image;
    private nickName: eui.Label;
    private teahouseName: eui.Label;
    private id: eui.Label;
    private enter: eui.Button;
    private quit: eui.Button;

    private isBoss: eui.Image;
    private isManager: eui.Image;

    private selfData = null;
    constructor() {
        super();
        this.skinName = skins.teahouse.JoinTeaHouseItem;
    }


    protected childrenCreated() {
        super.childrenCreated();
        this.enter.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            if (this.selfData.ID === GameData.teahouseID) {
                Tips.addTip("自己正在此茶馆 不用进入");
                return;
            }
            //进入别人的茶楼
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            joinTeaHouseReq.Action = teahouse.TeaHouseAction.Join;
            joinTeaHouseReq.UserID = GameData.userInfo.ID;
            joinTeaHouseReq.HouseID = this.selfData.ID;
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('进入茶楼', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('进入茶楼返回', msg, msg.Code);
                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                if (msg.Code === 0 || msg.Code === 5) {
                    MsgMgr.inst.offMsgAll(this)
                    this.parent.removeChild(this);
                    let teahouse = Main.inst.getChildByName('teahouse');
                    let teahouseMgr = Main.inst.getChildByName('teaHouseMgrPnl');
                    MsgMgr.inst.offMsgAll(teahouseMgr);
                    Main.inst.removeChild(teahouseMgr);
                    MsgMgr.inst.offMsgAll(teahouse);
                    Main.inst.removeChild(teahouse);
                    console.log('要进入的茶楼id', msg.HouseID);
                    //进入要进入的茶楼
                    GameData.EvtEnterTeahouse(msg.HouseID);
                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
            }, this);
        }, this);
        this.quit.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            //退出别人的茶楼
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            joinTeaHouseReq.Action = teahouse.TeaHouseAction.Exit;
            joinTeaHouseReq.UserID = GameData.userInfo.ID;
            joinTeaHouseReq.HouseID = this.selfData.ID;
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('退出茶楼', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('退出茶楼返回', msg, msg.Code);
                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                if (msg.Code === 0 || msg.Code === 5) {  //退出自己的现在的茶楼
                    MsgMgr.inst.offMsgAll(this)
                    this.parent.removeChild(this);
                    //退出的茶楼正式我现在呆的茶楼
                    if (this.selfData.ID === GameData.teahouseID) {
                        let teahouse = Main.inst.getChildByName('teahouse');
                        let teahouseMgr = Main.inst.getChildByName('teaHouseMgrPnl');
                        MsgMgr.inst.offMsgAll(teahouseMgr);
                        Main.inst.removeChild(teahouseMgr);
                        MsgMgr.inst.offMsgAll(teahouse);
                        Main.inst.removeChild(teahouse);
                    }
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMgr, false, { type: "joinQuitTeaHouse" });

                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
            }, this);
        }, this);



        // GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
        //     if (GameData.userInfo.ID === member.ID) {
        //         if (member.Limit === teahouse.TeaHouseLimit.Owner) this.isBoss.visible = true;  //判断自己是不是老板
        //         if (member.Limit === teahouse.TeaHouseLimit.Manager) this.isManager.visible = true;//判断自己是不是管理员
        //     }
        // });


    }
    protected dataChanged() {
        this.selfData = this.data;
        this.nickName.text = this.searchBossInfo(this.selfData.Member).NickName;
        this.id.text = this.searchBossInfo(this.selfData.Member).ID.toString();
        this.teahouseName.text = this.selfData.Config.Name + "(" + this.selfData.ID + ")" + "成员" + this.selfData.Member.length + "人";
        GameData.handleHeadImg(this.headImg, this.selfData.HeadIcon);
        if (GameData.teahouseID === this.selfData.ID) this.enter.visible = false;
    }
    //查找老板 返回老板信息
    private searchBossInfo(data) {
        let bossInfo: teahouse.ITeaHouseMember = null;
        for (let i = 0; i < data.length; i++) {
            if (data[i].Limit === teahouse.TeaHouseLimit.Owner) {
                bossInfo = data[i];
            }
        }
        return bossInfo;
    }
}

//我的茶楼
class MyHouseItem extends eui.ItemRenderer {

    public headImg: eui.Image;
    public nickName: eui.Label;
    public teahouseName: eui.Label;
    public rule: eui.Label;
    public id: eui.Label;
    public dismiss: eui.Button;
    public modify: eui.Button;
    public quit: eui.Button;

    private selfData = null;
    constructor() {
        super();
        this.skinName = skins.teahouse.MyTeaHouseItem;
    }

    protected childrenCreated() {
        super.childrenCreated();

        this.dismiss.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            //解散自己的茶楼 自己是老板
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            joinTeaHouseReq.Action = teahouse.TeaHouseAction.Dismiss;
            joinTeaHouseReq.UserID = GameData.userInfo.ID;
            joinTeaHouseReq.HouseID = this.selfData.ID;
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('解散茶楼', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('解散茶楼返回', msg, msg.Code);
                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                if (msg.Code === 0) {  //退出自己的现在的茶楼

                    MsgMgr.inst.offMsgAll(this);
                    this.parent.removeChild(this);
                    //退出的茶楼正式我现在呆的茶楼
                    if (this.selfData.ID === GameData.teahouseID) {
                        let teahouse = Main.inst.getChildByName('teahouse');
                        let teahouseMgr = Main.inst.getChildByName('teaHouseMgrPnl');
                        MsgMgr.inst.offMsgAll(teahouseMgr);
                        Main.inst.removeChild(teahouseMgr);
                        MsgMgr.inst.offMsgAll(teahouse);
                        Main.inst.removeChild(teahouse);

                    }
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMgr, false, { type: "dismissTeaHouse", index: this.itemIndex });
                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
            }, this);
        }, this);


        this.quit.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            //退出别人的茶楼
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            joinTeaHouseReq.Action = teahouse.TeaHouseAction.Exit;
            joinTeaHouseReq.UserID = GameData.userInfo.ID;
            joinTeaHouseReq.HouseID = this.selfData.ID;
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('退出茶楼', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('退出茶楼返回', msg, msg.Code);
                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                if (msg.Code === 0 || msg.Code === 5) {  //退出自己的现在的茶楼
                    MsgMgr.inst.offMsgAll(this)
                    this.parent.removeChild(this);
                    //退出的茶楼正式我现在呆的茶楼
                    if (this.selfData.ID === GameData.teahouseID) {
                        let teahouse = Main.inst.getChildByName('teahouse');
                        let teahouseMgr = Main.inst.getChildByName('teaHouseMgrPnl');
                        MsgMgr.inst.offMsgAll(teahouseMgr);
                        Main.inst.removeChild(teahouseMgr);
                        MsgMgr.inst.offMsgAll(teahouse);
                        Main.inst.removeChild(teahouse);
                    }
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMgr, false, { type: "myQuitTeaHouse", index: this.itemIndex });

                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
            }, this);
        }, this);


        this.modify.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            //修改自己的茶楼 随意修改
        }, this);
    }
    protected dataChanged() {
        this.selfData = this.data;
        this.nickName.text = this.searchBossInfo(this.selfData.Member).NickName;
        this.id.text = this.selfData.OwnerID.toString();
        this.teahouseName.text = this.selfData.Config.Name + "(" + this.selfData.ID + ") 成员" + this.selfData.Member.length + "人";
        this.rule.text = this.selfData.Ruler;
        GameData.handleHeadImg(this.headImg, this.selfData.HeadIcon);
        if (this.searchBossInfo(this.selfData.Member).ID === GameData.userInfo.ID) {
            GameData.teahouseInfo = this.data;
        }
        for (let i = 0; i < this.selfData.Member.length; i++) {
            if (this.selfData.Member[i].ID === GameData.userInfo.ID) {//找到自己
                if (this.selfData.Member[i].Limit === teahouse.TeaHouseLimit.Manager) {//判断自己是管理员还是老板
                    this.quit.visible = true;
                    this.dismiss.visible = false;
                } else {
                    this.quit.visible = false;
                    this.dismiss.visible = true;
                }
            }

        }

    }
    //查找老板 返回老板信息
    private searchBossInfo(data) {
        let bossInfo: teahouse.ITeaHouseMember = null;
        for (let i = 0; i < data.length; i++) {
            if (data[i].Limit === teahouse.TeaHouseLimit.Owner) {
                bossInfo = data[i];
            }
        }
        return bossInfo;
    }

}