class Teahouse extends eui.Component {
    public teaID: eui.Label;
    public deskList: eui.List;
    public scroller: eui.Scroller;
    public quit: eui.Button;
    public createTable: eui.Button;

    private arrTables: eui.ArrayCollection = new eui.ArrayCollection();

    private teahouseManagement: eui.Button;//茶楼管理
    private teahouseMembers: eui.Button;//茶楼成员
    private rankingList: eui.Button;//排行榜
    private record: eui.Button; //战绩
    private publicMsg: eui.Button;//公告
    private msg: eui.Button;
    private share: eui.Button;
    private Partner: eui.Button;
    private openScoreStore: eui.Button;


    private diamond: eui.Label;
    private clubScore: eui.Label;
    private clubScoreBg: eui.Group;


    private nickName: eui.Label;
    private teaHouseName: eui.Label;
    private userId: eui.Label;
    private teaClose: eui.Label;

    private teaHouseBg: eui.Image;

    private diamondBtn: eui.Button;
    private scoreBtn: eui.Button;
    private sex: eui.Image;
    private btnHead: eui.Button;


    private head: eui.Image;

    private tabelListData: eui.ArrayCollection;

    private emailRed: eui.Image;
    private mangerRed: eui.Image;

    private noticBg: eui.Image;
    private noticGroup: eui.Group;
    private noticLabl: eui.Label;

    private showTip: number = 0;
    public constructor() {
        super();


        this.skinName = skins.teahouse.TeaHouse;
        console.log(this.deskList, '-----')
        this.tabelListData = this.deskList.dataProvider = this.arrTables;
        this.deskList.layout = new eui.HorizontalLayout();
        this.deskList.itemRenderer = TeahouseTable;
        this.deskList.itemRendererSkinName = skins.teahouse.Table;
        this.scroller.viewport = this.deskList;

        this.diamond.text = GameData.userInfo.Diamond.toString();
        this.userId.text = GameData.userInfo.ID.toString();
        this.nickName.text = GameData.userInfo.Nickname;

        GameData.handleHeadImg(this.head, GameData.userInfo.HeadIcon);

        this.noticGroup.mask = this.noticBg;

        egret.Tween.get(this.noticLabl, { loop: true })
            .to({ x: -700 }, 12800).to({ x: 1280 }, 1);



        this.quit.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let quitTeaHouseReq = new niuniu.DisObserverTableListREQ();
            quitTeaHouseReq.HouseID = GameData.teahouseID;
            MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_DisObserverTableListREQ, niuniu.DisObserverTableListREQ, quitTeaHouseReq);
            MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_DeleteRoomACK, this);
            MsgMgr.inst.offMsg(login.LoginID.ID_DiamondChangeNTF, this);
            MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_ScoreChangeNTF, this);
            MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_TeahouseTableInfoNTF, this);
            MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, this);
            MsgMgr.inst.offMsgAll(this);
            this.parent.removeChild(this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.createTable.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let tc = new TableConfigPnl(CreateType.Room, parseInt(this.teaID.text));
            this.addChild(tc);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.teahouseManagement.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let teaHouseMgrPnl = new TeaHouseMgr();
            teaHouseMgrPnl.name = 'teaHouseMgrPnl';
            Main.inst.addChild(teaHouseMgrPnl);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.teahouseMembers.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let teahouseMembers = new TeahouseMember();
            this.addChild(teahouseMembers);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.rankingList.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            //积分管理 只有老板和管理员能查看积分
            //判断自己是不是管理员或者老板
            let addRank = false;
            GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                if (member.ID === GameData.userInfo.ID) {
                    if (member.Limit === teahouse.TeaHouseLimit.Owner || member.Limit === teahouse.TeaHouseLimit.Manager) {
                        addRank = true;
                    }
                }
            });

            if (addRank) {
                let awardPointSystem = new AwardPointSystem(true);
                this.addChild(awardPointSystem);
            } else {
                if (GameData.teahouseInfo.Config.ViewRank) {
                    if (GameData.teahouseInfo.Config.MatchType === structure.TableConfig.MatchTypeConfig.Normal) {
                        let awardPointSystem = new AwardPointSystem(true);
                        this.addChild(awardPointSystem);
                    } else {
                        let awardPointSystem = new AwardPointSystem(false);
                        this.addChild(awardPointSystem);
                    }

                } else {
                    Tips.addTip("您还没有权限哦！");
                }
            }
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.record.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let recordPnl = new RecordPnl();
            this.addChild(recordPnl);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.publicMsg.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let publicMsg = new PublicMsgs();
            this.addChild(publicMsg);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.msg.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let emailPnl = new EmailPnl();
            this.addChild(emailPnl);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.share.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let sharePnl = new SharePnl();
            this.addChild(sharePnl);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.Partner.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            //当前玩家在当前茶楼里面的身份
            if (GameData.teahouseInfo) {
                //自己是合伙人或者老板 可以查看合伙人积分  //自己是普通成员 可申请合伙人
                GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                    if (GameData.userInfo.ID === member.ID) {
                        switch (member.Limit) {
                            case teahouse.TeaHouseLimit.Owner:
                            case teahouse.TeaHouseLimit.Partner:
                                this.showPartnerSeePnl(member.Limit);
                                break;

                            case teahouse.TeaHouseLimit.Member:
                                this.showPartnerApplyPnl("联系茶楼老板成为合伙人");
                                break;

                            default:
                                Tips.addTip('您还没有权限哦');
                                break;
                        }

                    }
                }, this);


            }
        }, this);

        this.btnHead.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let plCenter = new PersonalCenter();
            this.addChild(plCenter);
        }, this);


        this.scoreBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let scoreStore = new StorePnl(StoreType.socre);
            this.addChild(scoreStore);
            scoreStore.visible = true;
        }, this);
        this.diamondBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let storePnl = new StorePnl(StoreType.diamond);
            this.addChild(storePnl);
        }, this);


        this.openScoreStore.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let scoreStore = new StorePnl(StoreType.socre);
            this.addChild(scoreStore);
            scoreStore.visible = true;
        }, this);
    }

    protected childrenCreated() {
        super.childrenCreated();
        //刷新钻石
        MsgMgr.inst.onMsg(login.LoginID.ID_DiamondChangeNTF, login.DiamondChangeNTF, (msg: login.DiamondChangeNTF) => {
            console.log("刷新钻石", msg);
            this.diamond.text = msg.Diamond.toString();
            GameData.userInfo.Diamond = msg.Diamond;
            // MsgMgr.inst.offMsg(login.LoginID.ID_DiamondChangeNTF, this);
        }, this);
        //刷新积分

        MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_ScoreChangeNTF, niuniu.ScoreChangeNTF, (msg: niuniu.ScoreChangeNTF) => {
            console.log("刷新积分", msg);
            this.clubScore.text = msg.Score.toString();
            // MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_ScoreChangeNTF, this);
        }, this);
        Main.inst.addEventListener(GameData.GameEvent.UpdateTeaHouseMgr, (evt: egret.Event) => {
            // SoundManager.ins.playEffect('sfx_button_click_mp3');
            console.log("茶楼信息改变-----------", GameData.teahouseInfo);
            if (evt.data.type === 'changeSetting') {
                this.teaClose.visible = GameData.teahouseInfo.Config.Close;
                this.teaHouseName.text = GameData.teahouseInfo.Config.Name;
                this.noticLabl.text = GameData.teahouseInfo.Config.Notice;
            }
        }, this);

        if (GameData.userInfo.Sex === 1) {
            this.sex.texture = RES.getRes('hall_nan_png');
        } else {
            this.sex.texture = RES.getRes('hall_nv_png');
        }

        Main.inst.addEventListener(GameData.GameEvent.UpdateTeaHouseTable, this.removeListener, this);

        MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_TeahouseTableInfoNTF, niuniu.TeahouseTableInfoNTF, (msg: niuniu.TeahouseTableInfoNTF) => {
            console.log("刷新桌子net msg", msg, GameData.teahouseTableInfo);
            if (GameData.teahouseTableInfo.length === 0) return;
            let tableIsTrue = false;
            GameData.teahouseTableInfo.forEach((table, i) => {
                if (table.TableID === msg.Info[0].TableID) {
                    console.log("刷新桌子ID", table.TableID, msg.Info);
                    GameData.teahouseTableInfo[i] = msg.Info[0];
                    this.tabelListData.replaceItemAt(msg.Info[0], i);
                    this.deskList.dataProviderRefreshed;
                    tableIsTrue = true;
                }
            }, this);
            //收到的桌子在桌子队列中不存在 需要创建
            if (tableIsTrue === false) {
                //创建一张桌子
                this.arrTables.addItem(msg.Info[0]);
                GameData.teahouseTableInfo.push(msg.Info[0]);

            }
            console.log("现在桌子数组", GameData.teahouseTableInfo);

            //只有老板提示
            // GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
            //     if (member.ID === GameData.userInfo.ID) {
            //         if (member.Limit === teahouse.TeaHouseLimit.Owner) {
            //             if (this.showTip === 1) {
            //                 if (msg.Info[0].Player.length === 0) {
            //                     Tips.addTip("修改抽水已生效");
            //                 } else {
            //                     Tips.addTip("修改抽水已成功,下局游戏生效");
            //                 }
            //             } else if (this.showTip === 2) {
            //                 if (msg.Info[0].Player.length === 0) {
            //                     Tips.addTip("修改桌子已生效");
            //                 } else {
            //                     Tips.addTip("修改桌子已成功,下局游戏生效");
            //                 }
            //             }
            //         }
            //     }
            // });


            // MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_TeahouseTableInfoNTF, this);
        }, this);




        MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_DeleteRoomACK, niuniu.DeleteRoomACK, (msg: niuniu.DeleteRoomACK) => {
            console.log("茶馆内收到刪除桌子信息", msg, GameData.teahouseTableInfo);
            if (msg.Code === 0) {
                //判断当前桌子中有没有这张桌子 有这张座子就删了
                GameData.teahouseTableInfo.forEach((table, i) => {
                    if (table.TableID === msg.TableID) {
                        GameData.teahouseTableInfo.splice(i, 1);
                        console.log('桌子', GameData.teahouseTableInfo);
                        this.tabelListData.removeItemAt(i);
                        this.tabelListData.refresh();
                        this.deskList.dataProviderRefreshed;
                        Tips.addTip('茶馆内删除桌子成功');;
                    }
                }, this)
            } else {
                Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code));
            }
            console.log("现在存在的桌子", GameData.teahouseTableInfo);
            // MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_DeleteRoomACK, this);
        }, this);


        //判断邮件是否有未读
        //判断是否有新邮件
        let mailReq = new login.MailREQ();
        MsgMgr.inst.sendMsg(login.MailID.ID_MailREQ, login.MailREQ, mailReq);
        MsgMgr.inst.onMsg(login.MailID.ID_MailACK, login.MailACK, (msg: login.MailACK) => {
            this.emailRed.visible = msg.Mail.length === 0 ? false : true;
            MsgMgr.inst.offMsg(login.MailID.ID_MailACK, this);
        }, this);

        //接收推送新邮件
        MsgMgr.inst.onMsg(login.MailID.ID_MailNTF, login.MailNTF, (msg: login.MailNTF) => {
            console.log("接收推送新邮件", msg);
            if (msg.Mail.Type === structure.Mail.MailType.Invite) {
                this.emailRed.visible = true;
            }
            if (msg.Mail.Type === structure.Mail.MailType.Join) {
                this.mangerRed.visible = true;
            }
        }, this);
        //刷新邮件红点
        Main.inst.addEventListener(GameData.GameEvent.UpdateEmail, (evt: egret.Event) => {
            let mailReq = new login.MailREQ();
            MsgMgr.inst.sendMsg(login.MailID.ID_MailREQ, login.MailREQ, mailReq);
            MsgMgr.inst.onMsg(login.MailID.ID_MailACK, login.MailACK, (msg: login.MailACK) => {
                this.emailRed.visible = msg.Mail.length === 0 ? false : true;
            }, this);
        }, this);
        //判断是否有新审核
        let teahouseMailReq = new login.TeahouseMailREQ();
        teahouseMailReq.HouseID = GameData.teahouseID;
        MsgMgr.inst.sendMsg(login.MailID.ID_TeahouseMailREQ, login.TeahouseMailREQ, teahouseMailReq);
        MsgMgr.inst.onMsg(login.MailID.ID_TeahouseMailACK, login.TeahouseMailACK, (msg: login.TeahouseMailACK) => {
            this.mangerRed.visible = msg.Mail.length > 0 ? true : false;
            MsgMgr.inst.offMsg(login.MailID.ID_TeahouseMailACK, this);
        }, this);

        //刷新茶楼审核红点
        Main.inst.addEventListener(GameData.GameEvent.UpdateExamineEmail, (evt: egret.Event) => {
            //判断是否有新审核
            let teahouseMailReq = new login.TeahouseMailREQ();
            teahouseMailReq.HouseID = GameData.teahouseID;
            MsgMgr.inst.sendMsg(login.MailID.ID_TeahouseMailREQ, login.TeahouseMailREQ, teahouseMailReq);
            MsgMgr.inst.onMsg(login.MailID.ID_TeahouseMailACK, login.TeahouseMailACK, (msg: login.TeahouseMailACK) => {
                this.mangerRed.visible = msg.Mail.length > 0 ? true : false;
                MsgMgr.inst.offMsg(login.MailID.ID_TeahouseMailACK, this);
            }, this);
        }, this);
    }

    private removeListener(evt: egret.Event) {
        // TableID:  ,Action: "update"
        console.log("刷新桌子事件", evt.data);
        if (evt.data.Action === 'updatePump') {
            // console.log("我要刷新的桌子信息", evt.data.TableID);
            // let getTableInfo = new niuniu.TeahouseTableInfoREQ();
            // getTableInfo.HouseId = GameData.teahouseID;
            // let ns = niuniu.NiuniuID;
            // MsgMgr.inst.onMsg(ns.ID_TeahouseTableInfoACK, niuniu.TeahouseTableInfoACK, (tableMsg: niuniu.TeahouseTableInfoACK) => {
            //     if (tableMsg.Info) {
            //         GameData.teahouseTableInfo = tableMsg.Info;
            //         this.tabelListData.refresh();
            //         MsgMgr.inst.offMsg(ns.ID_TeahouseTableInfoACK, this);
            //     }
            // }, this);
            Tips.addTip("修改抽水已成功,下局游戏生效");
            // MsgMgr.inst.sendMsg(ns.ID_TeahouseTableInfoREQ, niuniu.TeahouseTableInfoREQ, getTableInfo);
            this.showTip = 1;
        } else if (evt.data.Action === 'updateRoom') {
            this.showTip = 2;
            Tips.addTip("修改桌子已成功,下局游戏生效");
        }
        // else if (evt.data.Action === 'remove') {

        //     GameData.teahouseTableInfo.forEach((table, i) => {
        //         if (table.TableID === evt.data.TableID) {
        //             GameData.teahouseTableInfo.splice(i, 1);;
        //             this.tabelListData.removeItemAt(i);
        //             this.tabelListData.refresh();
        //             this.deskList.dataProviderRefreshed;
        //             console.log('----------', GameData.teahouseTableInfo,i);
        //             Tips.addTip('茶馆内删除桌子成功');;
        //         }
        //     }, this)
        //     // this.tabelListData.removeItemAt(evt.data.itemIndex);
        //     // this.tabelListData.refresh();
        //     // this.deskList.dataProviderRefreshed;

        // }
        // Main.inst.removeEventListener(GameData.GameEvent.UpdateTeaHouseTable, this.removeListener, this);
    }
    private showPartnerSeePnl(memberType) {
        let partnerSeePnl = new PartnerSeePnl(memberType);
        Main.inst.addChild(partnerSeePnl);
    }

    private showPartnerApplyPnl(str: string) {
        let partnerApplyPnl = new PartnerApplyPnl(str);
        Main.inst.addChild(partnerApplyPnl);
    }

    /**
     * 传入 所有桌子 数据显示
     *
     * @param {structure.ITableInfo[]} [arrTable] 桌子数据
     * @param {(protobuf.Long | number)} [houseID] 茶楼ID
     * @memberof Teahouse
     */
    public updateData(arrTable?: structure.ITableInfo[], houseID?: protobuf.Long | number) {
        this.teaID.text = houseID.toString();
        GameData.teahouseID = houseID;
        this.initViewData();
        if (!arrTable) return;
        arrTable.sort(function (table1, table2) {
            return table1.TableID - table2.TableID;
        });
        GameData.teahouseTableInfo = arrTable;

        for (let tableInfo of arrTable) {
            this.arrTables.addItem(tableInfo);
        }


    }


    /**
     * 增加一张桌子
     *
     * @memberof Teahouse
     */
    public addTable(tableInfo: niuniu.CreateTableACK) {
        this.arrTables.addItem(tableInfo.TableInfo);
    }
    private initViewData() {
        let teahouseInfo = new teahouse.TeaHouseInfoREQ();
        teahouseInfo.ID = [GameData.teahouseID];
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseInfoREQ, teahouse.TeaHouseInfoREQ, teahouseInfo);
        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, teahouse.TeaHouseInfoACK, (msg: teahouse.TeaHouseInfoACK) => {
            console.log('查询当前茶楼信息', msg);
            if (msg.Info.length === 0) return;

            let teahouseInfo = msg.Info[0];

            GameData.teahouseInfo = teahouseInfo;
            teahouseInfo.Member.sort(function (member1, member2) {
                return member1.Limit - member2.Limit;
            });
            for (let i = 0; i < teahouseInfo.Member.length; i++) {
                if (teahouseInfo.Member[i].ID === GameData.userInfo.ID) {
                    this.clubScore.text = teahouseInfo.Member[i].Score.toString();
                }
            }
            this.teaHouseName.text = GameData.teahouseInfo.Config.Name;
            let rankImg: eui.Image = <eui.Image>this.rankingList.getChildAt(0);
            console.log("游戲類型是多少", GameData.teahouseInfo.Config.MatchType);
            if (GameData.teahouseInfo.Config.MatchType === structure.TableConfig.MatchTypeConfig.Normal) {
                this.clubScoreBg.visible = false;
                this.teaHouseBg.texture = RES.getRes('Tex_BgChalou_png');
                rankImg.source = RES.getRes('paihangbang_png');
                this.Partner.visible = false;
            } else {
                this.clubScoreBg.visible = true;
                this.teaHouseBg.texture = RES.getRes('Tex_BgChalou2_png');
                rankImg.source = RES.getRes('jifenguanli_png');
                this.Partner.visible = true;
            }

            console.log("---------------------------", GameData.teahouseInfo.Config.Close);
            this.teaClose.visible = GameData.teahouseInfo.Config.Close;
            this.noticLabl.text = GameData.teahouseInfo.Config.Notice;

            GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                if (member.ID === GameData.userInfo.ID) {
                    if (member.Limit === teahouse.TeaHouseLimit.Owner || member.Limit === teahouse.TeaHouseLimit.Manager) {
                        this.createTable.visible = true;
                    } else {
                        this.createTable.visible = false;
                    }
                }
            });
            Main.inst.dispatchEventWith(GameData.GameEvent.UpdateMyMember);
            // MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, this);
            this.openScoreStore.visible = GameData.teahouseInfo.Config.MatchType === structure.TableConfig.MatchTypeConfig.Match ? true : false;
        }, this);



    }
}