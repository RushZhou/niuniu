class TeahouseTable extends eui.ItemRenderer {

    //桌子人数
    private peoples: number;
    public data: structure.ITableInfo;

    private desk6: eui.Group;
    private desk8: eui.Group;
    private desk10: eui.Group;

    private seat1: eui.Image;
    private seat2: eui.Image;
    private seat5: eui.Image;
    private seat4: eui.Image;
    private seat3: eui.Image;
    private seat10: eui.Image;
    private seat9: eui.Image;
    private seat8: eui.Image;
    private seat6: eui.Image;
    private seat7: eui.Image;

    private seat11: eui.Image;
    private seat12: eui.Image;
    private seat13: eui.Image;
    private seat14: eui.Image;
    private seat15: eui.Image;
    private seat16: eui.Image;
    private seat17: eui.Image;
    private seat18: eui.Image;

    private seat21: eui.Image;
    private seat24: eui.Image;
    private seat23: eui.Image;
    private seat22: eui.Image;
    private seat26: eui.Image;
    private seat25: eui.Image;



    private joinTabel: eui.Button;
    private deletTabel: eui.Button;
    private modify: eui.Button;

    private difen: eui.Label;
    private deskNum: eui.Label;

    private peopleCount: eui.Label;
    private waiting: eui.Image;
    private starting: eui.Image;


    private downPlayer = 0;//坐下玩家

    public constructor() {
        super();
    }

    protected childrenCreated() {
        super.childrenCreated();
        this.joinTabel.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);

        this.deletTabel.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let isSend = true;
            if (this.data.Player.length > 0) {
                if (this.downPlayer > 0) {
                    Tips.addTip('游戏内有玩家正在游戏,不能删除');
                } else {
                    Tips.addTip('游戏内有玩家正在观战,不能删除');
                }
                isSend = false;
            }
            let deleteTableReq = new niuniu.DeleteRoomREQ();
            deleteTableReq.TableID = this.data.TableID;
            if (isSend) {
                MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_DeleteRoomREQ, niuniu.DeleteRoomREQ, deleteTableReq);
            }

        }, this);

        this.modify.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            if (GameData.teahouseInfo.Config.MatchType === structure.TableConfig.MatchTypeConfig.Normal) {
                //修改规则
                let tc = new TableConfigPnl(CreateType.Modify, null, this.data.TableID);
                Main.inst.addChild(tc);
            } else {
                let promptPnl = new PromptPnl(this.data);
                Main.inst.addChild(promptPnl);
            }

        }, this);
    }


    protected dataChanged() {
        this.desk6.visible = false;
        this.desk8.visible = false;
        this.desk10.visible = false;

        switch (this.data.Config.MaxPlayer) {
            case 6:
                {
                    this.desk6.visible = true;
                    this.addMaskToHeadImg(this.desk6, this.seat21);
                    this.addMaskToHeadImg(this.desk6, this.seat22);
                    this.addMaskToHeadImg(this.desk6, this.seat23);
                    this.addMaskToHeadImg(this.desk6, this.seat24);
                    this.addMaskToHeadImg(this.desk6, this.seat25);
                    this.addMaskToHeadImg(this.desk6, this.seat26);
                }
                break;
            case 8:
                {
                    this.desk8.visible = true;
                    this.addMaskToHeadImg(this.desk8, this.seat11);
                    this.addMaskToHeadImg(this.desk8, this.seat12);
                    this.addMaskToHeadImg(this.desk8, this.seat13);
                    this.addMaskToHeadImg(this.desk8, this.seat14);
                    this.addMaskToHeadImg(this.desk8, this.seat15);
                    this.addMaskToHeadImg(this.desk8, this.seat16);
                    this.addMaskToHeadImg(this.desk8, this.seat17);
                    this.addMaskToHeadImg(this.desk8, this.seat18);
                }
                break;
            case 10:
                {
                    this.desk10.visible = true;
                    this.addMaskToHeadImg(this.desk10, this.seat1);
                    this.addMaskToHeadImg(this.desk10, this.seat2);
                    this.addMaskToHeadImg(this.desk10, this.seat3);
                    this.addMaskToHeadImg(this.desk10, this.seat4);
                    this.addMaskToHeadImg(this.desk10, this.seat5);
                    this.addMaskToHeadImg(this.desk10, this.seat6);
                    this.addMaskToHeadImg(this.desk10, this.seat7);
                    this.addMaskToHeadImg(this.desk10, this.seat8);
                    this.addMaskToHeadImg(this.desk10, this.seat9);
                    this.addMaskToHeadImg(this.desk10, this.seat10);

                }
                break;

        }

        this.initTableView();
        this.showDeskHeadImg();

    }
    private addMaskToHeadImg(parent: eui.Group, HeadImg: eui.Image) {
        //画圆形
        var circle: egret.Shape = new egret.Shape();
        circle.graphics.beginFill(0xffffff);
        circle.graphics.drawCircle(37, 37, 37);
        circle.graphics.endFill();
        circle.x = HeadImg.x;
        circle.y = HeadImg.y;
        parent.addChild(circle);
        HeadImg.mask = circle;
    }
    private initTableView() {

        this.deskNum.text = (this.itemIndex + 1).toString() + '号';
        this.peoples = this.data.Config.MaxPlayer;
        this.modify.visible = this.data.Owner === GameData.userInfo.ID ? true : false;
        this.deletTabel.visible = this.data.Owner === GameData.userInfo.ID ? true : false;

        let arrScore = this.data.Config.BaseScoreCfg.ValidBaseScore;
        this.difen.text = arrScore[0] + "/" + arrScore[1] + '/' + arrScore[2];
        if (this.data.IsHandPlay) {
            this.waiting.visible = false;
            this.starting.visible = true;
        } else {
            this.waiting.visible = true;
            this.starting.visible = false;
        }

        this.downPlayer = 0;
        this.data.Player.forEach(element => {
            if (element.Seat !== -1) {
                this.downPlayer++;
            }
        });
        this.peopleCount.text = this.downPlayer + "/" + this.peoples;
    }

    private showDeskHeadImg() {

        let imgRes = RES.getRes('NiuNiuView184_png');
        if (this.peoples === 6) {
            this.seat21.source = imgRes;
            this.seat22.source = imgRes;
            this.seat23.source = imgRes;
            this.seat24.source = imgRes;
            this.seat25.source = imgRes;
            this.seat26.source = imgRes;
        }
        if (this.peoples === 8) {
            this.seat11.source = imgRes;
            this.seat12.source = imgRes;
            this.seat13.source = imgRes;
            this.seat14.source = imgRes;
            this.seat15.source = imgRes;
            this.seat16.source = imgRes;
            this.seat17.source = imgRes;
            this.seat18.source = imgRes;
        }
        if (this.peoples === 10) {
            this.seat1.source = imgRes;
            this.seat2.source = imgRes;
            this.seat3.source = imgRes;
            this.seat4.source = imgRes;
            this.seat5.source = imgRes;
            this.seat6.source = imgRes;
            this.seat7.source = imgRes;
            this.seat8.source = imgRes;
            this.seat9.source = imgRes;
            this.seat10.source = imgRes;
        }



        this.data.Player.forEach(element => {
            if (element.Seat !== -1) {
                switch (element.Seat) {
                    case 0:
                        {
                            if (this.peoples === 6) GameData.handleHeadImg(this.seat21, element.HeadIcon);
                            else if (this.peoples === 8) GameData.handleHeadImg(this.seat11, element.HeadIcon);
                            else if (this.peoples === 10) GameData.handleHeadImg(this.seat1, element.HeadIcon);
                        }

                        break;
                    case 1:
                        {
                            if (this.peoples === 6) GameData.handleHeadImg(this.seat22, element.HeadIcon);
                            else if (this.peoples === 8) GameData.handleHeadImg(this.seat12, element.HeadIcon);
                            else if (this.peoples === 10) GameData.handleHeadImg(this.seat2, element.HeadIcon);
                        }

                        break;
                    case 2:
                        {
                            if (this.peoples === 6) GameData.handleHeadImg(this.seat23, element.HeadIcon);
                            else if (this.peoples === 8) GameData.handleHeadImg(this.seat13, element.HeadIcon);
                            else if (this.peoples === 10) GameData.handleHeadImg(this.seat3, element.HeadIcon);
                        }

                        break;
                    case 3:
                        {
                            if (this.peoples === 6) GameData.handleHeadImg(this.seat24, element.HeadIcon);
                            else if (this.peoples === 8) GameData.handleHeadImg(this.seat14, element.HeadIcon);
                            else if (this.peoples === 10) GameData.handleHeadImg(this.seat4, element.HeadIcon);
                        }

                        break;
                    case 4:
                        {
                            if (this.peoples === 6) GameData.handleHeadImg(this.seat25, element.HeadIcon);
                            else if (this.peoples === 8) GameData.handleHeadImg(this.seat15, element.HeadIcon);
                            else if (this.peoples === 10) GameData.handleHeadImg(this.seat5, element.HeadIcon);
                        }
                        break;
                    case 5:
                        {
                            if (this.peoples === 6) GameData.handleHeadImg(this.seat26, element.HeadIcon);
                            else if (this.peoples === 8) GameData.handleHeadImg(this.seat16, element.HeadIcon);
                            else if (this.peoples === 10) GameData.handleHeadImg(this.seat6, element.HeadIcon);
                        }
                        break;
                    case 6:
                        {
                            if (this.peoples === 8) GameData.handleHeadImg(this.seat17, element.HeadIcon);
                            else if (this.peoples === 10) GameData.handleHeadImg(this.seat7, element.HeadIcon);
                        }

                        break;
                    case 7:
                        {
                            if (this.peoples === 8) GameData.handleHeadImg(this.seat18, element.HeadIcon);
                            else if (this.peoples === 10) GameData.handleHeadImg(this.seat8, element.HeadIcon);
                        }
                        break;
                    case 8:
                        {
                            GameData.handleHeadImg(this.seat9, element.HeadIcon);
                        }

                        break;
                    case 9:
                        {
                            GameData.handleHeadImg(this.seat10, element.HeadIcon);
                        }

                        break;
                }
            }
        });

    }

    private tapHdl(evt: egret.TouchEvent) {
        SoundManager.ins.playEffect('sfx_button_click_mp3');
        MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_EnterGameACK, niuniu.EnterGameACK, (msg: niuniu.EnterGameACK) => {
            console.log("进入桌子", msg);
            if (!msg.Code) {
                Main.inst.dispatchEventWith(GameData.GameEvent.ChangeSound, false, { SoundType: "Game" });
                Main.inst.dispatchEventWith(GameData.GameEvent.DeleteTeaHousePnl);
                Main.inst.addChild(new GameTable(msg.TableInfo));
                MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_EnterGameACK, this);

            } else {
                Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code));
            }
        }, this);

        let enterReq = new niuniu.EnterGameREQ();
        enterReq.GameID = structure.GameID.Niuniu;
        enterReq.TableID = parseInt(this.data.TableID.toString());
        MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_EnterGameREQ, niuniu.EnterGameREQ, enterReq)

    }
}