class PublicMsgs extends eui.Component  {
    public constructor() {
        super();
        this.skinName = "skins.teahouse.PublicMsg";
    }


    public closeBtn: eui.Button;
    public desc: eui.Label;
    public radio: eui.RadioButton;

    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.desc.text = GameData.teahouseInfo.Config.Notice;
    }
    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
}

