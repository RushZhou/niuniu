
class ModifyScore extends eui.Component {

    public constructor(typeStr: string) {
        super();
        this.skinName = 'skins.teaHouse.ChangeScorePnl';
        this.typeStr = typeStr;
    }
    private TouchNumberGroup: eui.Group;
    private closeBtn: eui.Button;
    private sureBtn: eui.Button;
    private showLabel: eui.Label;
    private gamescore: eui.Group;
    private title: eui.Label;
    private btn_minus: eui.Button;
    private typeStr: string;

    protected childrenCreated() {
        super.childrenCreated();
        for (let i = 0; i < this.TouchNumberGroup.numElements; i++) {
            this.TouchNumberGroup.getElementAt(i).addEventListener(egret.TouchEvent.TOUCH_TAP, (e: egret.TouchEvent) => {
                // console.log('------------', i, e.target.name, e.target.name.length);
                 SoundManager.ins.playEffect('sfx_button_click_mp3');
                if (e.target.name.length === 1) {
                    this.showLabel.text = this.showLabel.text + e.target.name;
                } else if (e.target.name === "minus") {
                    if (this.showLabel.text.length === 0) {
                        this.showLabel.text = '-';
                    }
                } else if (e.target.name === "delete") {
                    this.showLabel.text = this.showLabel.text.slice(0, this.showLabel.text.length - 1);
                }
            }, this);
        }


        let minusImg: eui.Image = <eui.Image>this.btn_minus.getChildAt(0);
        minusImg.source = RES.getRes('chongshu_png');


        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            MsgMgr.inst.offMsgAll(this);
            this.parent.removeChild(this);
             SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);



        this.sureBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            Main.inst.dispatchEventWith(GameData.GameEvent.ReturnString, false, { typeStr: this.typeStr, ReturnString: this.showLabel.text });
            this.parent.removeChild(this);
             SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
    }

}
