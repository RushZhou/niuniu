/**
 * 创建房间面板
 */

class TableConfigPnl extends eui.Component {
    public tableCount: eui.Group;
    public wangRule: eui.Group;

    //combobox 选项组
    public diFen: Combobox;
    public round: Combobox;
    public roomFee: Combobox;
    public gameStart: Combobox;
    public tuiZhu: Combobox;
    public qiangZhuang: Combobox;
    public fanBei: Combobox;

    //btn组
    public createNormal: eui.Button;
    public createMatch: eui.Button;
    public close: eui.Button;
    public createRoom: eui.Button;

    //confirmPnl
    public confirmPnl: eui.Group;
    public closeConfirm: eui.Button;
    public createTeahouse: eui.Button;
    public inputName: eui.TextInput;
    public ModifyRoom: eui.Button;

    //radio 归组
    private tableCountRG = new eui.RadioButtonGroup();
    private wangRuleRG = new eui.RadioButtonGroup();

    //当前 tableCofig的状态
    private configState: CreateType;
    private teahouseID: number;
    private tableId: number | Long;

    private specialCardDsc: eui.Label;
    private createC: eui.Image;
    private createR: eui.Image;

    //创建茶楼的类型
    private createType: structure.TableConfig.MatchTypeConfig;

    public constructor(type: CreateType, teahouseID?: number, tableId?: number | Long) {
        super();
        this.teahouseID = teahouseID;
        if (tableId) this.tableId = tableId;
        this.skinName = skins.teahouse.TableConfig;
        this.configState = type;
        console.log("修改桌子信息", this.configState);
        switch (type) {
            case CreateType.Room:
            case CreateType.WorldRoom:
                this.createNormal.visible = false;
                this.createMatch.visible = false;
                this.ModifyRoom.visible = false;
                this.createR.visible = true;
                this.createRoom.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapBtnHdl, this)
                break;

            case CreateType.Teahouse:
                this.createRoom.visible = false;
                this.ModifyRoom.visible = false;
                this.createC.visible = true;
                this.createNormal.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
                    this.confirmPnl.visible = true;
                    this.createType = structure.TableConfig.MatchTypeConfig.Normal;
                    SoundManager.ins.playEffect('sfx_button_click_mp3');
                }, this)

                this.createMatch.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
                    this.confirmPnl.visible = true;
                    this.createType = structure.TableConfig.MatchTypeConfig.Match;
                    SoundManager.ins.playEffect('sfx_button_click_mp3');
                }, this)

                this.closeConfirm.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
                    this.confirmPnl.visible = false;
                    SoundManager.ins.playEffect('sfx_button_click_mp3');
                }, this)
                break;
            case CreateType.Modify:
                {
                    console.log("修改桌子信息", this.tableId);
                    this.createNormal.visible = false;
                    this.createMatch.visible = false;
                    this.createRoom.visible = false;
                    this.createR.visible = true;
                    this.ModifyRoom.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapBtnHdl, this)

                }
                break;
        }
        //confirmPnl
        this.confirmPnl.visible = false;
        this.inputName.prompt = "请输入茶楼名字";
        // this.inputName.textColor = 0xffffff;


        let nsTC = structure.TableConfig;



        this.tableCountRG.addEventListener(eui.UIEvent.CHANGE, (evt) => {
            console.log(evt.target)
            // let desc0 = (evt.target.selectedValue - 4) / 2 * 3 * Math.floor(this.round.selectItem.value / 10);
            // let desc1 = Math.floor(this.round.selectItem.value / 10);
            // let item0 = { name: "房主支付", value: nsTC.ChargeConfig.OwnerCharge, Diamond: desc0 };
            // let item1 = { name: "AA支付", value: nsTC.ChargeConfig.OwnerCharge, Diamond: desc1 };
            // this.roomFee.replaceItem(item0, item1);
        }, this)

        this.wangRuleRG.addEventListener(eui.UIEvent.CHANGE, (evt) => {
            console.log(evt.target)
        }, this)



        //combobox 填充数据
        this.diFen.addItem({ name: "1/2/4", value: "1" });
        this.diFen.addItem({ name: "2/4/8", value: "2" });
        this.diFen.addItem({ name: "3/6/12", value: "3" });
        this.diFen.addItem({ name: "4/8/16", value: "4" });
        this.diFen.addItem({ name: "5/10/20", value: "5" });
        this.diFen.addItem({ name: "10/20/40", value: "10" });


        // this.round.addItem({ name: "1", value: 1 });
        // this.round.addItem({ name: "2", value: 2 });
        // this.round.addItem({ name: "3", value: 3 });
        this.round.addItem({ name: "10", value: 10 });
        this.round.addItem({ name: "20", value: 20 });
        this.round.addItem({ name: "30", value: 30 });



        this.roomFee.addItem({ name: "房主支付", value: nsTC.ChargeConfig.OwnerCharge });
        this.roomFee.addItem({ name: "AA支付", value: nsTC.ChargeConfig.AACharge });


        this.gameStart.addItem({ name: "房主开始", value: nsTC.AutoStart.OwnerStart });
        this.gameStart.addItem({ name: "首位开始", value: nsTC.AutoStart.FirstStart });
        this.gameStart.addItem({ name: "满2人开始", value: nsTC.AutoStart.PlayerCount });
        this.gameStart.addItem({ name: "满3人开始", value: 3 });
        this.gameStart.addItem({ name: "满4人开始", value: 4 });
        this.gameStart.addItem({ name: "满5人开始", value: 5 });


        this.tuiZhu.addItem({ name: "无", value: 0 });
        this.tuiZhu.addItem({ name: 5, value: 5 });
        this.tuiZhu.addItem({ name: 10, value: 10 });
        this.tuiZhu.addItem({ name: 15, value: 15 });
        this.tuiZhu.addItem({ name: 20, value: 20 });


        this.qiangZhuang.addItem({ name: "1倍", value: 1 });
        this.qiangZhuang.addItem({ name: "2倍", value: 2 });
        this.qiangZhuang.addItem({ name: "3倍", value: 3 });
        this.qiangZhuang.addItem({ name: "4倍", value: 4 });



        this.fanBei.addItem({ name: "牛牛x4 牛九x3 牛八x2 牛七x2", value: [1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10] });
        this.fanBei.addItem({ name: "牛牛x3 牛九x2 牛八x2 牛七x1", value: [1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 5, 5, 6, 7, 8, 9, 10] });
        this.fanBei.addItem({ name: "牛牛x3 牛九x2 牛八x2 牛七x2", value: [1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 5, 5, 6, 7, 8, 9, 10] });
        this.fanBei.addItem({ name: "牛牛x5 牛九x4 牛八x3 牛七x2", value: [1, 1, 1, 1, 1, 1, 1, 2, 3, 4, 5, 6, 6, 7, 8, 9, 10, 11] });
        this.fanBei.addItem({ name: "牛一~牛牛 分别对应 1~10倍", value: [1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 11, 12, 13, 14, 15, 16] });
        this.fanBei.listItems.addEventListener(eui.ItemTapEvent.ITEM_TAP, () => {
            let arrNum = <number[]>this.fanBei.selectItem.value;
            this.specialCardDsc.text = '';
            for (let i = 11; i < arrNum.length; i++) {
                switch (i) {
                    case 11:
                        this.specialCardDsc.text += "顺子牛x" + arrNum[i] + ' ';
                        break;
                    case 12:
                        this.specialCardDsc.text += "五花牛x" + arrNum[i] + ' ';
                        break;
                    case 13:
                        this.specialCardDsc.text += "同花牛x" + arrNum[i] + ' ';
                        break;
                    case 14:
                        this.specialCardDsc.text += "葫芦牛x" + arrNum[i] + ' ';
                        break;
                    case 15:
                        this.specialCardDsc.text += "炸弹牛x" + arrNum[i] + ' ';
                        break;
                    case 16:
                        this.specialCardDsc.text += "五小牛x" + arrNum[i] + ' ';
                        break;
                    case 17:
                        this.specialCardDsc.text += "同花顺牛x" + arrNum[i] + ' ';
                        break;
                }
            }
        }, this);

        console.log("房间消息", GameData.teahouseInfo, GameData.teahouseTableInfo, type, teahouseID, tableId);
        //在茶楼创建房间时
        if (GameData.teahouseTableInfo && tableId) {
            GameData.teahouseTableInfo.forEach((tableInfo: structure.ITableInfo) => {
                if (tableInfo.TableID === tableId) {

                    let maxPlayer = tableInfo.Config.MaxPlayer;
                    let index = (maxPlayer - 6) / 2;

                    let jokerType = tableInfo.Config.JokerType;
                    console.log("-0----------", this.tableCountRG, index, this.wangRuleRG, jokerType);
                    this.radioSelet(nsTC, maxPlayer, jokerType);

                    let difen = tableInfo.Config.BaseScoreCfg.BaseScore;
                    if (difen <= 5) {
                        this.diFen.setSelect(<number>difen - 1);
                    } else {
                        this.diFen.setSelect(5);
                    }

                    let round = tableInfo.Config.MaxRound;
                    if (round <= 3) {
                        this.round.setSelect(round - 1);
                    } else {
                        this.round.setSelect(round / 10 - 1);
                    }

                    let roomFee = tableInfo.Config.ChargeCfg;
                    if (roomFee === nsTC.ChargeConfig.OwnerCharge) {
                        this.roomFee.setSelect(0);
                    } else if (roomFee === nsTC.ChargeConfig.AACharge) {
                        this.roomFee.setSelect(1);
                    }

                    this.gameStart.setSelect(nsTC.AutoStart.FirstStart);

                    let tuiZhu = tableInfo.Config.TuizhuRule;
                    this.tuiZhu.setSelect(tuiZhu / 5);

                    let qiangzhuang = tableInfo.Config.MaxRankerMulti;
                    this.qiangZhuang.setSelect(qiangzhuang - 1);

                    let fanbei = tableInfo.Config.MultiRuler;
                    if (fanbei[2] === 2) {
                        this.fanBei.setSelect(4);
                    } else {
                        if (fanbei[7] === 1) {
                            this.fanBei.setSelect(1);
                        } else {
                            if (fanbei[8] === 3) {
                                this.fanBei.setSelect(3);
                            } else {
                                if (fanbei[9] === 3) {
                                    this.fanBei.setSelect(0);
                                } else {
                                    this.fanBei.setSelect(2);
                                }
                            }
                        }
                    }

                }
            }, this);
        }
        //野房间创建
        else {
            this.diFen.setSelect(0);
            this.round.setSelect(0);
            this.roomFee.setSelect(0);
            this.gameStart.setSelect(nsTC.AutoStart.FirstStart);
            this.tuiZhu.setSelect(0);
            this.qiangZhuang.setSelect(0);
            this.fanBei.setSelect(0);
            this.radioSelet(nsTC);
        }


        this.createTeahouse.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapBtnHdl, this)

        this.close.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapBtnHdl, this)


        this.addEventListener(egret.TouchEvent.TOUCH_TAP, (evt: egret.TouchEvent) => {
            if (evt.target instanceof eui.Label || evt.target instanceof eui.Image) {
                GameData.evtLisener.dispatchEventWith(TableConfigEvt.SomeBtnHasBeClicked);
            }
        }, this);
        //combobox 互斥
        GameData.evtLisener.addEventListener(TableConfigEvt.SomeBtnHasBeClicked, () => {
            this.diFen.enable(false);
            this.round.enable(false);
            this.roomFee.enable(false);
            this.gameStart.enable(false);
            this.tuiZhu.enable(false);
            this.qiangZhuang.enable(false);
            this.fanBei.enable(false);
        }, this);
    }

    private radioSelet(nsTC, tCount?, wRule?) {
        console.log("rcount", tCount, "wRule", wRule);
        for (let i = 0; i < this.wangRule.numChildren; i++) {
            let radio = this.wangRule.getChildAt(i) as eui.RadioButton;
            radio.group = this.wangRuleRG;
            switch (i) {
                case 0:
                    radio.value = nsTC.JokerTypeConfig.None;
                    break;
                case 1:
                    radio.value = nsTC.JokerTypeConfig.Classic;
                    break;
                case 2:
                    radio.value = nsTC.JokerTypeConfig.Crazy;
                    break;
            }
            if (wRule) {
                if (radio.value === wRule) {
                    radio.selected = true;
                }
            } else {
                if (i === 0) {
                    radio.selected = true;
                }
            }

        }
        for (let i = 0; i < this.tableCount.numChildren; i++) {
            let radio = this.tableCount.getChildAt(i) as eui.RadioButton;
            radio.value = 6 + (i * 2);
            if (tCount) {
                if (radio.value === tCount) {
                    radio.selected = true;
                }
            } else {
                if (i === 0) {
                    radio.selected = true;
                }
            }

            radio.group = this.tableCountRG;
        }
    }

    public destroy() {
        MsgMgr.inst.offMsgAll(this);
        console.log(this.hashCode, '___tb code')
        this.parent.removeChild(this);
    }


    /**
     * 所有点击按钮处理  暂未处理完毕
     *
     * @private
     * @param {egret.TouchEvent} evt
     * @memberof TableConfigPnl
     */
    private tapBtnHdl(evt: egret.TouchEvent) {
        SoundManager.ins.playEffect('sfx_button_click_mp3');
        switch (evt.target) {
            case this.createTeahouse:
                this.createTeahouseHdl();
                break;

            case this.close:
                this.destroy();
                break;

            case this.createRoom:
                this.createRoomHdl(this.teahouseID);
                break;

            case this.ModifyRoom:
                this.ModifyRoomHdl(this.tableId);
                break;

        }
    }


    /**
     * 创建 茶楼处理
     *
     * @private
     * @returns
     * @memberof TableConfigPnl
     */
    private createTeahouseHdl() {
        if (!this.inputName.text) return;
        if (this.inputName.text.split('').length > 6) {
            Tips.addTip('茶馆名字应不大于6个字');
            return;
        }

        //创建茶楼消息 创建桌子
        let req = new teahouse.CreateTeaHouseREQ();
        req.Name = this.inputName.text;
        req.MatchType = this.createType;


        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_CreateTeaHouseACK, teahouse.CreateTeaHouseACK, (msg: teahouse.CreateTeaHouseACK) => {
            if (msg.Code) {
                Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code));
            }
            console.log(msg.Info, "___创建的茶楼信息");
            if (msg.Info) {
                console.log(this, 'onmsg CreateTeaHouseACK')
                this.createRoomHdl(msg.Info.ID, true)
            }
        }, this);

        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_CreateTeaHouseREQ, teahouse.CreateTeaHouseREQ, req);
    }


    /**
     * 创建 房间 处理
     *
     * @private
     * @memberof TableConfigPnl
     */
    public createRoomHdl(teahouseID: number | Long, isClear = false) {
        MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_CreateTableACK, niuniu.CreateTableACK, (msg: niuniu.CreateTableACK) => {
            if (msg.Code) {
                Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code));
            }
            else {
                //创建桌子成功
                console.log(msg, '___收到创建桌子ACK');
                if (isClear) {
                    GameData.teahouseTableInfo = [];
                }
                if (typeof GameData.teahouseTableInfo === 'undefined') {
                    GameData.teahouseTableInfo = [];
                }

                GameData.teahouseTableInfo.push(msg.TableInfo);
                switch (this.configState) {
                    case CreateType.Room:
                        {
                            let th = this.parent as Teahouse;
                            th.addTable(msg);
                            this.destroy();
                        }

                        break;

                    case CreateType.Teahouse:
                        {
                            console.log(msg.TableInfo, "__收到桌子info");
                            let th = new Teahouse();
                            th.updateData(null, msg.HouseID);
                            th.addTable(msg);
                            th.name = 'teahouse';
                            Main.inst.addChild(th);
                            this.destroy();
                        }
                        break;

                    case CreateType.WorldRoom:
                        {
                            let nc = niuniu;
                            MsgMgr.inst.onMsg(nc.NiuniuID.ID_EnterGameACK, nc.EnterGameACK, (msg: niuniu.EnterGameACK) => {
                                console.log('-------收到 enterGameACK  worldRoom-----', msg)
                                if (msg.Code) {
                                    Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code));
                                }
                                else {
                                    Main.inst.addChild(new GameTable(msg.TableInfo));
                                    this.destroy();
                                }

                            }, this);
                            let enterReq = new nc.EnterGameREQ();
                            enterReq.GameID = structure.GameID.Niuniu;
                            enterReq.TableID = msg.TableID;
                            MsgMgr.inst.sendMsg(nc.NiuniuID.ID_EnterGameREQ, nc.EnterGameREQ, enterReq);
                        }
                }
            }
        }, this);

        let createTable = new niuniu.CreateTableREQ();
        let st = structure.TableConfig;
        createTable.HouseID = teahouseID;
        let bsc = new st.BaseScoreConfig();
        bsc.BaseScore = parseInt(<string>this.diFen.selectItem.value);
        let cfg = new structure.TableConfig();
        cfg.BaseScoreCfg = bsc;
        cfg.AutoStartCfg = new st.AutoStartConfig();
        //少一个选项 这里要分情况 赋值
        let autoVal: number = this.gameStart.selectItem.value;
        cfg.AutoStartCfg.AutoStart = autoVal > 2 ? 2 : autoVal;
        cfg.AutoStartCfg.PlayerCount = autoVal < 2 ? 0 : autoVal;

        cfg.ChargeCfg = this.roomFee.selectItem.value;
        cfg.MultiRuler = this.fanBei.selectItem.value;
        cfg.JokerType = this.wangRuleRG.selectedValue;
        cfg.MaxRankerMulti = this.qiangZhuang.selectItem.value;
        cfg.MaxRound = this.round.selectItem.value;
        let maxPlayer = this.tableCountRG.selectedValue;
        cfg.MaxPlayer = maxPlayer;
        cfg.MatchType = structure.TableConfig.MatchTypeConfig.Normal;
        cfg.TuizhuRule = this.tuiZhu.selectItem.value;
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Bomb);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Flush);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Hulu);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Straight);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.StraightFlush);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Wuhua);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Wuxiao);
        createTable.Cfg = cfg;

        MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_CreateTableREQ, niuniu.CreateTableREQ, createTable);
    }

    //修改房間信息
    public ModifyRoomHdl(tableId: number | Long) {

        let changeTabelReq = new niuniu.TableConfigChangeREQ();
        changeTabelReq.TableId = parseInt(tableId.toString());

        let st = structure.TableConfig;
        let bsc = new st.BaseScoreConfig();
        let cfg = new structure.TableConfig();
        bsc.BaseScore = parseInt(<string>this.diFen.selectItem.value);
        cfg.BaseScoreCfg = bsc;
        cfg.AutoStartCfg = new st.AutoStartConfig();

        //少一个选项 这里要分情况 赋值
        let autoVal: number = this.gameStart.selectItem.value;
        cfg.AutoStartCfg.AutoStart = autoVal > 2 ? 2 : autoVal;
        cfg.AutoStartCfg.PlayerCount = autoVal < 2 ? 0 : autoVal;


        cfg.ChargeCfg = this.roomFee.selectItem.value;
        cfg.MultiRuler = this.fanBei.selectItem.value;
        cfg.JokerType = this.wangRuleRG.selectedValue;
        cfg.MaxRankerMulti = this.qiangZhuang.selectItem.value;
        cfg.MaxRound = this.round.selectItem.value;
        let maxPlayer = this.tableCountRG.selectedValue;
        cfg.MaxPlayer = maxPlayer;
        cfg.MatchType = structure.TableConfig.MatchTypeConfig.Normal;
        cfg.TuizhuRule = this.tuiZhu.selectItem.value;
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Bomb);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Flush);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Hulu);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Straight);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.StraightFlush);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Wuhua);
        cfg.SpecialCardMulti.push(structure.TableConfig.SpecialCardMultiple.Wuxiao);
        changeTabelReq.Cfg = cfg;

        MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_TableConfigChangeREQ, niuniu.TableConfigChangeREQ, changeTabelReq);

        MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_TableConfigChangeACK, niuniu.TableConfigChangeACK, (msg: niuniu.TableConfigChangeACK) => {
            if (msg.Code) {
                Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code));
            } else {
                //刷新桌子信息
                Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseTable, false, { TableID: this.tableId, Action: "updateRoom" });
            }
            this.destroy();
        }, this);

    }

}


/**
 * 打开桌子配置面板时候的状态
 * 分创建房间 创建茶楼 修改
 * @enum {number}
 */
enum CreateType {
    //在茶楼创建房间
    Room,
    //创建茶楼 && 创建房间
    Teahouse,
    //修改房间
    Modify,
    //创建世界房间
    WorldRoom
}

enum TableConfigEvt {
    SomeBtnHasBeClicked = "SomeBtnHasBeClicked",
}

