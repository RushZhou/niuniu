// TypeScript file

class PartnerApplyPnl extends eui.Component {

    public constructor(textStr: string, obj = {}) {
        super();
        this.skinName = "skins.teahouse.PartnerApplyPnl";
        this.textStr = textStr;
        this.obj = obj;
    }
    private title: eui.Label;
    private desc: eui.Label;
    private date: eui.Label;
    private cancel: eui.Button;
    private sure: eui.Button;
    private textStr: string = '';
    private obj;
    protected childrenCreated() {
        super.childrenCreated();
        this.desc.text = this.textStr;
        this.cancel.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            MsgMgr.inst.offMsgAll(this);
            this.parent.removeChild(this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);


        this.sure.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {

            SoundManager.ins.playEffect('sfx_button_click_mp3');
            if (Object.keys(this.obj).length != 0) {
                if (this.obj.action === teahouse.TeaHouseAction.AddPartner) {
                    let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
                    joinTeaHouseReq.Action = this.obj.action;
                    joinTeaHouseReq.HouseID = GameData.teahouseID;
                    joinTeaHouseReq.UserID = this.obj.tagetID;
                    joinTeaHouseReq.SetLimit = this.obj.isTrue;
                    MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
                    MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                        console.log('删除合伙人操作返回', msg, msg.Code);
                        MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                        if (msg.Code === 0 /*|| msg.Code === 5*/) {
                            Tips.addTip('删除合伙人成功');
                            Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseAction, false, { type: msg.Action });
                        } else {
                            Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                        }
                    }, this);
                } else if (this.obj.action === teahouse.TeaHouseAction.Remove) {
                    let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
                    joinTeaHouseReq.Action = this.obj.action;
                    joinTeaHouseReq.UserID = this.obj.tagetID;
                    joinTeaHouseReq.HouseID = GameData.teahouseID;
                    MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
                    console.log('移除玩家', joinTeaHouseReq);
                    MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                        console.log('操作返回', msg);
                        MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                        if (msg.Code === 0) {
                            if (msg.Action === teahouse.TeaHouseAction.Remove) {
                                // let tag:eui.ItemRenderer = this.obj.tag;
                                // tag.parent.removeChild(tag);
                                Tips.addTip('移除玩家成功');
                                Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMenbersItems, false, { type: msg.Action, itemIndex: this.obj.itemidx });
                            }
                        } else {
                            Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                        }
                    }, this);

                }

            }
            this.parent.removeChild(this);


        }, this);
    }

}