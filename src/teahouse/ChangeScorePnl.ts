
class ChangeScorePnl extends eui.Component {

    public constructor(tagId: number, action: teahouse.TeaHouseAction, isSetLimit?: boolean, isItemIndex?) {
        super();
        this.skinName = 'skins.teaHouse.ChangeScorePnl';
        this.tagetId = tagId;
        this.action = action;
        this.isSetLimit = isSetLimit;
        this.itemIndex = isItemIndex;
    }
    private TouchNumberGroup: eui.Group;
    private closeBtn: eui.Button;
    private sureBtn: eui.Button;
    private showLabel: eui.Label;
    private tagetId = -1;
    private gamescore: eui.Group;
    private title: eui.Label;
    private isSetLimit = null;
    private btn_minus: eui.Button;
    private itemIndex;

    private action: teahouse.TeaHouseAction;

    protected childrenCreated() {
        super.childrenCreated();

        for (let i = 0; i < this.TouchNumberGroup.numElements; i++) {
            this.TouchNumberGroup.getElementAt(i).addEventListener(egret.TouchEvent.TOUCH_TAP, (e: egret.TouchEvent) => {
                // console.log('------------', i, e.target.name, e.target.name.length);
                SoundManager.ins.playEffect('sfx_button_click_mp3');
                if (e.target.name.length === 1) {
                    this.showLabel.text = this.showLabel.text + e.target.name;
                } else if (e.target.name === "minus") {
                    if (this.showLabel.text.length === 0 && this.action === teahouse.TeaHouseAction.AddScore) {
                        this.showLabel.text = '-';
                    } else {
                        this.showLabel.text = '';
                    }
                } else if (e.target.name === "delete") {
                    this.showLabel.text = this.showLabel.text.slice(0, this.showLabel.text.length - 1);
                }

            }, this);
        }

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            MsgMgr.inst.offMsgAll(this);
            this.parent.removeChild(this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        switch (this.action) {
            case teahouse.TeaHouseAction.AddScore:
                this.title.text = "请输入分数";
                break;
            case teahouse.TeaHouseAction.ActionRate:
                {
                    this.title.text = "合作比例调整";
                    let minusImg: eui.Image = <eui.Image>this.btn_minus.getChildAt(0);
                    minusImg.source = RES.getRes('chongshu_png');
                }

                break;
            case teahouse.TeaHouseAction.AddPartner:
                {
                    if (this.isSetLimit) {
                        this.title.text = "请输入要添加的合伙人ID";
                    } else {
                        this.title.text = "请输入要删除的合伙人ID";
                    }
                    let minusImg: eui.Image = <eui.Image>this.btn_minus.getChildAt(0);
                    minusImg.source = RES.getRes('chongshu_png');
                }

                break;
            case teahouse.TeaHouseAction.Invite:
                {
                    this.title.text = "请输入要邀请的玩家ID";
                    let minusImg: eui.Image = <eui.Image>this.btn_minus.getChildAt(0);
                    minusImg.source = RES.getRes('chongshu_png');
                }

                break;
            case teahouse.TeaHouseAction.Assign:
                {
                    this.title.text = "请输入要分配给合伙人的玩家ID";
                    let minusImg: eui.Image = <eui.Image>this.btn_minus.getChildAt(0);
                    minusImg.source = RES.getRes('chongshu_png');
                }

                break;
            case teahouse.TeaHouseAction.Transfer:
                {
                    this.title.text = "请确认接收茶楼用户ID";
                    let minusImg: eui.Image = <eui.Image>this.btn_minus.getChildAt(0);
                    minusImg.source = RES.getRes('chongshu_png');
                }
                break;
            case teahouse.TeaHouseAction.Recycle:
                {
                    this.title.text = '请输入老板ID确认清除';
                    let minusImg: eui.Image = <eui.Image>this.btn_minus.getChildAt(0);
                    minusImg.source = RES.getRes('chongshu_png');
                }
                break;
            default:
                break;


        }

        this.sureBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            joinTeaHouseReq.Action = this.action;
            joinTeaHouseReq.HouseID = GameData.teahouseID;

            let isSend = true;
            switch (this.action) {
                case teahouse.TeaHouseAction.AddScore: // userid 自己 AssignID 是要上下分的对象
                    {
                        joinTeaHouseReq.Score = parseInt(this.showLabel.text);
                        joinTeaHouseReq.AssignID = this.tagetId;
                        joinTeaHouseReq.UserID = GameData.userInfo.ID;
                        GameData.teahouseInfo.Member.forEach(member => {
                            if (member.ID === GameData.userInfo.ID) {
                                if (member.Score < parseInt(this.showLabel.text)) {
                                    Tips.addTip('积分不足，无法调整');
                                    isSend = false;
                                }
                            }
                        }, this);
                    }
                    break;
                case teahouse.TeaHouseAction.ActionRate: //userid 修改合夥人的對象  rate  0-100
                    {
                        joinTeaHouseReq.UserID = this.tagetId;
                        joinTeaHouseReq.Rate = parseInt(this.showLabel.text);
                        if (100 < parseInt(this.showLabel.text)) {
                            Tips.addTip('比例不能大于100');
                            isSend = false;
                        }
                    }

                    break;
                case teahouse.TeaHouseAction.AddPartner: //userid 修改合夥人的對象
                    {
                        joinTeaHouseReq.UserID = parseInt(this.showLabel.text);
                        joinTeaHouseReq.SetLimit = this.isSetLimit;
                        let exist = false;
                        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                            if (member.ID === joinTeaHouseReq.UserID) {
                                if (member.Limit === teahouse.TeaHouseLimit.Manager) {
                                    Tips.addTip('管理员不能添加为合伙人');
                                    isSend = false;
                                }
                                exist = true;
                            }
                        }, this);

                        if (!exist) {
                            Tips.addTip('输入玩家不存在此茶楼');
                            isSend = false;
                        }

                    }

                    break;
                case teahouse.TeaHouseAction.Recycle:  //回收哪些用的的積分
                    {
                        joinTeaHouseReq.RecyleUserID = []; //要清除的人的集合
                        let exist = false;
                        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                            if (member.ID === parseInt(this.showLabel.text)) {
                                if (member.Limit !== teahouse.TeaHouseLimit.Owner) {
                                    Tips.addTip('您输入的ID不是老板ID,清除失败');
                                    isSend = false;
                                }
                                exist = true;
                            }
                        }, this);
                        if (!exist) {
                            Tips.addTip('输入玩家不存在此茶楼');
                            isSend = false;
                        }
                    }

                    break;
                case teahouse.TeaHouseAction.Invite: // userid 要邀請玩家的id
                    {
                        joinTeaHouseReq.UserID = parseInt(this.showLabel.text);
                        let exist = true;
                        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                            if (member.ID === parseInt(this.showLabel.text)) {
                                exist = false;
                            }
                        });
                        if (!exist) {
                            Tips.addTip('输入玩家已存在此茶楼');
                            isSend = false;
                        }
                    }
                    break;
                case teahouse.TeaHouseAction.Assign:
                    {
                        joinTeaHouseReq.UserID = this.tagetId;
                        joinTeaHouseReq.AssignID = parseInt(this.showLabel.text);
                        joinTeaHouseReq.SetLimit = this.isSetLimit;

                        let exist = false;
                        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                            if (member.ID === parseInt(this.showLabel.text)) {
                                exist = true;
                            }
                        });
                        if (!exist) {
                            Tips.addTip('输入玩家不存在此茶楼');
                            isSend = false;
                        }

                    }

                    break;
                case teahouse.TeaHouseAction.Transfer:
                    {
                        joinTeaHouseReq.UserID = parseInt(this.showLabel.text);
                        if (joinTeaHouseReq.UserID !== this.tagetId) {
                            Tips.addTip('输入ID与转让茶楼ID不一致');
                            isSend = false;
                        }
                        let exist = false;
                        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                            if (member.ID === parseInt(this.showLabel.text)) {
                                exist = true;
                            }
                        });
                        if (!exist) {
                            Tips.addTip('输入玩家不存在此茶楼');
                            isSend = false;
                        }
                    }
                    break;
                default:
                    break;
            }


            if (isSend) {
                MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
                console.log('操作', this.action, joinTeaHouseReq);
            }



            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('操作返回', this.action, msg, msg.Code);
                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                if (msg.Action === teahouse.TeaHouseAction.Transfer) {
                    if (msg.Code === 0) {
                        Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseMenbersItems, false, { type: msg.Action, itemIndex: this.itemIndex });
                    } else {
                        Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                    }
                } else if (msg.Action === teahouse.TeaHouseAction.Invite) {
                    if (msg.Code === 0) {
                        let str = "已经向玩家ID:" + this.showLabel.text + "发送邀请";
                        Tips.addTip(str);
                    } else {
                        Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                    }
                }
                else if (msg.Action === teahouse.TeaHouseAction.Assign) {//调配玩家
                    if (msg.Code === 0) {
                        Main.inst.dispatchEventWith(GameData.GameEvent.UpdatePartnerList, false, { type: msg.Action });
                    } else {
                        Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                    }
                }
                else {
                    if (msg.Code === 0 /*|| msg.Code === 5*/) {
                        Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseAction, false, { type: msg.Action });
                    } else {
                        Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                    }
                }
                MsgMgr.inst.offMsgAll(this);
            }, this);
            this.parent.removeChild(this);
        }, this);
    }

}
