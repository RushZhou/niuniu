// TypeScript file

class PartnerSeePnl extends eui.Component {

    public constructor(memberType: teahouse.TeaHouseLimit) {
        super();
        this.skinName = "skins.teahouse.PartnerSeePnl";

        this.memberType = memberType;
    }

    private tips: eui.Group;
    private BossRoot: eui.Group;


    private partnerList: eui.List;
    private partnerScroll: eui.Scroller;
    private gameCount: eui.Label;
    private tipUpdate: eui.Label;


    private closeBtn: eui.Button;
    private addPartner: eui.Button;
    private clearPartnerScore: eui.Button;


    private memberType: teahouse.TeaHouseLimit;
    private bossListData: eui.ArrayCollection;

    protected childrenCreated() {
        super.childrenCreated();
        //关闭
        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            Main.inst.removeEventListener(GameData.GameEvent.UpdateTeaHouseAction, this.removeListener, this);
            MsgMgr.inst.offMsgAll(this);
            this.parent.removeChild(this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
        //添加合伙人
        this.addPartner.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            console.log("添加合伙人");
            let changeScorePnl = new ChangeScorePnl(parseInt(GameData.userInfo.ID.toString()), teahouse.TeaHouseAction.AddPartner, true);
            Main.inst.addChild(changeScorePnl);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
        //清除积分
        this.clearPartnerScore.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            console.log("清除合伙人的所有积分");
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let changeScorePnl = new ChangeScorePnl(parseInt(GameData.userInfo.ID.toString()), teahouse.TeaHouseAction.Recycle);
            Main.inst.addChild(changeScorePnl);

            // let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            // joinTeaHouseReq.Action = teahouse.TeaHouseAction.Recycle;
            // joinTeaHouseReq.HouseID = GameData.teahouseID;
            // joinTeaHouseReq.RecyleUserID = []; //要清除的人的集合
            // MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            // console.log('操作清除合伙人的所有积分');
            // MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
            //     console.log('操作返回', msg, msg.Code);
            //     MsgMgr.inst.offMsgAll(this);
            //     if (msg.Code === 0 /*|| msg.Code === 5*/) {
            //         Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseAction, false, { type: msg.Action });
            //     } else {
            //         Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
            //     }
            // }, this);

        }, this);

        //如果我不是老板
        console.log('我的 合伙状态', this.memberType, teahouse.TeaHouseLimit.Owner);
        if (this.memberType !== teahouse.TeaHouseLimit.Owner) this.BossRoot.visible = false;


        Main.inst.addEventListener(GameData.GameEvent.UpdateTeaHouseAction, this.removeListener, this);
        Main.inst.addEventListener(GameData.GameEvent.CloseNetMsg, (evt: egret.Event) => {
            MsgMgr.inst.offMsg(evt.data.ID, this);
        }, this);
        this.addPertners();
    }
    private removeListener(evt: egret.Event) {
        console.log("合伙人界面 刷新监听", evt, evt.bubbles, evt.data);
        switch (evt.data.type) {
            case teahouse.TeaHouseAction.ActionRate:
            case teahouse.TeaHouseAction.AddPartner:
                this.addPertners();
                break;
            default:
                break;
        }

        // Main.inst.removeEventListener(GameData.GameEvent.UpdateTeaHouseAction, this.removeListener, this);
    }
    private addPertners() {
        //根据自己是不是老板来区分显示 
        //自己是老板显示所有合伙人
        //自己是合伙人  显示合伙人本身

        //请求合伙人列表
        //根据茶楼ID查找茶楼信息
        let teahouseInfo = new teahouse.TeaHouseInfoREQ();
        teahouseInfo.ID = [GameData.teahouseID];
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseInfoREQ, teahouse.TeaHouseInfoREQ, teahouseInfo);

        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, teahouse.TeaHouseInfoACK, (msg: teahouse.TeaHouseInfoACK) => {
            console.log('查询茶楼信息返回', msg);
            if (msg.Info.length === 0) return;
            GameData.teahouseInfo = msg.Info[0];
            let bossListData: Array<teahouse.ITeaHouseMember> = [];
            let partnerListData: Array<teahouse.ITeaHouseMember> = [];
            msg.Info[0].Member.forEach((member: teahouse.ITeaHouseMember) => {
                if (member.Limit === teahouse.TeaHouseLimit.Owner || member.Limit === teahouse.TeaHouseLimit.Partner) {
                    if (member.Limit !== teahouse.TeaHouseLimit.Owner) {
                        bossListData.push(member);
                    }

                    if (member.Limit === teahouse.TeaHouseLimit.Partner && member.ID === GameData.userInfo.ID) {
                        partnerListData.push(member);
                    }
                }
            });
            //收益积分

            bossListData.forEach((member: teahouse.ITeaHouseMember) => {
                let rankReq = new teahouse.RankREQ();
                rankReq.Type = teahouse.RankType.PartnerDraw;
                rankReq.HouseID = GameData.teahouseID;
                rankReq.PartnerID = member.ID;
                rankReq.Start = 0;
                rankReq.PageCount = 30;
                MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_RankREQ, teahouse.RankREQ, rankReq);
            });
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_RankACK, teahouse.RankACK, (msg: teahouse.RankACK) => {
                console.log("縂抽水", msg);
                if (msg.Type != teahouse.RankType.PartnerDraw) {
                    return
                }

                for (let i = 0; i < bossListData.length; i++) {
                    if (bossListData[i].ID === msg.PartnerID) {
                        if (msg.Content) {
                            let players = msg.Content.Player;
                            let allValue = 0;
                            players.forEach((player) => {
                                allValue += parseInt(player.Value.toString());
                            });
                            bossListData[i]['allGetScore'] = allValue;
                        } else {
                            bossListData[i]['allGetScore'] = 0;
                        }

                    }
                }

                if (this.bossListData) {
                    this.bossListData.replaceAll(bossListData);
                    this.bossListData.refresh();
                    this.partnerList.dataProviderRefreshed;
                }
            }, this);

            if (this.memberType === teahouse.TeaHouseLimit.Owner) { //自己是老板
                if (this.bossListData) {
                    this.bossListData.replaceAll(bossListData);
                    this.bossListData.refresh();
                    this.partnerList.dataProviderRefreshed;
                } else {
                    this.bossListData = new eui.ArrayCollection(bossListData);
                    this.partnerList.dataProvider = this.bossListData;
                    this.partnerList.itemRenderer = PartnerItems;
                    this.partnerScroll.viewport = this.partnerList;
                }
                console.log("我是老板,加载老板下面的合伙人", bossListData);
                if (bossListData.length > 0) this.tips.visible = false;
            } else if (this.memberType === teahouse.TeaHouseLimit.Partner) {//自己是合伙人
                partnerListData.sort(function (member1, member2) {
                    return member1.Limit - member2.Limit;
                });
                let memberPnl = Main.inst.getChildByName('myMembersPnl');
                if (memberPnl) {
                    MsgMgr.inst.offMsgAll(memberPnl);
                    Main.inst.removeChild(memberPnl);
                }
                let myMembersPnl = new MyMemberPnl(partnerListData[0], this);
                myMembersPnl.name = 'myMembersPnl';
                Main.inst.addChild(myMembersPnl);
            }

        }, this);

    }

}


class PartnerItems extends eui.ItemRenderer {
    public constructor() {
        super();
        this.skinName = 'skins.teahouse.PartnerItem';
    }
    private headImg: eui.Image;
    private nickname: eui.Label;
    private id: eui.Label;
    private partnerItem: eui.Group;


    private allNum: eui.Label;
    private cooperationRatio: eui.Label;
    private adjustRatio: eui.Button;
    private details: eui.Button;
    private delete: eui.Button;
    private frozened: eui.Label;

    private selfData: teahouse.TeaHouseMember;

    protected childrenCreated() {
        super.childrenCreated();
        this.partnerItem.visible = true;
        //调整合作比例
        this.adjustRatio.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let changeScorePnl = new ChangeScorePnl(parseInt(this.selfData.ID.toString()), teahouse.TeaHouseAction.ActionRate);
            Main.inst.addChild(changeScorePnl);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
        //详细情况
        this.details.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            //我的成员
            Main.inst.dispatchEventWith(GameData.GameEvent.CloseNetMsg, false, { ID: teahouse.TeaHouseID.ID_RankACK });
            let myMembersPnl = new MyMemberPnl(this.data);
            Main.inst.addChild(myMembersPnl);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
        //删除合伙人
        this.delete.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            // let changeScorePnl = new ChangeScorePnl(parseInt(this.selfData.ID.toString()), teahouse.TeaHouseAction.AddPartner, false);
            // Main.inst.addChild(changeScorePnl);
            let obj = { action: teahouse.TeaHouseAction.AddPartner, isTrue: false, tagetID: parseInt(this.selfData.ID.toString()) }
            let partnerApplyPnl = new PartnerApplyPnl("确认删除次合伙人？", obj);
            Main.inst.addChild(partnerApplyPnl);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
    }
    protected dataChanged() {
        // console.log('PartnerItems', this.data);
        this.selfData = this.data;
        GameData.handleHeadImg(this.headImg, this.selfData.HeadIcon);
        this.nickname.text = this.data.NickName;
        this.id.text = this.data.ID;
        this.cooperationRatio.text = this.data.PartnerRate + '%';
        this.allNum.text = this.data.allGetScore;

        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
            if (GameData.userInfo.ID === member.ID && this.selfData.ID === GameData.userInfo.ID) {
                if (member.Limit !== teahouse.TeaHouseLimit.Owner) {//判断自己是不是老板
                    this.adjustRatio.visible = false;
                    this.delete.visible = false;
                }

            }
        });

        if (this.selfData.State === 0) {
            this.frozened.visible = false;
        } else if (this.selfData.State === 1) {
            this.frozened.visible = true;
        }

    }
}