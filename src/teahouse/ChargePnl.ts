class ChargePnl extends eui.Component {
    private btnConfirm: eui.Button;
    private btnCancel: eui.Button;
    private inputMoney: eui.EditableText;
    private labelExchange: eui.Label;
    private pnl: eui.Group;
    private btnShowPnl: eui.Button;

    private readonly chargeUrl = 'http://116.62.237.212:6017/H5/zpUnsigned/qrCode.html?';

    public constructor() {
        super();
        this.skinName = skins.teahouse.ChargePnl;

        this.btnShowPnl.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.btnCancel.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.btnConfirm.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);


        this.inputMoney.addEventListener(eui.UIEvent.CHANGING, () => {
            console.log(this.inputMoney.text);
        }, this);
        this.inputMoney.addEventListener(eui.UIEvent.CHANGE, () => {
            console.log(this.inputMoney.text);
        }, this);
    }

    protected childrenCreated() {
        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_ChargeOrderACK, teahouse.ChargeOrderACK, (msg: teahouse.ChargeOrderACK) => {
            console.log('---order ack---', msg)
            switch (GameData.getPlatform()) {
                case WhichPlatform.web:
                    window.open(this.chargeUrl + msg.Url);
                    break

                case WhichPlatform.android:
                    egret.ExternalInterface.call("openUrl", this.chargeUrl + msg.Url);
                    break;
            }

        }, this);

        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_ChargeOrderNTF, teahouse.ChargeOrderNTF, (msg: teahouse.ChargeOrderNTF) => {
            console.log('---order ntf---', msg)
        }, this);
    }

    private tapHdl(evt: egret.TouchEvent) {
        let btn = evt.target;
        switch (btn) {
            case this.btnConfirm:
                this.pnl.visible = false;
                let chargeReq = new teahouse.ChargeOrderREQ();
                chargeReq.HouseID = GameData.teahouseID;
                try {
                    chargeReq.Money = parseFloat(this.inputMoney.text);
                    chargeReq.Type = '1';
                }
                catch (e) {
                    console.log('Money error')
                    return;
                }
                MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_ChargeOrderREQ, teahouse.ChargeOrderREQ, chargeReq);

                break;
            case this.btnCancel:
                this.pnl.visible = false;
                break;
            case this.btnShowPnl:
                this.pnl.visible = true;
                break;
        }
    }
}