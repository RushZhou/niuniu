class PromptPnl extends eui.Component {

    public constructor(tableInfo: structure.ITableInfo) {
        super();
        this.skinName = "skins.teahouse.PromptPnl";
        this.tableInfo = tableInfo;
    }

    public closeBtn: eui.Button;
    public changeRule: eui.Button;
    public changeCs: eui.Button;

    private tableInfo:  structure.ITableInfo;

    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.changeRule.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            //修改规则
            let tc = new TableConfigPnl(CreateType.Modify, null,this.tableInfo.TableID);
            Main.inst.addChild(tc);
            this.parent.removeChild(this);
             SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
        this.changeCs.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            //修改抽水
            let modifPumpPnl = new ModifPumpPnl(this.tableInfo);
            Main.inst.addChild(modifPumpPnl);
            this.parent.removeChild(this);
             SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        GameData.teahouseInfo.Member.forEach(menubar => {
            if (GameData.userInfo.ID === menubar.ID && menubar.Limit === teahouse.TeaHouseLimit.Owner) {
                if (GameData.teahouseInfo.Config.MatchType === structure.TableConfig.MatchTypeConfig.Normal) {
                    this.changeCs.visible = false;
                    this.changeRule.x = 535;
                }
            }
        });

    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
    }


}