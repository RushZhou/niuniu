// TypeScript file
class MyMemberPnl extends eui.Component {

    public constructor(partnerData: teahouse.ITeaHouseMember, tag?) {
        super();
        this.skinName = "skins.teahouse.MyMembersPnl";
        this.partnerData = partnerData;
        this.taget = tag;
    }


    private heMember: eui.Image;
    private myMember: eui.Image;
    private tips: eui.Group;


    private menbersList: eui.List;
    private membersScroll: eui.Scroller;

    private BossRoot: eui.Group; //是不是老板旗下
    private reback: eui.Button;//返回
    private invitePlayer: eui.Button;//邀请玩家
    private deploymentPlayer: eui.Button;//调配玩家
    private clearSelfTagScore: eui.Button;

    private partnerData: teahouse.ITeaHouseMember;
    private isBoss = false; //当前邀请人是不是老板
    private taget = null;
    private mySelfMember: eui.ArrayCollection;
    protected childrenCreated() {
        super.childrenCreated();
        console.log("详情里面的成员", this.partnerData);
        //返回
        this.reback.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            MsgMgr.inst.offMsgAll(this);
            this.parent.removeChild(this);
            if (this.taget) {
                Main.inst.removeChild(this.taget);
                MsgMgr.inst.offMsgAll(this.taget);
            }
        }, this);
        //邀请成员  是邀请不是本茶楼的成员
        this.invitePlayer.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let tagId = this.isBoss ? GameData.userInfo.ID : this.partnerData.ID
            let changeScorePnl = new ChangeScorePnl(parseInt(tagId.toString()), teahouse.TeaHouseAction.Invite);
            Main.inst.addChild(changeScorePnl);
        }, this);
        //调配玩家
        this.deploymentPlayer.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let changeScorePnl = new ChangeScorePnl(parseInt(this.partnerData.ID.toString()), teahouse.TeaHouseAction.Assign, true);
            Main.inst.addChild(changeScorePnl);
        }, this);
        //清除合伙人下的玩家积分
        this.clearSelfTagScore.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let allMember = [];
            GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                if (member.BelongToPartner === this.partnerData.ID || member.ID === this.partnerData.ID) {
                    allMember.push(member.ID);
                }
            });


            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            joinTeaHouseReq.Action = teahouse.TeaHouseAction.Recycle;
            joinTeaHouseReq.HouseID = GameData.teahouseID;
            joinTeaHouseReq.RecyleUserID = allMember; //要清除的人的集合
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('操作清除合伙人下玩家的所有积分', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('操作返回', msg, msg.Code);
                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                if (msg.Code === 0 /*|| msg.Code === 5*/) {
                    Tips.addTip('清除合伙人以及合伙人下面所有玩家积分成功');
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseAction, false, { type: msg.Action });
                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
            }, this);
        }, this);


        if (this.partnerData.ID === GameData.userInfo.ID) {
            this.myMember.visible = true;
        } else {
            this.heMember.visible = true;
        }
        if (this.partnerData.Limit !== teahouse.TeaHouseLimit.Owner) this.deploymentPlayer.visible = false;


        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
            if (GameData.userInfo.ID === member.ID) {//&& this.partnerData.ID === GameData.userInfo.ID
                if (member.Limit === teahouse.TeaHouseLimit.Owner) {//判断自己是不是老板
                    this.deploymentPlayer.visible = true;
                    this.clearSelfTagScore.visible = false;
                    this.invitePlayer.visible = false;
                    this.isBoss = true;
                } else {
                    this.deploymentPlayer.visible = false;
                    this.clearSelfTagScore.visible = false;
                    this.isBoss = false;
                }

            }
        });

        this.updateList();
        Main.inst.addEventListener(GameData.GameEvent.UpdatePartnerList, (evt: egret.Event) => {
            console.log('刷新合夥人下的玩家');
            if (evt.data.type === teahouse.TeaHouseAction.Assign) {
                let teahouseInfo = new teahouse.TeaHouseInfoREQ();
                teahouseInfo.ID = [GameData.teahouseID];
                MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseInfoREQ, teahouse.TeaHouseInfoREQ, teahouseInfo);
                MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, teahouse.TeaHouseInfoACK, (msg: teahouse.TeaHouseInfoACK) => {
                    MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, this);
                    GameData.teahouseInfo = msg.Info[0];
                    GameData.teahouseInfo.Member.sort(function (member1, member2) {
                        return member1.Limit - member2.Limit;
                    });
                    this.updateList();
                }, this);

            }

        }, this);
    }
    private updateList() {
        let mySelfMember = [];
        //根据我的ID去找我下面的玩家
        if (this.partnerData.Limit === teahouse.TeaHouseLimit.Partner) {
            GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
                // if (member.ID === this.partnerData.ID) {
                //     member['isPartner'] = true;
                //     mySelfMember.push(member);
                // }
                if (member.BelongToPartner === this.partnerData.ID) {
                    member['isPartner'] = false;
                    mySelfMember.push(member);
                }

            }, this);
        } else {
            mySelfMember = GameData.teahouseInfo.Member;
        }
        if (mySelfMember.length !== 0) this.tips.visible = false;

        // console.log(this.partnerData.ID, "下旗下的玩家", mySelfMember, "房间信息", GameData.teahouseInfo);

        mySelfMember.sort(function (member1, member2) {
            return member1.Limit - member2.Limit;
        });

        this.mySelfMember = new eui.ArrayCollection(mySelfMember);
        this.menbersList.dataProvider = this.mySelfMember;
        this.menbersList.itemRenderer = MembersItems;
        this.membersScroll.viewport = this.menbersList;


        this.getPartnerDrawByID();
        this.getPartnerDrawDayByID();
        this.getPartnerHandByID();
        this.getPartnerHandDayByID();
        this.getPartnerUserScoreDayByID();
        this.getPartnerUserScoreTotalByID();
        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_RankACK, teahouse.RankACK, (msg: teahouse.RankACK) => {
            // console.log("排行榜排行", msg, msg.Type);
            for (let i = 0; i < mySelfMember.length; i++) {
                let players = msg.Content.Player;
                if (players.length === 0) {
                    let typeStr = '';
                    switch (msg.Type) {
                        case teahouse.RankType.PartnerHandDay:
                            typeStr = 'PartnerHandDay';
                            break;
                        case teahouse.RankType.PartnerDrawDay:
                            typeStr = 'PartnerDrawDay';
                            break;
                        case teahouse.RankType.PartnerHand:
                            typeStr = 'PartnerHand';
                            break;
                        case teahouse.RankType.PartnerDraw:
                            typeStr = 'PartnerDraw';
                            break;
                        case teahouse.RankType.PartnerUserScoreDay:
                            typeStr = 'PartnerUserScoreDay';
                            break;
                        case teahouse.RankType.PartnerUserScoreTotal:
                            typeStr = 'PartnerUserScoreTotal';
                            break;
                    }
                    mySelfMember[i][typeStr] = 0;
                } else {
                    players.forEach((player) => {
                        let typeStr = '';
                        switch (msg.Type) {
                            case teahouse.RankType.PartnerHandDay:
                                typeStr = 'PartnerHandDay';
                                break;
                            case teahouse.RankType.PartnerDrawDay:
                                typeStr = 'PartnerDrawDay';
                                break;
                            case teahouse.RankType.PartnerHand:
                                typeStr = 'PartnerHand';
                                break;
                            case teahouse.RankType.PartnerDraw:
                                typeStr = 'PartnerDraw';
                                break;
                            case teahouse.RankType.PartnerUserScoreDay:
                                typeStr = 'PartnerUserScoreDay';
                                break;
                            case teahouse.RankType.PartnerUserScoreTotal:
                                typeStr = 'PartnerUserScoreTotal';
                                break;
                        }
                        if (player.UserID === mySelfMember[i].ID) {
                            mySelfMember[i][typeStr] = player.Value;
                        } else {
                            mySelfMember[i][typeStr] = 0;
                        }
                    });
                }

            }
            // console.log('成员参数', mySelfMember);
            if (this.mySelfMember) {
                this.mySelfMember.replaceAll(mySelfMember);
                this.mySelfMember.refresh();
                this.menbersList.dataProviderRefreshed;
            }
        }, this);

    }
    //查找这个玩家昨天玩了多少吧
    private getPartnerHandDayByID() {
        let rankReq = new teahouse.RankREQ();
        rankReq.Type = teahouse.RankType.PartnerHandDay;
        rankReq.HouseID = GameData.teahouseID;
        rankReq.PartnerID = this.partnerData.ID;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_RankREQ, teahouse.RankREQ, rankReq);

    }
    //查找这个玩家昨天抽了多少水
    private getPartnerDrawDayByID() {
        let rankReq = new teahouse.RankREQ();
        rankReq.Type = teahouse.RankType.PartnerDrawDay;
        rankReq.HouseID = GameData.teahouseID;
        rankReq.PartnerID = this.partnerData.ID;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_RankREQ, teahouse.RankREQ, rankReq);
    }

    //查找这个玩家一共玩了多少吧
    private getPartnerHandByID() {
        let rankReq = new teahouse.RankREQ();
        rankReq.Type = teahouse.RankType.PartnerHand;
        rankReq.HouseID = GameData.teahouseID;
        rankReq.PartnerID = this.partnerData.ID;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_RankREQ, teahouse.RankREQ, rankReq);
    }
    //查找这个玩家一共抽了多少水
    private getPartnerDrawByID() {
        let rankReq = new teahouse.RankREQ();
        rankReq.Type = teahouse.RankType.PartnerDraw;
        rankReq.HouseID = GameData.teahouseID;
        rankReq.PartnerID = this.partnerData.ID;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_RankREQ, teahouse.RankREQ, rankReq);
    }
    //查找这个玩家昨日输赢
    private getPartnerUserScoreDayByID() {
        let rankReq = new teahouse.RankREQ();
        rankReq.Type = teahouse.RankType.PartnerUserScoreDay;
        rankReq.HouseID = GameData.teahouseID;
        rankReq.PartnerID = this.partnerData.ID;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_RankREQ, teahouse.RankREQ, rankReq);

    }
    //查找这个玩家总输赢
    private getPartnerUserScoreTotalByID() {
        let rankReq = new teahouse.RankREQ();
        rankReq.Type = teahouse.RankType.PartnerUserScoreTotal;
        rankReq.HouseID = GameData.teahouseID;
        rankReq.PartnerID = this.partnerData.ID;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_RankREQ, teahouse.RankREQ, rankReq);
    }
}



class MembersItems extends eui.ItemRenderer {
    public constructor() {
        super();
        this.skinName = 'skins.teahouse.PartnerItem';
    }
    private headImg: eui.Image;
    private nickname: eui.Label;
    private id: eui.Label;

    private myPerpleItem: eui.Group;
    private allGameCount: eui.Label;
    private allRatio: eui.Label;
    private loserScore: eui.Label;

    private personalData: eui.Button;
    private adjustScore1: eui.Button;
    private delete1: eui.Button;
    private frozened: eui.Label;
    private selfData: teahouse.TeaHouseMember;

    protected childrenCreated() {
        super.childrenCreated();
        this.myPerpleItem.visible = true;
        //个人数据
        this.personalData.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let personalData = new PersonalData(this.selfData.ID);
            Main.inst.addChild(personalData);
        }, this);
        //调整积分
        this.adjustScore1.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            //上下分
            let changeScorePnl = new ChangeScorePnl(parseInt(this.selfData.ID.toString()), teahouse.TeaHouseAction.AddScore);
            Main.inst.addChild(changeScorePnl);

        }, this);
        //删除成员
        this.delete1.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            joinTeaHouseReq.Action = teahouse.TeaHouseAction.Assign;
            joinTeaHouseReq.HouseID = GameData.teahouseID;
            joinTeaHouseReq.UserID = this.selfData.BelongToPartner;
            joinTeaHouseReq.AssignID = this.selfData.ID;
            joinTeaHouseReq.SetLimit = false;

            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('删除合伙人下成员', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('删除合伙人下成员返回', msg, msg.Code);
                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
                if (msg.Code === 0 /*|| msg.Code === 5*/) {
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdatePartnerList, false, { type: msg.Action });
                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
            }, this);

        }, this);
        this.personalData.visible = false;
    }
    protected dataChanged() {
        console.log('MembersItems', this.data);
        this.selfData = this.data;
        GameData.handleHeadImg(this.headImg, this.selfData.HeadIcon);
        this.nickname.text = this.data.NickName;
        this.id.text = this.data.ID;
        if (this.selfData.State === 0) {
            this.frozened.visible = false;
        } else if (this.selfData.State === 1) {
            this.frozened.visible = true;
        }
        if (this.data.isPartner) {
            this.delete1.visible = false;
            this.adjustScore1.visible = false;
        }
        if (typeof this.data.PartnerHandDay !== 'undefined') {
            this.allGameCount.text = this.data.PartnerHandDay + '/' + this.data.PartnerHand;
            this.allRatio.text = this.data.PartnerDrawDay + '/' + this.data.PartnerDraw;
            this.loserScore.text = this.data.PartnerUserScoreDay + '/' + this.data.PartnerUserScoreTotal;
        }

    }
}