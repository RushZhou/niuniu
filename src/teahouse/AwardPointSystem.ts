class AwardPointSystem extends eui.Component implements eui.UIComponent {
    public constructor(isNorPeople?: boolean) {
        super();
        this.skinName = 'skins.AwardPointSystem';
        this.isNorPeople = isNorPeople;

    }

    private closeBtn: eui.Button;
    private noRecord: eui.Group;

    private radioBtn: eui.RadioButton;
    private viewStack: eui.ViewStack;
    private scrollBar0: eui.Scroller;
    private List0: eui.List;
    private scrollBar1: eui.Scroller;
    private List1: eui.List;
    private scrollBar2: eui.Scroller;
    private List2: eui.List;
    private scrollBar3: eui.Scroller;
    private List3: eui.List;
    private scrollBar4: eui.Scroller;
    private List4: eui.List;


    private YesertdayTable: eui.Label;
    private TodayTable: eui.Label;
    private YesertdayDiamond: eui.Label;
    private TodayDiamond: eui.Label;
    private radioGroup: eui.Group;
    private socreImg: eui.Image;
    private rankImg: eui.Image;

    private cszs: eui.Label;
    private zrcs: eui.Label;
    private jrcs: eui.Label;

    private isNorPeople = true;

    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);

        this.radioBtn.group.$name = 'onChangeAward'
        this.radioBtn.group.addEventListener(eui.UIEvent.CHANGE, this.onChangeAward, this);

        Main.inst.addEventListener(GameData.GameEvent.UpdateTeaHouseAction, this.removeListener, this);
        Main.inst.addEventListener(GameData.GameEvent.UpdateAward, () => {
            let radio = <eui.RadioButton>this.radioGroup.getChildAt(0);
            this.viewStack.selectedIndex = 0;
            radio.selected = true;
        }, this);
        if (this.isNorPeople === false) {
            for (let i = 0; i < this.radioGroup.numChildren; i++) {
                if (i === 0) continue;
                this.radioGroup.getChildAt(i).visible = false;
            }
        }

        if (GameData.teahouseInfo.Config.MatchType === structure.TableConfig.MatchTypeConfig.Normal) {
            this.deleteRadioByValue("0");

            this.deleteRadioByValue("1");

            this.deleteRadioByValue("4");

            this.deleteRadioByValue("5");

            this.deleteRadioByValue("6");
            this.rankImg.visible = true;
            let radio = <eui.RadioButton>this.radioGroup.getChildAt(0);
            this.viewStack.selectedIndex = 2;
            radio.selected = true;
            this.reqLoserRank();

        } else {
            //请求茶楼玩家的信息 负责给比赛积分  做增减
            this.reqMatchScoreInfo();
            this.socreImg.visible = true;
            this.deleteRadioByValue("1");
        }

    }
    private deleteRadioByValue(value: string) {
        for (let i = 0; i < this.radioGroup.numChildren; i++) {
            let radio = <eui.RadioButton>this.radioGroup.getChildAt(i);
            if (radio.value === value) {
                this.radioGroup.removeChild(radio);
            }
        }
    }

    private removeListener(evt: egret.Event) {
        console.log("积分管理界面 监听", evt, evt.bubbles, evt.data);
        if (evt.data.type === teahouse.TeaHouseAction.AddScore || evt.data.type === teahouse.TeaHouseAction.Recycle) {
            this.reqMatchScoreInfo();
        }
        //  Main.inst.removeEventListener("UpdateTeaHouseAction", this.removeListener, this);
    }

    private onCloseFunc() {
        MsgMgr.inst.offMsgAll(this);
        this.radioBtn.group.removeEventListener(eui.UIEvent.CHANGE, this.onChangeAward, this);
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        Main.inst.removeEventListener("UpdateTeaHouseAction", this.removeListener, this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
    private onChangeAward(e: eui.UIEvent) {

        let radioGroup: eui.RadioButtonGroup = e.target;
        console.log('AwardPointSystem', 'radio');
        if (radioGroup.$name !== 'onChangeAward') return;
        this.viewStack.selectedIndex = radioGroup.selectedValue;

        switch (this.viewStack.selectedIndex) {
            case 0:
                {
                    this.reqMatchScoreInfo();
                }
                break;
            case 1://积分记录
                {
                    this.reqScoreLog();
                }
                break;
            case 2:
                {
                    this.reqLoserRank();
                }

                break;
            case 3://大赢家
                {
                    this.reqWinerRank();
                }

                break;
            case 4://比赛统计
                {
                    this.reqMatchInfo();
                }
                break;
            case 5://茶楼统计
                {
                    this.reqTableStatis();
                }
                break;
            case 6://玩家抽水
                {
                    this.reqMemberInfo();
                }
            default:
                break;
        }
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
    private reqMatchInfo() {
        let drawREQ = new teahouse.DrawREQ();
        drawREQ.HouseID = GameData.teahouseID;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_DrawREQ, teahouse.DrawREQ, drawREQ);
        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_DrawACK, teahouse.DrawACK, (msg: teahouse.DrawACK) => {
            console.log('获取开房间的抽水信息', msg);

            this.cszs.text = msg.Total.toString();
            this.jrcs.text = msg.Today.toString();
            this.zrcs.text = msg.Yesterday.toString();
            this.noRecord.visible = false;

            MsgMgr.inst.offMsgAll(this);
        }, this);


    }
    private reqMatchScoreInfo() {
        //请求茶楼玩家的信息 负责给比赛积分  做增减
        let teahouseInfo = new teahouse.TeaHouseInfoREQ();
        teahouseInfo.ID = [GameData.teahouseID];
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseInfoREQ, teahouse.TeaHouseInfoREQ, teahouseInfo);
        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseInfoACK, teahouse.TeaHouseInfoACK, (msg: teahouse.TeaHouseInfoACK) => {
            console.log('查询当前茶楼信息', msg);
            this.noRecord.visible = true;
            if (msg.Info.length === 0) return;
            let teahouseInfo = msg.Info[0];
            GameData.teahouseInfo = msg.Info[0];
            teahouseInfo.Member.forEach((mb) => {
                mb["isShowRank"] = this.isNorPeople;
            }, this);

            teahouseInfo.Member.sort(function (member1, member2) {
                return member1.Limit - member2.Limit;
            });

            this.List0.dataProvider = new eui.ArrayCollection(teahouseInfo.Member);
            this.List0.itemRenderer = ScoreMgrItem;
            this.scrollBar0.viewport = this.List0;

            this.isShowNoRecord(teahouseInfo.Member);

            MsgMgr.inst.offMsgAll(this);
        }, this);
    }

    private reqScoreLog() {
        let scoreLogReq = new teahouse.ScoreLogREQ();
        scoreLogReq.HouseID = GameData.teahouseID;
        scoreLogReq.Start = 0;
        scoreLogReq.PageCount = 30;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_ScoreLogREQ, teahouse.ScoreLogREQ, scoreLogReq);
        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_ScoreLogACK, teahouse.ScoreLogACK, (msg: teahouse.ScoreLogACK) => {
            console.log("scoreLogReq积分记录", msg);
            this.noRecord.visible = true;
            if (msg.ChangeLog.length === 0) return;
            let changeLogArr = [];
            msg.ChangeLog.forEach(element => {
                if (element.Tag) {
                    changeLogArr.push(element);
                }
            }, this);
            this.List1.dataProvider = new eui.ArrayCollection(changeLogArr);
            this.List1.itemRenderer = ScoreRecordItem;
            this.scrollBar1.viewport = this.List1;
            // console.log("积分记录显示数据",changeLogArr);
            this.isShowNoRecord(changeLogArr);
            MsgMgr.inst.offMsgAll(this);
        }, this);
    }

    private reqLoserRank() {
        let rankReq = new teahouse.RankREQ();
        rankReq.Type = teahouse.RankType.LoseTotal;
        rankReq.HouseID = GameData.teahouseID;
        rankReq.Start = 0;
        rankReq.PageCount = 30;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_RankREQ, teahouse.RankREQ, rankReq);

        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_RankACK, teahouse.RankACK, (msg: teahouse.RankACK) => {
            console.log("土豪帮", msg);
            this.noRecord.visible = true;
            if (msg.Type === teahouse.RankType.LoseTotal) {
                let Players = msg.Content.Player;
                let LoseData = new eui.ArrayCollection(Players);
                this.List2.dataProvider = LoseData;
                this.List2.itemRenderer = LoseItem;
                this.scrollBar2.viewport = this.List2;

                this.isShowNoRecord(Players);
                MsgMgr.inst.offMsgAll(this);
            }

        }, this);
    }

    private reqWinerRank() {
        let rankReq = new teahouse.RankREQ();
        rankReq.Type = teahouse.RankType.WinTotal;
        rankReq.HouseID = GameData.teahouseID;
        rankReq.Start = 0;
        rankReq.PageCount = 30;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_RankREQ, teahouse.RankREQ, rankReq);

        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_RankACK, teahouse.RankACK, (msg: teahouse.RankACK) => {
            console.log("大赢家", msg);
            this.noRecord.visible = true;
            if (msg.Type === teahouse.RankType.WinTotal) {
                let Players = msg.Content.Player;
                let WinData = new eui.ArrayCollection(Players);
                this.List3.dataProvider = WinData;
                this.List3.itemRenderer = WinItem;
                this.scrollBar3.viewport = this.List3;
                this.isShowNoRecord(Players);
                MsgMgr.inst.offMsgAll(this);
            }

        }, this);
    }


    private reqTableStatis() {
        let tableStatisReq = new teahouse.TableStatisticsREQ();
        tableStatisReq.HouseID = GameData.teahouseID;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TableStatisticsREQ, teahouse.TableStatisticsREQ, tableStatisReq);

        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TableStatisticsACK, teahouse.TableStatisticsACK, (msg: teahouse.TableStatisticsACK) => {
            console.log('获取开房间的统计信息', msg);

            this.YesertdayTable.text = msg.YesertdayTable.toString();
            this.TodayTable.text = msg.TodayTable.toString();
            this.YesertdayDiamond.text = msg.YesertdayDiamond.toString();
            this.TodayDiamond.text = msg.TodayDiamond.toString();

            this.noRecord.visible = false;
            MsgMgr.inst.offMsgAll(this);
        }, this);

    }

    private isShowNoRecord(data) {
        this.noRecord.visible = data.length > 0 ? false : true;
    }

    private reqMemberInfo() {
        let rankReq = new teahouse.RankREQ();
        rankReq.Type = teahouse.RankType.UserDraw;
        rankReq.HouseID = GameData.teahouseID;
        rankReq.Start = 0;
        rankReq.PageCount = 30;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_RankREQ, teahouse.RankREQ, rankReq);

        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_RankACK, teahouse.RankACK, (msg: teahouse.RankACK) => {
            console.log("玩家抽水", msg);
            this.noRecord.visible = true;
            if (msg.Type === teahouse.RankType.UserDraw) {
                let csMemberArr = [];//被抽水玩家

                msg.Content.Player.forEach(player => {
                    if (player.Value > 0) {
                        csMemberArr.push(player);
                    }
                });
                csMemberArr.sort(function (player1, player2) {
                    return player2.Value - player1.Value;
                });

                this.List4.dataProvider = new eui.ArrayCollection(csMemberArr);
                this.List4.itemRenderer = PumpMemberItem;
                this.scrollBar4.viewport = this.List4;
                this.isShowNoRecord(csMemberArr);
            }
            MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_RankACK, this);
        }, this);
    }

}

class ScoreMgrItem extends eui.ItemRenderer {

    public constructor() {
        super();
        this.skinName = "skins.teahouse.AwardPointItem";
        console.log("create club item");
    }

    private headImg: eui.Image;
    private nickName: eui.Label;
    private score: eui.Label;
    private id: eui.Label;
    private clear: eui.Button;
    private peopleData: eui.Button;
    private changeScore: eui.Button;
    private winGroup: eui.Group;
    private gamescore: eui.Group;

    private isBoss: eui.Image;
    private isManager: eui.Image;
    private partner: eui.Image;

    private selfData = null;
    private selfLimit = -1;
    protected childrenCreated() {
        super.childrenCreated();
        this.gamescore.visible = true;
        this.clear.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            //清除积分
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            joinTeaHouseReq.Action = teahouse.TeaHouseAction.Recycle;
            joinTeaHouseReq.UserID = GameData.userInfo.ID;
            joinTeaHouseReq.HouseID = GameData.teahouseID;
            joinTeaHouseReq.RecyleUserID = [this.selfData.ID];
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('清除积分', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('清除积分返回', msg, msg.Code);
                MsgMgr.inst.offMsgAll(this);
                if (msg.Code === 0 /*|| msg.Code === 5*/) {
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdateTeaHouseAction, false, { type: msg.Action });
                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }

                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
            }, this);
        }, this);

        this.peopleData.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            //个人信息 
            let personalData = new PersonalData(this.selfData.ID);
            Main.inst.addChild(personalData);
        }, this);

        this.changeScore.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            //上下分
            let changeScorePnl = new ChangeScorePnl(this.selfData.ID, teahouse.TeaHouseAction.AddScore);
            Main.inst.addChild(changeScorePnl);

        }, this);

    }

    private limitAdmin() {

        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
            if (member.ID === this.selfData.ID) {
                if (this.selfLimit === teahouse.TeaHouseLimit.Owner) {
                    if (member.Limit === teahouse.TeaHouseLimit.Owner) {
                        this.clear.visible = false;
                        this.changeScore.visible = false;
                    } else {
                        this.clear.visible = true;
                        this.changeScore.visible = true;
                    }
                } else if (this.selfLimit === teahouse.TeaHouseLimit.Manager) {
                    if (member.Limit === teahouse.TeaHouseLimit.Owner
                        || member.Limit === teahouse.TeaHouseLimit.Manager
                        || member.Limit === teahouse.TeaHouseLimit.Partner) {
                        this.clear.visible = false;
                        this.changeScore.visible = false;
                    }
                }

            }
        });

        if (this.selfData.isShowRank == false) {
            this.clear.visible = false;
            this.changeScore.visible = false;
            this.peopleData.visible = false;
        }

        this.peopleData.visible = false;
    }
    protected dataChanged() {
        //console.log("ScoreMgrItem", this.data);
        this.selfData = this.data;
        GameData.handleHeadImg(this.headImg, this.selfData.HeadIcon);


        this.nickName.text = this.data.NickName;
        this.score.text = this.data.Score;
        this.id.text = this.data.ID;

        GameData.teahouseInfo.Member.forEach((member: teahouse.TeaHouseMember) => {
            if (this.selfData.ID === member.ID) {
                if (member.Limit === teahouse.TeaHouseLimit.Owner) {
                    this.isBoss.visible = true;
                } else if (member.Limit === teahouse.TeaHouseLimit.Manager) {
                    this.isManager.visible = true;
                } else if (member.Limit === teahouse.TeaHouseLimit.Partner) {
                    this.partner.visible = true;
                } else {
                    this.isBoss.visible = false;
                    this.isManager.visible = false;
                    this.partner.visible = false;
                }

            }
            if (member.Limit === teahouse.TeaHouseLimit.Owner) {
                if (GameData.userInfo.ID === member.ID) {
                    this.selfLimit = teahouse.TeaHouseLimit.Owner;
                }
            }

            if (member.Limit === teahouse.TeaHouseLimit.Manager) {
                if (GameData.userInfo.ID === member.ID) {
                    this.selfLimit = teahouse.TeaHouseLimit.Manager;
                }
            }

        });
        //限制条件 
        this.limitAdmin();
    }

}



class WinItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "skins.teahouse.AwardPointItem";
    }
    private headImg: eui.Image;
    private nickName: eui.Label;
    private score: eui.Label;
    private id: eui.Label;
    private idLabel: eui.Label;
    private winOrLoseGroup: eui.Group;
    private gamescore: eui.Group;
    private count: eui.Label;
    private rankImg: eui.Image;
    private rankNum: eui.Label;

    protected childrenCreated() {
        super.childrenCreated();
        this.winOrLoseGroup.visible = true;
        this.headImg.x += 50;
        this.nickName.x += 50;
        this.id.x += 50;
        this.idLabel.x += 50;

    }
    protected dataChanged() {
        console.log("WinData", this.data);
        this.id.text = this.data.UserID;
        this.count.text = this.data.Value;
        GameData.handleHeadImg(this.headImg, this.data.HeadIcon);
        switch (this.itemIndex) {
            case 0:
                this.rankImg.visible = true;
                this.rankNum.visible = false;
                this.rankImg.texture = RES.getRes('NiuNiuView173_png');
                break;

            case 1:
                this.rankImg.visible = true;
                this.rankNum.visible = false;
                this.rankImg.texture = RES.getRes('NiuNiuView032_png');
                break;

            case 2:
                this.rankImg.visible = true;
                this.rankNum.visible = false;
                this.rankImg.texture = RES.getRes('NiuNiuView009_png');
                break;

            default:
                this.rankImg.visible = false;
                this.rankNum.text = (this.itemIndex + 1).toString();
                break;

        }
    }
}
class PumpMemberItem extends eui.ItemRenderer {

    constructor() {
        super();
        this.skinName = "skins.teahouse.AwardPointItem";
    }

    private headImg: eui.Image;
    private nickName: eui.Label;
    private id: eui.Label;

    private pumpCount: eui.Group;
    private pumpNum: eui.Label;

    protected childrenCreated() {
        super.childrenCreated();

        this.pumpCount.visible = true;
    }
    protected dataChanged() {
        console.log("PumpMemberItem", this.data);
        this.nickName.text = this.data.NickName;
        this.id.text = this.data.UserID;
        this.pumpNum.text = this.data.Value;
        GameData.handleHeadImg(this.headImg, this.data.HeadIcon);
    }
}
class LoseItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "skins.teahouse.AwardPointItem";
    }
    private headImg: eui.Image;
    private nickName: eui.Label;
    private score: eui.Label;
    private id: eui.Label;
    private idLabel: eui.Label;
    private winOrLoseGroup: eui.Group;
    private gamescore: eui.Group;
    private count: eui.Label;
    private rankImg: eui.Image;
    private rankNum: eui.Label;


    protected childrenCreated() {
        super.childrenCreated();
        this.winOrLoseGroup.visible = true;
        this.headImg.x += 50;
        this.nickName.x += 50;
        this.id.x += 50;
        this.idLabel.x += 50;

    }
    protected dataChanged() {
        console.log("LoseData", this.data);
        this.id.text = this.data.UserID;
        this.count.text = this.data.Value;
        GameData.handleHeadImg(this.headImg, this.data.HeadIcon);
        switch (this.itemIndex) {
            case 0:
                this.rankImg.visible = true;
                this.rankNum.visible = false;
                this.rankImg.texture = RES.getRes('NiuNiuView173_png');
                break;

            case 1:
                this.rankImg.visible = true;
                this.rankNum.visible = false;
                this.rankImg.texture = RES.getRes('NiuNiuView032_png');
                break;

            case 2:
                this.rankImg.visible = true;
                this.rankNum.visible = false;
                this.rankImg.texture = RES.getRes('NiuNiuView009_png');
                break;

            default:
                this.rankImg.visible = false;
                this.rankNum.text = (this.itemIndex + 1).toString();
                break;

        }
    }
}

class ScoreRecordItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "skins.teahouse.AwardPointItem";
    }
    private headImg: eui.Image;
    private nickName: eui.Label;
    private id: eui.Label;
    private scoreChange: eui.Group;
    private change_Score: eui.Label;
    private change_syScore: eui.Label;
    private change_Player: eui.Label;
    private change_date: eui.Label;

    protected childrenCreated() {
        super.childrenCreated();
        this.scoreChange.visible = true;

    }
    protected dataChanged() {
        // console.log("ScoreRecordItem", this.data);
        this.id.text = this.data.UserID;
        // this.nickName.text = this.data.
        this.change_Player.text = this.data.Reason;
        GameData.handleHeadImg(this.headImg, this.data.HeadIcon);
        this.change_Score.text = this.data.DeltaScore + "";
        this.change_syScore.text = this.data.Score + "";
        this.change_date.text = GameData.getTimeStr(this.data.Date);
    }
}
