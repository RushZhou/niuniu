/**
 * 负责存储游戏公用数据 和 各模块之间事件通讯
 * 
 * @class GameData
 */
class GameData {
    public static inst: GameData;
    // public static readonly ipAdress: string = "192.168.1.104";
    public static readonly ipAdress: string = "154.216.2.131";
    // public static readonly ipAdress: string = "192.168.1.104";
    public static readonly updateAdress: string = "154.216.2.131";

    public static loginToken: string;
    public static userInfo: structure.IUserInfo;
    public static teahouseID: number | Long;

    public static teahouseInfo: teahouse.ITeaHouseInfo;
    public static teahouseTableInfo: structure.ITableInfo[];

    public static evtLisener: egret.EventDispatcher = new egret.EventDispatcher();

    public static GameEvent = {
        BindIdentify: 'BindIdentify',//绑定身份证
        BindPhone: 'BindPhone',//綁定手机号
        UpdateEmail: 'UpdateEmail',//刷新邮件
        UpdateTeaHouseAction: 'UpdateTeaHouseAction', //茶楼操作刷新
        UpdateTeaHouseMenbersItems: 'UpdateTeaHouseMenbersItems',//刷新茶楼成员列表数据
        UpdateTeaHouseMgr: 'UpdateTeaHouseMgr',//刷新茶楼管理设置的状态
        UpdateTeaHouseTable: 'UpdateTeaHouseTable',//刷新茶楼桌子
        ReturnString: 'ReturnString', //修改抽水设置中底分
        ExitGame: 'ExitGame',//退出游戏
        DeleteGamePnl: 'DeleteGamePnl',//删除游戏面板
        DeleteTeaHousePnl: "DeleteTeaHousePnl",//删除茶楼面板
        ChangeSound: 'ChangeSound',//改变背景音乐
        UpdateAward: 'UpdateAward',//刷新积分
        UpdateExamineItem: 'UpdateExamineItem',//刷新审核管理
        UpdateMyMember: 'UpdateMyMember',//刷新我的成员
        UpdatePartnerList: 'UpdatePartnerList',//刷新合伙人列表
        UpdateCheckItem: 'UpdateCheckItem',//已弃用
        UpdateSgined: 'UpdateSgined',//刷新签到红点
        CloseNetMsg: 'CloseNetMsg',
        UpdateExamineEmail: 'UpdateExamineEmail',
        RemoveEvent: 'RemoveEvent',
    }
    //进入茶楼事件
    public static EvtEnterTeahouse: (houseID: number | Long) => void;
    public static EvtEnterRoom: (tableInfo: structure.ITableInfo) => void;

    public constructor() {
        GameData.inst = this;

        GameData.EvtEnterTeahouse = this.getTablesInfo;
        GameData.EvtEnterRoom = this.enterRoom;
    }

    /**
    * 获取某个茶楼 所有桌子 && 进入茶楼
    * 
    * @private
    * @memberof TableConfigPnl
    */
    private getTablesInfo(houseID: number | Long) {
        let getTableInfo = new niuniu.TeahouseTableInfoREQ();
        getTableInfo.HouseId = houseID;
        let ns = niuniu.NiuniuID;
        console.log('---------------------', getTableInfo);
        MsgMgr.inst.onMsg(ns.ID_TeahouseTableInfoACK, niuniu.TeahouseTableInfoACK, (tableMsg: niuniu.TeahouseTableInfoACK) => {
            if (tableMsg.Info) {
                console.log("當前茶樓所有桌子消息", tableMsg.Info, houseID, "__收到桌子info");
                GameData.teahouseTableInfo = tableMsg.Info;
                let th = new Teahouse();
                th.name = 'teahouse';
                th.updateData(tableMsg.Info, houseID);
                Main.inst.addChild(th);
                MsgMgr.inst.offMsgAll(this);
            }
        }, this);

        MsgMgr.inst.sendMsg(ns.ID_TeahouseTableInfoREQ, niuniu.TeahouseTableInfoREQ, getTableInfo);
    }

    private enterRoom(tableInfo: structure.ITableInfo) {
        Main.inst.addChild(new GameTable(tableInfo))
    }

    /**
     * 根据byte获取一张牌
     * 高4位：花色，0-无，1-方块，2-梅花，3-红桃，4-黑桃
     * 低4位：点数
     * @private
     * @param {number} card
     * @returns {eui.Image}
     * @memberof Player
     */
    public static getCardByByte(card: number, isBig?: boolean, justGetName?: boolean): eui.Image | string {
        if (card > 0) {
            let type = card >> 4;
            let point = card & 0x0f;
            // console.log(type, '-----', point);
            let cardName = '';
            switch (type) {
                case 1:
                    cardName = 'Square';
                    break;
                case 2:
                    cardName = 'Flower';
                    break;
                case 3:
                    cardName = 'Red';
                    break;
                case 4:
                    cardName = 'Black';
                    break;
                case 0:
                    cardName = 'Joker';
                    break;
            }
            if (type !== 0) {
                cardName += point + (isBig ? '_b_png' : '_png');
            }
            else {
                cardName += point === 14 ? 'Black_png' : 'Color_png';
            }

            if (justGetName) {
                return cardName;
            }
            else {
                let texture = <egret.Texture>RES.getRes(cardName);
                let img = new eui.Image(texture);
                return img;
            }

        }
        else {
            let cardName = isBig ? 'BgOrigin_b_png' : 'BgOrigin_png';
            if (justGetName) {
                return cardName;
            }
            else {
                let texture = <egret.Texture>RES.getRes(cardName);
                let img = new eui.Image(texture);
                return img;
            }

        }
    }

    public static getTimeStr(dateStamp: number | Long) {

        var d = new Date(parseInt(dateStamp.toString()) * 1000);
        var date = (d.getFullYear()) + "-" +
            (d.getMonth() + 1) + "-" +
            (d.getDate()) + " " +
            (d.getHours()) + ":" +
            (d.getMinutes()) + ":" +
            (d.getSeconds());
        //console.log("根据时间戳生成的时间对象", date);
        return date;

    }

    public static getPlatform(): WhichPlatform {
        if (egret.Capabilities.isMobile && egret.Capabilities.runtimeType === egret.RuntimeType.RUNTIME2) {
            switch (egret.Capabilities.os) {
                case "Android":
                    return WhichPlatform.android;

                case "iOS":
                    return WhichPlatform.ios;
            }
        }
        else {
            return WhichPlatform.web;
        }
    }

    public static async handleHeadImg(img: eui.Image, url: string, imgWidth?, imgHeight?) {
        console.log("加载头像"+img.name);
        let platform = GameData.getPlatform();
        if (platform === WhichPlatform.android || platform === WhichPlatform.ios) {
            console.log("----headicon native----", url);
            RES.getResByUrl(url, (data: ArrayBuffer) => {
                // img.texture = data;
                console.log(data.byteLength, '------头像array buffer--');
                egret.BitmapData.create('arraybuffer', data, (bp: egret.BitmapData) => {
                    let texture = new egret.Texture();
                    texture.bitmapData = bp;
                    img.texture = texture;
                });
            }, this);
        }
        else {
            console.log("----headicon web----", url);
            img.source = <egret.Texture>RES.getRes("head_temp_png");
            if (imgWidth && imgHeight) {
                img.width = imgWidth;
                img.height = imgHeight;
            }
            // RES.getResByUrl(url, (data: egret.Texture) => {
            //     img.source = data;
            // }, this);
        }
    }

}

enum WhichPlatform {
    android,
    ios,
    web
}