class SerCodeReflect {

    /**
     *
     * 传入 枚举类型 和 code 获得中文信息
     * @param {*} type
     * @param {number} codeNum
     * @returns {string}
     * @memberof ServerCodeReflect
     */
    public static reflect(type: any, codeNum: number): string {
        let str = '';

        let nng = niuniu.GameCode;
        let thc = teahouse.TeaHouseCode;
        let lc = login.LoginCode;

        switch (type) {
            case nng:
                switch (codeNum) {
                    case nng.DIAMOND_NOT_ENOUGH:
                        str = '钻石不足';
                        break;
                    case nng.DUPLICATED_ENTER:
                        str = '重复加入桌子';
                        break;
                    case nng.INVALID_REQ:
                        str = '无效的请求';
                        break;
                    case nng.NOT_ENTER_RIGHT:
                        str = '没有加入的权限';
                        break;
                    case nng.QUIT_FAIL_GAME_START:
                        str = '游戏已经开始,不能退出';
                        break;
                    case nng.SERVER_BUSY:
                        str = '服务器桌子已经满了';
                        break;
                    case nng.SUCCESS:
                        break;
                    case nng.TABLE_FULL:
                        str = '桌子已经满了';
                        break;
                    case nng.TABLE_NUMBER_INVALID:
                        str = '无效的房间号';
                        break;
                    case nng.TEAHOUSE_ACC_FREEZE:
                        str = '该账号在茶楼被封禁';
                        break;
                    case nng.TEAHOUSE_CLOSED:
                        str = '茶楼已经关闭';
                        break;
                    case nng.TEAHOUSE_TABLE_FULL:
                        str = '该茶楼可以创建的桌子已经满了(最大20张)';
                        break;
                    case nng.UNKOWN:
                        str = '服务器忙';
                        break;
                    case nng.SCORE_NOT_ENOUGH:
                        str = '积分不足';
                        break;
                    case nng.CHARGET_DIAMOND_NOT_ENOUGH:
                        str = '代付的钻石不足';
                        break;
                }
                break;

            case thc:
                switch (codeNum) {
                    case thc.Duplicate_Name:
                        str = '名称重复';
                        break;
                    case thc.Duplicate_User:
                        str = '已经在茶楼里的';
                        break;
                    case thc.Invalid_Order:
                        str = '订单号无效';
                        break;
                    case thc.Invalid_OrderID:
                        str = '订单号无效';
                        break;
                    case thc.Invalid_TeaHouse:
                        str = '找不到茶楼';
                        break;
                    case thc.Invalid_User:
                        str = '找不到用户';
                        break;
                    case thc.Message_Send:
                        str = '信息已经发送';
                        break;
                    case thc.Partner_Exits:
                        str = '该用户已经被分配给其他合伙人了';
                        break;
                    case thc.Success:
                        break;
                    case thc.Table_Not_Allow_AddScore:
                        str = '桌子设置为不可上下分';
                        break;
                    case thc.Unknown:
                        str = '服务器忙';
                        break;
                    case thc.User_Not_In_TeaHouse:
                        str = '用户不是茶楼成员';
                        break;
                    case thc.User_Not_Normal:
                        str = '用户不是普通成员';
                        break;
                }

            case lc:
                switch (codeNum) {
                    case lc.FORBID:
                        str = '账号被封禁';
                        break;
                    case lc.INVALID_PASSWORD_OR_ACCOUNT:
                        str = '无效的电话号码或者密码';
                        break;
                    case lc.INVALID_PHONE_NUMBER:
                        str = '无效的电话号码';
                        break;
                    case lc.INVALID_VERIFY_CODE:
                        str = '无效的验证码';
                        break;
                    case lc.LOGIN_SUCCESS:
                        break;
                    case lc.REGISTER_SUCCESS:
                        str = 'REGISTER_SUCCESS';
                        break;
                    case lc.TOKEN_EXPIRE:
                        str = '请重新登录';
                        break;
                    case lc.TOKEN_INVALID:
                        str = '请重新登录';
                        break;
                    case lc.UNKNOWN:
                        str = '服务器忙';
                        break;
                    case lc.UNREGISTER_USER:
                        str = '未注册用户';
                        break;
                }
        }

        return str;
    }

}