class RoomInfo extends eui.Component {

    private close: eui.Button;
    private rule: eui.Label;
    private muilt: eui.Label;
    private advanced: eui.Label;
    private difen: eui.Label;
    private open: eui.Button;
    private group: eui.Group;

    public constructor() {
        super();
    }

    public setInfo(tableInfo: structure.ITableInfo) {
        this.skinName = skins.game.RoomInfo;
        this.close.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            this.group.visible = false;
        }, this);
        this.open.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            this.group.visible = true;
        }, this);

        let s = structure;
        let cfg = tableInfo.Config

        this.rule.text = cfg.ChargeCfg === s.TableConfig.ChargeConfig.AACharge ? "AA支付" : "房主支付" + '  ' + '推注' + cfg.TuizhuRule + '倍' + '  '
            + '最大抢庄' + cfg.MaxRankerMulti + '倍';

        this.muilt.text = '牛牛' + cfg.MultiRuler[10] + '倍' + '  ' + '牛九' + cfg.MultiRuler[9] + '倍' + '牛八' + cfg.MultiRuler[8] + '倍' + '牛七' + cfg.MultiRuler[7] + '倍' + '\n\n'
            + '顺子牛(' + cfg.MultiRuler[11] + ')倍 ' + '五花牛(' + cfg.MultiRuler[12] + ')倍 ' + '同花牛(' + cfg.MultiRuler[13] + ')倍 ' + '葫芦牛(' + cfg.MultiRuler[14] + ')倍 ' + '\n' + '炸弹牛(' + cfg.MultiRuler[15] + ')倍 '
            + '五小牛(' + cfg.MultiRuler[16] + ')倍 ' + '同花顺(' + cfg.MultiRuler[17] + ')倍 ';

        let arrScore = cfg.BaseScoreCfg.ValidBaseScore;
        this.difen.text = arrScore[0] + "/" + arrScore[1] + '/' + arrScore[2];

        this.advanced.text = this.getJoker(tableInfo) + ' ' + '允许搓牌' + ' ' + '允许中途加入';
    }

    private getJoker(tableInfo: structure.ITableInfo): string {
        let str = ''
        switch (tableInfo.Config.JokerType) {
            case structure.TableConfig.JokerTypeConfig.None:
                str = '无大小王'
                break;
            case structure.TableConfig.JokerTypeConfig.Classic:
                str = '经典王癞'
                break;
            case structure.TableConfig.JokerTypeConfig.Crazy:
                str = '疯狂王癞'
                break;
        }
        return str;
    }


}