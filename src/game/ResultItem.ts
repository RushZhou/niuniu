class ResultItem extends eui.ItemRenderer {
    private portrait: eui.Image;
    private userName: eui.Label;
    private rankTimes: eui.Label;
    private rankerTimes: eui.Label;
    private betTimes: eui.Label;
    private id: eui.Label;
    private score: eui.Label;

    private winer: eui.Image;
    private loser: eui.Image;

    public data;//: record.INiuniuUserRecord

    public constructor() {
        super();
    }

    protected dataChanged() {
        this.portrait.source = RES.getRes('head_temp_png');
        this.userName.text = this.data.NickName;
        let totalScore = 0;
        for (let score of this.data.HandRec) {
            totalScore += <number>score.Score;
        }

        this.score.text = totalScore >= 0 ? '+' + totalScore : totalScore.toString();
        this.rankTimes.text = this.data.RankCount.toString();
        this.rankerTimes.text = this.data.RankerCount.toString();
        this.betTimes.text = this.data.TuizhuCount.toString();
        this.id.text = this.data.UserId.toString();
        this.winer.visible = this.data.tag === 1 ? true : false;
        this.loser.visible = this.data.tag === -1 ? true : false;
    }
}