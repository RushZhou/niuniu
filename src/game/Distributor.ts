class DistributorPnl extends eui.Component {

    public constructor() {
        super();
        this.skinName = 'skins.game.DistributorSkins';
    }

    // public color1: ModifCheckBox;
    // public value1: ModifCheckBox;
    // public color2: ModifCheckBox;
    // public value2: ModifCheckBox;
    // public color3: ModifCheckBox;
    // public value3: ModifCheckBox;
    // public color4: ModifCheckBox;
    // public value4: ModifCheckBox;
    // public color5: ModifCheckBox;
    // public value5: ModifCheckBox;
    public Confirm: eui.Button;

    private PukeNum = 5;
    private ColorTypeCount = 4;
    private ValueCount = 13;

    protected childrenCreated() {
        super.childrenCreated();

        let allCheckBox = [];

        for (let i = 0; i < this.PukeNum; i++) {
            let color = new ModifCheckBox();
            let value = new ModifCheckBox();
            this.addChild(color);
            color.x = 238;
            color.y = 78 * (i + 1);
            value.x = 664;
            value.y = 78 * (i + 1);
            this.addChild(value);
            allCheckBox.push({ color: color, value: value });
        }
        for (let i = 1; i <= this.ColorTypeCount; i++) {
            let color = '';
            switch (i) {
                case 1:
                    color = "方片";
                    break;
                case 2:
                    color = "梅花";
                    break;
                case 3:
                    color = "红桃";
                    break;
                case 4:
                    color = "黑桃";
                    break;
            }
            for (let j = 0; j < this.PukeNum; j++) {
                allCheckBox[j].color.addItem({ name: color, value: i });
                allCheckBox[j].color.setSelect(0);
            }
        }

        for (let i = 1; i <= this.ValueCount; i++) {
            let value = '';
            if (i === 1) {
                value = 'A';
            } else if (i > 1 && i <= 10) {
                value = i.toString();
            } else if (i === 11) {
                value = 'J';
            } else if (i === 12) {
                value = 'Q';
            } else if (i === 13) {
                value = 'K';
            }
            for (let j = 0; j < this.PukeNum; j++) {
                allCheckBox[j].value.addItem({ name: value, value: i });
                allCheckBox[j].value.setSelect(0);
            }
        }

        this.Confirm.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let pukeCards = [];

            for (let i = 0; i < allCheckBox.length; i++) {
                let color = allCheckBox[i].color.selectItem.value;
                let value = allCheckBox[i].value.selectItem.value;
                console.log("--------",color,value);
                let endVale = color * 16  + value;
                pukeCards.push(endVale);
            }
            console.log('上传的扑克', pukeCards);
            let modifyCardReq = new niuniu.ModifyCardREQ();
            modifyCardReq.Card = pukeCards;
            modifyCardReq.UserID = GameData.userInfo.ID;
            MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_ModifyCardREQ,niuniu.ModifyCardREQ,modifyCardReq);
            this.parent.removeChild(this);
        }, this);

    }


}