class GameTable extends eui.Component {
    private people6: eui.Group;
    private people8: eui.Group;
    private people10: eui.Group;

    private btnStart: eui.Button;
    private btnSeat: eui.Button;
    private btnPrepare: eui.Button;

    // private quit: eui.Button;

    //房间属性显示
    private round: eui.Label;
    private diFen: eui.Label;
    private teahouseID: eui.Label;
    private tableNum: eui.Label;
    private tuiZhu: eui.Label;

    //bet组
    private betGroup: eui.Group;
    private bet10: eui.Button;
    private bet20: eui.Button;
    private bet30: eui.Button;
    private crazyBet: CrazyBet;

    //抢庄btn 组
    private qiangZhuangGroup: eui.Group;
    private qiangZhuang1: eui.Button;
    private qiangZhuang2: eui.Button;
    private qiangZhuang3: eui.Button;
    private qiangZhuang4: eui.Button;
    private qiangZhuang0: eui.Button;
    //座位组 group
    private seatGroup6: eui.Group;
    private seatGroup8: eui.Group;
    private seatGroup10: eui.Group;

    //发牌点
    private sendPokerPoint: EmptyPoint;

    //当前使用的座位组
    private usingSeatGroup: eui.Group;

    //已经在这个房间的玩家
    private playersExist: structure.ITablePlayer[];
    //已经坐下的玩家实例
    private playersInst: Player[] = new Array();

    private maxPlayer: number;
    //是否坐下
    private isSelfSeat: boolean;
    //是否在玩牌
    private isPlaying: boolean;
    private tableInfo: structure.ITableInfo;

    //控制btn组
    private ctrlGroup: eui.Group;
    //给所有玩家显牌
    private btnShowCard: eui.Button;
    //获取牌型btn
    private btnGetCardType: eui.Button;
    //翻牌btn
    private btnTurn: eui.Button;
    //搓牌
    private btnMakeCard: eui.Button;

    //倒计时组
    private countDown: eui.Group;
    private timer: egret.Timer = new egret.Timer(1000);
    //抢庄 闪烁框 loopID
    private blinkID: number;
    //是否为重连 进入
    private isRec: boolean;
    //第一手牌 已经下发
    private isHandPlay: boolean;
    private holdTip: eui.Group;
    private roomInfo: RoomInfo;
    //保存推注的数据
    private numsCrazyBet: number[];

    //层级管理
    //玩家层
    private layer1: eui.Group;
    //金币动画飞行
    private layer2: eui.Group;
    //挂机图层
    private layer3: eui.Group;



    /**
     * 座位号偏移值 所有人坐下都要加上该偏移值
     * 该值是 当自己坐下时 动态计算出来的
     *
     * @type {number}
     * @memberof GameTable
     */
    private seatOffset: number = 0;
    /**
     *Creates an instance of GamePlay.
     * @param {number} tableInfo 桌子的配置
     * @memberof GamePlay
     * @memberof rec        是否为断线重连
     */
    public constructor(tableInfo: structure.ITableInfo, rec?: boolean) {
        super();
        MsgMgr.inst.msgLock = true;
        this.isRec = rec;
        this.tableInfo = tableInfo;
        this.skinName = skins.game.GameTable;
        this.isHandPlay = tableInfo.IsHandPlay
        this.holdTip.visible = this.isHandPlay;
        RES.loadGroup('poker').then(() => {
            console.log('poker res load cpl')
            RES.loadGroup('game').then(() => {
                console.log('game res load cpl')
                this.init(tableInfo);
                //断线重连处理 模拟sitAck消息 断线消息里面没有sitAck
                if (rec) {
                    this.isPlaying = true;
                    for (let info of tableInfo.Player) {
                        if (info.Seat >= 0) {
                            let SitACK = new niuniu.SitACK();
                            SitACK.Seat = info.Seat;
                            SitACK.UserID = info.UserID;
                            this.seatAckHdl(SitACK, this);
                        }
                    }
                    this.holdTip.visible = false;
                }
                MsgMgr.inst.msgLock = false;
            })
        })
        this.addEventListener(egret.Event.ADDED_TO_STAGE, () => {
            this.roomInfo.setInfo(tableInfo);
        }, this);
    }

    private init(tableInfo: structure.ITableInfo) {
        this.addEventListener(egret.Event.DEACTIVATE, () => {
            console.log('----------------deactivie------------')
        }, this)

        this.addEventListener(egret.Event.ACTIVATE, () => {
            console.log('----------------active-----------')
        }, this)


        this.round.text = tableInfo.Hand + '/' + tableInfo.Config.MaxRound;
        let arrScore = tableInfo.Config.BaseScoreCfg.ValidBaseScore;
        this.diFen.text = arrScore[0] + "/" + arrScore[1] + "/" + arrScore[2];
        this.tableNum.text = "房号:" + tableInfo.TableID;

        this.teahouseID.text = tableInfo.HouseID ? tableInfo.HouseID.toString() : "无";
        this.tuiZhu.text = tableInfo.Config.TuizhuRule + "倍";

        this.people6.visible = false;
        this.people8.visible = false;
        this.people10.visible = false;

        this.countDown.visible = false;

        this.maxPlayer = tableInfo.Config.MaxPlayer;
        switch (tableInfo.Config.MaxPlayer) {
            case 6:
                this.people6.visible = true;
                this.usingSeatGroup = this.seatGroup6;
                break;
            case 8:
                this.people8.visible = true;
                this.usingSeatGroup = this.seatGroup8;
                break;
            case 10:
                this.people10.visible = true;
                this.usingSeatGroup = this.seatGroup10;
                break;
        }


        //根据房间配置  配置 ranker倍率
        let maxRanker = tableInfo.Config.MaxRankerMulti;
        this.qiangZhuangGroup.visible = false;
        this.rankEnable(false);

        for (let i = 0; i <= maxRanker; i++) {
            this.qiangZhuangGroup.getChildAt(i).visible = false;
        }
        //根据房间配置 实例化rank 倍率数量
        let rankerGroupLoop = this.qiangZhuangGroup.numChildren;
        let aiRankBtns = [];
        for (let i = 0; i < rankerGroupLoop; i++) {
            let child = <eui.Button>this.qiangZhuangGroup.getChildAt(i);
            if (!child.visible) {
                child.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
                child.visible = true;
                aiRankBtns.push(child);
            }
            else {
                this.qiangZhuangGroup.removeChild(child);
                i--;
                rankerGroupLoop -= 1;
            }
        }
        AiPlay.inst.registOrder({ order: niuniu.NiuniuID.ID_RankREQ, btns: aiRankBtns });
        let layoutHor = new eui.HorizontalLayout();
        layoutHor.gap = 20;
        layoutHor.horizontalAlign = egret.HorizontalAlign.CENTER;
        this.qiangZhuangGroup.layout = layoutHor;


        for (let i = 0; i < this.ctrlGroup.numChildren; i++) {
            let btn = <eui.Button>this.ctrlGroup.getChildAt(i);
            btn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
            btn.visible = false;
        }
        this.btnSeat.visible = true;
        // this.quit.visible = true;

        //根据房间配置 配置 bet数值
        let aiBetBtns = [];
        for (let i = 0; i < this.betGroup.numChildren; i++) {
            let btn = <eui.Button>this.betGroup.getChildAt(i);
            btn.label = this.tableInfo.Config.BaseScoreCfg.ValidBaseScore[i].toString();
            btn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
            aiBetBtns.push(btn)
        }
        this.betGroup.visible = false;
        AiPlay.inst.registOrder({ order: niuniu.NiuniuID.ID_BetREQ, btns: aiBetBtns });

        let nID = niuniu.NiuniuID;
        AiPlay.inst.registOrder({ order: niuniu.NiuniuID.ID_PrepareREQ, btns: [this.btnPrepare] });
        AiPlay.inst.registOrder({ order: niuniu.NiuniuID.ID_ShowCardREQ, btns: [this.btnShowCard] });
        this.layer3.addChild(AiPlay.inst);

        this.addEventListener(egret.TouchEvent.TOUCH_TAP, (evt: egret.TouchEvent) => {
            let tar = evt.target
            if (tar instanceof eui.Label || tar instanceof eui.Image || tar instanceof eui.Button || tar instanceof eui.Group) {
                AiPlay.inst.resetTime();
            }
        }, this);

        this.layer3.addChild(MakeCard.inst);
        MakeCard.inst.addEventListener(MakeCardEvt.MakeCardFinish, () => {
            this.btnMakeCard.visible = false;
            this.btnTurn.visible = false;
            this.btnShowCard.visible = true;
            this.btnGetCardType.visible = true;
            this.getTablePlayerInst(GameData.userInfo.ID).turnLastCard();
        }, this);



        //=============================================================================================================
        //侦听 房间信息变化
        MsgMgr.inst.onMsg(nID.ID_TableInfoNTF, niuniu.TableInfoNTF, (msg: niuniu.TableInfoNTF) => {
            console.log(msg.Info, '------房间信息变化------');
            if (this.isHandPlay && !this.isRec) return;
            this.playersExist = msg.Info[0].Player;
            //刷新座位
            for (let info of msg.Info[0].Player) {
                if (info.UserID === GameData.userInfo.ID) {
                    this.seatOffset = (this.maxPlayer - 1) - (info.Seat);
                }
            }
            for (let inst of this.playersInst) {
                for (let info of msg.Info[0].Player) {
                    if (inst.userID === info.UserID) {
                        inst.playerInfo.Seat = info.Seat;
                        inst.refreshUIstyle(this.maxPlayer, this.getOffsetSeat(inst.playerInfo.Seat));
                        let seatPos = this.usingSeatGroup.getChildAt(this.getOffsetSeat(inst.playerInfo.Seat));
                        inst.x = seatPos.x;
                        inst.y = seatPos.y;
                    }
                }
            }

        }, this);

        //侦听玩家 坐下
        MsgMgr.inst.onMsg(nID.ID_SitACK, niuniu.SitACK, this.seatAckHdl, this);

        //侦听玩家 退出
        MsgMgr.inst.onMsg(nID.ID_LeaveGameACK, niuniu.LeaveGameACK, (msg: niuniu.LeaveGameACK) => {
            console.log(msg, '-----有玩家退出------');
            if (!msg.Code) {
                switch (msg.Reason) {
                    //游戏结束 不处理等待用户点击按钮 退出
                    case niuniu.LeaveGameACK.LeaveReason.GameOver:
                        break;
                    //其他情况 需要根据msg 清理
                    default:
                        if (msg.UserID === GameData.userInfo.ID) {
                            this.destroy();
                        }
                        else {
                            // 观察者和坐下的玩家 都要判断
                            for (let i = 0; i < this.playersExist.length; i++) {
                                let player = this.playersExist[i];
                                if (player.UserID === msg.UserID) {
                                    this.playersExist.splice(i, 1);
                                }
                            }

                            for (let inst of this.playersInst) {
                                if (inst.userID === msg.UserID) {
                                    inst.destroy();
                                }
                            }
                        }
                        break;
                }
            }
            else {
                Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code));
            }
        }, this);


        MsgMgr.inst.onMsg(nID.ID_StartGameACK, niuniu.StartGameACK, (msg: niuniu.StartGameACK) => {
            console.log(msg, "------开始游戏ACk-----");
            this.btnStart.visible = false;
        }, this);

        MsgMgr.inst.onMsg(nID.ID_BeginHandNTF, niuniu.BeginHandNTF, (msg: niuniu.BeginHandNTF) => {
            console.log(msg, '------开始一局牌------')
            SoundManager.ins.playEffect('game_start_mp3');
            this.tableInfo.Hand = msg.Hand + 1;
            this.round.text = this.tableInfo.Hand + '/' + tableInfo.Config.MaxRound;
            for (let inst of this.playersInst) {
                inst.clearPoker();
                inst.enableRanker(false);
                inst.enableBetGroup(false);
                inst.enableRankMulti(false);
                inst.enableCardType(false);
                inst.enablePrepare(false);
            }

            for (let player of msg.Info) {
                if (player.UserID === GameData.userInfo.ID) {
                    this.isPlaying = true;
                }
            }
            this.countDownEnable(false);
            this.btnPrepare.visible = false;
            AiPlay.inst.start();
        }, this);

        MsgMgr.inst.onMsg(nID.ID_PrepareACK, niuniu.PrepareACK, (msg: niuniu.PrepareACK) => {
            console.log(msg, '------------玩家主动准备-------');
            this.getTablePlayerInst(msg.UserID).enablePrepare(true);
        }, this);

        //侦听 发牌前4张
        MsgMgr.inst.onMsg(nID.ID_DispatchFirstFourCardNTF, niuniu.DispatchFirstFourCardNTF, (msg: niuniu.DispatchFirstFourCardNTF) => {
            console.log(msg, "------发牌 前4张-----");
            if (this.isHandPlay && !this.isRec) return;
            let inst = this.getTablePlayerInst(msg.UserID);
            inst.sendPoker(msg.Cards, this.sendPokerPoint);
            if (msg.UserID === GameData.userInfo.ID) {
                //显示抢庄按钮
                this.rankEnable(true, inst.myCoin);
                console.log('-----倒计时---', msg.CountDown);
                this.countDownEnable(false);
                this.countDownEnable(true, '请操作抢庄', msg.CountDown);
                this.numsCrazyBet = <number[]>msg.CanBet;
                AiPlay.inst.orderInReady(niuniu.NiuniuID.ID_RankREQ);
            }
            console.log(msg.UserID, msg.CanBet.length, typeof msg.CanBet)
            if (msg.CanBet.length > 0) {
                inst.enableCrazyBetMark(true);
            }
        }, this);

        //侦听 发牌最后1张
        MsgMgr.inst.onMsg(nID.ID_DispatchLastCardNTF, niuniu.DispatchLastCardNTF, (msg: niuniu.DispatchLastCardNTF) => {
            console.log(msg, "----发牌-----发最后一张-----");
            if (this.isHandPlay && !this.isRec) return;
            this.betGroup.visible = false;
            this.crazyBet.enable(false);
            for (let inst of this.playersInst) {
                if (inst.userID === msg.UserID) {
                    inst.sendPoker([msg.Card], this.sendPokerPoint);
                    console.log('-----倒计时---', msg.CountDown);
                    this.countDownEnable(false);
                    this.countDownEnable(true, '查看手牌', msg.CountDown);
                    if (inst.userID === GameData.userInfo.ID) {
                        AiPlay.inst.orderInReady(niuniu.NiuniuID.ID_ShowCardREQ);
                    }
                }
            }
            if (this.isPlaying) {
                this.btnTurn.visible = true;

                this.btnMakeCard.visible = true;
            }
        }, this);

        //侦听 某一个人抢庄
        MsgMgr.inst.onMsg(nID.ID_RankACK, niuniu.RankACK, (msg: niuniu.RankACK) => {
            console.log(msg, '-----有人---抢庄-----');
            if (this.isHandPlay && !this.isRec) return;
            if (!msg.Code) {
                console.log(msg.UserId, msg.Multiple);
                this.getTablePlayerInst(msg.UserId).enableRankMulti(true, msg.Multiple);
            }
        }, this);

        //侦听 抢庄结果
        MsgMgr.inst.onMsg(nID.ID_RankNTF, niuniu.RankNTF, (msg: niuniu.RankNTF) => {
            console.log(msg, '--------抢庄结果下发-----');
            if (this.isHandPlay && !this.isRec) return;
            if (this.isPlaying) {
                console.log('-----倒计时---', msg.CountDown);
                this.countDownEnable(false);
                this.countDownEnable(true, '请操作下注', msg.CountDown);
            }

            this.rankEnable(false);
            let blinkInterval = 40;
            let i = 0;
            let lastInst: Player;

            this.blinkID = egret.setInterval(
                () => {
                    if (msg.RankUser.length === 1) {
                        let currentInst = this.getTablePlayerInst(msg.RankUser[msg.RankUser.length - 1]);
                        lastInst = currentInst;
                        currentInst.enabledBlinkRank(true, true);
                    } else {
                        if (lastInst) lastInst.enabledBlinkRank(false);
                        let currentInst = this.getTablePlayerInst(msg.RankUser[i % msg.RankUser.length]);
                        lastInst = currentInst;
                        currentInst.enabledBlinkRank(true);
                    }
                    i++
                }, this, blinkInterval, i, lastInst
            )


            let delayFunc = () => {
                egret.clearInterval(this.blinkID);
                setTimeout(() => {
                    for (let inst of this.playersInst) {
                        inst.enabledBlinkRank(false);
                    }

                    //自己不是ranker 显示下注按钮
                    if (msg.RankerUser !== GameData.userInfo.ID && this.isSelfSeat && this.isPlaying) {
                        this.betGroup.visible = true;
                        this.crazyBet.enable(true, this.numsCrazyBet, this.getTablePlayerInst(GameData.userInfo.ID).myCoin, this.tableInfo.Config.MatchType, msg.RankerMultiple);
                        AiPlay.inst.orderInReady(nID.ID_BetREQ);
                    }
                    //显示 玩家 ranker mark
                    this.getTablePlayerInst(msg.RankerUser).enableRanker(true);
                    this.getTablePlayerInst(msg.RankerUser).enableCrazyBetMark(false);

                    for (let inst of this.playersInst) {
                        //非 ranker && 已经在玩牌的 显示下注数量
                        if (inst.userID !== msg.RankerUser && inst.isPlaying) {
                            inst.enableBetGroup(true);
                            inst.enableRankMulti(false);
                        }
                    }
                }, blinkInterval * 2)
            }
            setTimeout(delayFunc, 1500);

        }, this);

        //下注消息
        MsgMgr.inst.onMsg(nID.ID_BetACK, niuniu.BetACK, (msg: niuniu.BetACK) => {
            console.log(msg, '------下注ACK-----')
            if (this.isHandPlay && !this.isRec) return;
            if (!msg.Code) {
                let inst = this.getTablePlayerInst(msg.UserID);
                inst.bet(msg.Count);
                inst.enableCrazyBetMark(false);
                if (msg.UserID === GameData.userInfo.ID) {
                    this.betGroup.visible = false;
                    this.crazyBet.enable(false);
                }
            }

        }, this);

        //有人亮牌
        MsgMgr.inst.onMsg(nID.ID_ShowCardACK, niuniu.ShowCardACK, (msg: niuniu.ShowCardACK) => {
            console.log(msg, '---------有人亮牌---------')
            if (this.isHandPlay && !this.isRec) return;
            let inst = this.getTablePlayerInst(msg.UserId);
            inst.showPoker(msg.Card);
            inst.isPlaySound = false;
            inst.enableCardType(true, msg.CardType);
        }, this);

        //提示牌面
        MsgMgr.inst.onMsg(nID.ID_TipACK, niuniu.TipACK, (msg: niuniu.TipACK) => {
            console.log(msg, '------收到 提示牌面-----')
            if (this.isHandPlay && !this.isRec) return;
            let inst = this.getTablePlayerInst(GameData.userInfo.ID);
            inst.showPoker(msg.Card);
            inst.isPlaySound = true;
            inst.enableCardType(true, msg.CardType);
        }, this);



        //一把结束 显示所有玩家的牌 
        MsgMgr.inst.onMsg(nID.ID_SettlementNTF, niuniu.SettlementNTF, (msg: niuniu.SettlementNTF) => {
            console.log(msg, '---------- 显示所有玩家的牌-------')
            if (this.isHandPlay && !this.isRec) return;

            let winners: niuniu.SettlementNTF.ISettleUser[] = [];
            let losers: niuniu.SettlementNTF.ISettleUser[] = [];
            for (let user of msg.Users) {
                let inst = this.getTablePlayerInst(user.UserID);
                inst.showPoker(user.Pokers);
                inst.settlement(user);
                inst.enableCardType(true, user.PokerType);
                if (user.UserID !== msg.Ranker) {
                    if (user.Win > 0) winners.push(user);
                    if (user.Win < 0) losers.push(user);
                }
                inst.isPlaySound = false;
            }

            let rankerInst = this.getTablePlayerInst(msg.Ranker);
            console.log(winners, losers, msg.Ranker);

            for (let loser of losers) {
                let inst = this.getTablePlayerInst(loser.UserID);
                let pos = rankerInst.getCoinPosGlobal();
                inst.winLoseCoinAction(<number>loser.Win, pos, this.layer2);
            }
            egret.setTimeout(() => {
                for (let winner of winners) {
                    let inst = this.getTablePlayerInst(winner.UserID);
                    let pos = inst.getCoinPosGlobal();
                    rankerInst.winLoseCoinAction(<number>winner.Win, pos, this.layer2);
                }
            }, this, 1500)

            this.btnTurn.visible = false;
            this.btnGetCardType.visible = false;
            this.btnShowCard.visible = false;
            this.btnMakeCard.visible = false;
            MakeCard.inst.enableMakeCard(false);
            this.countDownEnable(false);
            if (this.tableInfo.Hand < this.tableInfo.Config.MaxRound) this.countDownEnable(true, '下局即将开始', msg.CountDown);
        }, this);

        //一把结束 桌子回复到 初始状态
        MsgMgr.inst.onMsg(nID.ID_EndHandNTF, niuniu.EndHandNTF, (msg: niuniu.EndHandNTF) => {
            console.log(msg, '---------一把结束--- 收到 end hand')
            this.isHandPlay = false;
            this.holdTip.visible = false;
            if (this.tableInfo.Hand < this.tableInfo.Config.MaxRound) this.btnPrepare.visible = true;
            AiPlay.inst.orderInReady(niuniu.NiuniuID.ID_PrepareREQ);
        }, this);

        //刷新玩家score 可能是桌上 || 观战的
        MsgMgr.inst.onMsg(nID.ID_ScoreChangeNTF, niuniu.ScoreChangeNTF, (msg: niuniu.ScoreChangeNTF) => {
            console.log("---------收到score change ntf---------", msg.Score, msg.UserID);

            let tableInst = this.getTablePlayerInst(msg.UserID);
            if (tableInst) {
                tableInst.updateScore(msg);
            }
            else {
                this.getPlayerInfo(msg.UserID).Score = msg.Score;
            }
        }, this);

        //显示 已经坐在 桌上的玩家
        //如果是断线重连 会模拟sitAck 处理 这里就不再需要了
        if (!this.isRec) {
            for (let playerInfo of tableInfo.Player) {
                if (playerInfo.Seat >= 0) {
                    let player = new Player(playerInfo, tableInfo.Config, this.getOffsetSeat(playerInfo.Seat), this.maxPlayer)
                    if (this.usingSeatGroup) {
                        let seatPos = this.usingSeatGroup.getChildAt(playerInfo.Seat);
                        player.x = seatPos.x;
                        player.y = seatPos.y;
                        this.layer1.addChild(player);
                    }
                    this.playersInst.push(player);
                }
            }
        }
        else {
            this.btnSeat.visible = false;
            this.btnStart.visible = false;
        }


        //一轮游戏结束 推送结果消息
        MsgMgr.inst.onMsg(nID.ID_RounRecordNTF, niuniu.RounRecordNTF, (msg: niuniu.RounRecordNTF) => {
            console.log(msg, '--------一轮结束---收到结果---')
            let key = egret.setTimeout(() => {
                console.log('-----------------------显示结算面板--------')
                this.destroy();
                let keyres = egret.setTimeout(() => {
                    let resultPnl = new ResultPnl(msg, tableInfo);
                    Main.inst.addChild(resultPnl);
                    egret.clearTimeout(keyres);
                }, this, 500);
                // let num = Main.inst.numChildren;// 先获取皮肤上的元件个数
                // Main.inst.setChildIndex(resultPnl, num + 100)
                egret.clearTimeout(key);
            }, this, 4000);

        }, this);

        this.playersExist = tableInfo.Player;

        //timer 事件
        this.timer.addEventListener(egret.TimerEvent.TIMER, () => {
            let label = <eui.Label>this.countDown.getChildAt(1);
            let strSplit: string[] = label.text.split(':');
            let time = parseInt(strSplit[1]);
            if (time === 0) {
                this.countDownEnable(false);
            }
            label.text = strSplit[0] + ":" + (time - 1) + '';
        }, this);
        this.timer.addEventListener(egret.TimerEvent.TIMER_COMPLETE, () => {
            this.countDownEnable(false);
        }, this);

        this.crazyBet.addEventListener(EnumCrazyBet.Bet, (evt: egret.TouchEvent) => {
            console.log('crazyBet')
            SoundManager.ins.playEffect('game_bet2_mp3');
            this.crazyBet.enable(false);
            this.betGroup.visible = false;
            let betReq = new niuniu.BetREQ();
            betReq.Count = evt.data;
            MsgMgr.inst.sendMsg(nID.ID_BetREQ, niuniu.BetREQ, betReq);
        }, this);
    }

    private seatAckHdl(msg: niuniu.SitACK, caller: GameTable) {
        console.log(msg, '-----有玩家坐下----')
        if (!msg.Code) {
            let isSelf = false;
            if (msg.UserID === GameData.userInfo.ID) {
                caller.isSelfSeat = true;
                isSelf = true;
                caller.seatOffset = (caller.maxPlayer - 1) - (msg.Seat);
                console.log(caller.seatOffset);
                //自己坐下时 根据偏移座位 刷新所有玩家的座位号
                for (let inst of caller.playersInst) {
                    inst.refreshUIstyle(caller.maxPlayer, caller.getOffsetSeat(inst.playerInfo.Seat));
                    let seatPos = caller.usingSeatGroup.getChildAt(caller.getOffsetSeat(inst.playerInfo.Seat));
                    inst.x = seatPos.x;
                    inst.y = seatPos.y;
                }
                //是否显示开始游戏 如果已经开始不显示按钮
                if (!this.isPlaying) {
                    let startEnum = structure.TableConfig.AutoStart;
                    switch (caller.tableInfo.Config.AutoStartCfg.AutoStart) {
                        case startEnum.FirstStart:
                            if (msg.Seat === 0) {
                                caller.btnStart.visible = true;
                            }
                            break;

                        case startEnum.OwnerStart:
                            if (caller.tableInfo.Owner === GameData.userInfo.ID) {
                                caller.btnStart.visible = true;
                            }
                            break;
                    }
                }


            }
            let getPlayer = caller.getPlayerInfo(msg.UserID);
            if (!getPlayer) {
                console.log('----------当前发送的 userID  查无此人----', msg)
            }
            getPlayer.Seat = msg.Seat;
            let player = new Player(getPlayer, caller.tableInfo.Config, isSelf ? caller.maxPlayer - 1 : caller.getOffsetSeat(msg.Seat), caller.maxPlayer);
            let seatPos = caller.usingSeatGroup.getChildAt(isSelf ? caller.maxPlayer - 1 : caller.getOffsetSeat(msg.Seat));
            player.x = seatPos.x;
            player.y = seatPos.y;
            caller.layer1.addChild(player);
            caller.playersInst.push(player);
        }
        else {
            Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code))
            caller.btnSeat.visible = true;
        }
    }

    private tapHdl(evt: egret.TouchEvent) {

        let nID = niuniu.NiuniuID;
        switch (evt.target) {
            case this.btnStart:
                let startReq = new niuniu.StartGameREQ();
                MsgMgr.inst.sendMsg(nID.ID_StartGameREQ, niuniu.StartGameREQ, startReq);
                break;

            case this.btnSeat:
                let seatReq = new niuniu.SitREQ();
                MsgMgr.inst.sendMsg(nID.ID_SitREQ, niuniu.SitREQ, seatReq);
                this.btnSeat.visible = false;
                SoundManager.ins.playEffect('sfx_button_click_mp3');
                break;

            case this.qiangZhuang0:
            case this.qiangZhuang1:
            case this.qiangZhuang2:
            case this.qiangZhuang3:
            case this.qiangZhuang4:
                let qzReq = new niuniu.RankREQ();
                switch (evt.target) {
                    case this.qiangZhuang0:
                        qzReq.Multiple = 0;
                        break;
                    case this.qiangZhuang1:
                        qzReq.Multiple = 1;
                        break;
                    case this.qiangZhuang2:
                        qzReq.Multiple = 2;
                        break;
                    case this.qiangZhuang3:
                        qzReq.Multiple = 3;
                        break;
                    case this.qiangZhuang4:
                        qzReq.Multiple = 4;
                        break;
                }
                MsgMgr.inst.sendMsg(nID.ID_RankREQ, niuniu.RankREQ, qzReq);
                this.rankEnable(false);
                AiPlay.inst.resetTime();
                SoundManager.ins.playEffect('sfx_button_click_mp3');
                break;

            // case this.qiangZhuang0:
            //     this.rankEnable(false);
            //     break;

            case this.bet10:
            case this.bet20:
            case this.bet30:
                let betReq = new niuniu.BetREQ();
                let clickBtn = <eui.Button>evt.target;
                betReq.Count = parseInt(clickBtn.label);
                MsgMgr.inst.sendMsg(nID.ID_BetREQ, niuniu.BetREQ, betReq);
                this.betGroup.visible = false;
                this.crazyBet.enable(false);
                AiPlay.inst.resetTime();
                SoundManager.ins.playEffect('game_bet2_mp3');
                break;

            // case this.quit:
            //     let quitReq = new niuniu.LeaveGameREQ();
            //     MsgMgr.inst.sendMsg(nID.ID_LeaveGameREQ, niuniu.LeaveGameREQ, quitReq);
            //     break;

            case this.btnTurn:
                this.btnTurn.visible = false;
                this.btnGetCardType.visible = true;
                this.btnShowCard.visible = true;
                this.getTablePlayerInst(GameData.userInfo.ID).turnLastCard();
                AiPlay.inst.resetTime();
                MakeCard.inst.enableMakeCard(false);
                this.btnMakeCard.visible = false;
                SoundManager.ins.playEffect('sfx_button_click_mp3');
                break;

            case this.btnShowCard://翻牌
                this.btnTurn.visible = false;
                this.btnGetCardType.visible = false;
                this.btnShowCard.visible = false;
                this.btnMakeCard.visible = false;
                MsgMgr.inst.sendMsg(nID.ID_ShowCardREQ, niuniu.ShowCardREQ, new niuniu.ShowCardREQ);
                AiPlay.inst.resetTime();
                MakeCard.inst.enableMakeCard(false);
                SoundManager.ins.playEffect('sfx_button_click_mp3');
                break;

            case this.btnGetCardType://提示
                this.btnGetCardType.visible = false;
                let tipReq = new niuniu.TipREQ();
                MsgMgr.inst.sendMsg(nID.ID_TipREQ, niuniu.TipREQ, tipReq);
                AiPlay.inst.resetTime();
                SoundManager.ins.playEffect('sfx_button_click_mp3');
                break;

            case this.btnPrepare://准备
                this.btnPrepare.visible = false;
                MsgMgr.inst.sendMsg(nID.ID_PrepareREQ, niuniu.PrepareREQ, new niuniu.PrepareREQ());
                AiPlay.inst.resetTime();
                SoundManager.ins.playEffect('sfx_button_click_mp3');
                break;

            case this.btnMakeCard://搓牌
                MakeCard.inst.enableMakeCard(true, this.getTablePlayerInst(GameData.userInfo.ID).getLastCard());
                SoundManager.ins.playEffect('sfx_button_click_mp3');
                break;
        }
    }

    private countDownEnable(enable: boolean, content?: string, time?: number) {
        let label = <eui.Label>this.countDown.getChildAt(1);
        if (enable) {
            label.text = content + ':' + time.toString();
            this.timer.start();
            this.countDown.visible = true;
        }
        else {
            this.countDown.visible = false;
            label.text = '';
            this.timer.reset();
        }
    }

    private rankEnable(enable: boolean, currentScore?: number) {
        for (let i = 1; i < this.qiangZhuangGroup.numChildren; i++) {
            let btn = <eui.Button>this.qiangZhuangGroup.getChildAt(i);
            btn.enabled = true;
        }

        if (this.tableInfo.Config.MatchType === structure.TableConfig.MatchTypeConfig.Match) {
            if (currentScore < this.tableInfo.DrawConfig.RankScore) {
                for (let i = 1; i < this.qiangZhuangGroup.numChildren; i++) {
                    let btn = <eui.Button>this.qiangZhuangGroup.getChildAt(i);
                    btn.enabled = false;
                }
            }
        }
        this.qiangZhuangGroup.visible = enable;
    }

    /**
     * 从所有玩家信息里面 获取玩家信息
     *
     * @private
     * @param {(number | Long)} id
     * @returns {structure.ITablePlayer}
     * @memberof GameTable
     */
    private getPlayerInfo(id: number | Long): structure.ITablePlayer {
        for (let player of this.playersExist) {
            if (player.UserID === id) {
                return player;
            }
        }
        return null;
    }


    /**
     * 获取正在桌上的玩家 实例
     *
     * @private
     * @param {(number | Long)} id
     * @returns {Player}
     * @memberof GameTable
     */
    private getTablePlayerInst(id: number | Long): Player {
        for (let inst of this.playersInst) {
            if (inst.userID === id) {
                return inst;
            }
        }
        return null;
    }

    private getOffsetSeat(seatNum: number): number {
        let seat = (this.seatOffset + seatNum) % (this.maxPlayer);
        return seat;
    }

    private destroy() {
        for (let i = 0; i < this.qiangZhuangGroup.numChildren; i++) {
            let child = <eui.Button>this.qiangZhuangGroup.getChildAt(i);
            child.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        }

        for (let i = 0; i < this.ctrlGroup.numChildren; i++) {
            let btn = <eui.Button>this.ctrlGroup.getChildAt(i);
            btn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        }

        for (let i = 0; i < this.betGroup.numChildren; i++) {
            let btn = <eui.Button>this.betGroup.getChildAt(i);
            btn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        }

        this.timer.removeEventListener(egret.TimerEvent.TIMER, () => {
            let label = <eui.Label>this.countDown.getChildAt(1);
            label.text = parseInt(label.text) - this.timer.currentCount + '';
        }, this);
        this.timer.removeEventListener(egret.TimerEvent.TIMER_COMPLETE, () => {
            this.countDownEnable(false);
        }, this);

        MsgMgr.inst.offMsgAll(this);
        UserInfo.inst.destroy();
        AiPlay.inst.destroy();
        this.parent.removeChild(this);
        Main.inst.dispatchEventWith(GameData.GameEvent.ChangeSound, false, { SoundType: "Hall" });
        console.log("-----------", this.tableInfo.Config);
        Main.inst.dispatchEventWith(GameData.GameEvent.DeleteGamePnl, false, { HouseType: this.tableInfo.HouseID });
        // SoundManager.ins.destroy();
    }
}