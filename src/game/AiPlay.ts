/**
 * AI挂机处理模块
 *
 * @class AiPlay
 * @extends {eui.Component}
 */
class AiPlay extends eui.Component {
    private static _inst: AiPlay;
    public static get inst() {
        if (this._inst) {
            return this._inst;
        }
        else {
            this._inst = new AiPlay();
            return this._inst;
        }
    }

    private timer: egret.Timer;
    private aiStarted: boolean;
    private mapOrder = {};
    private cancel: eui.Button;

    public constructor() {
        super();
        this.skinName = skins.game.AiPlay;
        this.cancel.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            this.timer.reset();
            this.aiStarted = false;
            this.visible = false;
            this.timer.start();
        }, this);
        this.visible = false;
        this.timer = new egret.Timer(15000);
        this.timer.addEventListener(egret.TimerEvent.TIMER, this.timerHdl, this);
    }


    /**
     * 重置挂机计时器
     *
     * @memberof AiPlay
     */
    public resetTime() {
        let isRunningBefore = this.timer.running;
        this.timer.reset();
        if (isRunningBefore) this.timer.start();
    }

    public aiStop() {
        this.timer.reset();
        this.aiStarted = false;
    }

    public start() {
        if (this.timer.running) return;
        this.timer.start();
    }

    private timerHdl() {
        this.aiStarted = true;
        this.visible = true;
        console.log("----------ai----------ai start ````");
    }

    /**
     * 当前准备触发的指令
     * 如果条件匹配 则由AI自动触发
     *
     * @param {niuniu.NiuniuID} orderID
     * @memberof AiPlay
     */
    public orderInReady(orderID: niuniu.NiuniuID) {
        if (!this.aiStarted) return;
        let order = <IAiOrder>this.mapOrder[orderID];
        if (order) {
            let delay = order.delay ? order.delay : 1000;
            setTimeout(() => {
                console.log('-------------触发次数-----id', order.order);
                order.btns[0].dispatchEventWith(egret.TouchEvent.TOUCH_TAP);
            }, delay
            )
        }

    }

    /**
     * 注册需要自动按下的按钮
     * key value 对应 事件 和 按钮
     *
     * @memberof AiPlay
     */
    public registOrder(order: IAiOrder) {
        if (this.mapOrder[order.order]) {
            console.log('已经有相同的order 存在')
        }
        else {
            this.mapOrder[order.order] = order;
        }
    }

    public destroy() {
        this.timer.stop();
        this.timer.removeEventListener(egret.TimerEvent.TIMER, this.timerHdl, this);

        this.cancel.removeEventListener(egret.TouchEvent.TOUCH_TAP, () => {

        }, this);
        AiPlay._inst = null;
        //TODO 测试map 需要清理否
    }
}


/**
 * 需要自动处理的指令
 * btn 按照 优先级从0 到 1 放入数组
 * delay 延迟多久触发该条指令
 * @class AiOrder
 */
interface IAiOrder {
    btns: eui.Button[];
    order: niuniu.NiuniuID;
    delay?: number;
}