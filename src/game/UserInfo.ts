class UserInfo extends eui.Component {
    public static get inst() {
        if (this._inst) {
            return this._inst;
        }
        else {
            this._inst = new UserInfo();
            return this._inst;
        }
    }


    private static _inst: UserInfo;
    private close: eui.Image;
    private headImg: eui.Image;
    private stars: eui.Group;
    private sex0: eui.Image;
    private nickName: eui.Label;
    private id: eui.Label;
    private space: eui.Label;
    private ip: eui.Label;
    private playCount: eui.Label;
    private registDay: eui.Label;
    private level0: eui.Image;


    public constructor() {
        super();
        this.skinName = skins.game.PlayerInfo;
        this.close.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            this.visible = false;
        }, this);
    }

    public open(info: structure.IUserInfo) {
        GameData.handleHeadImg(this.headImg, info.HeadIcon);
        this.sex0.source = info.Sex === 0 ? RES.getRes("hall_nan_png") : RES.getRes("hall_nv_png")
        this.nickName.text = info.Nickname;
        this.id.text = info.ID.toString();
        this.space.text = "暂无";
        this.ip.text = info.IpAddr;
        this.playCount.text = info.Round.toString();
        this.registDay.text = GameData.getTimeStr(info.RegistDate);
        this.level0.source = this.getLvSource(info.Round);
        Main.inst.addChild(this);
        this.visible = true;
    }

    private getLvSource(playerCount: number): string {
        let lv = Math.floor(playerCount / 30) + 1;
        if (lv > 7) lv = 7;
        return ("info_leave" + lv + "_png");
    }

    public destroy() {
        this.close.removeEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            this.visible = false;
        }, this);
        if (this.parent) this.parent.removeChild(this);
    }

}