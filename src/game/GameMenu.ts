class GameMenu extends eui.Component {
    private enable: eui.CheckBox;
    private dismiss: eui.Button;
    private leave: eui.Button;
    private setting: eui.Button;
    private btnGroup: eui.Group;

    private musicPnl: eui.Group;
    private effectSlider: eui.HSlider;
    private musicSlider: eui.HSlider;
    private close: eui.Button;

    private dismissGroup: eui.Group;
    private cancel: eui.Button;
    private confirm: eui.Button;
    private distributor: eui.Button;


    public constructor() {
        super();
        this.skinName = skins.game.GameMenu;

        this.enable.addEventListener(eui.UIEvent.CHANGE, this.uiChangeHdl, this);
        this.musicSlider.addEventListener(eui.UIEvent.CHANGE, this.uiChangeHdl, this);
        this.effectSlider.addEventListener(eui.UIEvent.CHANGE, this.uiChangeHdl, this);

        this.dismiss.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.leave.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.setting.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.cancel.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.confirm.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.close.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);

        // this.distributor.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
    }

    private uiChangeHdl(evt: eui.UIEvent) {
        switch (evt.target) {
            case this.effectSlider:
                console.log(this.effectSlider.value);
                break;
            case this.musicSlider:
                console.log(this.musicSlider.value);
                break;

            case this.enable:
                this.btnGroup.visible = this.enable.selected;
                break;
        }
    }

    private tapHdl(evt: egret.TouchEvent) {
        SoundManager.ins.playEffect('sfx_button_click_mp3');
        switch (evt.target) {
            case this.dismiss:
                this.dismissGroup.visible = true;
                break;

            case this.leave:
                //发送离开消息
                let quitReq = new niuniu.LeaveGameREQ();
                MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_LeaveGameREQ, niuniu.LeaveGameREQ, quitReq);
                break;

            case this.setting:
                this.musicPnl.visible = true;
                break;

            case this.cancel:
                this.dismissGroup.visible = false;
                break;

            case this.confirm:
                //发送解散消息
                break;

            case this.close:
                this.musicPnl.visible = false;
                break;
            case this.distributor:
                // this.btnGroup.visible = false;
                // this.enable.selected = !this.enable.selected;
                // let distributorPnl = new DistributorPnl();
                // Main.inst.addChild(distributorPnl);
                break;
        }
    }

}