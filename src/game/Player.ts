class Player extends eui.Component {

    public isMyself: boolean;
    private betNum: eui.Label;
    private portrait: eui.Image;
    private coinCount: eui.Label;
    private userName: eui.Label;
    private pokerPoint: EmptyPoint;
    private pokerInterval: number = 50;
    public userID: number | Long;
    private betGroup: eui.Group;
    private rankLight: eui.Image;
    private rankerMark: eui.Image;
    public isPlaying: boolean;
    private rankMulti: eui.Image;
    private prepare: eui.Image;

    public playerInfo: structure.ITablePlayer;
    private holdCards: eui.Image[] = new Array(0);

    public myCoin: number;
    private cardType: eui.Image;
    private multiImg: eui.Image;
    private coinPoint: EmptyPoint;
    private crazyBetMark: eui.Image;
    private tableConfig: structure.ITableConfig;
    private lastCardNumber: number;
    private lastCardMark: eui.Image;
    private openPlayerInfo: EmptyPoint;

    public isPlaySound = false;

    public constructor(_playerInfo: structure.ITablePlayer, _tableConfig: structure.ITableConfig, offsetSeatNum: number, maxPlayer: number) {
        super();
        this.isMyself = _playerInfo.UserID === GameData.userInfo.ID ? true : false;
        this.playerInfo = _playerInfo;
        this.userID = _playerInfo.UserID;
        this.myCoin = <number>_playerInfo.Score;
        this.tableConfig = _tableConfig;
        this.refreshUIstyle(maxPlayer, offsetSeatNum);

        //TODO 玩家一开始就在游戏里 游戏开始后 再坐下 刷新玩家UI的时候 没有刷新牌的坐标
        //TODO 2个玩家坐下， 起来一个 再进入坐下 没有该玩家消息
    }


    /**
     * 向玩家发牌处理
     * 如果牌名 <=0 则获得背面牌
     * 
     * @param {string[]} pokerName 
     * @memberof Player
     */
    public sendPoker(pokers: number[], sendPoint: EmptyPoint) {
        this.isPlaying = true;
        let selfCardScale = .9;
        let otherPlayerCardScale = .6;
        for (let i = 0; i < pokers.length; i++) {
            let poker = pokers[i];
            let img = <eui.Image>GameData.getCardByByte(poker);
            img.anchorOffsetX = (<egret.Texture>img.source).textureWidth / 2;
            //记录最后一张牌 需要加上标记
            if (pokers.length === 1) {
                this.lastCardNumber = poker;
            }

            let backImg = <eui.Image>GameData.getCardByByte(-1);
            backImg.anchorOffsetX = (<egret.Texture>backImg.source).textureWidth / 2;
            if (!this.isMyself) {
                img.scaleX = img.scaleY = otherPlayerCardScale;
                this.pokerInterval = 50 * otherPlayerCardScale;

                if (sendPoint) {
                    this.addChild(img);
                    let desPos = new egret.Point(this.pokerPoint.x + (this.holdCards.length * this.pokerInterval), this.pokerPoint.y);
                    img.x = sendPoint.x - this.x;
                    img.y = sendPoint.y - this.y;
                    let tw = egret.Tween.get(img);
                    tw.to({ x: desPos.x, y: desPos.y }, 120 + i * 120);
                }
                else {
                    this.addChild(img);
                    img.x = this.pokerPoint.x + (this.holdCards.length * this.pokerInterval);
                    img.y = this.pokerPoint.y;
                }
            }
            else {
                backImg.scaleX = backImg.scaleY = img.scaleX = img.scaleY = selfCardScale;
                this.pokerInterval = (<egret.Texture>backImg.source).textureWidth * selfCardScale;

                if (sendPoint) {
                    this.addChild(backImg);
                    this.addChild(img);
                    img.visible = false;
                    let desPos = new egret.Point(this.pokerPoint.x + (this.holdCards.length * this.pokerInterval), this.pokerPoint.y);
                    backImg.x = sendPoint.x - this.x;
                    backImg.y = sendPoint.y - this.y;
                    let tw = egret.Tween.get(backImg);
                    tw.to({ x: desPos.x, y: desPos.y }, 120 + i * 80).wait((pokers.length - i) * 80)
                        .call(() => {
                            //超过4张牌 才进行 自动翻牌 否则不翻牌
                            if (pokers.length > 1) {
                                egret.Tween.get(backImg).to({ scaleX: 0 }, 80).call(() => {
                                    img.scaleX = 0;
                                    img.x = backImg.x;
                                    img.y = backImg.y;
                                    //切到后台 再回来 会导致tween运行停止 这里判断显示
                                    if (img.parent) img.visible = true;
                                    this.removeChild(backImg);
                                    //TODO 确认 tween 声明在外面 无法启动的问题 this指向？
                                    egret.Tween.get(img).to({ scaleX: selfCardScale }, 80);
                                }, this);
                            }
                            else {
                                //没有翻的lastCard 放入数组存好 亮牌会进行操作

                            }
                        }, this);

                    if (pokers.length === 1) {
                        this.holdCards.push(backImg);
                    }
                }
                else {

                    this.addChild(img);
                    img.x = this.pokerPoint.x + (this.holdCards.length * this.pokerInterval);
                    img.y = this.pokerPoint.y;
                    if (this.lastCardNumber === poker) {
                        this.lastCardMark = new eui.Image("PokerTag_png");
                        let mark = this.addChild(this.lastCardMark);
                        mark.x = img.x;
                        mark.y = img.y;
                        mark.anchorOffsetX = img.anchorOffsetX;
                        mark.width = img.width;
                        mark.height = img.height;
                        mark.scaleX = img.scaleX;
                        mark.scaleY = img.scaleY;
                    }
                }
            }
            this.holdCards.push(img);
        }
    }



    /**
     * 翻开最后一张牌
     *
     * @memberof Player
     */
    public turnLastCard() {
        //tween是异步执行， 所以最后2张牌 不能根据push顺序判定 只能根据parent判定
        let img1 = this.holdCards[5];
        let img2 = this.holdCards[4];
        let img: eui.Image;
        let backImg: eui.Image;
        img = img1.visible ? img2 : img1;
        backImg = img1.visible ? img1 : img2;

        egret.Tween.get(backImg).to({ scaleX: 0 }, 80).call(() => {
            img.scaleX = 0;
            img.x = backImg.x;
            img.y = backImg.y;
            img.visible = true;
            this.removeChild(backImg);
            egret.Tween.get(img).to({ scaleX: .9 }, 80);
        }, this);

    }


    /**
     * 翻开手上所有牌
     *
     * @param {string[]} pokerName
     * @memberof Player
     */
    public showPoker(pokers: number[]) {
        this.clearPoker();
        this.sendPoker(pokers, null);

    }


    /**
     * 清理掉所有牌
     *
     * @memberof Player
     */
    public clearPoker() {
        for (let poker of this.holdCards) {
            //如果是手动翻牌的话 cards 里面 lastCard明牌 是没有被addChild的
            // console.log('清理牌', poker, "----", poker.parent)
            if (!poker || !poker.parent) continue;
            this.removeChild(poker);
        }
        while (this.holdCards.length > 0) {
            this.holdCards.splice(0, 1);
        }

        if (this.lastCardMark) this.removeChild(this.lastCardMark);
        this.lastCardMark = null;
    }

    public refreshUIstyle(maxPlayer: number, offsetSeatNum: number) {
        //根据座位号和 桌子最大人数 计算当前玩家 UI类型
        switch (maxPlayer) {
            case 6:
                switch (offsetSeatNum) {
                    case 0:
                        this.skinName = skins.game.RightPlayer;
                        break;
                    case 1:
                    case 2:
                    case 3:
                        this.skinName = skins.game.UpPlayer;
                        break;
                    case 4:
                        this.skinName = skins.game.LeftPlayer;
                        break;
                    case 5:
                        this.skinName = skins.game.SelfPlayer;
                        break;

                }
                break;
            case 8:
                switch (offsetSeatNum) {
                    case 0:
                    case 1:
                        this.skinName = skins.game.RightPlayer;
                        break;

                    case 2:
                    case 3:
                    case 4:
                        this.skinName = skins.game.UpPlayer;
                        break;
                    case 5:
                    case 6:
                        this.skinName = skins.game.LeftPlayer;
                        break;
                    case 7:
                        this.skinName = skins.game.SelfPlayer;
                        break;

                }
                break;
            case 10:
                switch (offsetSeatNum) {
                    case 0:
                    case 1:
                        this.skinName = skins.game.RightPlayer;
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        this.skinName = skins.game.UpPlayer;
                        break;
                    case 7:
                    case 8:
                        this.skinName = skins.game.LeftPlayer;
                        break;
                    case 9:
                        this.skinName = skins.game.SelfPlayer;
                        break;
                }
                break;
        }
        this.userName.text = this.playerInfo.NickName;
        this.coinCount.text = this.playerInfo.Score.toString();
        this.betGroup.visible = false;
        this.betNum.text = '0';
        this.rankLight.visible = false;
        this.rankerMark.visible = false;
        this.rankMulti.visible = false;
        this.crazyBetMark.visible = false;
        this.prepare.visible = false;

        // this.portrait.width = this.portrait.height = 80;
        GameData.handleHeadImg(this.portrait, this.playerInfo.HeadIcon);


        if (!this.openPlayerInfo.hasEventListener(egret.TouchEvent.TOUCH_TAP)) {
            this.openPlayerInfo.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
                SoundManager.ins.playEffect('sfx_button_click_mp3');
                let req = new login.UserInfoREQ();
                req.UserID = [this.userID];
                MsgMgr.inst.sendMsg(login.LoginID.ID_UserInfoREQ, login.UserInfoREQ, req);
            }, this);

            MsgMgr.inst.onMsg(login.LoginID.ID_USerInfoACK, login.UserInfoACK, (msg: login.UserInfoACK) => {
                if (msg.userInfo[0].ID === this.userID) {
                    UserInfo.inst.open(msg.userInfo[0]);
                }
            }, this);
        }
    }

    /**
     * 开关 下注显示
     *
     * @param {boolean} enable
     * @memberof Player
     */
    public enableBetGroup(enable: boolean) {
        this.betGroup.visible = enable;
        if (!enable) this.betNum.text = '0';
    }


    public bet(num: number | Long) {
        this.betCoinAction(num);
    }

    private betCoinAction(num: number | Long) {
        let texture = <egret.Texture>RES.getRes('Coin_png');
        SoundManager.ins.playEffect('game_bet2_mp3');
        for (let i = 0; i < 10; i++) {
            egret.setTimeout(
                () => {
                    let img = new eui.Image(texture);
                    img.x = this.coinPoint.x + Math.random() * 60 - 30;
                    img.y = this.coinPoint.y + Math.random() * 60 - 30;
                    this.addChild(img);
                    egret.Tween.get(img).to({ x: this.betGroup.x, y: this.betGroup.y }, 500).call(() => {
                        this.removeChild(img);
                    });
                }, this, i * 40
            )
        }
        this.betNum.text = num.toString();
    }

    public winLoseCoinAction(num: number, dest: egret.Point, layer: egret.DisplayObjectContainer) {
        // dest = dest.add(new egret.Point(this.coinPoint.x, this.coinPoint.y));
        let texture = <egret.Texture>RES.getRes('Coin_png');
        SoundManager.ins.playEffect('sfx_coin_down_mp3');
        for (let i = 0; i < 40; i++) {
            egret.setTimeout(
                () => {
                    let img = new eui.Image(texture);
                    // img.x = this.coinPoint.x + Math.random() * 60 - 30;
                    // img.y = this.coinPoint.y + Math.random() * 60 - 30;
                    img.x = this.getCoinPosGlobal().x + Math.random() * 60 - 30;
                    img.y = this.getCoinPosGlobal().y + Math.random() * 60 - 30;
                    layer.addChild(img);
                    egret.Tween.get(img).to({ x: dest.x, y: dest.y }, 500).call(() => {
                        layer.removeChild(img);
                    });
                }, this, i * 10
            )
        }
    }

    public getCoinPosGlobal(): egret.Point {
        let point = new egret.Point(this.coinPoint.x, this.coinPoint.y);
        return this.localToGlobal(point.x, point.y);
    }


    /**
     * 开关 庄家 组
     *
     * @param {boolean} enable
     * @memberof Player
     */
    public enableRanker(enable: boolean) {
        this.rankLight.visible = enable;
        this.rankerMark.visible = enable;
    }


    /**
     * 开关rank倍率
     *
     * @param {boolean} enable
     * @param {number} [multi]
     * @memberof Player
     */
    public enableRankMulti(enable: boolean, multi?: number) {
        if (enable) {
            multi = multi ? multi : 0;
            let imgName = 'multi' + multi + '_png';
            let texture = <egret.Texture>RES.getRes(imgName);
            this.rankMulti.source = texture;
            this.rankMulti.visible = true;
        }
        else {
            this.rankMulti.visible = false;
        }
    }


    /**
     * 一把结算
     *
     * @param {niuniu.SettlementNTF.ISettleUser} msg
     * @memberof Player
     */
    public settlement(msg: niuniu.SettlementNTF.ISettleUser) {
        let points = <number>msg.Win;
        let floatLabel = new eui.Label();
        floatLabel.text = points >= 0 ? '+' + points : points.toString();
        floatLabel.textColor = points >= 0 ? 0xff6633 : 0x0033ff;
        floatLabel.bold = true;
        floatLabel.x = this.coinCount.x;
        floatLabel.y = this.coinCount.y;
        this.addChild(floatLabel);
        egret.Tween.get(floatLabel).to({ y: floatLabel.y - 60 }, 500).wait(1000).call(() => {
            this.removeChild(floatLabel)
        })
        this.myCoin += points;
        this.playerInfo.Score = this.myCoin;
        this.coinCount.text = this.myCoin.toString();
    }

    public enableCardType(enable: boolean, cardType?: niuniu.NiuniuCardType) {
        let type = niuniu.NiuniuCardType;
        let imgName = '';
        let niuniuStr = '';
        let isSexNan = GameData.userInfo.Sex === 1 ? true : false;
        if (enable) {
            switch (cardType) {
                case type.CardType_Niu1:
                    imgName = 'niu_1_png';
                    niuniuStr = isSexNan ? 'nan_niu1_mp3' : 'nv_niu1_mp3';
                    break;
                case type.CardType_Niu2:
                    imgName = 'niu_2_png';
                    niuniuStr = isSexNan ? 'nan_niu2_mp3' : 'nv_niu2_mp3';
                    break;
                case type.CardType_Niu3:
                    imgName = 'niu_3_png';
                    niuniuStr = isSexNan ? 'nan_niu3_mp3' : 'nv_niu3_mp3';
                    break;
                case type.CardType_Niu4:
                    imgName = 'niu_4_png';
                    niuniuStr = isSexNan ? 'nan_niu4_mp3' : 'nv_niu4_mp3';
                    break;
                case type.CardType_Niu5:
                    imgName = 'niu_5_png';
                    niuniuStr = isSexNan ? 'nan_niu5_mp3' : 'nv_niu5_mp3';
                    break;
                case type.CardType_Niu6:
                    imgName = 'niu_6_png';
                    niuniuStr = isSexNan ? 'nan_niu6_mp3' : 'nv_niu6_mp3';
                    break;
                case type.CardType_Niu7:
                    imgName = 'niu_7_png';
                    niuniuStr = isSexNan ? 'nan_niu7_mp3' : 'nv_niu7_mp3';
                    break;
                case type.CardType_Niu8:
                    imgName = 'niu_8_png';
                    niuniuStr = isSexNan ? 'nan_niu8_mp3' : 'nv_niu8_mp3';
                    break;
                case type.CardType_Niu9:
                    imgName = 'niu_9_png';
                    niuniuStr = isSexNan ? 'nan_niu9_mp3' : 'nv_niu9_mp3';
                    break;
                case type.CardType_Niuniu:
                    imgName = 'niu_10_png';
                    niuniuStr = isSexNan ? 'nan_niu10_mp3' : 'nv_niu10_mp3';
                    break;
                case type.CardType_None:
                    imgName = 'niu_0_png';
                    niuniuStr = isSexNan ? 'nan_niu0_mp3' : 'nv_niu0_mp3';
                    break;
                case type.CardType_Wuhua:
                    imgName = 'niu_12_png';
                    niuniuStr = isSexNan ? 'nan_niu30_mp3' : 'nv_niu30_mp3';
                    break;
                case type.CardType_Wuxiao:
                    imgName = 'niu_16_png';
                    niuniuStr = isSexNan ? 'nan_niu70_mp3' : 'nv_niu70_mp3';
                    break;
                case type.CardType_Bomb:
                    imgName = 'niu_15_png';
                    niuniuStr = isSexNan ? 'nan_niu60_mp3' : 'nv_niu60_mp3';
                    break;
                case type.CardType_Flush:
                    imgName = 'niu_13_png';
                    niuniuStr = isSexNan ? 'nan_niu40_mp3' : 'nv_niu40_mp3';
                    break;
                case type.CardType_Fullhouse:
                    imgName = 'niu_14_png';
                    niuniuStr = isSexNan ? 'nan_niu50_mp3' : 'nv_niu50_mp3';
                    break;
                case type.CardType_Straight:
                    imgName = 'niu_11_png';
                    niuniuStr = isSexNan ? 'nan_niu20_mp3' : 'nv_niu20_mp3';
                    break;
                case type.CardType_StraightFlush:
                    imgName = 'niu_17_png';
                    niuniuStr = isSexNan ? 'nan_niu80_mp3' : 'nv_niu80_mp3';
                    break;
            }
            if (!this.isPlaySound) {
                SoundManager.ins.playEffect(niuniuStr);
                this.isPlaySound = true;
            }

            let typeSource = <egret.Texture>RES.getRes(imgName);
            this.cardType.source = typeSource;
            this.cardType.width = typeSource.textureWidth;
            this.cardType.height = typeSource.textureHeight;
            this.setChildIndex(this.cardType, this.numChildren - 1);
            this.cardType.visible = true;

            let multi = parseInt(imgName.split('_')[1]);
            if (multi > 0) {
                let multiName = 'multi_' + this.tableConfig.MultiRuler[multi] + '_png';
                let multiSource = <egret.Texture>RES.getRes(multiName);

                this.multiImg.source = multiSource;
                this.multiImg.width = multiSource.textureWidth;
                this.multiImg.height = multiSource.textureHeight;
                this.multiImg.x = this.cardType.x + this.cardType.width * this.cardType.scaleX + 4;
                this.multiImg.y = this.cardType.y + this.cardType.height * this.cardType.scaleY - this.multiImg.height;
                this.setChildIndex(this.multiImg, this.numChildren - 1);
                this.multiImg.visible = true;
            }


        }
        else {
            this.cardType.visible = false;
            this.multiImg.visible = false;
            this.cardType.source = null;
            this.multiImg.source = null;
        }
    }

    public enabledBlinkRank(enable: boolean, onlyOne?) {
        this.rankLight.visible = enable;
        if (!onlyOne) {
            SoundManager.ins.playEffect('random_banker_mp3');
        }
    }

    public enableCrazyBetMark(enable: boolean) {
        this.crazyBetMark.visible = enable;
    }

    public updateScore(msg: niuniu.ScoreChangeNTF) {
        this.myCoin = <number>msg.Score;
        this.coinCount.text = this.myCoin.toString();
    }

    public enablePrepare(enable: boolean) {
        this.prepare.visible = enable;
    }

    public getLastCard(): number {
        return this.lastCardNumber;
    }

    public destroy() {
        this.parent.removeChild(this);
    }
}

