class CrazyBet extends eui.Component {
    private g1: eui.Group;
    private g2: eui.Group;
    private g3: eui.Group;
    private g4: eui.Group;
    private g5: eui.Group;

    private enabledGroup: eui.Group;

    public constructor() {
        super();
        this.skinName = skins.game.CrazyBet;
    }

    public enable(enable: boolean, betNumbers?: number[], currentScore?: number, roomType?: structure.TableConfig.MatchTypeConfig, rankerMulti?: number) {

        if (betNumbers) {
            for (let i = 0; i < betNumbers.length; i++) {
                let num = betNumbers[i];
                if (num === 0) {
                    betNumbers.splice(i)
                    break;
                }
            }
        }


        if (enable) {
            switch (betNumbers.length) {
                case 1:
                    this.g1.visible = true;
                    this.enabledGroup = this.g1;
                    break;
                case 2:
                    this.g2.visible = true;
                    this.enabledGroup = this.g2;
                    break;
                case 3:
                    this.g3.visible = true;
                    this.enabledGroup = this.g3;
                    break;
                case 4:
                    this.g4.visible = true;
                    this.enabledGroup = this.g4;
                    break;
                case 5:
                    this.g5.visible = true;
                    this.enabledGroup = this.g5;
                    break;

            }

        }
        else {
            if (this.enabledGroup) this.enabledGroup.visible = false;
        }

        //如果child === 2 表示未初始化
        if (this.enabledGroup && this.enabledGroup.numChildren === 2) {
            let ep = this.enabledGroup.getChildAt(1);
            for (let i = 0; i < betNumbers.length; i++) {
                let btn = new eui.Button();
                btn.skinName = skins.game.BetBtn;
                btn.x = ep.x + i * btn.width + i * 10;
                btn.y = ep.y;
                btn.label = betNumbers[i].toString();
                this.enabledGroup.addChild(btn);
                btn.addEventListener(egret.TouchEvent.TOUCH_TAP, (evt: egret.TouchEvent) => {
                    SoundManager.ins.playEffect('sfx_button_click_mp3');
                    this.dispatchEventWith(EnumCrazyBet.Bet, false, parseInt(btn.label));
                }, this);

                if (roomType === structure.TableConfig.MatchTypeConfig.Match)
                    if (currentScore < betNumbers[i] * rankerMulti) {
                        btn.enabled = false;
                    }
            }
        }

    }
}

enum EnumCrazyBet {
    Bet = 'bet'
}