class ResultPnl extends eui.Component {

    private list: eui.List;
    private scroller: eui.Scroller;
    private arrData: eui.ArrayCollection = new eui.ArrayCollection();
    private back: eui.Button;
    private share: eui.Button;
    private continueGame: eui.Button;


    private teahouseID: eui.Label;
    private roomID: eui.Label;
    private round: eui.Label;
    private bet: eui.Label;
    private time: eui.Label;
    private tabelInfo: structure.ITableInfo;


    public constructor(msg: niuniu.RounRecordNTF, tableInfo: structure.ITableInfo) {
        super();

        this.tabelInfo = tableInfo;

        this.skinName = skins.game.ResultPnl;
        this.list.dataProvider = this.arrData;
        this.list.itemRendererSkinName = skins.game.ResultItem;
        this.list.itemRenderer = ResultItem;
        this.scroller.viewport = this.list;
        let layout = this.list.layout = new eui.TileLayout();
        layout.verticalGap = 30;
        layout.paddingLeft = 64;

        msg.Record.Rec.sort(function (rec1, rec2) {
            let totalScore1 = 0;
            let totalScore2 = 0;
            for (let score of rec1.HandRec) {
                totalScore1 += <number>score.Score;
            }
            for (let score of rec2.HandRec) {
                totalScore2 += <number>score.Score;
            }
            return totalScore2 - totalScore1;
        });
        // for (let record of msg.Record.Rec) {
        //     this.arrData.addItem(record);
        // }

        msg.Record.Rec.forEach((record, index) => {
            let tag = 0;
            if (index === 0) {
                tag = 1;
            } else if (index === msg.Record.Rec.length - 1) {
                tag = -1;
            }
            record['tag'] = tag;
            this.arrData.addItem(record);
        });


        this.teahouseID.text = msg.Record.HouseID.toString();
        this.roomID.text = msg.Record.TableId.toString();
        this.round.text = msg.Record.HandCount.toString();
        let arrScore = tableInfo.Config.BaseScoreCfg.ValidBaseScore;
        this.bet.text = arrScore[0] + '/' + arrScore[1] + '/' + arrScore[2];
        this.time.text = GameData.getTimeStr(msg.Record.Date);

        if (!this.tabelInfo.TableID) this.continueGame.visible = false;

        this.back.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.share.addEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.continueGame.addEventListener(egret.TouchEvent.TOUCH_TAP, this.continueGames, this);
    }

    private tapHdl(evt: egret.Event) {
        SoundManager.ins.playEffect('sfx_button_click_mp3');
        switch (evt.target) {
            case this.back:
            case this.share:
                this.destroy();
                break;
        }
    }

    private continueGames(evt: egret.Event) {
        this.destroy();
        MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_EnterGameACK, niuniu.EnterGameACK, (msg: niuniu.EnterGameACK) => {
            console.log("进入桌子", msg);
            if (!msg.Code) {
                Main.inst.dispatchEventWith(GameData.GameEvent.ChangeSound, false, { SoundType: "Game" });
                Main.inst.dispatchEventWith(GameData.GameEvent.DeleteTeaHousePnl);
                Main.inst.addChild(new GameTable(msg.TableInfo));
                MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_EnterGameACK, this);

            } else {
                Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code));
            }
        }, this);

        let enterReq = new niuniu.EnterGameREQ();
        enterReq.GameID = structure.GameID.Niuniu;
        enterReq.TableID = this.tabelInfo.TableID;
        MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_EnterGameREQ, niuniu.EnterGameREQ, enterReq);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private destroy() {
        this.back.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.share.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.tapHdl, this);
        this.continueGame.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.continueGames, this);
        this.parent.removeChild(this);
    }
}

