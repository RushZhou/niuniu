class MakeCard extends eui.Component {
    private static _inst: MakeCard;
    public static get inst() {
        if (this._inst) {
            return this._inst;
        }
        else {
            this._inst = new MakeCard();
            return this._inst;
        }
    }


    private backCard: eui.Image;
    private card: eui.Image;
    private touchSide: eui.Group;
    private lastTouchPos: egret.Point = new egret.Point();

    public constructor() {
        super();
        this.skinName = skins.game.MakeCard;
        this.touchSide.addEventListener(egret.TouchEvent.TOUCH_BEGIN, (evt: egret.TouchEvent) => {
            this.lastTouchPos.x = evt.localX;
            this.lastTouchPos.y = evt.localY;
        }, this);

        this.visible = false;

        this.touchSide.addEventListener(egret.TouchEvent.TOUCH_CANCEL, (evt: egret.TouchEvent) => {

        }, this);
    }

    public enableMakeCard(enable: boolean, cardNum?: number) {
        if (enable) {
            let cardName = GameData.getCardByByte(cardNum, true, true);
            this.card.source = <string>cardName;
            this.visible = true;
            this.backCard.visible = true;
            this.touchSide.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.touchMovedHdl, this);
        }
        else {
            this.backCard.visible = false;
            this.touchSide.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.touchMovedHdl, this);
            egret.Tween.get(this.card).to(
                { scaleX: .2, scaleY: .2 }, 700
            ).call(() => {
                this.visible = false;
                this.card.source = '';
                this.backCard.rotation = 0;
                this.card.scaleX = this.card.scaleY = .8;
            }, this)

        }
    }

    private touchMovedHdl(evt: egret.TouchEvent) {
        let nowPos = new egret.Point(evt.localX, evt.localY);
        let interval = nowPos.subtract(this.lastTouchPos);
        if (interval.x >= 0 && interval.y >= 0) {
            // console.log((interval.x + interval.y) / 3, '-----rotation--')
            this.backCard.rotation += (interval.x + interval.y) / 5;
        }
        if (this.backCard.rotation > 30) {
            this.enableMakeCard(false);
            //外部侦听手动搓牌 to hdl
            this.dispatchEventWith(MakeCardEvt.MakeCardFinish);
        }

        this.lastTouchPos.x = evt.localX;
        this.lastTouchPos.y = evt.localY;
    }
}

enum MakeCardEvt {
    MakeCardFinish = 'makeCardFinish'
}