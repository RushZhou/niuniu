class Tips {
    private static arrTips: Array<string> = new Array();
    private static isPlaying: boolean;


    /**
     * 全局提示控件
     * 调用该方法 传入需要显示的内容即可
     * 如果已经有内容需要 等待
     * @static
     * @param {string} content 需要显示的内容
     * @returns
     * @memberof Tips
     */
    public static addTip(content: string) {
        if (content === '') return;
        if (this.isPlaying) {
            this.arrTips.push(content);
            return;
        }
        else {
            this.isPlaying = true;
        }
        let item = new TipItem();
        item.setContent(content);
        let stageH = egret.MainContext.instance.stage.stageHeight;
        item.x = egret.MainContext.instance.stage.stageWidth / 2 - item.width / 2;
        item.y = stageH;
        Main.inst.addChild(item);
        let tw = egret.Tween.get(item);
        tw.to({ y: stageH / 2 }, 500).wait(1500).to({ alpha: 0 }, 300).call(
            () => {
                Main.inst.removeChild(item);
                Tips.arrTips.shift();
                Tips.isPlaying = false;
                if (Tips.arrTips.length > 0) {
                    this.addTip(Tips.arrTips[0]);
                }
            }, this
        );
    }


}

class TipItem extends eui.Component {
    private tip: eui.Label;

    public constructor() {
        super();
        this.skinName = skins.common.Tips;
        this.tip.text = '';

    }

    public setContent(content: string) {
        this.tip.text = content;
    }
}