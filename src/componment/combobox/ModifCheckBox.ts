class ModifCheckBox extends eui.Component implements eui.UIComponent {
	public listItems: eui.List;
	public showItems: eui.Button;
	public title: eui.Label;
	public selectItem: any;
	public scroller: eui.Scroller;

	private ac: eui.ArrayCollection;

	public constructor() {
		super();
		this.skinName = skins.combobox.ModifCheckBox;
		this.ac = this.listItems.dataProvider = new eui.ArrayCollection();
		this.listItems.visible = false;
		this.listItems.itemRenderer = MyListItem;
		this.scroller.viewport = this.listItems;

		this.showItems.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
			this.parent.addChild(this);
			this.listItems.visible = !this.listItems.visible;
			// SoundManager.ins.playEffect('sfx_button_click_mp3');
		}, this)

		this.listItems.addEventListener(eui.UIEvent.CHANGE, (evt) => {
			// SoundManager.ins.playEffect('sfx_button_click_mp3');
			let list = evt.target as eui.List;
			this.title.text = list.selectedItem.name;
			this.selectItem = list.selectedItem;
			this.listItems.visible = false;
		}, this)
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
	}

	// public addItem(itemName: string, itemVal?: any) {
	// 	this.ac.addItem(itemName);
	// }

	public addItem(itemName: Object) {
		this.ac.addItem(itemName);
	}

	public setSelect(index: number) {
		this.listItems.selectedIndex = index;
		this.listItems.dispatchEventWith(egret.Event.CHANGE, false);
	}

}

class MyListItem extends eui.ItemRenderer {
	public constructor() {
		super();
		this.skinName = skins.combobox.MyListItem;

	}

	protected childrenCreated(): void {
		super.childrenCreated();
		this.width = 240;
	}

	protected dataChanged() {

	}
}