class Combobox extends eui.Component implements eui.UIComponent {
	public listItems: eui.List;
	public showItems: eui.Button;
	public title: eui.Label;
	public selectItem: any;
	private itemLen: number;

	private ac: eui.ArrayCollection;

	public constructor() {
		super();
		this.skinName = skins.combobox.ComboboxSkin;
		this.ac = this.listItems.dataProvider = new eui.ArrayCollection();
		this.listItems.visible = false;
		this.listItems.itemRenderer = CheckListItem;

		this.showItems.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
			GameData.evtLisener.dispatchEventWith(TableConfigEvt.SomeBtnHasBeClicked);
			this.listItems.width = this.itemLen;
			this.parent.addChild(this);
			this.listItems.visible = !this.listItems.visible;
			// SoundManager.ins.playEffect('sfx_button_click_mp3');
		}, this)

		this.listItems.addEventListener(eui.ItemTapEvent.ITEM_TAP, (evt) => {
			let list = evt.target as eui.List;
			this.title.text = list.selectedItem.name;
			this.selectItem = list.selectedItem;
			this.listItems.visible = false;
			//  SoundManager.ins.playEffect('sfx_button_click_mp3');
			console.log(list.selectedItem);
		}, this)


		this.listItems.addEventListener(GameData.GameEvent.UpdateCheckItem, (evt: egret.Event) => {
			console.log("-------------", this.listItems.selectedIndex);
			if (this.listItems.selectedIndex) {
				this.title.text = evt.data.item0.name + "X" + evt.data.item0.Diamond;
			}

		}, this)
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
	}

	// public addItem(itemName: string, itemVal?: any) {
	// 	this.ac.addItem(itemName);
	// }

	public addItem(item: Object) {
		item['width'] = this.width;
		this.ac.addItem(item);
	}

	public replaceItem(item0, item1) {
		console.log('item0,item1', item0, item1);
		this.listItems.dispatchEventWith(GameData.GameEvent.UpdateCheckItem, false, { item0: item0, item1: item1 });
	}

	public enable(enable: boolean) {
		this.listItems.visible = enable;
	}


	public setSelect(index: number) {
		this.listItems.selectedIndex = index;
		this.listItems.dispatchEventWith(eui.ItemTapEvent.ITEM_TAP, false);
	}

}

class CheckListItem extends eui.ItemRenderer {

	constructor() {
		super();
		this.skinName = skins.combobox.MyListItem;
	}

	protected childrenCreated() {
		super.childrenCreated();
	}

	protected dataChanged() {
		this.width = this.data.width;
	}
}