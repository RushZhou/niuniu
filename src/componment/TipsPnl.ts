class TipsPnl extends eui.Component {
    public static inst: TipsPnl;
    private confirm: eui.Button;
    private content: eui.Label;

    public constructor() {
        super();
        TipsPnl.inst = this;
        this.skinName = skins.common.TipsPnl;
        this.confirm.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            this.parent.removeChild(this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
    }

    public show(content: string) {
        this.content.text = content;
        Main.inst.addChild(this);
    }
}