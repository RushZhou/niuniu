/**
 * 声音管理
 * @author 
 */
class SoundManager {
    protected static m_Ins: SoundManager = null;
    protected _lastMusicVolume: number = 1;
    protected _effectVolume: number = 1;

    protected backSound: egret.Sound;
    protected backChannel: egret.SoundChannel;

    protected isDestroy: boolean = false;
    public constructor() {
        if (SoundManager.m_Ins) {
            throw new Error("SoundManager.ins");
        }
    }
    public static get ins(): SoundManager {
        if (null == this.m_Ins) {
            this.m_Ins = new SoundManager();
        }
        return this.m_Ins;
    }
    /**播放背景音乐...*/
    public playMusic(source: string): void {
        this.isDestroy = false;
        if (this.backSound) {
            this.backSound.close();
            this.backSound = null;
        }
        if (this.backChannel) {
            this.backChannel.stop();
            this.backChannel = null;
        }
        if (source && 0 == source.length) {
            return;
        }
        var sourceType: Boolean = source.indexOf("/") >= 0 || source.indexOf("\\") >= 0;
        if (sourceType) {
            this.backSound = new egret.Sound();
            this.backSound.addEventListener(egret.Event.COMPLETE, this.backComplete, this);
            this.backSound.load(source);
        } else {
            //            if(RES.hasRes(source)) {
            //                this.backAsyncComplete(RES.getRes(source),source);
            //            } else {
            RES.getResAsync(source, this.backAsyncComplete, this);
            //            }

        }
    }
    protected backAsyncComplete(data, key): void {
        if (!this.isDestroy && data) {
            this.backSound = data;
            this.backSound.type = egret.Sound.MUSIC;
            this.backChannel = this.backSound.play(0, 0);
            this.backChannel.volume = this._lastMusicVolume;
        }
    }

    protected backComplete(event: egret.Event): void {
        var sound: egret.Sound = <egret.Sound>event.target;
        if (sound) {
            sound.type = egret.Sound.MUSIC;
            sound.removeEventListener(egret.Event.COMPLETE, this.backComplete, this);
            if (!this.isDestroy) {
                this.backChannel = sound.play(0, 0);
                this.backChannel.volume = this._lastMusicVolume;
            }
        }

    }
    /**设置背景音量...*/
    public setMusicVolume(value): void {
        this._lastMusicVolume = value;
        if (this.backChannel) {
            this.backChannel.volume = this._lastMusicVolume;
        }
    }
    public getLastMusicVolume() {
        return this._lastMusicVolume;
    }
    /**播放效果...*/
    public playEffect(source: string) {
        this.isDestroy = false;
        if (source && 0 == source.length) {
            return;
        }
        if (this._effectVolume > 0) {
            var sourceType: Boolean = source.indexOf("/") >= 0 || source.indexOf("\\") >= 0;
            if (sourceType) {
                var sound: egret.Sound = new egret.Sound();
                sound.addEventListener(egret.Event.COMPLETE, this.effectComplete, this);
                sound.load(source);
            } else {
                //                if(RES.hasRes(source)) {
                //                    this.effectAsyncComplete(RES.getRes(source),source);
                //                } else {
                RES.getResAsync(source, this.effectAsyncComplete, this);
                //                }

            }

        }
    }
    protected effectAsyncComplete(data, key): void {
        if (!this.isDestroy && data) {
            var sound: egret.Sound = data;
            sound.type = egret.Sound.EFFECT;
            var channel: egret.SoundChannel = sound.play(0, 1);
            channel.volume = this._effectVolume;
        }
    }
    protected effectComplete(event: egret.Event): void {
        var sound: egret.Sound = <egret.Sound>event.target;
        if (sound) {
            sound.removeEventListener(egret.Event.COMPLETE, this.backComplete, this);
            sound.type = egret.Sound.EFFECT;
            if (!this.isDestroy) {
                var channel: egret.SoundChannel = sound.play(0, 1);
                channel.volume = this._effectVolume;
            }
        }

    }
    public getEffectVolume() {
        return this._effectVolume;
    }
    /**设置效果音量...*/
    public setEffectVolume(value) {
        this._effectVolume = value;
    }
    /**
     * 设置声音是否禁用
     * @value true是启用，false是禁用
     * */
    public setSoundEnabled(value: boolean): void {
        this.setEffectVolume(value ? 1 : 0);
        this.setMusicVolume(value ? 1 : 0);
    }

    /**
     * 将音乐和音效存入缓存
     * type:'Music' 'Effect'
     */
    public setSoundcToStorage(type: string, value: string) {
        egret.localStorage.setItem(type, value);
    }
    public getSoundcToStorage(type: string) {
        egret.localStorage.getItem(type);
    }
    /**销毁数据*/
    public destroy(): void {
        this.isDestroy = true;
        if (this.backSound) {
            this.backSound.close;
            this.backSound = null;
        }
        if (this.backChannel) {
            this.backChannel.stop();
            this.backChannel = null;
        }
    }

}
