class VersionCheck extends eui.Component {

    public static readonly myVersion = "0.11.36";
    private readonly httpIP = "http://" + GameData.updateAdress + ":8401/update";
    private progress: eui.Image;
    private myMask: eui.Rect;

    public constructor() {
        super();
        this.skinName = skins.login.VersionCheck;
        this.myMask.scaleX = 0;
    }

    protected childrenCreated() {
        let httpReq = new egret.HttpRequest();
        this.progress.mask = this.myMask;
        httpReq.responseType = egret.HttpResponseType.TEXT;
        httpReq.addEventListener(egret.Event.COMPLETE, (evt: egret.Event) => {
            console.log('http res')
            egret.log("----", egret.getOption("egretnative"), "------");
            let request = <egret.HttpRequest>evt.currentTarget;
            if (request.response) {
                let json = JSON.parse(request.response);
                if (!json.error) {
                    egret.log("123--------config------" + request.response);
                    egret.log("---- egret.getOption egretnative", egret.getOption("egretnative"), "------");
                    let platform = GameData.getPlatform();
                    if (platform === WhichPlatform.android || platform === WhichPlatform.ios) {
                        let now = VersionCheck.myVersion.split('.');
                        let nowVersion: string = '';
                        for (let i of now) {
                            nowVersion += i;
                        }
                        let server: string[] = json.version.split('.');
                        let serverVersion: string = '';
                        for (let i of server) {
                            serverVersion += i;
                        }
                        egret.log("----------", parseInt(nowVersion), 'zzzzzzzzzzzzzzzzz', parseInt(serverVersion), "---------------");
                        if (parseInt(nowVersion) >= parseInt(serverVersion)) {
                            Main.inst.loadResource();
                        }
                        else {
                            egret.ExternalInterface.call("hotUpdateStart", "message from JS");
                        }
                        console.log("----------it's native")
                    }
                    else {
                        egret.log("------------It's web version")
                        Main.inst.loadResource();
                    }

                }
                else {
                    Tips.addTip(json.error);
                }
            }

        }, this);

        httpReq.open(this.httpIP, egret.HttpMethod.GET);
        httpReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        httpReq.send();
    }

    public setProgress(percent: number) {
        this.myMask.scaleX = percent;
    }
}