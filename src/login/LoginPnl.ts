class LoginPnl extends eui.Component {

    private wxBtn: eui.Button;
    private static inst: eui.Component;
    private httpIP: eui.TextInput;
    private socketIP: eui.TextInput;
    private httpPort: eui.TextInput;
    private socketPort: eui.TextInput;
    private username: eui.TextInput;
    private password: eui.TextInput;
    private version: eui.Label;
    private isWxLogin: eui.CheckBox
    private save: eui.CheckBox;


    public constructor() {
        super();
        let ls = egret.localStorage;

        LoginPnl.inst = this;
        this.skinName = "skins.login.LoginPnl";
        this.httpIP.text = ls.getItem('httpIP') ? ls.getItem('httpIP') : GameData.ipAdress;
        this.httpPort.text = ls.getItem('httpPort') ? ls.getItem('httpPort') : "8801";
        this.socketIP.text = ls.getItem('socketIP') ? ls.getItem('socketIP') : GameData.ipAdress;
        let interval = '8101'
        // let external = '8081'
        this.socketPort.text = ls.getItem('socketPort') ? ls.getItem('socketPort') : interval;
        this.username.text = ls.getItem('username') ? ls.getItem('username') : Math.ceil(Math.random() * 1000000000) + 'p';
        this.password.text = ls.getItem('password') ? ls.getItem('password') : "12345";
        this.save.selected = ls.getItem('save') ? true : false;
        this.isWxLogin.selected = ls.getItem('isWx') ? true : false;
        this.horizontalCenter = 0;
        this.verticalCenter = 0;

        this.version.text = VersionCheck.myVersion;

        //发送http请求 获取登录token
        let httpReq = new egret.HttpRequest();

        httpReq.responseType = egret.HttpResponseType.TEXT;
        httpReq.addEventListener(egret.Event.COMPLETE, (evt: egret.Event) => {

            let request = <egret.HttpRequest>evt.currentTarget;
            console.log('-----http res---', request.response);
            if (request.response) {
                let json = JSON.parse(request.response);
                if (!json.error) {
                    egret.log(request.response + "_1http 回复_")
                    GameData.loginToken = json.token;
                    MsgMgr.socketInitCplEvt.push(this.initGame);
                    new MsgMgr(this.socketIP.text + ":" + this.socketPort.text);
                }
                else {
                    Tips.addTip(json.error);
                    this.wxBtn.visible = true;
                }
            }

        }, this);
        httpReq.addEventListener(egret.IOErrorEvent.IO_ERROR, (e: egret.IOErrorEvent) => {
            egret.log(e + "_ioerror")
        }, this);



        this.wxBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            if (GameData.getPlatform() === WhichPlatform.ios || GameData.getPlatform() === WhichPlatform.android) {
                egret.ExternalInterface.call("loginByWx", "message from JS");
            }
            else {
                SoundManager.ins.playEffect('sfx_button_click_mp3');
                this.wxBtn.visible = false;
                this.saveChange();
                let guest = "/visitor_login";
                httpReq.open("http://" + this.httpIP.text + ":" + this.httpPort.text + guest, egret.HttpMethod.POST);
                httpReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                console.log({ "username": this.username.text, "password": this.password.text }, '_00000')
                httpReq.send(JSON.stringify({ "username": this.username.text, "password": this.password.text }));
            }

            // if (this.isWxLogin.selected) {
            //     egret.ExternalInterface.call("loginByWx", "message from JS");
            // }
            // else {

            //     console.log('wxBtn has be clicked !')
            //     SoundManager.ins.playEffect('sfx_button_click_mp3');
            //     this.wxBtn.visible = false;
            //     this.saveChange();
            //     let guest = "/visitor_login";
            //     httpReq.open("http://" + this.httpIP.text + ":" + this.httpPort.text + guest, egret.HttpMethod.POST);
            //     httpReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            //     console.log({ "username": this.username.text, "password": this.password.text }, '_00000')
            //     httpReq.send(JSON.stringify({ "username": this.username.text, "password": this.password.text }));
            // }

        }, this)

        egret.ExternalInterface.addCallback("getWxCode", (wxCode: string) => {
            console.log(wxCode, "------egret has get code");

            console.log('wxBtn has be clicked !')
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            this.wxBtn.visible = false;
            this.saveChange();
            let wx = "/wx_login";
            httpReq.open("http://" + this.httpIP.text + ":" + this.httpPort.text + wx, egret.HttpMethod.POST);
            httpReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            console.log({ "username": this.username.text, "password": this.password.text }, '_00000')
            httpReq.send(JSON.stringify({ "username": wxCode, "password": this.password.text }));
        });


        this.save.addEventListener(eui.UIEvent.CHANGE, this.saveChange, this);
        this.save.addEventListener(eui.UIEvent.CHANGING, this.saveChange, this);

    }

    private saveChange() {
        console.log('-----change-----', this.save.selected);
        let ls = egret.localStorage;
        if (this.save.selected) {
            ls.setItem('httpIP', this.httpIP.text);
            ls.setItem('socketIP', this.socketIP.text);
            ls.setItem('httpPort', this.httpPort.text);
            ls.setItem('socketPort', this.socketPort.text);
            ls.setItem('username', this.username.text);
            ls.setItem('password', this.password.text);
            ls.setItem('save', '1');
            ls.setItem('isWx', this.isWxLogin.selected ? "1" : "");
        }
        else {
            ls.setItem('httpIP', '');
            ls.setItem('socketIP', '');
            ls.setItem('httpPort', '');
            ls.setItem('socketPort', '');
            ls.setItem('username', '');
            ls.setItem('password', '');
            ls.setItem('save', '');
            ls.setItem('isWx', '');
        }
    }


    private initGame() {
        console.log('socket init cpl')
        new GameData();
        new TipsPnl();

        //短线重连消息
        MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_EnterGameACK, niuniu.EnterGameACK, (msg: niuniu.EnterGameACK) => {
            if (msg.Reconnect) {
                MsgMgr.inst.msgLock = true;
                Main.inst.addChild(new GameTable(msg.TableInfo, true));
                MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_EnterGameACK, this);
            }
        }, this);

        MsgMgr.inst.onMsg(login.LoginID.ID_LoginACK, login.LoginACK, (msg: login.LoginACK) => {
            console.log('收到login回复_' + JSON.stringify(msg));
            if (!msg.Code) {
                console.log('登录成功! id===' + msg.userInfo.ID);
                GameData.userInfo = msg.userInfo;
                let hall = new HallPnl();
                hall.name = 'HallPnl';
                Main.inst.addChild(hall);
                Main.inst.removeChild(LoginPnl.inst);
            }
            else {
                Tips.addTip(SerCodeReflect.reflect(login.LoginCode, msg.Code));
                this.wxBtn.visible = true;
            }
        }, LoginPnl.inst);
        let loginReq = new login.LoginREQ();
        loginReq.Token = GameData.loginToken;


        MsgMgr.inst.sendMsg(login.LoginID.ID_LoginREQ, login.LoginREQ, loginReq);
    }
}