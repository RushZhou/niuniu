class MsgMgr {

    private static _inst: MsgMgr;
    public static get inst() {
        if (MsgMgr._inst) {
            return MsgMgr._inst;
        }
        else {
            console.log("MsgMgr 尚未初始化");
        }
    }


    /**
     *
     * socket init 完成事件
     * @static
     * @type {Array<Function>}
     * @memberof MsgMgr
     */
    public static socketInitCplEvt: Array<Function> = new Array<Function>();

    /**
     * mapLisener key == 消息id     value == Array <Lisener>
     */
    private mapMsgs: Object = new Object();
    private mySocket: egret.WebSocket;
    private msgTypeEnum: Object = new Object();
    //消息队列 在必要的时候会存储消息
    private msgQueue: Array<egret.ByteArray> = new Array();
    //消息重连锁 打开后 所有消息推到队列里等待处理
    private _msgLock: boolean;

    private activeClose: boolean = false;

    public set msgLock(enable: boolean) {
        this._msgLock = enable;
        if (!enable) {
            console.log(this.msgQueue.length, '---------waitting for send msg in queue')
            while (this.msgQueue.length > 0) {
                let msg = this.msgQueue.splice(0, 1)[0];
                this.parseMsg(msg);
            }
        }
    }

    public get msgLock() {
        return this._msgLock;
    }

    private heartbeatTimer: egret.Timer;

    public constructor(ip: string) {
        MsgMgr._inst = this;
        //socket 测试
        this.mySocket = new egret.WebSocket();
        this.mySocket.type = egret.WebSocket.TYPE_BINARY;

        this.mySocket.addEventListener(egret.ProgressEvent.SOCKET_DATA, (evt: egret.Event) => {
            let reciveBytes = new egret.ByteArray();
            reciveBytes.endian = egret.Endian.LITTLE_ENDIAN;
            this.mySocket.readBytes(reciveBytes);

            if (!this.msgLock) {
                //先判定 队列里面的history 处理完没有 (暂时不做该处理,因为set 为false 时候已经处理完毕)
                // if (this.msgRecQueue.length > 0) {
                //     this.msgRecQueue.push(reciveBytes);
                //     let msg = this.msgRecQueue.splice(0, 1)[0];
                //     this.parseMsg(msg);
                // }
                this.parseMsg(reciveBytes);
            }
            else {
                this.msgQueue.push(reciveBytes);
            }
        }, this)

        MsgMgr.socketInitCplEvt.push(this.SocketInitCpl);
        this.mySocket.addEventListener(egret.Event.CONNECT, (data) => {

            // egret.log( data + "_aaaaaa" );
            // let loginMsg = new login.LoginREQ();
            // loginMsg.Token = GameData.loginToken;

            // let u8arr = login.LoginREQ.encode( loginMsg ).finish();
            // let structBytes = new egret.ByteArray( u8arr );

            // let msgBytes = new egret.ByteArray();
            // msgBytes.endian = egret.Endian.LITTLE_ENDIAN;
            // msgBytes.writeUnsignedShort( login.LoginID.ID_LoginREQ );
            // msgBytes.writeBytes( structBytes );

            // msgBytes.position = 0;
            // let abc = msgBytes.readUnsignedShort();
            // console.log( abc + '_abc' );

            // let typeArr = msgBytes.bytes.slice( 0, 2 );

            // let msgType = ( ( ( typeArr[ 0 ] & 0xFF ) << 8 ) | ( typeArr[ 1 ] & 0xFF ) );
            // console.log( msgType + '_msgType' )
            // this.mySocket.writeBytes( msgBytes, 0 );
            // this.mySocket.flush();
            console.log('socket connected');
            this.activeClose = false;
            for (let i in MsgMgr.socketInitCplEvt) {

                console.log(MsgMgr, MsgMgr.socketInitCplEvt.length, '-----111')
                MsgMgr.socketInitCplEvt[i]();
            }
        }, this);

        this.mySocket.addEventListener(egret.Event.CLOSE, () => {
            if (!this.activeClose) {
                TipsPnl.inst.show("网络连接已经断开");
            }
        }, this);

        let arrIP = ip.split(":");
        this.mySocket.connect(arrIP[0], parseInt(arrIP[1]));

    }
    public closeSocket() {
        this.activeClose = true;
        for (let id in this.mapMsgs) {
            let arrLisener: Array<Lisener> = this.mapMsgs[id];
            for (let lisener of arrLisener) {
                lisener.callback = null;
                lisener.caller = null;
                lisener.parser = null;
                //TODO 能移除该item 最好
                lisener = null;
            }

        }
        this.mapMsgs = [];
        MsgMgr.socketInitCplEvt = [];
        this.mySocket.close();

    }
    /**
     * 通过socket 发送消息
     */
    public sendMsg(msgID: number, msgType: any, msg: any) {
        let u8arr: Uint8Array = msgType.encode(msg).finish();
        let structBytes = new egret.ByteArray(u8arr);

        let msgBytes = new egret.ByteArray();
        msgBytes.endian = egret.Endian.LITTLE_ENDIAN;
        msgBytes.writeUnsignedShort(msgID);
        msgBytes.writeBytes(structBytes);
        this.mySocket.writeBytes(msgBytes, 0);
        this.mySocket.flush();
    }

    /**
     * 侦听解析好的消息
     * 同一实例 不支持 同一msg 多次侦听
     * 
     */
    public onMsg<T>(msgID: Number, msgType: T, callback: (msg: any, caller: any) => any, caller: Object) {
        let al: Array<Lisener> = this.mapMsgs[msgID.toString()];
        if (al) {
            for (let lisener of al) {
                if (lisener.caller === caller && lisener.parser === msgType) {
                    console.log(msgID, '____同一obj 多次侦听 同一evt__驳回')
                    return;
                }
            }
            let lisener = new Lisener(caller, callback, msgType);
            // console.log(msgID, '___同一evt 多次被 不同obj侦听')
            al.push(lisener);
        }
        else {
            let al = this.mapMsgs[msgID.toString()] = new Array<Lisener>();
            // console.log(msgID, '____新增事件')
            al.push(new Lisener(caller, callback, msgType));
        }
    }


    /**
     * 卸载某一条消息侦听
     *
     * @param {Number} msgID    消息号
     * @param {Object} _caller   侦听者
     * @memberof MsgMgr
     */
    public offMsg(msgID: Number, _caller: Object) {
        for (let id in this.mapMsgs) {
            if (parseInt(id) === msgID) {
                let arrLisener: Array<Lisener> = this.mapMsgs[id];
                for (let lisener of arrLisener) {
                    if (lisener.caller === _caller) {
                        lisener.callback = null;
                        lisener.caller = null;
                        lisener.parser = null;
                        //TODO 能移除该item 最好
                        lisener = null;
                        return;
                    }
                }
            }
        }
    }


    /**
     * 卸载当前实例的所有侦听
     *
     * @param {Object} _caller  侦听者
     * @memberof MsgMgr
     */
    public offMsgAll(_caller: Object) {
        for (let id in this.mapMsgs) {
            for (let lisener of this.mapMsgs[id]) {
                if (lisener.caller === _caller) {
                    lisener.callback = null;
                    lisener.caller = null;
                    lisener.parser = null;
                    //todo 能移除该item 最好
                    lisener = null;
                    console.log('remove msg ')
                    break;
                }

            }
        }
    }

    private parseMsg(reciveBytes: egret.ByteArray) {
        let msgID = reciveBytes.readUnsignedShort();
        egret.log(msgID + "_____________socket 收到的 消息ID---------");
        let msgBytes = new egret.ByteArray();
        msgBytes.writeBytes(reciveBytes, 2);
        for (let id in this.mapMsgs) {
            if (parseInt(id) === msgID) {
                let al: Array<Lisener> = this.mapMsgs[id];
                for (let lisener of al) {
                    if (!lisener.caller) continue;
                    lisener.callback(lisener.parser.decode(msgBytes.bytes), lisener.caller);
                }
            }
        }
    }

    /**
     * 搜索现有list是否包含 某个caller
     */
    // private findObjInList( caller: Object ): Lisener {
    //     for ( let i in this.mapLisener ) {
    //         let item = this.mapLisener[ i ];
    //         if ( item.caller === caller ) {
    //             return item;
    //         }
    //     }
    //     return null;
    // }

    private SocketInitCpl() {
        this.heartbeatTimer = new egret.Timer(10000);
        this.heartbeatTimer.addEventListener(egret.TimerEvent.TIMER, () => {
            let heartbeatReq = new agent.HeartBeatREQ();
            let t = new Date().getTime();
            heartbeatReq.Ping = t;
            MsgMgr.inst.sendMsg(agent.AgentID.ID_HeartBeatREQ, agent.HeartBeatREQ, heartbeatReq);
        }, this);
        this.heartbeatTimer.start();
    }
}

class Lisener {
    public caller: Object;
    public callback: Function;
    public parser: any;
    public constructor(_caller: Object, _func: Function, _parser: any) {
        this.caller = _caller;
        this.callback = _func;
        this.parser = _parser;
    }
}