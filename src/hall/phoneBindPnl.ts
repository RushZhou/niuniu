class PhoneBindPnl extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.phoneBindPnl';
    }
    private closeBtn: eui.Button;
    private nextStep: eui.Button;
    private phone: eui.TextInput;
    private verifyCode: eui.TextInput;
    public sendVerifyCode: eui.Button;



    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.nextStep.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onNextStepFunc, this);
        this.sendVerifyCode.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSendVerifyCodeFunc, this);

        this.phone.prompt = "请输入您的手机号码";
        this.verifyCode.prompt = "请输入验证码";

    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
    private onSendVerifyCodeFunc() {
        let strPhone = this.phone.text;
        let sendVerifyCodeReq = new login.SendVerifyCodeREQ();
        sendVerifyCodeReq.PhoneNumber = strPhone;
        SoundManager.ins.playEffect('sfx_button_click_mp3');
        MsgMgr.inst.sendMsg(login.LoginID.ID_SendVerifyCodeREQ, login.SendVerifyCodeREQ, sendVerifyCodeReq);
    }
    private onNextStepFunc() {
        let strPhone = this.phone.text;
        let strVerifyCode = this.verifyCode.text;

        let bindReq = new login.BindPhoneNumberREQ();
        bindReq.PhoneNumber = strPhone;
        bindReq.VerifyCode = strVerifyCode;

        MsgMgr.inst.sendMsg(login.LoginID.ID_BindPhoneNumberREQ, login.BindPhoneNumberREQ, bindReq);
        console.log('手机认证', bindReq);
        MsgMgr.inst.onMsg(login.LoginID.ID_BindPhoneNumberACK, login.BindPhoneNumberACK, (msg: login.BindPhoneNumberACK) => {
            console.log('手机认证', msg, msg.Code);
            if (msg.Code === 0) {
                Main.inst.dispatchEventWith(GameData.GameEvent.BindPhone);
                Tips.addTip('手机号绑定手机成功');
            } else {
                if (msg.Code === 1) {
                    Tips.addTip("您的手机号已经绑定过了");
                } else if (msg.Code === 2) {
                    Tips.addTip('绑定手机错误.请重新绑定');
                }

            }
        }, this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

}