class StorePnl extends eui.Component implements eui.UIComponent {
    private type: StoreType;

    public constructor(type: StoreType) {
        super();
        this.type = type;
        this.skinName = skins.hall.storePnl;
    }
    private closeBtn: eui.Button;
    private radio: eui.RadioButton;
    private viewStack: eui.ViewStack;
    private scroll: eui.Scroller;
    private list: eui.List;
    private readonly chargeUrl = 'http://116.62.237.212:6017/H5/zpUnsigned/qrCode.html?';
    private scoreLabel: eui.Label;
    private dimondsLabel: eui.Label;

    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);

        this.radio.group.$name = 'storePnl';
        this.radio.group.addEventListener(eui.UIEvent.CHANGE, this.onChange, this);
        switch (this.type) {
            case StoreType.diamond:
                this.radio.label = '充值钻石';
                break;

            case StoreType.socre:
                this.radio.label = '充值积分';
                break;
        }

        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_ChargeOrderACK, teahouse.ChargeOrderACK, (msg: teahouse.ChargeOrderACK) => {
            console.log('---order ack---', msg)
            switch (GameData.getPlatform()) {
                case WhichPlatform.web:
                    window.open(this.chargeUrl + msg.Url);
                    break

                case WhichPlatform.android:
                case WhichPlatform.ios:
                    egret.ExternalInterface.call("openUrl", this.chargeUrl + msg.Url);
                    break;
            }

        }, this);

        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_ChargeOrderNTF, teahouse.ChargeOrderNTF, (msg: teahouse.ChargeOrderNTF) => {
            console.log('---order ntf---', msg)
        }, this);

        let arrData: IStoreItemStruct[];
        if (this.type === StoreType.diamond) {
            arrData = [
                { imgSrc: "NiuNiuView527_png", itemCount: 10, money: 10, type: StoreType.diamond },
                { imgSrc: "NiuNiuView532_png", itemCount: 30, money: 30, type: StoreType.diamond },
                { imgSrc: "NiuNiuView530_png", itemCount: 50, money: 50, type: StoreType.diamond },
                { imgSrc: "NiuNiuView547_png", itemCount: 100, money: 100, type: StoreType.diamond },
                { imgSrc: "NiuNiuView562_png", itemCount: 200, money: 200, type: StoreType.diamond },
                { imgSrc: "NiuNiuView572_png", itemCount: 500, money: 500, type: StoreType.diamond }

            ];
            this.dimondsLabel.visible = true;
        }
        else {
            arrData = [
                { imgSrc: "rmb100_png", itemCount: 1000, money: 0.1, type: StoreType.socre },
                { imgSrc: "rmb200_png", itemCount: 2000, money: 0.2, type: StoreType.socre },
                { imgSrc: "rmb500_png", itemCount: 4000, money: 0.3, type: StoreType.socre },
                { imgSrc: "rmb1000_png", itemCount: 5000, money: 0.4, type: StoreType.socre },
            ];
            this.scoreLabel.visible = true;
        }


        let listData = new eui.ArrayCollection(arrData);
        this.list.dataProvider = listData;
        this.list.itemRenderer = StoreItem;
        this.scroll.viewport = this.list;
    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.radio.group.removeEventListener(eui.UIEvent.CHANGE, this.onChange, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private onChange(e: eui.UIEvent) {
        let radioGroup: eui.RadioButtonGroup = e.target;
        console.log('store', 'radio');
        if (radioGroup.$name !== 'storePnl') return;
        this.viewStack.selectedIndex = radioGroup.selectedValue;
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
}



class StoreItem extends eui.ItemRenderer {
    public data: IStoreItemStruct;
    public constructor() {
        super();
        this.skinName = hall.StoreItemSkins;
    }

    private img: eui.Image;
    private itemCount: eui.Label;
    private money: eui.Label;
    private buyBtn: eui.Button;
    //交换的物品图片
    private exchangeImg: eui.Image;
    private type: StoreType;

    protected childrenCreated() {
        super.childrenCreated();

        this.buyBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            switch (this.type) {
                case StoreType.diamond:
                    let feedBack = new FeedBackPnl();
                    Main.inst.addChild(feedBack);
                    break;

                case StoreType.socre:
                    let chargeReq = new teahouse.ChargeOrderREQ();
                    chargeReq.HouseID = GameData.teahouseID;
                    try {
                        chargeReq.Money = this.data.money;
                        chargeReq.Type = '1';
                    }
                    catch (e) {
                        console.log('Money error')
                        return;
                    }
                    MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_ChargeOrderREQ, teahouse.ChargeOrderREQ, chargeReq);
                    break;
            }

        }, this);

    }
    protected dataChanged() {
        this.exchangeImg.texture = RES.getRes(this.data.type === StoreType.diamond ? 'NiuNiuView055_png' : 'rmb500_png');
        this.img.texture = RES.getRes(this.data.imgSrc);
        this.img.width = 100;
        this.img.height = 92;
        this.itemCount.text = 'X' + this.data.itemCount;
        this.money.text = this.data.money + "元";
        this.type = this.data.type;
    }

}

interface IStoreItemStruct {
    imgSrc: string,
    itemCount: number,
    money: number,
    type: StoreType
}


/**
 * 商城类型 积分或者钻石
 *
 * @enum {number}
 */
enum StoreType {
    diamond,
    socre
}