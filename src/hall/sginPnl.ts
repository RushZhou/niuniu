class SginPnl extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.sginPnl';
    }
    private closeBtn: eui.Button;
    private sginBtns: eui.Button;


    private date: eui.Label;

    private sginList: eui.List;
    private ListMsg: eui.ArrayCollection;
    private sginIndex = -1;
    private sginCount: eui.Label;

    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.sginBtns.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSgin, this);

        var myDate = new Date();
        this.date.text = myDate.toLocaleDateString();
        let year = myDate.getFullYear();
        let month = myDate.getMonth();
        let day = myDate.getDate()
        //获取本月第一天周几
        var monthFirst = new Date(year, month, 1).getDay();
        //获取本月天数
        var totalDay = (new Date(year, month + 1, 0)).getDate();
        if (month === 0) {
            year--;
            month = 12;
        }
        //上个月一共有多少天
        var UptotalDay = (new Date(year, month, 0)).getDate();
        console.log(year, month, day, monthFirst, month, totalDay, UptotalDay);


        let fday = UptotalDay - monthFirst + 1;
        let _Listmsg = [];

        for (let i = 0; i < 42; i++) {
            let sday = fday + i;
            let tagMonth = -1;
            if (sday > UptotalDay) {
                sday -= UptotalDay;
                tagMonth = month;
                if (sday > totalDay) {
                    sday -= totalDay;
                    tagMonth = month + 1 > 11 ? 0 : month + 1;
                }
            } else {
                tagMonth = month - 1 < 0 ? 11 : month - 1;
            }
            if (day === sday && tagMonth === month) this.sginIndex = i;
            _Listmsg.push({ month: tagMonth, day: sday, sgin: false, today: day });
        }
        this.ListMsg = this.sginList.dataProvider = new eui.ArrayCollection(_Listmsg);
        this.sginList.itemRenderer = sginItemRender;


        let sginInfoReq = new login.SignInfoREQ();
        MsgMgr.inst.sendMsg(login.LoginID.ID_SignInfoREQ, login.SignInfoREQ, sginInfoReq);

        console.log('申请已签到', sginInfoReq);
        MsgMgr.inst.onMsg(login.LoginID.ID_SignInfoACK, login.SignInfoACK, (msg: login.SignInfoACK) => {
            console.log('已签到返回', msg);
            MsgMgr.inst.offMsg(login.LoginID.ID_SignInACK, this);
            for (let i = 0; i < msg.Day.length; i++) {
                let sgined = msg.Day[i];
                if (sgined === new Date().getDate()) this.sginBtns.visible = false;
                this.ListMsg.replaceItemAt({ month: new Date().getMonth(), day: sgined, sgin: true, today: new Date().getDate()}, this.sginIndex);
            }
            this.sginCount.text = '累计本月签到：' + msg.Day.length + '次';
        }, this);

    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private onSgin() {
        SoundManager.ins.playEffect('sfx_button_click_mp3');
        let sginReq = new login.SignInREQ();
        MsgMgr.inst.sendMsg(login.LoginID.ID_SignInREQ, login.SignInREQ, sginReq);
        console.log('签到', sginReq);
        MsgMgr.inst.onMsg(login.LoginID.ID_SignInACK, login.SignInACK, (msg: login.SignInACK) => {
            console.log('签到返回', msg, new Date().getDate());
            MsgMgr.inst.offMsg(login.LoginID.ID_SignInACK, this);
            this.sginBtns.visible = false;
            this.sginBtns.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onSgin, this);
            this.ListMsg.replaceItemAt({ month: new Date().getMonth(), day: new Date().getDate(), sgin: true, today: new Date().getDate() }, this.sginIndex);
            this.sginCount.text = '累计本月签到：' + msg.Day.length + '次';
            Main.inst.dispatchEventWith(GameData.GameEvent.UpdateSgined, false, { Sgin: true });
        }, this);

    }

}

class sginItemRender extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "sginItem";
    }
    private signBtn: eui.Button;
    private nosgin: eui.Image;
    private sgin: eui.Image;
    private maskGary: eui.Image;


    private sginData = null;
    protected childrenCreated() {
        super.childrenCreated();
    }
    private initSginInfo() {
        let day = this.sginData.day;
        let sgin = this.sginData.sgin;
        let today = this.sginData.today;
        let month = this.sginData.month;
        // console.log(month,new Date().getMonth(), day, sgin, today);
        if (new Date().getMonth() === month) {
            if (day < today) {
                if (sgin) {
                    this.sgin.visible = true;
                } else {
                    this.nosgin.visible = true;
                }
            } else if (day === today) {
                this.signBtn.visible = true;
                if (sgin) {
                    this.sgin.visible = true
                }
            } else {

            }
        } else {
            this.maskGary.visible = true;
        }

    }
    protected dataChanged() {
        this.sginData = this.data;
        this.initSginInfo();
    }
}
