class JoinRoomPnl extends eui.Component implements eui.UIComponent {
    public constructor(joinType: JoinType) {
        super();
        this.skinName = 'skins.hall.JoinRoomPnl';
        if (joinType == JoinType.club) {
            this.joinRoomRadio.selected = false;
            this.joinClubRadio.selected = true;
        } else {
            this.joinRoomRadio.selected = true;
            this.joinClubRadio.selected = false;
        }
        this.checkType = joinType;
    }
    private closeBtn: eui.Button;
    private btn_1: eui.Button;
    private btn_2: eui.Button;
    private btn_3: eui.Button;
    private btn_4: eui.Button;
    private btn_5: eui.Button;
    private btn_6: eui.Button;
    private btn_7: eui.Button;
    private btn_8: eui.Button;
    private btn_9: eui.Button;
    private btn_0: eui.Button;
    private btn_rewrite: eui.Button;
    private btn_delete: eui.Button;

    private labal_Group: eui.Group;
    private imageBg_Group: eui.Group;

    private joinRoomRadio: eui.RadioButton;
    private joinClubRadio: eui.RadioButton;

    private showLabel: eui.Label;

    private _inputIndex: number = 0;
    private _clubNum: number = 6;
    private _roomNum: number = 7;
    private checkType;
    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);

        this.btn_0.name = "btn_0";
        this.btn_1.name = "btn_1";
        this.btn_2.name = "btn_2";
        this.btn_3.name = "btn_3";
        this.btn_4.name = "btn_4";
        this.btn_5.name = "btn_5";
        this.btn_6.name = "btn_6";
        this.btn_7.name = "btn_7";
        this.btn_8.name = "btn_8";
        this.btn_9.name = "btn_9";

        this.btn_1.addEventListener(egret.TouchEvent.TOUCH_TAP, this.btnFunc, this);
        this.btn_2.addEventListener(egret.TouchEvent.TOUCH_TAP, this.btnFunc, this);

        this.btn_3.addEventListener(egret.TouchEvent.TOUCH_TAP, this.btnFunc, this);
        this.btn_4.addEventListener(egret.TouchEvent.TOUCH_TAP, this.btnFunc, this);

        this.btn_5.addEventListener(egret.TouchEvent.TOUCH_TAP, this.btnFunc, this);
        this.btn_6.addEventListener(egret.TouchEvent.TOUCH_TAP, this.btnFunc, this);

        this.btn_7.addEventListener(egret.TouchEvent.TOUCH_TAP, this.btnFunc, this);
        this.btn_8.addEventListener(egret.TouchEvent.TOUCH_TAP, this.btnFunc, this);

        this.btn_9.addEventListener(egret.TouchEvent.TOUCH_TAP, this.btnFunc, this);
        this.btn_0.addEventListener(egret.TouchEvent.TOUCH_TAP, this.btnFunc, this);


        this.btn_rewrite.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onResetClicked, this);
        this.btn_delete.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onDelClicked, this);

        this.joinClubRadio.group.$name = 'JoinRoomPnl';
        this.joinClubRadio.group.addEventListener(eui.UIEvent.CHANGE, this.radioChangeHandler, this);
    }

    private radioChangeHandler(e: eui.UIEvent) {
        this.onResetClicked();
        let radioGroup: eui.RadioButtonGroup = e.target;
        console.log('JoinRoomPnl','radio');
        if (radioGroup.$name !== 'JoinRoomPnl') return;
        if (radioGroup.selectedValue === '0') {
            this.showLabel.text = "请输入茶楼号";
            this.imageBg_Group.getElementAt(this.imageBg_Group.numElements - 1).visible = false;
            this.labal_Group.getElementAt(this.labal_Group.numElements - 1).visible = false;
            this.checkType = JoinType.club;
            this.imageBg_Group.x = 135;
            this.labal_Group.x = 135;
        } else {
            this.showLabel.text = "请输入房间号";
            this.imageBg_Group.getElementAt(this.imageBg_Group.numElements - 1).visible = true;
            this.labal_Group.getElementAt(this.labal_Group.numElements - 1).visible = true;
            this.checkType = JoinType.room;
            this.imageBg_Group.x = 100;
            this.labal_Group.x = 100;
        }

    }
    private btnFunc(e: egret.TouchEvent) {
        var name: string = e.currentTarget.name;
        let putId = name.split('_')[1];
        this.onInput(putId);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.joinClubRadio.group.removeEventListener(eui.UIEvent.CHANGE, this.radioChangeHandler, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private onResetClicked() {
        for (var i = 0; i < this.labal_Group.numElements; ++i) {
            let lab = this.labal_Group.getElementAt(i) as eui.Label;
            lab.text = '';
        }
        this._inputIndex = 0;
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private onDelClicked() {
        if (this._inputIndex > 0) {
            this._inputIndex -= 1;
            let lab = this.labal_Group.getElementAt(this._inputIndex) as eui.Label;
            lab.text = '';
        }
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private onInput(num) {
        let maxNum = this.checkType === JoinType.club ? this._clubNum : this._roomNum;
        if (this._inputIndex >= maxNum) {
            return;
        }

        let name = "lab" + this._inputIndex;
        let lab = this.labal_Group.getElementAt(this._inputIndex) as eui.Label;
        lab.text = "" + num;
        this._inputIndex += 1;

        if (this._inputIndex == maxNum) {
            var roomId = this.parseRoomID();
            if (parseInt(roomId) >= maxNum) {
                roomId = ("" + roomId).substring(0, maxNum);
                this.onInputFinished(roomId);
            }
        }
    }

    private parseRoomID() {
        var str = "";
        for (var i = 0; i < this.labal_Group.numElements; ++i) {
            let lab = this.labal_Group.getElementAt(i) as eui.Label;
            str += lab.text;
        }
        return str;
    }

    private onInputFinished(roomId: string) {
        if (this.joinRoomRadio.selected) {
            let joinReq = new niuniu.EnterGameREQ();
            joinReq.GameID = structure.GameID.Niuniu;
            joinReq.TableID = parseInt(roomId);
            MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_EnterGameREQ, niuniu.EnterGameREQ, joinReq);
            console.log('加入游戏房间', joinReq);
            MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_EnterGameACK, niuniu.EnterGameACK, (msg: niuniu.EnterGameACK) => {
                console.log('加入游戏房间返回', msg);
                if (msg.Code === 0 /*|| msg.Code === 5*/) {
                    //加入房间成功
                    Main.inst.removeChild(this);
                    MsgMgr.inst.offMsgAll(this);
                    GameData.EvtEnterRoom(msg.TableInfo);

                } else {
                    Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code));
                }
            }, this);
        }
        if (this.joinClubRadio.selected) {
            let joinTeaHouseReq = new teahouse.TeaHouseActionREQ();
            joinTeaHouseReq.Action = teahouse.TeaHouseAction.Join;
            joinTeaHouseReq.UserID = GameData.userInfo.ID;
            joinTeaHouseReq.HouseID = parseInt(roomId);
            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseActionREQ, teahouse.TeaHouseActionREQ, joinTeaHouseReq);
            console.log('加入茶楼', joinTeaHouseReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
                console.log('加入茶楼返回', msg, msg.Code);
                if (msg.Code === 0 /*|| msg.Code === 5*/) {
                    //加入茶楼成功
                    Main.inst.removeChild(this);
                    MsgMgr.inst.offMsgAll(this);
                    GameData.EvtEnterTeahouse(msg.HouseID);

                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
                MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, this);
            }, this);
        }
    }
}

enum JoinType {
    club,
    room
}