class HallPnl extends eui.Component implements eui.UIComponent {
	public constructor() {
		super();
		this.skinName = "skins.hallPnl";
		SoundManager.ins.playMusic("bg0_mp3");
	}

	protected partAdded(partName: string, instance: any): void {
		super.partAdded(partName, instance);
	}

	private btnCreateClub: eui.Button;
	private btnJoinGame: eui.Button;
	private btnCreateRoom: eui.Button;
	private btnOpenedClub: eui.Button;
	private btnStore: eui.Button;
	private btnActive: eui.Button;
	private btnSign: eui.Button;
	private btnRoomList: eui.Button;
	private btnDiamonds: eui.Button;
	private btnPeopleCenter: eui.Button;
	private btnPhoneCenter: eui.Button;
	private btnHead: eui.Button;
	private btnRecord: eui.Button;
	private btnShare: eui.Button;
	private btnnews: eui.Button;
	private btnInvitation: eui.Button;
	private btnMenu: eui.Button;
	private btnExit: eui.Button;
	private btnFeedBack: eui.Button;
	private btnRule: eui.Button;
	private btnSet: eui.Button;
	private closeBtn: eui.Button;

	private nickName: eui.Label;
	private Id: eui.Label;
	private diamond: eui.Label;

	private menuBg: eui.Group;

	private sex: eui.Image;

	private noticBg: eui.Image;
	private noticLabl: eui.Label;

	private peopleRed: eui.Image;
	private phoneRed: eui.Image;
	private noticGroup: eui.Group;

	private sginRed: eui.Image;
	private emailRed: eui.Image;

	private head: eui.Image;


	protected childrenCreated(): void {
		super.childrenCreated();
		GameData.handleHeadImg(this.head, GameData.userInfo.HeadIcon);
		// this.head.source = GameData.userInfo.HeadIcon;


		this.btnCreateClub.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCreateClubFunc, this);
		this.btnJoinGame.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onJoinGameFunc, this);
		this.btnCreateRoom.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCreateRoomFunc, this);
		this.btnOpenedClub.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onOpenedClubFunc, this);
		this.btnStore.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onStoreFunc, this);
		this.btnActive.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onActiveFunc, this);
		this.btnSign.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSignFunc, this);
		this.btnRoomList.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRoomListFunc, this);
		this.btnDiamonds.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onDiamondsFunc, this);
		this.btnPeopleCenter.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onPeopleCenterFunc, this);
		this.btnPhoneCenter.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onPhoneCenterFunc, this);
		this.btnHead.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onHeadFunc, this);
		this.btnRecord.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRecordFunc, this);
		this.btnShare.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onShareFunc, this);
		this.btnnews.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onNewsFunc, this);
		this.btnInvitation.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onInvitationFunc, this);
		this.btnMenu.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onMenuFunc, this);
		this.btnExit.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onExitFunc, this);
		this.btnFeedBack.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onFeedBackFunc, this);
		this.btnRule.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRuleFunc, this);
		this.btnSet.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSetFunc, this);
		this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);

		Main.inst.addEventListener(GameData.GameEvent.BindIdentify, (dat) => {
			if (GameData.userInfo.BindIdentify) {
				this.btnPeopleCenter.visible = false;
				this.peopleRed.visible = false;
			}
			// SoundManager.ins.playEffect('sfx_button_click_mp3');
		}, this);
		Main.inst.addEventListener(GameData.GameEvent.BindPhone, (dat) => {
			if (GameData.userInfo.BindPhone) {
				this.btnPhoneCenter.visible = false;
				this.phoneRed.visible = false;
			}
			// SoundManager.ins.playEffect('sfx_button_click_mp3');
		}, this);

		this.updateDiamonds();

		this.initUserInfo();
		this.initNotice();


		Main.inst.addEventListener(GameData.GameEvent.ExitGame, () => {
			SoundManager.ins.destroy();
			MsgMgr.inst.offMsgAll(this);
			MsgMgr.inst.closeSocket();
			Main.inst.removeChildren();
			Main.inst.addChild(new LoginPnl());
		}, this);
		Main.inst.addEventListener(GameData.GameEvent.DeleteTeaHousePnl, () => {
			let quitTeaHouseReq = new niuniu.DisObserverTableListREQ();
			quitTeaHouseReq.HouseID = GameData.teahouseID;
			MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_DisObserverTableListREQ, niuniu.DisObserverTableListREQ, quitTeaHouseReq);
			let teahouse = Main.inst.getChildByName('teahouse');
			MsgMgr.inst.offMsgAll(teahouse);
			Main.inst.removeChild(teahouse);
		}, this);


		//判断今日是否已签到
		let sginInfoReq = new login.SignInfoREQ();
		MsgMgr.inst.sendMsg(login.LoginID.ID_SignInfoREQ, login.SignInfoREQ, sginInfoReq);

		console.log('申请已签到', sginInfoReq);
		MsgMgr.inst.onMsg(login.LoginID.ID_SignInfoACK, login.SignInfoACK, (msg: login.SignInfoACK) => {
			console.log('已签到返回', msg);
			MsgMgr.inst.offMsg(login.LoginID.ID_SignInACK, this);
			for (let i = 0; i < msg.Day.length; i++) {
				let sgined = msg.Day[i];
				if (sgined === new Date().getDate()) this.sginRed.visible = false;
			}

		}, this);

		//刷新签到红点
		Main.inst.addEventListener(GameData.GameEvent.UpdateSgined, (evt: egret.Event) => {
			if (evt.data.Sgin) {
				this.sginRed.visible = false;
			}
		}, this);

		//判断是否有新邮件
		let mailReq = new login.MailREQ();
		MsgMgr.inst.sendMsg(login.MailID.ID_MailREQ, login.MailREQ, mailReq);
		MsgMgr.inst.onMsg(login.MailID.ID_MailACK, login.MailACK, (msg: login.MailACK) => {
			this.emailRed.visible = msg.Mail.length === 0 ? false : true;
			MsgMgr.inst.offMsg(login.MailID.ID_MailACK, this);
		}, this);

		//接收推送新邮件
		MsgMgr.inst.onMsg(login.MailID.ID_MailNTF, login.MailNTF, (msg: login.MailNTF) => {
			console.log("接收推送新邮件", msg);
			if (msg.Mail.Type === structure.Mail.MailType.Invite) {
				this.emailRed.visible = true;
			}
		}, this);

		//刷新邮件红点
		Main.inst.addEventListener(GameData.GameEvent.UpdateEmail, (evt: egret.Event) => {
			let mailReq = new login.MailREQ();
			MsgMgr.inst.sendMsg(login.MailID.ID_MailREQ, login.MailREQ, mailReq);
			MsgMgr.inst.onMsg(login.MailID.ID_MailACK, login.MailACK, (msg: login.MailACK) => {
				this.emailRed.visible = msg.Mail.length === 0 ? false : true;
			}, this);
		}, this);

	}
	private updateDiamonds() {
		MsgMgr.inst.onMsg(login.LoginID.ID_DiamondChangeNTF, login.DiamondChangeNTF, (msg: login.DiamondChangeNTF) => {
			console.log("刷新钻石", msg);
			this.diamond.text = msg.Diamond.toString();
			GameData.userInfo.Diamond = msg.Diamond;
			MsgMgr.inst.offMsgAll(this);
		}, this);

	}
	private initNotice() {
		this.noticGroup.mask = this.noticBg;
		egret.Tween.get(this.noticLabl, { loop: true })
			.to({ x: -700 }, 12800).to({ x: 1280 }, 1);

		// .call(() => {
		// 	i++;
		// 	console.log('坐标',i);
		// }, this);

		let noticReq = new login.NoticeREQ();
		MsgMgr.inst.sendMsg(login.LoginID.ID_NoticeREQ, login.NoticeREQ, noticReq);
		MsgMgr.inst.onMsg(login.LoginID.ID_NoticeACK, login.NoticeACK, ((msg: login.NoticeACK) => {
			console.log('收到消息', msg);
			this.noticLabl.text = msg.Notice;
			MsgMgr.inst.offMsg(login.LoginID.ID_NoticeACK, this);
		}), this);

		Main.inst.addEventListener(GameData.GameEvent.ChangeSound, (evt: egret.Event) => {
			console.log("-----ChangeSound------", evt.data);
			SoundManager.ins.destroy();
			if (evt.data.SoundType === "Hall") {
				SoundManager.ins.playMusic("bg0_mp3");
			} else if (evt.data.SoundType === "Game") {
				SoundManager.ins.playMusic("bg1_mp3");
			}
		}, this);
		Main.inst.addEventListener(GameData.GameEvent.DeleteGamePnl, (evt: egret.Event) => {
			console.log("-------------------", evt.data.HouseType);
			if (evt.data.HouseType !== 0) {
				let enterTeaHouseReq = new niuniu.ObserverTableListREQ();
				enterTeaHouseReq.HouseID = evt.data.HouseType;
				MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_ObserverTableListREQ, niuniu.ObserverTableListREQ, enterTeaHouseReq);
				console.log('进入茶楼', enterTeaHouseReq);
				MsgMgr.inst.offMsgAll(this);
				GameData.EvtEnterTeahouse(evt.data.HouseType);
			}
		}, this);
	}

	private initUserInfo() {
		this.nickName.text = GameData.userInfo.Nickname;
		this.Id.text = GameData.userInfo.ID.toString();
		this.diamond.text = GameData.userInfo.Diamond.toString();
		console.log('---------------------', GameData.userInfo.BindIdentify, GameData.userInfo.BindPhone);
		if (GameData.userInfo.BindIdentify) {
			this.btnPeopleCenter.visible = false;
			this.peopleRed.visible = false;
		}
		if (GameData.userInfo.BindPhone) {
			this.btnPhoneCenter.visible = false;
			this.phoneRed.visible = false;
		}

		if (GameData.userInfo.Sex === 1) {
			this.sex.texture = RES.getRes('hall_nan_png');
		} else {
			this.sex.texture = RES.getRes('hall_nv_png');
		}
	}

	private onShareFunc(): void {
		// this.btnShare.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onShareFunc, this);
		let share = new SharePnl();
		this.addChild(share);
		console.log("打开分享");
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}

	private onCreateClubFunc(): void {
		let tcp = new TableConfigPnl(CreateType.Teahouse);
		Main.inst.addChild(tcp);
		console.log("创建俱乐部");
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onJoinGameFunc(): void {
		console.log("加入游戏/俱乐部");
		let joinRoom = new JoinRoomPnl(JoinType.room);
		Main.inst.addChild(joinRoom);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onCreateRoomFunc(): void {
		console.log("创建房间");
		let tcp = new TableConfigPnl(CreateType.WorldRoom);
		Main.inst.addChild(tcp);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onOpenedClubFunc(): void {
		console.log("打开俱乐部");
		let roomPnl = new RoomListPnl(OpenType.club);
		roomPnl.name = 'RoomListPnl';
		Main.inst.addChild(roomPnl);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onStoreFunc(): void {
		console.log("打开商城");
		let storePnl = new StorePnl(StoreType.diamond);
		this.addChild(storePnl);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onActiveFunc(): void {
		console.log("打开活动");
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onSignFunc(): void {
		console.log("打开签到");
		let sgin = new SginPnl();
		this.addChild(sgin);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onRoomListFunc(): void {
		console.log("打开房间列表");
		let roomPnl = new RoomListPnl(OpenType.room);
		roomPnl.name = 'RoomListPnl';
		Main.inst.addChild(roomPnl);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onDiamondsFunc(): void {
		console.log("打开商城");
		let storePnl = new StorePnl(StoreType.diamond);
		this.addChild(storePnl);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onPeopleCenterFunc(): void {
		console.log("打开个人绑定");
		let authentication = new AuthenticationPnl();
		this.addChild(authentication);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onPhoneCenterFunc(): void {
		console.log("打开手机绑定");
		let phoneBind = new PhoneBindPnl();
		this.addChild(phoneBind);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onHeadFunc(): void {
		console.log("打开个人资料");
		let plCenter = new PersonalCenter();
		this.addChild(plCenter);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onRecordFunc(): void {
		console.log("打开战绩");
		let record = new RecordPnl();
		this.addChild(record);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onNewsFunc(): void {
		console.log("打开消息");
		let email = new EmailPnl();
		this.addChild(email);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onInvitationFunc(): void {
		console.log("打开邀请码");
		let invitation = new InvitationPnl();
		this.addChild(invitation);
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onMenuFunc(): void {
		console.log("打开菜单");
		let isMenuMove = this.menuBg.y <= 500 ? 1 : 0;

		if (isMenuMove) {
			egret.Tween.get(this.menuBg).to({ x: 520, y: 800, scaleX: 1, scaleY: 1, alpha: 1 }, 200).call(function () {
				this.menuBg.y = 800;
				this.closeBtn.visible = false;
				this.menuBg.visible = false;
			}, this);

		} else {
			this.menuBg.visible = true;
			egret.Tween.get(this.menuBg).to({ x: 520, y: 500, scaleX: 1, scaleY: 1, alpha: 1 }, 200).call(function () {
				this.menuBg.y = 500;

				this.closeBtn.visible = true;
			}, this);
		}
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onFeedBackFunc(): void {
		console.log("打开反馈");
		let feedBack = new FeedBackPnl();
		this.addChild(feedBack);
		this.onCloseFunc();
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onRuleFunc(): void {
		let rulePnl = new RuleOrPrivacyPnl("rule");
		this.addChild(rulePnl);
		console.log("打开规则");
		this.onCloseFunc();
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onSetFunc(): void {
		console.log("打开设置");
		let setPnl = new SettingPnl();
		this.addChild(setPnl);
		this.onCloseFunc();
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onExitFunc(): void {
		console.log("退出游戏");
		this.onCloseFunc();
		SoundManager.ins.playEffect('sfx_button_click_mp3');
	}
	private onCloseFunc() {
		console.log("移除菜单");
		egret.Tween.get(this.menuBg).to({ x: 520, y: 800, scaleX: 1, scaleY: 1, alpha: 1 }, 200).call(function () {
			this.menuBg.y = 800;
			this.closeBtn.visible = false;
			this.menuBg.visible = false;
		}, this);
	}

}