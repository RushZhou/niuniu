class AuthenticationPnl extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.authenticationPnl';
    }
    private closeBtn: eui.Button;
    private submitInfo: eui.Button;
    private putCID: eui.TextInput;
    private putName: eui.TextInput;

    protected childrenCreated() {
        super.childrenCreated();
        this.putName.prompt = "请输入您的名字";
        this.putCID.prompt = "请输入您的身份证号码";
        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.submitInfo.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSubmitInfoFunc, this);
    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private onSubmitInfoFunc() {
        let strCid = this.putCID.text;

        let strName = this.putName.text;

        let bindReq = new login.BindIdentifyREQ();
        bindReq.Identify = strCid;
        bindReq.RealName = strName;
        MsgMgr.inst.sendMsg(login.LoginID.ID_BindIdentifyREQ, login.BindIdentifyREQ, bindReq);
        console.log('实名认证', bindReq);
        MsgMgr.inst.onMsg(login.LoginID.ID_BindIdentifyACK, login.BindIdentifyACK, (msg: login.BindIdentifyACK) => {
            console.log('实名认证', msg);
            if (msg.Code === 0) {
                this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
                this.submitInfo.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onSubmitInfoFunc, this);
                this.parent.removeChild(this);
                GameData.userInfo.BindIdentify = true;
                Main.inst.dispatchEventWith(GameData.GameEvent.BindIdentify);
                Tips.addTip("实名认证成功");
            } else {
                if (msg.Code === 1) {
                    Tips.addTip("名字不合法");
                } else if (msg.Code === 3) {
                    Tips.addTip("格式错误,校验身份证失败");
                }

            }
            MsgMgr.inst.offMsg(login.LoginID.ID_BindIdentifyACK, this);
        }, this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
}