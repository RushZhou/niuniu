class RecordPnl extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.recordPnl';
    }

    private closeBtn: eui.Button;
    private normalRadio: eui.RadioButton;
    private viewSrack: eui.ViewStack;
    private normalList: eui.List;
    private normalScroll: eui.Scroller;
    private clubRecordList: eui.List;
    private clubScroll: eui.Scroller;
    public clubTips: eui.Label;
    public norTips: eui.Label;


    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);

        this.normalRadio.group.$name = 'rocordPnl';
        this.normalRadio.group.addEventListener(eui.UIEvent.CHANGE, this.radioChangeHandler, this);

        this.initNormalRecord();
        this.initClubRecord();


        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_UserRecordACK, teahouse.UserRecordACK, (msg: teahouse.UserRecordACK) => {

            if (msg.Rec.NiuNiuRoundRec.length) {
                if (msg.Rec.NiuNiuRoundRec[0].MatchType === structure.TableConfig.MatchTypeConfig.Normal) {
                    console.log("我的战绩", msg);
                    this.norTips.visible = false;
                    this.normalList.dataProvider = new eui.ArrayCollection(msg.Rec.NiuNiuRoundRec);
                    this.normalList.itemRenderer = normalRecordItem;
                    this.normalScroll.viewport = this.normalList;
                } else {
                    console.log("茶楼战绩", msg);
                    this.clubTips.visible = false;
                    this.clubRecordList.dataProvider = new eui.ArrayCollection(msg.Rec.NiuNiuRoundRec);
                    this.clubRecordList.itemRenderer = normalRecordItem;
                    this.clubScroll.viewport = this.clubRecordList;
                }

            }
        }, this);
    }

    private radioChangeHandler(e: eui.UIEvent) {
        let radioGroup: eui.RadioButtonGroup = e.target;
        this.viewSrack.selectedIndex = radioGroup.selectedValue;
        console.log("rocordPnl", this.viewSrack.selectedIndex);
        if (radioGroup.$name !== 'rocordPnl') return;
        SoundManager.ins.playEffect('sfx_button_click_mp3');

    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.normalRadio.group.removeEventListener(eui.UIEvent.CHANGE, this.radioChangeHandler, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private initNormalRecord() {
        let recordReq = new teahouse.UserRecordREQ();
        recordReq.MatchType = structure.TableConfig.MatchTypeConfig.Normal;
        recordReq.Start = 0;
        recordReq.PageCount = 30;
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_UserRecordREQ, teahouse.UserRecordREQ, recordReq);
        console.log('recordReq', recordReq);
    }

    private initClubRecord() {

        let recordReq = new teahouse.UserRecordREQ();
        recordReq.MatchType = structure.TableConfig.MatchTypeConfig.Match;
        recordReq.Start = 0;
        recordReq.PageCount = 30;
        console.log('recordReq', recordReq);
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_UserRecordREQ, teahouse.UserRecordREQ, recordReq);
    }
}

class normalRecordItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "skins.ClubRecordItem";
    }

    private showChildItem: eui.Group;

    private gameType: eui.Label;
    private roomId: eui.Label;
    private bankerType: eui.Label;
    private bottomScr: eui.Label;
    private gameCount: eui.Label;
    private createTime: eui.Label;
    private details: eui.Button;
    private share: eui.Button;
    private selfData: record.NiuniuRoundRecord;
    protected childrenCreated() {
        super.childrenCreated();

        this.details.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            //详情
            let deteilRecordPnl = new DetailsRecordPnl(this.selfData);
            Main.inst.addChild(deteilRecordPnl);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
        this.share.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            //分享
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        // this.details.visible = false;
        // this.share.visible = false;
    }
    protected dataChanged() {
        this.selfData = this.data;
        this.showChildItem.removeChildren();

        this.gameType.text = this.selfData.MatchType ? "茶楼战绩" : "普通战绩";
        this.bankerType.text = '明牌抢庄';
        this.createTime.text = GameData.getTimeStr(this.selfData.Date);
        this.roomId.text = this.selfData.MatchType ? this.selfData.HouseID.toString() : this.selfData.TableId.toString();
        this.gameCount.text = "局数:" + this.selfData.MaxRound.toString();
        this.bottomScr.text = "底分:" + this.selfData.BaseScore[0] + "/" + this.selfData.BaseScore[1] + "/" + this.selfData.BaseScore[2];
        // console.log('当前战绩', this.data);
        //根据输赢总积分排序
        let newRoundRecord: record.NiuniuRoundRecord = this.data;
        let self = this;
        newRoundRecord.Rec.sort(function (userRecord1, userRecord2) {
            return self.getAllScore(userRecord2) - self.getAllScore(userRecord1);
        });
        newRoundRecord.Rec.forEach((newScoreRec, index) => {
            let winer: boolean = false, loser: boolean = false;
            if (index === 0) winer = true;
            if (index === newRoundRecord.Rec.length - 1) loser = true;
            let childItem = new clubRecordChildItem(newScoreRec, winer, loser);
            this.showChildItem.addChild(childItem);
        });


        let itemHeight = 84 + Math.ceil(this.showChildItem.$children.length / 5) * 183;   // > 5 ? 450 : 267;
        this.height = itemHeight;

    }

    private getAllScore(userRecord: record.INiuniuUserRecord) {
        let allScore: any = 0;
        for (let i = 0; i < userRecord.HandRec.length; i++) {
            allScore += userRecord.HandRec[i].Score;
        }
        return allScore;
    }

}

class clubRecordChildItem extends eui.Component {
    constructor(data, winer = false, loser = false) {
        super();
        this.skinName = "skins.clubRecordChildItem";
        this.selfData = data;
        this.winerB = winer;
        this.loserB = loser;
    }
    private head: eui.Image;
    private nickName: eui.Label;
    private id: eui.Label;
    private score: eui.Label;
    private rob: eui.Label;
    private when: eui.Label;
    private push: eui.Label;
    private winScore: eui.Label;
    private winer: eui.Image;
    private loser: eui.Image;
    private selfData: record.INiuniuUserRecord;

    private loserB = false;
    private winerB = false;

    protected childrenCreated() {
        super.childrenCreated();
        GameData.handleHeadImg(this.head, this.selfData.HeadIcon);

        this.nickName.text = '昵称:' + this.selfData.NickName;
        this.id.text = 'ID:' + this.selfData.UserId.toString();
        this.score.text = '积分:' + this.selfData.Score.toString(); //剩余积分
        this.rob.text = this.selfData.RankCount.toString()//暂时还没有
        this.when.text = this.selfData.RankerCount.toString();//当庄次数
        this.push.text = this.selfData.TuizhuCount.toString();

        let allScore: any = 0;
        for (let i = 0; i < this.selfData.HandRec.length; i++) {
            allScore += this.selfData.HandRec[i].Score;
        }
        this.winScore.text = allScore;  //根据每局积分自己算 输赢积分

        this.winer.visible = this.winerB;

        this.loser.visible = this.loserB;


    }
}
