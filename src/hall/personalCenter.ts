class PersonalCenter extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.personalPnl';
    }


    private closeBtn: eui.Button;

    private nickName: eui.Label;
    private id: eui.Label;
    private space: eui.Label;
    private ip: eui.Label;
    private playCount: eui.Label;
    private birthday: eui.Label;

    private level: eui.Image;
    private stars: eui.Group;
    private headImg: eui.Image;
    private sex: eui.Image;


    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.initView();
    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private initView() {

        let playCount = GameData.userInfo.Round;
        let sex = GameData.userInfo.Sex;
        let registDate = GameData.userInfo.RegistDate;

        this.nickName.text = GameData.userInfo.Nickname;
        this.id.text = GameData.userInfo.ID.toString();
        this.birthday.text = GameData.getTimeStr(registDate).split(' ')[0];
        this.playCount.text = GameData.userInfo.Round.toString();
        this.ip.text = GameData.userInfo.IpAddr.toString();

        // this.reloadRespase(GameData.userInfo.IpAddr);

        let leave = 'info_leave' + (Math.floor(playCount / 30) + 1) + '_png';
        this.level.texture = RES.getRes(leave);

        if (sex === 1) {
            this.sex.texture = RES.getRes('hall_nan_png');
        } else {
            this.sex.texture = RES.getRes('hall_nv_png');
        }
        GameData.handleHeadImg(this.headImg, GameData.userInfo.HeadIcon);
    }
}