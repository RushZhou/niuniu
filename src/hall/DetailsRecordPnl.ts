class DetailsRecordPnl extends eui.Component implements eui.UIComponent {
    public constructor(date) {
        super();
        this.skinName = 'skins.DetailsRecordPnl';
        this.detailData = date;
    }

    private closeBtn: eui.Button;
    private detailsRecordList: eui.List;
    private nameList: eui.List;
    private allScoreList: eui.List;
    private detailsRecordScroll: eui.Scroller;

    private detailData: record.INiuniuRoundRecord;

    protected childrenCreated() {
        super.childrenCreated();
        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);

        console.log("战绩详情", this.detailData);

        let nameDataArray = [];
        let allScoreDataArray = [];
        for (let i = 0; i < this.detailData.Rec.length; i++) {
            nameDataArray.push({ name: this.detailData.Rec[i].NickName });
            let allScore: any = 0;
            for (let j = 0; j < this.detailData.Rec[i].HandRec.length; j++) {
                allScore += this.detailData.Rec[i].HandRec[j].Score;
            }
            allScoreDataArray.push({ score: allScore });
        }

        this.nameList.dataProvider = new eui.ArrayCollection(nameDataArray);
        this.nameList.itemRendererSkinName = skins.combobox.NameItem;

        this.allScoreList.dataProvider = new eui.ArrayCollection(allScoreDataArray);
        this.allScoreList.itemRendererSkinName = skins.combobox.ScoreItem;


        let detailItemData = [];

        // //後期要 修改的   暫時先放過你
        // for (let j = 0; j < this.detailData.Rec[0].HandRec.length; j++) {
        //     let detailHandRecordInfo = [];
        //     for (let i = 0; i < this.detailData.Rec.length; i++) {
        //         detailHandRecordInfo.push(this.detailData.Rec[i].HandRec[j]);
        //     }

        //     if (typeof (detailItemData[j]) === 'undefined') {
        //         detailItemData[j] = [];
        //     };
        //     detailItemData[j].push({ index: j + 1, handRecord: detailHandRecordInfo });
        // }

        let maxRound = this.detailData.MaxRound;//最大轮数
        let playersRecordArr = this.detailData.Rec;
        for (let i = 0; i < maxRound; i++) {
            let indexRecord = [];//第I局战绩中的所有玩家的战绩
            for (let j = 0; j < playersRecordArr.length; j++) {
                let playRecord: record.INiuniuUserRecord = playersRecordArr[j]; //第 j 个玩家
                let handRec = this.getHandRecByIndex(playRecord.HandRec, i);
                indexRecord.push(handRec);
            }
            detailItemData.push(indexRecord);
        }
        console.log("战绩详情", detailItemData);
        this.detailsRecordList.dataProvider = new eui.ArrayCollection(detailItemData);
        this.detailsRecordList.itemRenderer = DetailsRecordItem;
        this.detailsRecordScroll.viewport = this.detailsRecordList;

    }
    /**
     * HandRecArr 当前玩家手中打过的牌  
     * index //第几手牌
     */
    private getHandRecByIndex(HandRecArr, index) {
        let maxRound = this.detailData.MaxRound;//最大轮数
        let handRec: record.NiuniuUserRecord.HandRecord = null;
        for (let i = 0; i < maxRound; i++) {
            HandRecArr.forEach((playerIndexRecord: record.NiuniuUserRecord.HandRecord) => {
                if (index === playerIndexRecord.Hand) { //判断玩家手中的牌中 有没有 第index手的牌
                    handRec = playerIndexRecord;
                }
            }, this);
        }
        return handRec;
    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

}

class DetailsRecordItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "skins.teahouse.DetailsRecordItem";
    }
    private scoreList: eui.List;
    private ItemList: eui.List;
    private gamesNum: eui.Label;
    private checkBtn: eui.Button;
    private scoreScroll: eui.Scroller;
    private itemScroll: eui.Scroller;

    private isChecked = true;

    protected childrenCreated() {
        super.childrenCreated();
        this.checkBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            if (this.isChecked) {
                this.height = 60;
                this.ItemList.visible = false;
            } else {
                this.height = 260;
                this.ItemList.visible = true;
            }
            this.isChecked = !this.isChecked;
        }, this);
    }
    protected dataChanged() {

        this.gamesNum.text = "第" + (this.itemIndex + 1) + "局";

        this.ItemList.dataProvider = new eui.ArrayCollection(this.data);
        this.ItemList.itemRenderer = DetailRecordChildItems;
        this.itemScroll.viewport = this.scoreList;


        let allScore = [];

        for (let i = 0; i < this.data.length; i++) {
            if (this.data[i]) {
                allScore.push(this.data[i].Score);
            } else {
                allScore.push(null);
            }
        }

        this.scoreList.dataProvider = new eui.ArrayCollection(allScore);
        this.scoreList.itemRenderer = ScoreItem;
        this.scoreScroll.viewport = this.scoreList;

    }

}

class ScoreItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = 'skins.record.scoreItem';
    }
    private score: eui.Label;

    protected childrenCreated() {
        super.childrenCreated();
    }
    protected dataChanged() {
        if (this.data === null) {
            this.score.visible = false;
        } else {
            this.score.text = this.data;
        }
    }

}


class DetailRecordChildItems extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "skins.teahouse.DetailRecordChildItem";
    }

    private card_0: eui.Image;
    private card_1: eui.Image;
    private card_2: eui.Image;
    private card_3: eui.Image;
    private card_4: eui.Image;
    private multiple: eui.Image;
    private cardType: eui.Image;
    private Bets: eui.Label;
    private betImg: eui.Image;


    protected childrenCreated() {
        super.childrenCreated();

    }
    protected dataChanged() {
        // console.log("DetailRecordChildItems", this.data);
        if (this.data === null) {
            this.visible = false;
            return;
        }
        this.card_0.source = (<eui.Image>GameData.getCardByByte(this.data.Cards[0])).source;
        this.card_1.source = (<eui.Image>GameData.getCardByByte(this.data.Cards[1])).source;
        this.card_2.source = (<eui.Image>GameData.getCardByByte(this.data.Cards[2])).source;
        this.card_3.source = (<eui.Image>GameData.getCardByByte(this.data.Cards[3])).source;
        this.card_4.source = (<eui.Image>GameData.getCardByByte(this.data.Cards[4])).source;
        let type = niuniu.NiuniuCardType;
        let imgName = '';
        switch (this.data.CardType) {
            case type.CardType_Niu1:
                imgName = 'niu_1_png';
                break;
            case type.CardType_Niu2:
                imgName = 'niu_2_png';
                break;
            case type.CardType_Niu3:
                imgName = 'niu_3_png';
                break;
            case type.CardType_Niu4:
                imgName = 'niu_4_png';
                break;
            case type.CardType_Niu5:
                imgName = 'niu_5_png';
                break;
            case type.CardType_Niu6:
                imgName = 'niu_6_png';
                break;
            case type.CardType_Niu7:
                imgName = 'niu_7_png';
                break;
            case type.CardType_Niu8:
                imgName = 'niu_8_png';
                break;
            case type.CardType_Niu9:
                imgName = 'niu_9_png';
                break;
            case type.CardType_Niuniu:
                imgName = 'niu_10_png';
                break;
            case type.CardType_None:
                imgName = 'niu_0_png';
                break;
            case type.CardType_Wuhua:
                imgName = 'niu_12_png';
                break;
            case type.CardType_Wuxiao:
                imgName = 'niu_16_png';
                break;
            case type.CardType_Bomb:
                imgName = 'niu_15_png';
                break;
            case type.CardType_Flush:
                imgName = 'niu_13_png';
                break;
            case type.CardType_Fullhouse:
                imgName = 'niu_14_png';
                break;
            case type.CardType_Straight:
                imgName = 'niu_11_png';
                break;
            case type.CardType_StraightFlush:
                imgName = 'niu_17_png';
                break;
        }
        this.cardType.source = <egret.Texture>RES.getRes(imgName);

        this.multiple.texture = RES.getRes('multi' + this.data.RankMulti + '_png');
        this.Bets.text = this.data.Bet + "";

        if (this.data.Ranker) {
            this.betImg.visible = false;
            this.Bets.visible = false;
            this.multiple.visible = true;
        } else {
            this.multiple.visible = false;
            this.Bets.y = 105;
            this.betImg.y = 95;
        }
    }

}
