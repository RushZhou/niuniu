class EmailPnl extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.emailPnl';
    }
    private closeBtn: eui.Button;
    private radioBtn: eui.RadioButton;
    private viewStack: eui.ViewStack;
    private emilList: eui.List;
    private scrollBar: eui.Scroller;
    private notips: eui.Group;

    private ListMsg: eui.ArrayCollection;

    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.radioBtn.group.$name = 'emailPnl';
        this.radioBtn.group.addEventListener(eui.UIEvent.CHANGE, this.onChange, this);

        Main.inst.addEventListener(GameData.GameEvent.UpdateEmail, (evt: egret.Event) => {
            let mailReq = new login.MailREQ();
            MsgMgr.inst.sendMsg(login.MailID.ID_MailREQ, login.MailREQ, mailReq);
            MsgMgr.inst.onMsg(login.MailID.ID_MailACK, login.MailACK, (msg: login.MailACK) => {
                console.log('刷新邮件', msg, msg.Mail);
                this.notips.visible = msg.Mail.length === 0 ? true : false;
                this.ListMsg.replaceAll(msg.Mail);
                MsgMgr.inst.offMsg(login.MailID.ID_MailACK, this);
            }, this);
        }, this);

        this.onNetListener();
    }
    private onNetListener() {

        let mailReq = new login.MailREQ();
        MsgMgr.inst.sendMsg(login.MailID.ID_MailREQ, login.MailREQ, mailReq);
        console.log('查询邮件', mailReq);
        MsgMgr.inst.onMsg(login.MailID.ID_MailACK, login.MailACK, (msg: login.MailACK) => {
            console.log('查询邮件返回', msg, msg.Mail);
            this.notips.visible = msg.Mail.length === 0 ? true : false;
            this.ListMsg = this.emilList.dataProvider = new eui.ArrayCollection(msg.Mail);
            this.emilList.itemRenderer = emilItem;
            MsgMgr.inst.offMsgAll(this);
        }, this);



    }
    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.radioBtn.group.removeEventListener(eui.UIEvent.CHANGE, this.onChange, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
    private onChange(e: eui.UIEvent) {
        let radioGroup: eui.RadioButtonGroup = e.target;
        console.log('emailPnl', 'radio');
        if (radioGroup.$name !== 'emailPnl') return;
        this.viewStack.selectedIndex = radioGroup.selectedValue;
        if(this.viewStack.selectedIndex !== 0){
            this.notips.visible = false;
        }
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
}

class emilItem extends eui.ItemRenderer {

    public constructor() {
        super();
        this.skinName = skins.hall.emilItem;
    }

    private selfData = null;
    private seeEmailBtn: eui.Button;
    private desc: eui.Label;
    private title: eui.Label;
    private isRead: eui.Label;

    protected childrenCreated() {
        super.childrenCreated();
        this.seeEmailBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSeeEmailFunc, this);
    }

    protected dataChanged() {
        this.selfData = this.data;

        // let allStr = this.selfData.Content.split('');
        // console.log('-------',this.selfData.Content,allStr);

        this.desc.text = this.selfData.Content.replace(/\\n/g, "\n");;
        this.title.text = this.selfData.Title;
    }

    private onSeeEmailFunc() {
        let emailTip = new EmailTips(this.selfData);
        Main.inst.addChild(emailTip);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

}

class EmailTips extends eui.Component {
    private selfData = null;
    private desc: eui.Label;
    private sure: eui.Button;
    private refuse: eui.Button;
    private agree: eui.Button;
    private title: eui.Label;
    private date: eui.Label;
    private closeBtn: eui.Button;


    public constructor(data: any) {
        super();
        this.skinName = emilTips;
        this.selfData = data;
    }
    protected childrenCreated() {
        super.childrenCreated();

        let desc = <string>this.selfData.Content;
        this.desc.text = desc.replace(/\\n/g, "\n");
        this.title.text = this.selfData.Title;
        this.date.text = GameData.getTimeStr(this.selfData.CreateDate);

        if (this.selfData.Type === 0) {//纯文本
            this.sure.visible = true;
        } else {
            this.agree.visible = true;
            this.refuse.visible = true;
        }

        this.sure.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let teaHouseOrderReq = new teahouse.TeaHouseHandleOrderREQ();
            teaHouseOrderReq.OrderID = this.selfData.OrderID;
            teaHouseOrderReq.Accept = true;

            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseHandleOrderREQ, teahouse.TeaHouseHandleOrderREQ, teaHouseOrderReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseHandleOrderACK, teahouse.TeaHouseHandleOrderACK, (msg: teahouse.TeaHouseHandleOrderACK) => {
                console.log("确认msg", msg.Code, msg);
                if (msg.Code === 0) {
                    Main.inst.removeChild(this);
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdateEmail);
                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
                MsgMgr.inst.offMsgAll(this);

            }, this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');

        }, this);
        this.agree.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {

            let teaHouseOrderReq = new teahouse.TeaHouseHandleOrderREQ();
            teaHouseOrderReq.OrderID = this.selfData.OrderID;
            teaHouseOrderReq.Accept = true;

            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseHandleOrderREQ, teahouse.TeaHouseHandleOrderREQ, teaHouseOrderReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseHandleOrderACK, teahouse.TeaHouseHandleOrderACK, (msg: teahouse.TeaHouseHandleOrderACK) => {
                console.log("同意msg", msg.Code, msg);
                if (msg.Code === 0) {
                    Main.inst.removeChild(this);
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdateEmail);
                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
                MsgMgr.inst.offMsgAll(this);
            }, this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
        this.refuse.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {

            let teaHouseOrderReq = new teahouse.TeaHouseHandleOrderREQ();
            teaHouseOrderReq.OrderID = this.selfData.OrderID;
            teaHouseOrderReq.Accept = false;

            MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseHandleOrderREQ, teahouse.TeaHouseHandleOrderREQ, teaHouseOrderReq);
            MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseHandleOrderACK, teahouse.TeaHouseHandleOrderACK, (msg: teahouse.TeaHouseHandleOrderACK) => {
                console.log("拒绝msg", msg.Code, msg);
                if (msg.Code === 0) {
                    Main.inst.removeChild(this);
                    Main.inst.dispatchEventWith(GameData.GameEvent.UpdateEmail);
                } else {
                    Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
                }
                MsgMgr.inst.offMsgAll(this);
            }, this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            MsgMgr.inst.offMsgAll(this);
            this.parent.removeChild(this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
    }
}