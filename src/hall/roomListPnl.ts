class RoomListPnl extends eui.Component implements eui.UIComponent {
    public constructor(opentype: OpenType) {
        super();
        this.skinName = 'skins.hall.roomListPnl';
        this.viewStack.selectedIndex = opentype;
        if (opentype === OpenType.club) {
            this.clubList.selected = true;
            this.roomList.selected = false;
        } else {
            this.clubList.selected = false;
            this.roomList.selected = true;
        }
    }


    private closeBtn: eui.Button;
    private clubList: eui.RadioButton;
    private roomList: eui.RadioButton;


    private viewStack: eui.ViewStack;

    private dataList0: eui.List;
    private dataList1: eui.List;
    private ListMsg0: eui.ArrayCollection;
    private ListMsg1: eui.ArrayCollection;
    private roomScroll: eui.Scroller;
    private clubScroll: eui.Scroller;


    private icon0: eui.Image;
    private icon1: eui.Image;
    private lab0: eui.Label;
    private lab1: eui.Label;

    private _Listmsg = [];

    protected childrenCreated() {
        super.childrenCreated();
        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);

        this.clubList.group.addEventListener(eui.UIEvent.CHANGE, this.radioChangeHandler, this);
        this.clubList.group.$name = 'roomlist';
        this.roomScroll.scrollPolicyH = eui.ScrollPolicy.OFF;
        this.clubScroll.scrollPolicyH = eui.ScrollPolicy.OFF;

        if (this.viewStack.selectedIndex === OpenType.club) {
            this.initClubData();
        } else {
            this.initRoomData();
        }

        Main.inst.addEventListener(GameData.GameEvent.RemoveEvent, () => {
            this.clubList.group.removeEventListener(eui.UIEvent.CHANGE, this.radioChangeHandler, this);
        }, this);
    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.clubList.group.removeEventListener(eui.UIEvent.CHANGE, this.radioChangeHandler, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private initClubData() {

        let queryTeaHouseReq = new teahouse.TeaHouseJoinedREQ();
        MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseJoinedREQ, teahouse.TeaHouseJoinedREQ, queryTeaHouseReq);
        //console.log('查询茶楼房间', queryTeaHouseReq);
        MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeeHouseJoinedACK, teahouse.TeeHouseJoinedACK, (msg: teahouse.TeeHouseJoinedACK) => {
            console.log('查询茶楼房间返回1  ', msg);
            MsgMgr.inst.offMsg(teahouse.TeaHouseID.ID_TeeHouseJoinedACK, this);
            // let teahouseInfo = new teahouse.TeaHouseInfoREQ();
            // teahouseInfo.ID = msg.ID;
            // MsgMgr.inst.sendMsg(teahouse.TeaHouseID.ID_TeaHouseInfoREQ, teahouse.TeaHouseInfoREQ, teahouseInfo);
            let _Listmsg = [
            ];
            for (let i = 0; i < msg.info.length; i++) {
                let itemInfo = {};
                itemInfo["club"] = msg.info[i].Config.Name;
                itemInfo["clubId"] = msg.info[i].ID;
                itemInfo["openNum"] = "1";
                itemInfo["gamesNum"] = msg.info[i].MemberCount;
                itemInfo["boss"] = msg.info[i].OwnerNickname;
                itemInfo['isBoss'] = msg.info[i].OwnerID;
                itemInfo['isMatch'] = msg.info[i].Config.MatchType;
                _Listmsg.push(itemInfo);

                let teahouseTableInfoReq = new niuniu.TeahouseTableInfoREQ();
                teahouseTableInfoReq.HouseId = msg.info[i].ID;
                //console.log('teahouseTableInfoReq', teahouseTableInfoReq);
                MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_TeahouseTableInfoREQ, niuniu.TeahouseTableInfoREQ, teahouseTableInfoReq);
            }
            if (_Listmsg.length > 0) {
                this.icon0.visible = false;
                this.lab0.visible = false;
            }
            this._Listmsg = _Listmsg;
            this.ListMsg0 = this.dataList0.dataProvider = new eui.ArrayCollection(_Listmsg);
            this.dataList0.itemRenderer = ClubItem;
            this.clubScroll.viewport = this.dataList0;
        }, this);
      
        MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_TeahouseTableInfoACK, niuniu.TeahouseTableInfoACK, (msg: niuniu.TeahouseTableInfoACK) => {
            //console.log('查询茶楼房间返回3  ', msg, msg.Info);
            MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_TeahouseTableInfoACK, this);
            if (msg.Info.length === 0) return;
            for (let i = 0; i < this._Listmsg.length; i++) {
                if (this._Listmsg[i].clubId === msg.Info[0].HouseID) {//msg内没有表示此桌子存在的房间
                    this._Listmsg[i].openNum = msg.Info.length.toString();
                    this.ListMsg0.replaceItemAt(this._Listmsg[i], i);
                }
            }

        }, this);

    }

    private initRoomData() {
        let roomInfo = new niuniu.OwnTableInfoREQ();
        MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_OwnTableInfoREQ, niuniu.OwnTableInfoREQ, roomInfo);
        MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_OwnTableInfoACK, niuniu.OwnTableInfoACK, (msg: niuniu.OwnTableInfoACK) => {
            //console.log('查询房间返回  ', msg, msg.TableInfo);
            MsgMgr.inst.offMsg(niuniu.NiuniuID.ID_OwnTableInfoACK, this);
            let _Listmsg = [
            ];
            for (let i = 0; i < msg.TableInfo.length; i++) {
                if (msg.TableInfo[i].HouseID != 0) continue;
                let itemInfo = {};
                itemInfo["roomId"] = msg.TableInfo[i].TableID;
                itemInfo["gamesNum"] = msg.TableInfo[i].Config.MaxRound;
                let arrScore = msg.TableInfo[i].Config.BaseScoreCfg.ValidBaseScore;
                itemInfo["bottomScore"] = arrScore[0] + '/' + arrScore[1] + '/' + arrScore[2];
                itemInfo["peopleNum"] = msg.TableInfo[i].Player.length;
                itemInfo["rule"] = "明牌抢庄";
                itemInfo['isBoss'] = msg.TableInfo[i].Owner;
                _Listmsg.push(itemInfo);
            }
            if (_Listmsg.length > 0) {
                this.icon1.visible = false;
                this.lab1.visible = false;
            }
            this.dataList1.dataProvider = new eui.ArrayCollection(_Listmsg);
            this.dataList1.itemRenderer = RoomItem;
            this.roomScroll.viewport = this.dataList1;
        }, this);
    }
    private radioChangeHandler(e: eui.UIEvent) {
        let radioGroup: eui.RadioButtonGroup = e.target;
        console.log("roomlist", 'radio');
        if (radioGroup.$name !== 'roomlist') return;

        this.viewStack.selectedIndex = radioGroup.selectedValue;
        if (this.viewStack.selectedIndex === 0) {
            this.initClubData();
        } else if (this.viewStack.selectedIndex === 1) {
            this.initRoomData();
        }
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    //查找老板 返回老板信息
    private searchBossInfo(data) {
        let bossInfo: teahouse.ITeaHouseMember = null;
        for (let i = 0; i < data.length; i++) {
            if (data[i].Limit === teahouse.TeaHouseLimit.Owner) {
                bossInfo = data[i];
            }
        }
        return bossInfo;
    }
}
class RoomItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "roomListItem";
    }
    private roomListBtn: eui.Button;
    private owner: eui.Image;
    private roomId = -1;
    protected childrenCreated() {
        super.childrenCreated();

        this.roomListBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let joinReq = new niuniu.EnterGameREQ();
            joinReq.GameID = structure.GameID.Niuniu;
            joinReq.TableID = this.roomId;
            MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_EnterGameREQ, niuniu.EnterGameREQ, joinReq);
            //console.log('加入游戏房间', joinReq);
            MsgMgr.inst.onMsg(niuniu.NiuniuID.ID_EnterGameACK, niuniu.EnterGameACK, (msg: niuniu.EnterGameACK) => {
                // console.log('加入游戏房间返回', msg);
                if (msg.Code === 0 || msg.Code === 5) {
                    MsgMgr.inst.offMsgAll(this);
                    let roomPnl = Main.inst.getChildByName('RoomListPnl');
                    MsgMgr.inst.offMsgAll(roomPnl);
                    Main.inst.removeChild(roomPnl);
                    Main.inst.dispatchEventWith(GameData.GameEvent.RemoveEvent);
                    Main.inst.dispatchEventWith(GameData.GameEvent.ChangeSound, false, { SoundType: "Game" });
                    //加入房间成功
                    GameData.EvtEnterRoom(msg.TableInfo);

                } else {
                    Tips.addTip(SerCodeReflect.reflect(niuniu.GameCode, msg.Code));
                }
            }, this);
        }, this);
    }
    protected dataChanged() {
        this.roomId = this.data.roomId;
        this.owner.visible = this.data.isBoss == GameData.userInfo.ID ? true : false;
    }

}

class ClubItem extends eui.ItemRenderer {
    constructor() {
        super();
        this.skinName = "clubListItem";
        // console.log("create club item");
    }
    private clubListBtn: eui.Button;
    private owner: eui.Image;
    private matchTag: eui.Image;
    private matchTips: eui.Image;

    private clubId = -1;
    protected childrenCreated() {
        super.childrenCreated();

        this.clubListBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            SoundManager.ins.playEffect('sfx_button_click_mp3');
            let enterTeaHouseReq = new niuniu.ObserverTableListREQ();
            enterTeaHouseReq.HouseID = this.clubId;
            MsgMgr.inst.sendMsg(niuniu.NiuniuID.ID_ObserverTableListREQ, niuniu.ObserverTableListREQ, enterTeaHouseReq);
            console.log('进入茶楼', enterTeaHouseReq);
            Main.inst.dispatchEventWith(GameData.GameEvent.RemoveEvent);
            MsgMgr.inst.offMsgAll(this);
            let roomPnl = Main.inst.getChildByName('RoomListPnl');
            MsgMgr.inst.offMsgAll(roomPnl);
            Main.inst.removeChild(roomPnl);
            GameData.EvtEnterTeahouse(this.clubId);
            // MsgMgr.inst.onMsg(teahouse.TeaHouseID.ID_TeaHouseActionACK, teahouse.TeaHouseActionACK, (msg: teahouse.TeaHouseActionACK) => {
            //     console.log('进入茶楼返回', msg, msg.Code);
            //     if (msg.Code === 0 || msg.Code === 5) {
            //         MsgMgr.inst.offMsgAll(this);
            //         let roomPnl = Main.inst.getChildByName('RoomListPnl');
            //         MsgMgr.inst.offMsgAll(roomPnl);
            //         Main.inst.removeChild(roomPnl);
            //         //进入茶楼成功
            //         GameData.EvtEnterTeahouse(msg.HouseID);

            //     } else {
            //         Tips.addTip(SerCodeReflect.reflect(teahouse.TeaHouseCode, msg.Code));
            //     }
            // }, this);
        }, this);
    }
    protected dataChanged() {
        //console.log("ClubItem", this.data,this.data.isMatch);
        this.clubId = this.data.clubId;
        this.owner.visible = this.data.isBoss == GameData.userInfo.ID ? true : false;
        this.matchTag.visible = this.data.isMatch === structure.TableConfig.MatchTypeConfig.Match ? true : false;
        this.matchTips.visible = this.data.isMatch === structure.TableConfig.MatchTypeConfig.Match ? true : false;
    }
}

enum OpenType {
    club,
    room
}