class SharePnl extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.sharePnl';
    }

    private closeBtn: eui.Button;
    private weChatShare: eui.Button;
    private friendsShare: eui.Button;
    protected childrenCreated() {
        super.childrenCreated();
        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.weChatShare.addEventListener(egret.TouchEvent.TOUCH_TAP, this.weChareShareFunc, this);
        this.friendsShare.addEventListener(egret.TouchEvent.TOUCH_TAP, this.friendsShareFunc, this);
    }

    private weChareShareFunc() {
        console.log("微信分享");
        SoundManager.ins.playEffect('sfx_button_click_mp3');
        egret.ExternalInterface.call("shareToWxSession", "http://kffapk.com/code/3u");
    }

    private friendsShareFunc() {
        console.log("朋友圈分享");
        SoundManager.ins.playEffect('sfx_button_click_mp3');
        egret.ExternalInterface.call("shareToWxTimeline", "http://kffapk.com/code/3u");
    }
    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
}