class FeedBackPnl extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.feedBackPnl';
    }
    private closeBtn: eui.Button;
    private feedBack1: eui.Button;
    private feedBack2: eui.Button;
    private shareCode1: eui.Label;
    private shareCode2: eui.Label;


    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.feedBack1.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            Tips.addTip('复制成功');
            egret.ExternalInterface.call("copyWords", this.shareCode1.text)
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
        this.feedBack2.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            Tips.addTip('复制成功');
            egret.ExternalInterface.call("copyWords", this.shareCode2.text)
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);
    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }
}