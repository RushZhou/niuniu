class SettingPnl extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.settingPnl';
    }
    private closeBtn: eui.Button;
    private effectSlider: eui.HSlider;
    private musicSlider: eui.HSlider;

    private closeEffect: eui.Image;
    private openEffect: eui.Image;
    private closeMusic: eui.Image;
    private openMusic: eui.Image;

    private privacyPolicy: eui.Button;
    private changeUser: eui.Button;


    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.initMusicSlider();


        this.privacyPolicy.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            let privacyPnl = new RuleOrPrivacyPnl("privacy");
            Main.inst.addChild(privacyPnl);
            this.parent.removeChild(this);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);


        this.changeUser.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            Main.inst.dispatchEventWith(GameData.GameEvent.ExitGame);
            SoundManager.ins.playEffect('sfx_button_click_mp3');
        }, this);

    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private initMusicSlider() {

        var musicSlider: eui.HSlider = new SetupHSlider();
        musicSlider.width = 450;
        musicSlider.x = 420;
        musicSlider.y = 370;
        musicSlider.addEventListener(eui.UIEvent.CHANGE, this.changeMHandler, this);
        this.addChild(musicSlider);

        var effectSlider: eui.HSlider = new SetupHSlider();
        effectSlider.width = 450;
        effectSlider.x = 420;
        effectSlider.y = 260;
        effectSlider.addEventListener(eui.UIEvent.CHANGE, this.changeEHandler, this);
        this.addChild(effectSlider);



        let musicVolume = SoundManager.ins.getLastMusicVolume();

        musicSlider.value = musicVolume * 400;
        if (musicVolume === 0) {
            this.openMusic.visible = false;
            this.closeMusic.visible = true;
        } else {
            this.openMusic.visible = true;
            this.closeMusic.visible = false;
        }

        let effectVolume = SoundManager.ins.getEffectVolume();
        effectSlider.value = effectVolume * 400;
        if (effectVolume === 0) {
            this.openEffect.visible = false;
            this.closeEffect.visible = true;
        } else {
            this.openEffect.visible = true;
            this.closeEffect.visible = false;
        }
        console.log('音乐大小', musicVolume, "音效大小", effectVolume);
    }
    private changeMHandler(evt: eui.UIEvent): void {
        console.log('changeMHandler', evt.target.value);

        if (evt.target.value === 0) {
            this.openMusic.visible = false;
            this.closeMusic.visible = true;
        } else {
            this.openMusic.visible = true;
            this.closeMusic.visible = false;
        }
        SoundManager.ins.setMusicVolume(evt.target.value / 400);
    }
    private changeEHandler(evt: eui.UIEvent): void {
        console.log('changeEHandler', evt.target.value);
        if (evt.target.value === 0) {
            this.openEffect.visible = false;
            this.closeEffect.visible = true;
        } else {
            this.openEffect.visible = true;
            this.closeEffect.visible = false;
        }
        SoundManager.ins.setEffectVolume(evt.target.value / 400);
    }
}

class SetupHSlider extends eui.HSlider {
    public constructor() {
        super();
        this.skinName = 'skins.hall.HSlider';
        this.width = 450;
        this.minimum = 0;
        this.maximum = 400;
        this.value = 0;

    }
    public updateSkinDisplayList(): void {
        if (this.value == 0) {
            this.trackHighlight.visible = false;
        }
        else {
            this.trackHighlight.visible = true;
        }
        var x: number = (this.width - this.thumb.width) * (this.value / this.maximum);
        this.thumb.x = x;
        this.trackHighlight.x = 0;
        this.trackHighlight.width = x + this.thumb.width / 2 + 5;
        // console.log("----实时刷新------", this.value, this.thumb.x);
    }

}