class CreateRoomPnl extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.createRoomPnl';
    }
    private closeBtn: eui.Button;

    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.ononCloseFunc, this);
    }

    private ononCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.ononCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
        SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

}