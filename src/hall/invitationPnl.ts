class InvitationPnl extends eui.Component implements eui.UIComponent {
    public constructor() {
        super();
        this.skinName = 'skins.hall.invitationPnl';
    }

    private closeBtn: eui.Button;
    private bindBtn: eui.Button;
    private InvitationCode: eui.TextInput;


    protected childrenCreated() {
        super.childrenCreated();

        this.closeBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        this.bindBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.BindInvitationFunc, this);

        this.InvitationCode.prompt = "请输入验证码";
    }

    private onCloseFunc() {
        this.closeBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseFunc, this);
        MsgMgr.inst.offMsgAll(this);
        this.parent.removeChild(this);
         SoundManager.ins.playEffect('sfx_button_click_mp3');
    }

    private BindInvitationFunc() {
        let invitationCode = this.InvitationCode.text;

    }

}