declare module skins{
	class ButtonSkin extends eui.Skin{
	}
}
declare module skins{
	class CheckBoxSkin extends eui.Skin{
	}
}
declare module skins.combobox{
	class ComboboxSkin extends eui.Skin{
	}
}
declare module skins.combobox{
	class ModifCheckBox extends eui.Skin{
	}
}
declare module skins.combobox{
	class MyListItem extends eui.Skin{
	}
}
declare module skins.commom{
	class closeBtn extends eui.Skin{
	}
}
declare module skins.combobox{
	class NameItem extends eui.Skin{
	}
}
declare module skins.combobox{
	class ScoreItem extends eui.Skin{
	}
}
declare module skins.common{
	class label extends eui.Skin{
	}
}
declare module skins.record{
	class scoreItem extends eui.Skin{
	}
}
declare module skins.common{
	class TextInputSkin extends eui.Skin{
	}
}
declare module skins.common{
	class Tips extends eui.Skin{
	}
}
declare module skins.common{
	class TipsPnl extends eui.Skin{
	}
}
declare module skins.game{
	class AiPlay extends eui.Skin{
	}
}
declare module skins.game{
	class BetBtn extends eui.Skin{
	}
}
declare module skins.game{
	class CrazyBet extends eui.Skin{
	}
}
declare module skins.game{
	class DistributorSkins extends eui.Skin{
	}
}
declare class FunctionalPnl extends eui.Skin{
}
declare module skins.game{
	class GameMenu extends eui.Skin{
	}
}
declare module skins.game{
	class GameTable extends eui.Skin{
	}
}
declare module skins.game{
	class LeftPlayer extends eui.Skin{
	}
}
declare module skins.game{
	class MakeCard extends eui.Skin{
	}
}
declare module skins.game{
	class MusicHSlider extends eui.Skin{
	}
}
declare module skins.game{
	class PlayerInfo extends eui.Skin{
	}
}
declare module skins.game{
	class ResultItem extends eui.Skin{
	}
}
declare module skins.game{
	class ResultPnl extends eui.Skin{
	}
}
declare module skins.game{
	class RightPlayer extends eui.Skin{
	}
}
declare module skins.game{
	class RoomInfo extends eui.Skin{
	}
}
declare module skins.game{
	class SelfPlayer extends eui.Skin{
	}
}
declare module skins.game{
	class UpPlayer extends eui.Skin{
	}
}
declare module skins.hall{
	class authenticationPnl extends eui.Skin{
	}
}
declare class clubListItem extends eui.Skin{
}
declare module skins{
	class clubRecordChildItem extends eui.Skin{
	}
}
declare module skins{
	class ClubRecordItem extends eui.Skin{
	}
}
declare module skins.teahouse{
	class DetailRecordChildItem extends eui.Skin{
	}
}
declare module skins.teahouse{
	class DetailsRecordItem extends eui.Skin{
	}
}
declare module skins{
	class DetailsRecordPnl extends eui.Skin{
	}
}
declare module skins.hall{
	class emailPnl extends eui.Skin{
	}
}
declare module skins.hall{
	class emilItem extends eui.Skin{
	}
}
declare class emilTips extends eui.Skin{
}
declare module skins.hall{
	class feedBackPnl extends eui.Skin{
	}
}
declare module skins{
	class hallPnl extends eui.Skin{
	}
}
declare module skins.hall{
	class HSlider extends eui.Skin{
	}
}
declare module skins.hall{
	class invitationPnl extends eui.Skin{
	}
}
declare module skins.hall{
	class JoinRoomPnl extends eui.Skin{
	}
}
declare module skins.hall{
	class personalPnl extends eui.Skin{
	}
}
declare module skins.hall{
	class phoneBindPnl extends eui.Skin{
	}
}
declare module skins{
	class radio extends eui.Skin{
	}
}
declare module skins.hall{
	class recordPnl extends eui.Skin{
	}
}
declare class roomListItem extends eui.Skin{
}
declare module skins.hall{
	class roomListPnl extends eui.Skin{
	}
}
declare module skins.hall{
	class RuleOrPrivacyPnl extends eui.Skin{
	}
}
declare module skins.hall{
	class settingPnl extends eui.Skin{
	}
}
declare class sginItem extends eui.Skin{
}
declare module skins.hall{
	class sginPnl extends eui.Skin{
	}
}
declare module skins.hall{
	class sharePnl extends eui.Skin{
	}
}
declare module hall{
	class StoreItemSkins extends eui.Skin{
	}
}
declare module skins.hall{
	class storePnl extends eui.Skin{
	}
}
declare module skins{
	class HScrollBarSkin extends eui.Skin{
	}
}
declare module skins{
	class HSliderSkin extends eui.Skin{
	}
}
declare module skins{
	class ItemRendererSkin extends eui.Skin{
	}
}
declare module skins.login{
	class LoginPnl extends eui.Skin{
	}
}
declare module skins.login{
	class VersionCheck extends eui.Skin{
	}
}
declare module skins{
	class PanelSkin extends eui.Skin{
	}
}
declare module skins{
	class ProgressBarSkin extends eui.Skin{
	}
}
declare module skins{
	class RadioButtonSkin extends eui.Skin{
	}
}
declare module skins{
	class ScrollerSkin extends eui.Skin{
	}
}
declare module skins{
	class ScrollerSkins extends eui.Skin{
	}
}
declare module skins.teahouse{
	class AwardPointItem extends eui.Skin{
	}
}
declare module skins{
	class AwardPointSystem extends eui.Skin{
	}
}
declare module skins.teaHouse{
	class ChangeScorePnl extends eui.Skin{
	}
}
declare module skins.teahouse{
	class ChargePnl extends eui.Skin{
	}
}
declare module skins.teahouse{
	class Desk extends eui.Skin{
	}
}
declare module skins.teahouse{
	class ExamineItem extends eui.Skin{
	}
}
declare class GameChangeItem extends eui.Skin{
}
declare class IntegralvariationItem extends eui.Skin{
}
declare module skins.teahouse{
	class JoinTeaHouseItem extends eui.Skin{
	}
}
declare module skins.teahouse{
	class MembersItem extends eui.Skin{
	}
}
declare module skins.teahouse{
	class ModifPump extends eui.Skin{
	}
}
declare module skins.teahouse{
	class MyMembersPnl extends eui.Skin{
	}
}
declare module skins.teahouse{
	class MyTeaHouseItem extends eui.Skin{
	}
}
declare module skins.teahouse{
	class PartnerApplyPnl extends eui.Skin{
	}
}
declare module skins.teahouse{
	class PartnerItem extends eui.Skin{
	}
}
declare module skins.teahouse{
	class PartnerSeePnl extends eui.Skin{
	}
}
declare module skins.teahouse{
	class PersonalData extends eui.Skin{
	}
}
declare module skins.teahouse{
	class PersonalDataItem extends eui.Skin{
	}
}
declare module skins.teahouse{
	class PromptPnl extends eui.Skin{
	}
}
declare module skins.teahouse{
	class PublicMsg extends eui.Skin{
	}
}
declare module skins.teahouse{
	class SeniorManagementItem extends eui.Skin{
	}
}
declare module skins.teahouse{
	class Table extends eui.Skin{
	}
}
declare module skins.teahouse{
	class TableConfig extends eui.Skin{
	}
}
declare module skins.teahouse{
	class TeaHouse extends eui.Skin{
	}
}
declare class TeahouseMembers extends eui.Skin{
}
declare module skins.teahouse{
	class TeaHouseMgrPnl extends eui.Skin{
	}
}
declare module skins{
	class TextInputSkin extends eui.Skin{
	}
}
declare module skins{
	class ToggleSwitchSkin extends eui.Skin{
	}
}
declare module skins{
	class VScrollBarSkin extends eui.Skin{
	}
}
declare module skins{
	class VSliderSkin extends eui.Skin{
	}
}
